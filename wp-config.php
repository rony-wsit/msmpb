<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'msmpb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`f)}sCds2$5mY%DKC,D1Z{Bw*Vx]3b*>.ya+CJC:9S`sZiP:.k_*}L}dLCb=^S<o' );
define( 'SECURE_AUTH_KEY',  ')%.#87HzP7]P>;~.3&^Dgt8w0M5i2WVVoWH%V5VnnOIK.+y)wu,H2^uIC8`m1Du/' );
define( 'LOGGED_IN_KEY',    '2g!cHI=a:_Jh1QtRH: 9fG3:PP(~>[J^bu1TBOaDnubio]h5L^]?1dE$NhV7`59P' );
define( 'NONCE_KEY',        '=A(G86/)aedf}s[PHZ>#BpL|qm+AySt2^MA&4Y15^Yj^U`Aa0#>iGOJZ! *VjIlM' );
define( 'AUTH_SALT',        'IL4owJ7tN6*ag,N%A`t8WTSOqRxs99t|Dy:*txWow[>0C0d*KdZ=Y=8Fa H6y83H' );
define( 'SECURE_AUTH_SALT', 'B<Y<y)8g@#q;P8lt{m[1`bV|(T_:pX!ZjgAj*ZjLQ$jzC%[BmR0Z %Q<&&-(pAj7' );
define( 'LOGGED_IN_SALT',   'co.fFB9%Jp53gx fXr4[H=s%>xri_t=Vh6Jc/D#=%0SjZTC[wFc*]T@yL!BiE=uB' );
define( 'NONCE_SALT',       'f^_S}Y`tn::!q.R[GD(VzaWVjq4o8O?gO.9eC`fycODQX3QZ<!hQa)2Q8-C*$N@`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
