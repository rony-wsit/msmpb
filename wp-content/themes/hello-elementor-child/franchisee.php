<?php
/*
*
Template Name: Franchisee Page Teamplate
*/
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main" style="margin-left: 20%;">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
                the_content();
                // Include the page content template.
                get_template_part( 'template-parts/content', 'page' );

                ?>
                    <h2>For Heading H1: </h2><?php the_field('test_field'); ?><br/>
                    
                <?php

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }

                // End of the loop.
            endwhile;
            ?>

        </main><!-- .site-main -->
    </div><!-- .content-area -->
<?php get_footer(); ?>