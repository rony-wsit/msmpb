<?php
/*
*
Template Name: New Page Teamplate
*/


get_header();
?>
<div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">
                                <?php
                                // Start the loop.
                                while ( have_posts() ) : the_post();
                                    the_content();
                                                // Include the page content template.
                                                get_template_part( 'template-parts/content', 'page' );

                                    $post_objects = get_field('franchisee_select');
                                    if ($post_objects):
                                    //$i = 0;
//                                    $cntPost = 0;
                                    foreach ($post_objects as $countPost) {
//                                        $cntPost++;
                                    }
                                    for ($j = 0; $j < 40; $j++):
                                        //$i++;
                                        $post = $post_objects[$j];
                                        ?>
                                        <?php //setup_postdata($post); ?>
                                                <div class="color-desc">
                                                    <?php the_field('name') ?>
                                                </div>
                                                <div class="team-text-content min-content">
                                                    <?php the_field('location') ?>

                                                </div>
                                    <?php endfor; ?>
                                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                    <?php endif; ?>

                                    <?php
                                                // If comments are open or we have at least one comment, load up the comment template.
                                                if ( comments_open() || get_comments_number() ) {
                                                                comments_template();
                                                }
                                                // End of the loop.
                                endwhile;
                                ?>
                </main><!-- .site-main -->
                <?php get_sidebar( 'content-bottom' ); ?>
</div><!-- .content-area -->

<?php get_footer(); ?>


