<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css');
}

//function my_acf_init() {
//    acf_update_setting('google_api_key', 'AIzaSyC5XVNFMnqqv_Amq3rdHczCFJ5R9oCpbIQ');
//}
//add_action('acf/init', 'my_acf_init');

/*Custom Post type start*/
function cw_post_type_franchisee() {
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        'excerpt', // post excerpt
        'custom-fields', // custom fields
        'comments', // post comments
        'revisions', // post revisions
        'post-formats', // post formats
    );
    $labels = array(
        'name' => _x('franchisee', 'plural'),
        'singular_name' => _x('franchisee', 'singular'),
        'menu_name' => _x('Franchisee', 'admin menu'),
        'name_admin_bar' => _x('franchisee', 'admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New news'),
        'new_item' => __('New franchisee'),
        'edit_item' => __('Edit franchisee'),
        'view_item' => __('View franchisee'),
        'all_items' => __('All franchisee'),
        'search_items' => __('Search franchisee'),
        'not_found' => __('No franchisee found.'),
    );
    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'franchisee'),
        'has_archive' => true,
        'hierarchical' => false,
    );
    register_post_type('franchisee', $args);
}
add_action('init', 'cw_post_type_franchisee');
/*Custom Post type end*/







/* Review Custom Post type start*/
function cw_post_type_review() {
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        'excerpt', // post excerpt
        'custom-fields', // custom fields
        'comments', // post comments
        'revisions', // post revisions
        'post-formats', // post formats
    );
    $labels = array(
        'name' => _x('Review', 'plural'),
        'singular_name' => _x('review', 'singular'),
        'menu_name' => _x('Review', 'admin menu'),
        'name_admin_bar' => _x('review', 'admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New news'),
        'new_item' => __('New review'),
        'edit_item' => __('Edit franchisee'),
        'view_item' => __('View review'),
        'all_items' => __('All review'),
        'search_items' => __('Search review'),
        'not_found' => __('No review found.'),
    );
    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'review'),
        'has_archive' => true,
        'hierarchical' => false,
    );
    register_post_type('review', $args);
}
add_action('init', 'cw_post_type_review');
/*Custom Post type end*/

function register_acf_options_pages() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Theme General Settings'),
        'menu_title'    => __('Theme Settings'),
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages');


//badge Option page
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Badge Settings',
        'menu_title'	=> 'Badge Settings',
        'menu_slug' 	=> 'badge-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

}

//Post Search function
function SearchFilter($query)
{
    if ($query->is_search) {
        $query->set('post_type', 'post');
    }
    return $query;
}

add_filter('pre_get_posts', 'SearchFilter');
