<?php
/*
*
Template Name: Ms Schema Page Teamplate
*/
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main" style="margin-left: 20%;">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
                the_content();
                // Include the page content template.
                get_template_part( 'template-parts/content', 'page' );

                ?>
                    <h2>For Heading H1: </h2><?php the_field('heading_h1'); ?><br/>
                    <h2>For Description H2: </h2><?php the_field('description_h2'); ?><br/>
                    <h2>For Price 1 H3: </h2><?php the_field('price1_h3'); ?><br/>
                    <h2>For Price 2 H3: </h2><?php the_field('price2_h3'); ?><br/>
                    <h2>For Price 1 Description: </h2><?php the_field('price1_description'); ?><br/>
                    <h2>For Price 2 Description: </h2><?php the_field('price2_description'); ?><br/>
                    <h2>For Name: </h2><?php the_field('name'); ?><br/>
                    <h2>For Mobile No.: </h2><?php the_field('mobile_no'); ?><br/>
                    <h2>For Link: </h2><?php the_field('link'); ?><br/>
                <?php

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }

                // End of the loop.
            endwhile;
            ?>

        </main><!-- .site-main -->
    </div><!-- .content-area -->
<?php get_footer(); ?>