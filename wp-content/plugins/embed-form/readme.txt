=== Embed Form ===
Contributors: jotform
Donate link: N/A
Tags: jotform, form, embed, contact form, survey form, online form, web form, online forms, web forms
Requires at least: 3.3
Tested up to: 5.1.1
Stable tag: 1.2.2
License: GNU General Public License v3
License URI: http://www.gnu.org/licenses/gpl.html

Add JotForm forms to your blog posts with a click of a button. Create contact forms, surveys, order forms, event registration forms to your posts.

== Description ==

Adding a web form to your blog post is now very easy using JotForm's Embed Form plugin. When you install it a JotForm icon will be added to your editor. You can click on it to choose one of your JotForm forms or create a new form without leaving your draft.

This plugin is great if you want to add a survey to your blog post. You can survey your readers and then write about the results on a future blog post.

Another common usage of forms in blogs is to do giveways. You can tell your users you'll be giving away something valuable to the first 10 people who fill the form.

JotForm is easy to use drag and drop form builder trusted by over 1 million users. Its intuitive user interface makes form building a breeze. JotForm is FREE upto 100 submissions per month. If your forms get submitted more than 100 times it is $9.95/month.


== Installation ==

1. Download repo's tarball/zip and extract it to your Wordpress's Plugins folder ({wordpress-folder}/wp-content/plugins)
2. Enter your Wordpress Administrator panel (http://my-wordpress.url/wp-admin)
3. Activate plugin under Plugins section
4. Go to post/page you want to embed your form
5. If you are using Gutenberg editor, add a Classic block to your post/page
6. Click on JotForm's Form Picker tool (pencil icon)
7. Select your form from your JotForm account
8. Save/Preview it
9. Voila! Your form is embedded to your post/page :)

== Frequently asked questions ==

= How to add a form? =

Adding a form to your blog post is very easy. Simply click the JotForm pencil icon on the classic editor. If you are using Gutenberg editor, you must add a Classic block first.

= What kinds of forms can I create? =

Using JotForm you can create [contact forms](http://www.jotform.com/form-templates/category/contact-form), [surveys](http://www.jotform.com/form-templates/category/survey), [lead generation forms](http://www.jotform.com/form-templates/category/lead-generation), [order forms](http://www.jotform.com/form-templates/category/order-form), [event registration forms](http://www.jotform.com/form-templates/category/event-registration), [signup forms](http://www.jotform.com/form-templates/category/signup-form), [award application forms](http://www.jotform.com/form-templates/category/awards-and-contests), [giveaway forms](http://www.jotform.com/form-templates/search/giveaway), [feedback forms](http://www.jotform.com/form-templates/category/feedback), [paypal forms](http://www.jotform.com/form-templates/search/paypal), [booking forms](http://www.jotform.com/form-templates/category/booking), [polls](http://www.jotform.com/form-templates/category/polls), [petition forms](http://www.jotform.com/form-templates/category/petition), [upload forms](http://www.jotform.com/form-templates/category/upload-forms) and [appointment forms](http://www.jotform.com/form-templates/category/appointment-form). There are [over 2000 ready-to-use form templates](http://www.jotform.com/form-templates) on JotForm. You can find one and save time creating your form.

= What is JotForm? =

[JotForm](http://www.jotform.com) is a free form builder. It is trusted by over 1 millions users. Two things make JotForm great: Easy to use form builder and large number of ready-to-use form templates. You can choose from over 2500 form templates and quickly create your forms.

= Is it Free? =

JotForm is free up to 100 form submissions per month. If you go over that limit you will need to upgrade to a Bronze plan at $19/month.




== Screenshots ==

1. JotForm - Form Picker icon

2. Select your forms from your JotForm account

3. JotForm code on your post/page

4. Your Form in Action

5. Classic block on Gutenberg editor

6. JotForm - Form Picker icon on Gutenberg editor

== Changelog ==

1. Form picker added to select your forms from your JotForm account.

== Upgrade notice ==

N/A
