
	jQuery( document ).ready( function( $ )
	{
		$( '#SchemaAppAdvanced-Rating-ShortCode' ).focus( function()
		{
			if ( $( this ).val() )
			{
				$( this ).select();
			}
		});

		$( '#SchemaAppAdvanced-Rating .editor-insert' ).click( function( e )
		{
			e.preventDefault();

			if ( $( '#SchemaAppAdvanced-Rating-ShortCode' ).val() )
			{
				if ( tinyMCE && tinyMCE.activeEditor )
				{
					tinyMCE.activeEditor.execCommand( 'mceInsertContent', false, $( '#SchemaAppAdvanced-Rating-ShortCode' ).val() );
				}
				else
				{
					var Elm = $( 'textarea#content' );

					Elm.val( Elm.val().substring( 0, Elm.prop( 'selectionStart' ) ) + $( '#SchemaAppAdvanced-Rating-ShortCode' ).val() + Elm.val().substring( Elm.prop( 'selectionEnd' ), Elm.val().length ) );
				}
			}
		});

	});
