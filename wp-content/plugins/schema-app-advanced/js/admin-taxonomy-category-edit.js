jQuery( document ).ready( function( $ )
{
	$( '.term-hunch-schema-wikipedia-url-wrap .description .button.search' ).click( function( e )
	{
		e.preventDefault();

		var ElmWrap = $( '.term-hunch-schema-wikipedia-url-wrap' );
		var InputName = ElmWrap.find( 'input[name="HunchSchemaWikipediaSearch"]' );
		var InputURL = ElmWrap.find( 'input[name="HunchSchemaWikipediaURL"]' );
		var SearchResult = ElmWrap.find( '.search-result' );

		if ( InputName.val() )
		{
			SearchResult.html( 'Loading...' );

			if ( this.ajax )
			{
				this.ajax.abort();
			}

			this.ajax = $.ajax
			({
				type: 'POST',
				url: ajaxurl,
				data: { 'action': 'HunchSchemaWikipediaURLSearch', 'Name': InputName.val() },
				dataType: 'json'
			})
			.done( function( Response )
			{
				if ( Response && Response.Status == 'Ok' && Response.Data.query.pages )
				{
					SearchResult.html( 'Search Results:' );

					for ( var I in Response.Data.query.pages )
					{
						SearchResult.append( '<p><input type="radio" name="TempHunchSchemaWikipediaURL" value="' + Response.Data.query.pages[I].fullurl + '"> <a href="' + Response.Data.query.pages[I].fullurl + '" target="_blank">' + Response.Data.query.pages[I].title + '</a></p>' );
					}
				}
				else if ( Response && Response.Status == 'Error' )
				{
					console.log( 'Wikipedia:' );
					console.log( Response );

					SearchResult.html( Response.Message );
				}
				else
				{
					SearchResult.html( 'No data received' );
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown )
			{
				SearchResult.html( 'Connection error: ' + errorThrown );
			});
		}
		else
		{
			SearchResult.html( 'Please enter search term' );
		}
	});


	$( '.term-hunch-schema-wikipedia-url-wrap' ).on( 'click', 'input[name="TempHunchSchemaWikipediaURL"]', function()
	{
		$( '.term-hunch-schema-wikipedia-url-wrap' ).find( 'input[name="HunchSchemaWikipediaURL"]' ).val( $( this ).val() );
	});


	$( '.term-hunch-schema-wikipedia-url-wrap .description .button.clear' ).click( function( e )
	{
		e.preventDefault();

		var ElmWrap = $( '.term-hunch-schema-wikipedia-url-wrap' );

		ElmWrap.find( 'input[name="HunchSchemaWikipediaURL"]' ).val( '' );
		ElmWrap.find( '.search-result' ).empty();
	});


	if ( ! $( '.term-hunch-schema-wikipedia-url-wrap input[name="HunchSchemaWikipediaURL"]' ).val() )
	{
		$( '.term-hunch-schema-wikipedia-url-wrap .description .button.search' ).click();
	}


	$( '.term-hunch-schema-wikidata-url-wrap .description .button.search' ).click( function( e )
	{
		e.preventDefault();

		var ElmWrap = $( '.term-hunch-schema-wikidata-url-wrap' );
		var InputName = ElmWrap.find( 'input[name="HunchSchemaWikiDataSearch"]' );
		var InputURL = ElmWrap.find( 'input[name="HunchSchemaWikiDataURL"]' );
		var SearchResult = ElmWrap.find( '.search-result' );

		if ( InputName.val() )
		{
			SearchResult.html( 'Loading...' );

			if ( this.ajax )
			{
				this.ajax.abort();
			}

			this.ajax = $.ajax
			({
				type: 'POST',
				url: ajaxurl,
				data: { 'action': 'HunchSchemaWikiDataURLSearch', 'Name': InputName.val() },
				dataType: 'json'
			})
			.done( function( Response )
			{
				if ( Response && Response.Status == 'Ok' && Response.Data.search )
				{
					SearchResult.html( 'Search Results:' );

					for ( var I in Response.Data.search )
					{
						SearchResult.append( '<p><input type="radio" name="TempHunchSchemaWikiDataURL" value="' + Response.Data.search[I].concepturi + '"> <a href="' + Response.Data.search[I].concepturi + '" target="_blank">' + Response.Data.search[I].label + '</a></p>' );
					}
				}
				else if ( Response && Response.Status == 'Error' )
				{
					console.log( 'WikiData:' );
					console.log( Response );

					SearchResult.html( Response.Message );
				}
				else
				{
					SearchResult.html( 'No data received' );
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown )
			{
				SearchResult.html( 'Connection error: ' + errorThrown );
			});
		}
		else
		{
			SearchResult.html( 'Please enter search term' );
		}
	});


	$( '.term-hunch-schema-wikidata-url-wrap' ).on( 'click', 'input[name="TempHunchSchemaWikiDataURL"]', function()
	{
		$( '.term-hunch-schema-wikidata-url-wrap' ).find( 'input[name="HunchSchemaWikiDataURL"]' ).val( $( this ).val() );
	});


	$( '.term-hunch-schema-wikidata-url-wrap .description .button.clear' ).click( function( e )
	{
		e.preventDefault();

		var ElmWrap = $( '.term-hunch-schema-wikidata-url-wrap' );

		ElmWrap.find( 'input[name="HunchSchemaWikiDataURL"]' ).val( '' );
		ElmWrap.find( '.search-result' ).empty();
	});


	if ( ! $( '.term-hunch-schema-wikidata-url-wrap input[name="HunchSchemaWikiDataURL"]' ).val() )
	{
		$( '.term-hunch-schema-wikidata-url-wrap .description .button.search' ).click();
	}

});