<?php
/*
	Plugin Name: Schema App Advanced
	Plugin URI:  http://www.schemaapp.com
	Description: This plugin adds advanced Schema.org structured data use cases, Products, AggregateRating, Linked Entities
	Version:     1.8.9
	Author:      Hunch Manifest
	Author URI:  https://www.hunchmanifest.com
*/


	if ( ! defined( 'ABSPATH' ) )
	{
		exit;
	}


	if ( ! class_exists( 'SchemaAppAdvanced' ) )
	{
		class SchemaAppAdvanced
		{
			private static $Instance;


			public static function Instance()
			{
				if ( ! self::$Instance )
				{
					self::$Instance = new self();
				}

				return self::$Instance;
			}


			public function __construct()
			{
				$this->Version = '1.8.9';
				$this->Name = 'Schema App Advanced';
				$this->File = __FILE__;
				$this->Path = trailingslashit( dirname( $this->File ) );
				$this->URL = trailingslashit( plugin_dir_url( $this->File ) );
				$this->UpdateURL = 'https://app.schemaapp.com/assets/scripts/schema-app-advanced-plugin-api.php';
				$this->SettingsName = 'SchemaAppAdvanced';
				$this->Settings = get_option( $this->SettingsName );
				$this->SettingsSchemaApp = get_option( 'schema_option_name' );

				$this->SettingsDefaults = array
				(
					'Version' => $this->Version,
					'LicenseStatus' => 0,
					'NoticeLicenseInactive' => 0,
					'Rating' => array
					(
						'ShowOnPages' => 0,
						'ShowOnPosts' => 0,
						'Placement' => 2,
						'AJAXRetrieve' => 0,
						'CustomCSS' => 'star-injector { 
    width: 100%; 
    margin: 3em 0;
    text-align: center;
    display: inline-block;
}',
						'injectFontAwesome' => 1,
						'injectCustomStyle' => 1,
						'fontFamily' => 'helvetica',
						'starOutline' => 1,
						'starPrimaryColour' => 'gold',
						'starUnselectedColour' => 'lightgray',
					),
					'WooCommerce' => array
					(
						'itemCondition' => 'NewCondition',
						'V3DefaultMarkup' => 0,
						'description' => 'short',
					),
					'CPTSchemaMapping' => array(),
				);

				$this->FormFieldStatusOptions = array( 1 => 'Enabled', 0 => 'Disabled' );

				$this->Init();
			}


			public function Init()
			{
				spl_autoload_register( array( $this, 'AutoloadRegister' ) );


				register_activation_hook( $this->File, array( $this, 'HookActivation' ) );

				//add_filter( 'plugin_action_links_' . plugin_basename( $this->File ), array( $this, 'HookPluginActionLinks' ) );
				add_action( 'admin_init', array( $this, 'HookAdminInit' ) );
				add_action( 'admin_enqueue_scripts', array( $this, 'HookAdminEnqueueScripts' ), 10, 1 );
				add_action( 'admin_notices', array( $this, 'HookAdminNotices' ) );
				//add_action( 'admin_menu', array( $this, 'HookAdminMenu' ) );

				add_action( 'hunch_schema_settings_sanitize', array( $this, 'HookHunchSchemaSettingsSanitize' ), 10, 3 );
				add_action( 'hunch_schema_settings_section', array( $this, 'HookHunchSchemaSettingsSection' ), 10, 3 );
				add_action( 'hunch_schema_settings_nav_tab', array( $this, 'HookHunchSchemaSettingsNavTab' ) );
				add_action( 'hunch_schema_settings_nav_tab_content', array( $this, 'HookHunchSchemaSettingsNavTabContent' ) );

				add_action( "$this->SettingsName-Cron-Single-Activation", array( $this, 'HookWPSchedule' ) );


				if ( wp_next_scheduled ( 'wp_scheduled_delete' ) )
				{
					add_action( 'wp_scheduled_delete', array( $this, 'HookWPSchedule' ), 100 );
				}
				elseif ( wp_next_scheduled ( 'wp_version_check' ) )
				{
					add_action( 'wp_version_check', array( $this, 'HookWPSchedule' ), 100 );
				}
				elseif ( ! wp_next_scheduled ( "$this->SettingsName-Cron-Activation" ) )
				{
					wp_schedule_event( time(), 'daily', "$this->SettingsName-Cron-Activation" );
					add_action( "$this->SettingsName-Cron-Activation", array( $this, 'HookWPSchedule' ) );
				}


				if ( ! empty( $this->Settings['LicenseStatus'] ) )
				{
					add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'HookSiteTransientUpdatePlugins' ) );
					add_filter( 'plugins_api', array( $this, 'HookPluginsApi' ), 10, 3 );
				}


				SchemaAppWooCommerce::get_instance();
				SchemaAppRating::get_instance();
				SchemaAppLinked::get_instance();
				SchemaAppCPT::get_instance();
			}


			public function HookActivation( $NetworkWide = false )
			{
				if ( ! current_user_can( 'activate_plugins' ) )
				{
					return;
				}

				if ( is_multisite() && ( $NetworkWide || is_plugin_active_for_network( plugin_basename( $this->File ) ) ) )
				{
					foreach ( get_blog_list( 0, 'all' ) as $Blog )
					{
						switch_to_blog( $Blog['blog_id'] );

							$SettingsCurrent = get_option( $this->SettingsName );

							if ( is_array( $SettingsCurrent ) )
							{
								$Settings = array_replace_recursive( $this->SettingsDefaults, $SettingsCurrent );
								$Settings = array_intersect_key( $Settings, $this->SettingsDefaults );

								$Settings['Version'] = $this->Version;

								update_option( $this->SettingsName, $Settings );
							}
							else
							{
								delete_option( $this->SettingsName );
								add_option( $this->SettingsName, $this->SettingsDefaults );
							}

						restore_current_blog();
					}
				}
				else
				{
					$SettingsCurrent = get_option( $this->SettingsName );

					if ( is_array( $SettingsCurrent ) )
					{
						$Settings = array_replace_recursive( $this->SettingsDefaults, $SettingsCurrent );
						$Settings = array_intersect_key( $Settings, $this->SettingsDefaults );

						$Settings['Version'] = $this->Version;

						update_option( $this->SettingsName, $Settings );
					}
					else
					{
						delete_option( $this->SettingsName );
						add_option( $this->SettingsName, $this->SettingsDefaults );
					}

					$this->Settings = get_option( $this->SettingsName );

					wp_schedule_single_event( time(), "$this->SettingsName-Cron-Single-Activation" );
				}
			}


			/*public function HookPluginActionLinks( $Links )
			{
				$Link = sprintf( '<a href="%s">Settings</a>', admin_url( "options-general.php?page=$this->SettingsName" ) );

				array_push( $Links, $Link );

				return $Links;
			}*/


			public function HookSiteTransientUpdatePlugins( $Transient )
			{
				if ( $Transient )
				{
					$PluginData = get_plugin_data( $this->File );

					$Response = wp_remote_retrieve_body( wp_remote_post( $this->UpdateURL, array( 'body' => array( 'Action' => 'Version', 'URL' => site_url() ) ) ) );
					$Response = $Response ? unserialize ( $Response ) : $Response;

					if ( is_object( $Response ) && version_compare( $PluginData['Version'], $Response->new_version, '<' ) )
					{
						$Transient->response[ plugin_basename( $this->File ) ] = $Response;
					}
				}

				return $Transient;
			}


			public function HookPluginsApi( $False, $Action, $Arguments )
			{
				if ( ! empty( $Arguments->slug ) && $Arguments->slug == basename( $this->File, '.php' ) && $Action == 'plugin_information' )
				{
					$Response = wp_remote_retrieve_body( wp_remote_post( $this->UpdateURL, array( 'body' => array( 'Action' => 'Info', 'URL' => site_url() ) ) ) );
					$Response = $Response ? unserialize ( $Response ) : $Response;

					if ( is_object( $Response ) )
					{
						return $Response;
					}
					else
					{
						return new WP_Error( 'plugin_api_failed', 'An unexpected error occurred. Please <a href="#" onclick="document.location.reload(); return false;">try again</a>.' );
					}
				}

				return $False;
			}


			public function HookAdminInit()
			{
				register_setting( $this->SettingsName, $this->SettingsName, array( $this, 'SettingsSanitize' ) );


				if ( empty( $this->Settings['Version'] ) || version_compare( $this->Settings['Version'], $this->Version, '<' ) )
				{
					$this->HookActivation();

					add_action( 'admin_notices', function() { print "<div class=\"notice notice-success is-dismissible\"> <p><strong>{$this->Name}:</strong> Plugin settings has been successfully updated.</p> </div>"; } );
				}

				if ( isset( $_GET["$this->SettingsName-Dismiss"] ) && $_GET["$this->SettingsName-Dismiss"] == 'LicenseInactive' )
				{
					$this->Settings['NoticeLicenseInactive'] = 1;

					update_option( $this->SettingsName, $this->Settings );

					wp_redirect( add_query_arg( "$this->SettingsName-Dismiss", false ) );
					exit;
				}

				if ( isset( $_GET[$this->SettingsName] ) && $_GET[$this->SettingsName] == 'LicenseCheck' )
				{
					$this->HookWPSchedule();

					wp_redirect( add_query_arg( $this->SettingsName, false ) );
					exit;
				}
			}


			public function SettingsSanitize( $Settings )
			{
				if ( ! empty( $Settings['CPTSchemaMapping'] ) )
				{
					foreach ( $Settings['CPTSchemaMapping'] as $Key => $Value )
					{
						$Settings['CPTSchemaMapping'][$Key] = str_ireplace( array( 'http://', 'https://', 'schema.org/' ), '', $Value );
					}
				}

				if ( isset( $Settings['Rating']['CustomCSS'] ) )
				{
					$Settings['Rating']['CustomCSS'] = trim( $Settings['Rating']['CustomCSS'] );

					if ( empty( $Settings['Rating']['CustomCSS'] ) )
					{
						unset( $Settings['Rating']['CustomCSS'] );
					}
				}

				$Settings = array_merge( $this->Settings, $Settings );
				$Settings = array_replace_recursive( $this->SettingsDefaults, $Settings );
				$Settings = array_intersect_key( $Settings, $this->SettingsDefaults );

				$Settings = $this->SettingsSanitizeText( $Settings );

				return $Settings;
			}


			public function SettingsSanitizeText( $Settings )
			{
				foreach ( $Settings as $Key => $Value )
				{
					if ( is_array( $Value ) )
					{
						$this->SettingsSanitizeText( $Value );
					}
					else
					{
						$Value = sanitize_text_field( $Value );
					}

					$Settings[$Key] = $Value;
				}

				return $Settings;
			}


			public function HookAdminEnqueueScripts( $Hook )
			{
				if ( $Hook == 'post.php' || $Hook == 'post-new.php' )
				{
					wp_enqueue_script( "$this->SettingsName-Admin-Post", $this->URL . 'js/admin-post.js', array( 'jquery' ), $this->Version );
				}
			}


			public function HookAdminNotices()
			{
				if ( empty( $this->Settings['NoticeLicenseInactive'] ) && empty( $this->Settings['LicenseStatus'] ) )
				{
					printf( '<div class="error"><p><strong>%s:</strong> Your license is not active, please <a href="options-general.php?page=schema-app-setting">check the Account ID</a> is associated with your paid <a href="https://app.schemaapp.com/subaccount" target="_blank">Schema App account</a>. &nbsp; <a href="%s">Dismiss</a></p></div>', $this->Name, add_query_arg( "$this->SettingsName-Dismiss", 'LicenseInactive' ) );
				}
			}


			/*public function HookAdminMenu()
			{
				$PageHook = add_options_page( $this->Name, $this->Name, 'manage_options', $this->SettingsName, array( $this, 'AdminMenuRender' ) );
			}


			public function AdminMenuRender()
			{
				if ( ! current_user_can( 'manage_options' ) )
				{
					wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
				}

				if ( isset( $_POST['action'] ) && ! wp_verify_nonce( $_POST['nonce'], $this->SettingsName ) )
				{
					wp_die( __( 'Security check failed! Settings not saved.' ) );
				}

				?>

					<style>
						.nav-tab-content { display: none; }
						.nav-tab-content-active { display: block; }
					</style>


					<div class="wrap">

						<h2><?php print $this->Name; ?></h2>


						<?php if ( isset( $_GET['Debug'] ) ) : ?>

							<h3>Debug Data:</h3>

							<div style="height: 400px; margin: 15px 0; padding: 0 15px; background-color: #ffffff; overflow: auto;">

								<pre>Settings: <?php print_r( $this->Settings ); ?> </pre>

							</div>

						<?php endif; ?>


						<form method="post" action="options.php">


							<?php settings_fields( $this->SettingsName ); ?>


							<h2 class="nav-tab-wrapper">
								<a href="#WooCommerce" class="nav-tab nav-tab-active" data-tab="WooCommerce">WooCommerce</a>
								<a href="#Rating" class="nav-tab" data-tab="Rating">Rating</a>
							</h2>


							<div class="nav-tab-content nav-tab-content-active" data-tab="WooCommerce">

								<?php do_settings_sections( "$this->SettingsName-WooCommerce" ); submit_button(); ?>

							</div>


							<div class="nav-tab-content" data-tab="Rating">

								<?php do_settings_sections( "$this->SettingsName-Rating" ); submit_button(); ?>

							</div>

						</form>


					</div>

					<script>

						jQuery( document ).ready( function( $ )
						{
							$( '.nav-tab-wrapper a' ).click( function( e )
							{
								Elm = $( this );

								Elm.blur();

								Elm.parent().find( 'a.nav-tab-active' ).removeClass( 'nav-tab-active' );
								Elm.addClass( 'nav-tab-active' );

								Elm.parent().siblings( '.nav-tab-content-active' ).fadeOut( 'fast', function()
								{
									$( this ).removeClass( 'nav-tab-content-active' );
									Elm.parent().siblings( '.nav-tab-content[data-tab="' + Elm.attr( 'data-tab' ) + '"]' ).fadeIn( 'fast' ).addClass( 'nav-tab-content-active' );
								});
							});


							function NavTabHash()
							{
								if ( document.location.hash )
								{
									var Hash = document.location.hash.substring( 1 );

									if ( Hash.search( '-' ) )
									{
										var Hash1 = Hash.slice( 0, Hash.indexOf( '-' ) );
										$( 'a.nav-tab[data-tab="' + Hash1 + '"]' ).click();
									}

									$( 'a.nav-tab[data-tab="' + Hash + '"]' ).click();
								}
							}

							NavTabHash();

							$( window ).on( 'hashchange', function()
							{
								NavTabHash();
							});
						});

					</script>

				<?php

			}*/


			public function HookHunchSchemaSettingsSanitize( $Name, $Old, $New )
			{
				if ( $Name == 'graph_uri' && $Old != $New )
				{
					wp_schedule_single_event( time(), "$this->SettingsName-Cron-Single-Activation" );
				}
			}


			public function HookHunchSchemaSettingsSection( $Name, $Page, $Section )
			{
				if ( $Page == 'schema-app-license' && $Section == 'license_settings' )
				{
					add_settings_field( 'schema_license_advanced_status', 'Schema App Advanced Status', array( $this, 'SettingsFieldRenderLicenseAdvancedStatus' ), $Page, $Section );
				}
			}


			public function SettingsFieldRenderLicenseAdvancedStatus( $Arguments )
			{
				printf( '<p><strong>%s</strong> &nbsp; <a href="%s">Check</a></p> <p class="description">This license Activates for AccountID associated with current Schema App Subscribers. If this is inactive, advanced features have no schema markup effects.</p>', ! empty( $this->Settings['LicenseStatus'] ) ? '<span style="color: green;">Active</span>' : '<span style="color: red;">Inactive</span>', add_query_arg( $this->SettingsName, 'LicenseCheck' ) );
			}


			public function HookHunchSchemaSettingsNavTab()
			{

				?>

					<a class="nav-tab" href="#WooCommerce" data-tab="WooCommerce">WooCommerce</a>
					<a class="nav-tab" href="#Rating" data-tab="Rating">Rating</a>
					<a class="nav-tab" href="#Linked" data-tab="Linked">Linked Data</a>
					<a class="nav-tab" href="#CustomPostTypes" data-tab="CustomPostTypes">Custom Posts</a>

				<?php

			}


			public function HookHunchSchemaSettingsNavTabContent()
			{

				?>

					<section id="schema-app-settings-WooCommerce">
						<form method="post" action="options.php">
							<?php
								settings_fields( $this->SettingsName );
								do_settings_sections( "$this->SettingsName-WooCommerce" );
								submit_button();
							?>
						</form>
					</section>

					<section id="schema-app-settings-Rating">
						<form method="post" action="options.php">
							<?php
								settings_fields( $this->SettingsName );
								do_settings_sections( "$this->SettingsName-Rating" );
								submit_button();
							?>
						</form>
					</section>

					<section id="schema-app-settings-Linked">
						<?php
							settings_fields( $this->SettingsName );
							do_settings_sections( "$this->SettingsName-Linked" );
						?>
					</section>

					<section id="schema-app-settings-CustomPostTypes">
						<form method="post" action="options.php">
							<?php
								settings_fields( $this->SettingsName );
								do_settings_sections( "$this->SettingsName-CPT" );
								submit_button();
							?>
						</form>
					</section>

				<?php

			}


			public function HookWPSchedule()
			{
				if ( ! empty( $this->SettingsSchemaApp['graph_uri'] ) )
				{
					$Response = wp_remote_retrieve_body( wp_remote_get( sprintf( 'https://app.schemaapp.com/api/utility/paid-account?accountId=%s&_base=<http://schemaapp.com/resources/admin>&_format=json-simple', $this->SettingsSchemaApp['graph_uri'] ) ) );

					if ( $Response )
					{
						$ResponseJson = json_decode( $Response );

						if ( $ResponseJson && count( $ResponseJson ) )
						{
							if ( empty( $this->Settings['LicenseStatus'] ) )
							{
								$this->Settings['LicenseStatus'] = 1;

								update_option( $this->SettingsName, $this->Settings );
							}
						}
						else
						{
							if ( ! empty( $this->Settings['LicenseStatus'] ) )
							{
								$this->Settings['LicenseStatus'] = 0;

								update_option( $this->SettingsName, $this->Settings );
							}
						}
					}
				}
				else
				{
					if ( ! empty( $this->Settings['LicenseStatus'] ) )
					{
						$this->Settings['LicenseStatus'] = 0;

						update_option( $this->SettingsName, $this->Settings );
					}
				}
			}


			public function SettingsFormField( $Section = '', $Name = '', $Type = 'Text', $Options = array(), $Description = '' )
			{
				if ( ! $Name )
				{
					return;
				}


				$FieldName = sprintf( '%s%s[%s]', $this->SettingsName, ( $Section ? "[$Section]" : '' ), $Name );

				if ( $Section )
				{
					$Value = isset( $this->Settings[$Section][$Name] ) ? $this->Settings[$Section][$Name] : '';
				}
				else
				{
					$Value = isset( $this->Settings[$Name] ) ? $this->Settings[$Name] : '';
				}


				if ( $Type == 'Text' )
				{
					printf( '<input type="text" name="%s" value="%s" class="regular-text">', $FieldName, esc_attr( $Value ) );
				}
				elseif ( $Type == 'Textarea' )
				{
					printf( '<textarea name="%s" rows="4" class="regular-text">%s</textarea>', $FieldName, esc_attr( $Value ) );
				}
				elseif ( $Type == 'Checkbox' )
				{
					printf( '<input type="checkbox" name="%s" value="%s"%s>', $FieldName, esc_attr( $Value ), checked( $Value, 1, false ) );
				}
				elseif ( $Type == 'Select' && count( $Options ) )
				{
					printf( '<select name="%s">', $FieldName );

					foreach ( $Options as $OptionKey => $OptionValue )
					{
						if ( isset( $OptionValue['Id'], $OptionValue['Label'] ) )
						{
							$OptionId = $OptionValue['Id'];
							$OptionLabel = $OptionValue['Label'];
						}
						else
						{
							$OptionId = $OptionKey;
							$OptionLabel = $OptionValue;
						}

						printf( '<option value="%s" %s>%s</option>', $OptionId, selected( $Value, $OptionId, false ), $OptionLabel );
					}

					print '</select>';
				}


				if ( $Description )
				{
					printf( '<p class="description">%s</p>', $Description );
				}
			}


			public function markup_encode( $data = array() ) {
				if ( ! empty( $data ) ) {
					foreach ( $data as $key => &$value ) {
						if ( is_array( $value ) ) {
							$this->markup_encode( $value );
						} else {
							if ( $value === null ) {
								unset( $data[$key] );
							}
						}
					}


					// https://wiki.php.net/rfc/precise_float_value
					if ( version_compare( phpversion(), '7.1', '>=' ) ) {
						$ini_serialize_precision = ini_get( 'serialize_precision' );

						ini_set( 'serialize_precision', -1 );

						$json = wp_json_encode( $data );

						ini_set( 'serialize_precision', $ini_serialize_precision );
					} else {
						$json = wp_json_encode( $data );
					}


					return $json;
				}

				return;
			}


			public function AutoloadRegister( $ClassName )
			{
				if ( stripos( $ClassName, 'SchemaApp' ) !== false || stripos( $ClassName, 'HunchSchema_CustomPostType' ) !== false )
				{
					$ClassName = ltrim( $ClassName, '\\' );
					$FileName = '';
					$Namespace = '';

					if ( $LastNamespace = strrpos( $ClassName, '\\' ) )
					{
						$Namespace = substr( $ClassName, 0, $LastNamespace );
						$ClassName = substr( $ClassName, $LastNamespace + 1 );
						$FileName  = str_replace( '\\', DIRECTORY_SEPARATOR, $Namespace ) . DIRECTORY_SEPARATOR;
					}

					$FileName .= str_replace( '_', DIRECTORY_SEPARATOR, $ClassName ) . '.php';

					$FileName = $this->Path . 'classes' . DIRECTORY_SEPARATOR . $FileName;

					require_once $FileName;
				}
			}

		}


		SchemaAppAdvanced::Instance();
	}

?>