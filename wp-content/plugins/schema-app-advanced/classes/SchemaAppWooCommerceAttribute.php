<?php

	if ( ! defined( 'ABSPATH' ) )
	{
		exit;
	}


	if ( ! class_exists( 'SchemaAppWooCommerceAttribute' ) )
	{
		class SchemaAppWooCommerceAttribute
		{
			private $_attributes; 
			private $_product; 
			
			public function __construct($product)
			{
				$this->_attributes = $product->get_attributes();
				$this->_product = $product;
			}
			
			// Find attribute for property
			public function assignProperty(&$schemaObj, $needle)
			{
				foreach ( $this->_attributes as $key => $attrib ) {
					if ( stristr($key, $needle)) {
						$schemaObj[$needle] = $this->_product->get_attribute( $key );
						unset($this->_attributes[$key]);
						return;
					}
				}
			}
			
			// Map remaining PropertyValue
			public function assignRemaining(&$schemaObj)
			{
				$prop = array();
				foreach ( $this->_attributes as $key => $attrib ) {
					if ( $attrib['is_visible'] === 1 && $attrib['is_variation'] === 0 ) {
						$prop[] = array(
							'@type' => 'PropertyValue',
							'name' => $key,
							'value' => $this->_product->get_attribute( $key )
						);
					}
				}
				if ( count($prop) > 0 ) {
					$schemaObj['additionalProperty'] = $prop;
				}
			}

		}
	}

?>