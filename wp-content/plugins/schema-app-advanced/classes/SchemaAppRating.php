<?php

	if ( ! defined( 'ABSPATH' ) )
	{
		exit;
	}


	if ( ! class_exists( 'SchemaAppRating' ) )
	{
		class SchemaAppRating extends SchemaAppAdvanced
		{
			private static $instance;


			public static function get_instance() {
				if ( ! self::$instance ) {
					self::$instance = new self();
				}

				return self::$instance;
			}


			public function __construct()
			{
				if ( self::$instance ) {
					return self::$instance;
				}

				parent::__construct();

				add_action( 'admin_init', array( $this, 'HookAdminInit' ) );
				add_action( 'admin_enqueue_scripts', array( $this, 'HookAdminEnqueueScripts' ), 10, 1 );
				add_action( 'add_meta_boxes', array( $this, 'HookMetaBoxes' ), 10, 2 );
				add_action( 'hunch_schema_meta_box', array( $this, 'HookHunchSchemaMetaBox' ), 10, 1 );
				add_action( 'widgets_init', array( $this, 'HookWidgetsInit' ) );
				add_action( 'save_post', array( $this, 'HookSavePost' ), 10, 3 );

				add_shortcode( 'schemaapprating', array( $this, 'HookShortCode' ) );

				if ( ! empty( $this->Settings['LicenseStatus'] ) )
				{
					add_action( 'wp_enqueue_scripts', array( $this, 'HookWPEnqueueScripts' ) );
					add_action( 'hunch_schema_markup_render', array( $this, 'HookHunchSchemaMarkupRender' ), 10, 5 );
					add_filter( 'hunch_schema_markup', array( $this, 'HookHunchSchemaMarkup' ), 10, 4 );
					add_filter( 'the_content', array( $this, 'HookTheContent' ) );
					add_action( 'wp_ajax_HunchSchemaRatingGet', array( $this, 'HookJAAXHunchSchemaRatingGet' ) );
					add_action( 'wp_ajax_nopriv_HunchSchemaRatingGet', array( $this, 'HookJAAXHunchSchemaRatingGet' ) );
					add_action( 'wp_ajax_HunchSchemaRatingSubmit', array( $this, 'HookJAAXHunchSchemaRatingSubmit' ) );
					add_action( 'wp_ajax_nopriv_HunchSchemaRatingSubmit', array( $this, 'HookJAAXHunchSchemaRatingSubmit' ) );
				}
			}


			public function Init()
			{
			}


			public function HookAdminInit()
			{
				add_settings_section( 'SchemaAppRatingGuide', '', array( $this, 'SettingsSectionRenderGuide' ), "$this->SettingsName-Rating" );

				add_settings_section( 'SchemaAppRatingGeneral', '', array( $this, 'SettingsSectionRenderGeneral' ), "$this->SettingsName-Rating" );
				add_settings_field( 'ShowOnPages', 'Show on Pages', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingGeneral', array( 'Name' => 'ShowOnPages', 'Type' => 'Select', 'Options' => $this->FormFieldStatusOptions, 'Description' => 'Show on all Pages.' ) );
				add_settings_field( 'ShowOnPosts', 'Show on Posts', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingGeneral', array( 'Name' => 'ShowOnPosts', 'Type' => 'Select', 'Options' => $this->FormFieldStatusOptions, 'Description' => 'Show on all Posts.' ) );
				add_settings_field( 'Placement', 'Placement', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingGeneral', array( 'Name' => 'Placement', 'Type' => 'Select', 'Options' => array( 1 => 'Before Content', 2 => 'After Content' ), 'Description' => 'Placement of Rating around content.' ) );
				add_settings_field( 'AJAXRetrieve', 'Retrieve using AJAX', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingGeneral', array( 'Name' => 'AJAXRetrieve', 'Type' => 'Select', 'Options' => $this->FormFieldStatusOptions, 'Description' => 'Retrieve initial rating using AJAX. Useful when you want to load current rating data on cached pages.' ) );

				add_settings_section( 'SchemaAppRatingDisplay', '', array( $this, 'SettingsSectionRenderDisplay' ), "$this->SettingsName-Rating" );
				add_settings_field( 'CustomCSS', 'Custom CSS', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingDisplay', array( 'Name' => 'CustomCSS', 'Type' => 'Textarea', 'Description' => 'Optimize the placement of the widget using custom CSS.' ) );
				add_settings_field( 'injectFontAwesome', 'Font Awesome', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingDisplay', array( 'Name' => 'injectFontAwesome', 'Type' => 'Select', 'Options' => $this->FormFieldStatusOptions, 'Description' => 'Inject the fontAwesome CSS classes.' ) );
				add_settings_field( 'injectCustomStyle', 'Custom Styles', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingDisplay', array( 'Name' => 'injectCustomStyle', 'Type' => 'Select', 'Options' => $this->FormFieldStatusOptions, 'Description' => 'Inject custom styles used by the rating widget.' ) );
				add_settings_field( 'fontFamily', 'Font Family', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingDisplay', array( 'Name' => 'fontFamily', 'Type' => 'Text', 'Description' => 'Sets the font family of displayed text.' ) );
				add_settings_field( 'starOutline', 'Star Outline', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingDisplay', array( 'Name' => 'starOutline', 'Type' => 'Select', 'Options' => $this->FormFieldStatusOptions, 'Description' => 'Turns the outline around the stars on or off.' ) );
				add_settings_field( 'starPrimaryColour', 'Star Primary Colour', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingDisplay', array( 'Name' => 'starPrimaryColour', 'Type' => 'Text', 'Description' => 'Determines the colour of the stars that are filled.' ) );
				add_settings_field( 'starUnselectedColour', 'Star Unselected Colour', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-Rating", 'SchemaAppRatingDisplay', array( 'Name' => 'starUnselectedColour', 'Type' => 'Text', 'Description' => 'Determines the colour of the stars that are not filled.' ) );
			}


			public function SettingsSectionRenderGuide( $Arguments )
			{
				print '<h2>Instructions</h2>';
				print '<p>The Rating Widget fixes the "islands" of markup that are common with ratings widgets. Other star rating widgets create schema markup that is not connected to the main data item of the page. This plugin aims to fix these silos of markup, providing AggregateRating schema markup that is directly connected to both custom data items or default markup.</p>';
				print '<p>You can enable the Rating widget in several ways. <ol>';
				print '<li>Include the WP Shortcode <code>[schemaapprating]</code> anywhere on your site</li>';
				print '<li>Include the Rating Widget for all Pages or Posts with General Settings below</li>';
				print '<li>Use Rating Widget Feature in side bar, footer, etc</li>';
				print '</ol></p>';
			}
                        
			public function SettingsSectionRenderGeneral( $Arguments )
			{
				print '<h3>General Settings</h3>';
			}


			public function SettingsSectionRenderDisplay( $Arguments )
			{
				print '<h3>Display Settings</h3>';
				print '<p>Use the settings below for star widget CSS customization.</p>';

				if ( ! empty( $this->SettingsSchemaApp['graph_uri'] ) )
				{
					wp_enqueue_style( "$this->SettingsName-Style-Rating" );
					wp_enqueue_script( "$this->SettingsName-Script-Rating" );

					printf( '<table class="form-table"> <tbody> <tr> <th scope="row">Preview</th> <td> <div style="float: left;"><star-injector data-account-id="%s"></star-injector></div> </td> </tr> </tbody></table>', $this->SettingsSchemaApp['graph_uri'] );
				}
			}


			public function SettingsFieldRender( $Arguments )
			{
				$this->SettingsFormField( 'Rating', $Arguments['Name'], $Arguments['Type'], ( ! empty( $Arguments['Options'] ) ? $Arguments['Options'] : array() ), ( ! empty( $Arguments['Description'] ) ? $Arguments['Description'] : '' ) );
			}


			public function HookAdminEnqueueScripts( $Hook )
			{
				if ( $Hook == 'settings_page_schema-app-setting' )
				{
					$this->HookWPEnqueueScripts();

					wp_localize_script( "$this->SettingsName-Script-Rating", 'HunchSchemaMarkupRatingSettings', array( 'SubmitURL' => admin_url( 'admin-ajax.php' ), 'MarkupType' => 'Default', 'PostId' => 0, 'RatingCount' => 10, 'RatingValue' => 3.5 ) );
				}
			}


			public function HookMetaBoxes( $PostType, $Post )
			{
				add_meta_box( "$this->SettingsName-Rating", "$this->Name - Rating", array( $this, 'MetaBoxRender' ), 'page', 'side' );
				add_meta_box( "$this->SettingsName-Rating", "$this->Name - Rating", array( $this, 'MetaBoxRender' ), 'post', 'side' );

				if ( in_array( $PostType, get_post_types( array( 'public' => true, '_builtin' => false ) ) ) )
				{
					add_meta_box( "$this->SettingsName-Rating", "$this->Name - Rating", array( $this, 'MetaBoxRender' ), $PostType, 'side' );
				}
			}


			public function MetaBoxRender( $Post )
			{

				?>

					<!--<p><strong>Inject FontAwesome</strong></p>
					<select id="<?php print $this->SettingsName; ?>-Rating-FontAwesome">
						<option value="">Default</option>
						<option value="1">true</option>
						<option value="0">false</option>
					</select>

					<p><strong>Inject Custom Style</strong></p>
					<select id="<?php print $this->SettingsName; ?>-Rating-CustomStyle">
						<option value="">Default</option>
						<option value="1">true</option>
						<option value="0">false</option>
					</select>

					<p><strong>FontFamily</strong></p>
					<input id="<?php print $this->SettingsName; ?>-Rating-fontFamily" type="text" placeholder="helvetica">

					<p><strong>Star Outline</strong></p>
					<select id="<?php print $this->SettingsName; ?>-starOutline">
						<option value="">Default</option>
						<option value="1">true</option>
						<option value="0">false</option>
					</select>

					<p><strong>Star Primary Colour</strong></p>
					<input id="<?php print $this->SettingsName; ?>-Rating-starPrimaryColour" type="text" placeholder="gold">

					<p><strong>Star Unselected Colour</strong></p>
					<input id="<?php print $this->SettingsName; ?>-Rating-fontFamily" type="text" placeholder="lightgray">-->

					<div>
						<p><strong>ShortCode</strong></p>
						<textarea id="<?php print $this->SettingsName; ?>-Rating-ShortCode" class="large-text" rows="1">[schemaapprating]</textarea>
						<button class="button button-small editor-insert">Insert into content</button>
					</div>

				<?php
			}


			public function HookHunchSchemaMetaBox( $Post )
			{
				$Visible = get_post_meta( $Post->ID, '_HunchSchemaRatingVisible', true );

				wp_nonce_field( $this->Name, 'HookHunchSchemaMetaBox' );

				?>

					<h4>Rating Options</h4>

					<table class="widefat fixed wp-list-table">
						<tbody>

							<tr>
								<td style="width: 30%;">
									Visible
								</td>
								<td>
									<select name="HunchSchemaRatingVisible">
										<option value="">Default</option>
										<option value="1" <?php selected( $Visible, 1 ); ?>>Show</option>
										<option value="-1" <?php selected( $Visible, -1 ); ?>>Hide</option>
									</select>
								</td>
							</tr>

						</tbody>
					</table>


				<?php

			}


			public function HookWidgetsInit()
			{
				register_widget( 'SchemaAppRatingWidget' );
			}


			public function HookSavePost( $PostId, $Post, $Update )
			{
				if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || wp_is_post_revision( $PostId ) )
				{
					return $PostId;
				}

				if ( ! empty( $_POST['HookHunchSchemaMetaBox'] ) && wp_verify_nonce( $_POST['HookHunchSchemaMetaBox'], $this->Name ) )
				{
					if ( isset( $_POST['HunchSchemaRatingVisible'] ) && $_POST['HunchSchemaRatingVisible'] != '' )
					{
						update_post_meta( $PostId, '_HunchSchemaRatingVisible', $_POST['HunchSchemaRatingVisible'] );
					}
					else
					{
						delete_post_meta( $PostId, '_HunchSchemaRatingVisible' );
					}
				}
			}


			public function HookWPEnqueueScripts()
			{
				$SettingsRating = $this->Settings['Rating'];
				$SettingsRating = array_intersect_key( $SettingsRating, array( 'injectFontAwesome' => '', 'injectCustomStyle' => '', 'fontFamily' => '', 'starOutline' => '', 'starPrimaryColour' => '', 'starUnselectedColour' => '' ) );
				array_walk( $SettingsRating, array( $this, 'CastSettingsRating' ) );


				wp_register_style( "$this->SettingsName-Style-Rating", false );

				wp_add_inline_style( "$this->SettingsName-Style-Rating", esc_html( $this->Settings['Rating']['CustomCSS'] ) );


				wp_register_script( "$this->SettingsName-Script-Rating", $this->URL . 'js/rating.js', null, $this->Version, true );

				wp_localize_script( "$this->SettingsName-Script-Rating", 'hunchRatingWidgetConfig', $SettingsRating );
			}


			public function HookHunchSchemaMarkupRender( $Markup, $MarkupType, $Post, $PostType, $JSON )
			{
				if ( ! empty( $Post ) )
				{
					$Count = $Ratings = 0;

					foreach ( range( 1, 5 ) as $I )
					{
						$Rating = intval( get_post_meta( $Post->ID, "_HunchSchemaRating$I", true ) );

						if ( $Rating )
						{
							$Count += $Rating;
							$Ratings += $I * $Rating;
						}
					}

					if ( ! $JSON )
					{
						wp_localize_script( "$this->SettingsName-Script-Rating", 'HunchSchemaMarkupRatingSettings', array( 'SubmitURL' => admin_url( 'admin-ajax.php' ), 'MarkupType' => $MarkupType, 'AJAXRetrieve' => $this->Settings['Rating']['AJAXRetrieve'], 'PostId' => $Post->ID, 'RatingCount' => $Count, 'RatingValue' => ( $Ratings ? round( $Ratings / $Count, 2 ) : 0 ) ) );
					}
				}
			}


			public function HookHunchSchemaMarkup( $schema_markup, $schema_markup_type, $post, $post_type ) {
				if ( ! empty( $schema_markup ) && $schema_markup_type == 'Default' && ! empty( $post ) && in_array( $post_type, array( 'page', 'post' ) ) && $this->widget_is_visible() ) {
					$widget_ratings = $this->get_ratings();

					if ( $widget_ratings ) {
						$schema_markup = json_decode( $schema_markup );

						$schema_markup->aggregateRating = array (
							'@type' => 'AggregateRating',
							'@id' => get_permalink( $post->ID ) . '#aggregate',
							'url' => get_permalink( $post->ID ),
							'ratingValue' => $widget_ratings['rating_value'],
							'ratingCount' => $widget_ratings['rating_count'],
						);

						$schema_markup = $this->markup_encode( $schema_markup );
					}
				}

				return $schema_markup;
			}


			public function HookTheContent( $content ) {
				if ( $this->widget_to_show() ) {
					if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {
						return $content;
					}

					$widget = '';

					if ( ! empty( $this->SettingsSchemaApp['graph_uri'] ) ) {
						wp_enqueue_style( "$this->SettingsName-Style-Rating" );
						wp_enqueue_script( "$this->SettingsName-Script-Rating" );

						$widget = sprintf( '<star-injector data-account-id="%s"></star-injector>', $this->SettingsSchemaApp['graph_uri'] );
					} else {
						$widget = '<p>Rating: Required parameter Account Id missing.</p>';
					}

					if ( $this->Settings['Rating']['Placement'] == 1 ) {
						$content = $widget . $content;
					} else {
						$content = $content . $widget;
					}
				}

				return $content;
			}


			public function HookShortCode( $Attributes, $Content = null, $Code = '' )
			{
				if ( empty( $this->Settings['LicenseStatus'] ) )
				{
					return;
				}

				if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) 
				{
					return;
				}

				if ( ! empty( $this->SettingsSchemaApp['graph_uri'] ) )
				{
					wp_enqueue_style( "$this->SettingsName-Style-Rating" );
					wp_enqueue_script( "$this->SettingsName-Script-Rating" );

					return sprintf( '<star-injector data-account-id="%s"></star-injector>', $this->SettingsSchemaApp['graph_uri'] );
				}
				else
				{
					return '<p>Rating: Required parameter Account Id missing.</p>';
				}
			}


			public function HookJAAXHunchSchemaRatingGet()
			{
				if ( ! empty( $_POST['PostId'] ) )
				{
					$PostId = sanitize_text_field( $_POST['PostId'] );

					$Count = $Ratings = 0;

					foreach ( range( 1, 5 ) as $I )
					{
						$Rating = intval( get_post_meta( $PostId, "_HunchSchemaRating$I", true ) );

						if ( $Rating )
						{
							$Count += $Rating;
							$Ratings += $I * $Rating;
						}
					}

					print $this->markup_encode( array( 'Status' => 'Ok', 'RatingCount' => $Count, 'RatingValue' => ( $Ratings ? round( $Ratings / $Count, 1 ) : 0 ) ) );
				}
				else
				{
					print wp_json_encode( array( 'Status' => 'Error', 'Message' => 'Required parameter missing.' ) );
				}

				exit;
			}


			public function HookJAAXHunchSchemaRatingSubmit()
			{
				if ( ! empty( $_POST['PostId'] ) && ! empty( $_POST['Rating'] ) )
				{
					$PostId = sanitize_text_field( $_POST['PostId'] );
					$Rating = sanitize_text_field( $_POST['Rating'] );

					if ( in_array( $Rating, range( 1, 5 ) ) )
					{
						$Count = intval( get_post_meta( $PostId, "_HunchSchemaRating$Rating", true ) );

						update_post_meta( $PostId, "_HunchSchemaRating$Rating", ++$Count );

						print wp_json_encode( array( 'Status' => 'Ok' ) );
					}
				}

				exit;
			}


			private function widget_to_show() {
				global $post;

				if ( is_page() ) {

					$visible = intval( get_post_meta( $post->ID, '_HunchSchemaRatingVisible', true ) );

					if (  $visible !== -1  &&  ( $this->Settings['Rating']['ShowOnPages'] ||  $visible === 1 )  ) {
						return true;
					}
				} elseif ( is_singular( 'post' ) ) {

					$visible = intval( get_post_meta( $post->ID, '_HunchSchemaRatingVisible', true ) );

					if (  $visible !== -1  &&  ( $this->Settings['Rating']['ShowOnPosts']  ||  $visible === 1 )  ) {
						return true;
					}
				}

				return false;
			}


			public function widget_is_visible() {
				global $post;

				if ( $this->widget_to_show() ) {
					return true;
				} elseif ( is_singular() && ( stripos( $post->post_content, '[schemaapprating]' ) !== false || stripos( $post->post_excerpt, '[schemaapprating]' ) !== false ) ) {
					return true;
				}

				return false;
			}


			public function get_ratings() {
				global $post;

				if ( is_singular() ) {
					$rating_count = $rating_total = 0;

					foreach ( range( 1, 5 ) as $rating_value ) {
						$rating_per_level = intval( get_post_meta( $post->ID, "_HunchSchemaRating{$rating_value}", true ) );

						if ( $rating_per_level ) {
							$rating_count += $rating_per_level;
							$rating_total += $rating_value * $rating_per_level;
						}
					}

					if ( $rating_count && $rating_total ) {
						return array ( 'rating_value' => ( $rating_total ? round( $rating_total / $rating_count, 1 ) : 0 ), 'rating_count' => $rating_count );
					}
				}
			}


			public function CastSettingsRating( &$Value, $Key )
			{
				if ( in_array( $Key, array( 'injectFontAwesome', 'injectCustomStyle', 'starOutline' ) ) )
				{
					settype( $Value, 'boolean' );
				}
			}

		}
	}

?>