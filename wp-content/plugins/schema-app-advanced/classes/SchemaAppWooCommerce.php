<?php

	if ( ! defined( 'ABSPATH' ) )
	{
		exit;
	}


	if ( ! class_exists( 'SchemaAppWooCommerce' ) )
	{
		class SchemaAppWooCommerce extends SchemaAppAdvanced
		{
			private static $instance;


			public static function get_instance() {
				if ( ! self::$instance ) {
					self::$instance = new self();
				}

				return self::$instance;
			}


			public function __construct()
			{
				if ( self::$instance ) {
					return self::$instance;
				}

				parent::__construct();

				$this->schema_markup_from_app = false;

				add_action( 'plugins_loaded', array( $this, 'HookPluginsLoaded' ) );
				add_action( 'admin_init', array( $this, 'HookAdminInit' ) );
			}


			public function Init()
			{
			}


			public function HookPluginsLoaded()
			{
				if ( class_exists( 'WooCommerce' ) && version_compare( WC()->version, '2.4', '>=' ) && ! empty( $this->Settings['LicenseStatus'] ) )
				{
					add_action( 'init', array( $this, 'HookInit' ) );
					add_filter( 'wc_get_template', array( $this, 'HookWCGetTemplate' ), 10, 3 );
					add_filter( 'wc_get_template_part', array( $this, 'HookWCGetTemplate' ), 10, 3 );
					add_action( 'wp_footer', array( $this, 'HookWPFooter' ) );

					add_filter( 'hunch_schema_markup', array( $this, 'hook_hunch_schema_markup' ), 10, 4 );
				}
			}


			public function HookAdminInit()
			{
				add_settings_section( 'SchemaAppWooCommerce', '', array( $this, 'SettingsSectionRender' ), "$this->SettingsName-WooCommerce" );
				add_settings_field( 'itemCondition', 'itemCondition', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-WooCommerce", 'SchemaAppWooCommerce', array( 'Name' => 'itemCondition', 'Type' => 'Select', 'Options' => array( 'DamagedCondition' => 'DamagedCondition', 'NewCondition' => 'NewCondition', 'RefurbishedCondition' => 'RefurbishedCondition', 'UsedCondition' => 'UsedCondition' ), 'Description' => 'Set default condition for products.' ) );
				add_settings_field( 'V3DefaultMarkup', 'Default Markup', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-WooCommerce", 'SchemaAppWooCommerce', array( 'Name' => 'V3DefaultMarkup', 'Type' => 'Select', 'Options' => $this->FormFieldStatusOptions, 'Description' => 'Default WooCommerce 3 JSON/LD structured data format.' ) );
				add_settings_field( 'description', 'Generate description from', array( $this, 'SettingsFieldRender' ), "$this->SettingsName-WooCommerce", 'SchemaAppWooCommerce', array( 'Name' => 'description', 'Type' => 'Select', 'Options' => array( 'short' => 'Short description', 'full' => 'Full description' ), 'Description' => 'Select source for generating description.' ) );
			}


			public function SettingsSectionRender( $Arguments )
			{
				if ( ! class_exists( 'WooCommerce' ) )
				{
					print '<p><strong>Please activate WooCommerce plugin.</strong></p>';
				}
				elseif ( version_compare( WC()->version, '2.4', '<' ) )
				{
					print '<p>We do not support legacy version of WooCommerce, please upgrade to version 2.4.0 or above.</p>';
				}
				else
				{

					?>

						<p>
							This plugin adds <a href="http://schema.org/Product">Schema.org/Product</a> Markup for all your Product data.
							It enables Product and Review Rich Snippets, while fixing then significantly extends the default WooCommerce markup.
							The plugin also provides the necessary Schema Markup for Google Merchant Shopping Feed. For more information about the benefits see the <a href="https://www.schemaapp.com/product/schema-app-woocommerce/">product description page</a> and test a product page using the <a href="https://developers.google.com/structured-data/testing-tool/">Google Structured Data Testing Tool</a>
						</p>

						<p>If you have any questions about the plugin email <a href="mailto:support@schemaapp.com">support@schemaapp.com</a></p>

					<?php

				}

				print '<h3>Settings</h3>';
			}


			public function SettingsFieldRender( $Arguments )
			{
				$this->SettingsFormField( 'WooCommerce', $Arguments['Name'], $Arguments['Type'], ( ! empty( $Arguments['Options'] ) ? $Arguments['Options'] : array() ), ( ! empty( $Arguments['Description'] ) ? $Arguments['Description'] : '' ) );
			}


			public function HookInit()
			{
				if ( version_compare( WC()->version, '3', '>=' ) && empty( $this->Settings['WooCommerce']['V3DefaultMarkup'] ) )
				{
					remove_action( 'wp_footer', array( WC()->structured_data, 'output_structured_data' ), 10 );
					remove_action( 'woocommerce_email_order_details', array( WC()->structured_data, 'output_email_structured_data' ), 30 );
				}
			}


			public function HookWCGetTemplate( $Template, $Slug, $Name )
			{
				if ( strstr( $Template, 'themes' ) )
				{
					return $Template;
				}

				if ( version_compare( WC()->version, '2.6', '>=' ) )
				{
					$NewTemplatePath = $this->Path . 'templates/woocommerce-2.6/';
				}
				else
				{
					$NewTemplatePath = $this->Path . 'templates/woocommerce/';
				}

				$NewTemplateName = substr( $Template, strpos( $Template, '/templates/') + 11 );
				$NewTemplate = $NewTemplatePath . $NewTemplateName;

				if ( file_exists( $NewTemplate ) )
				{
					return $NewTemplate;
				}
				else
				{
					return $Template;
				}
			}


			public function HookWPFooter()
			{
				if ( $this->schema_markup_from_app ) {
					return;
				}

				global $post;

				$QueriedObject = get_queried_object();

				if ( is_product() )
				{
					$this->SchemaProduct();
				}
				elseif ( is_product_category() || is_product_tag() || is_shop() || ( is_tax() && taxonomy_is_product_attribute( $QueriedObject->taxonomy ) ) )
				{
					$this->SchemaProductCategory();
				}
			}


			public function hook_hunch_schema_markup( $markup, $markup_type, $post, $post_type ) {

				if ( ! empty( $markup ) && $markup_type == 'App' && $post_type == 'product' ) {
					$this->schema_markup_from_app = true;
				}

				return $markup;
			}


			public function SchemaProduct()
			{
				global $post, $woothemes_testimonials;

				$product = wc_get_product(get_the_ID());
				$attributes = new SchemaAppWooCommerceAttribute($product);

				// Downloadable product schema handling
				$productClass = "Product";
				if ($product->is_downloadable()) {
					switch ($product->download_type) {
						case 'application' :
							$productClass = "SoftwareApplication";
							break;
						case 'music' :
							$productClass = "MusicAlbum";
							break;
						default :
							$productClass = "Product";
							break;
					}
				}

				// Compile the base product data
				$productSchema = array(
					'@context' => 'http://schema.org',
					'@type' => $productClass,
					'@id' => get_permalink() . '#' . $productClass,
					'name' => $product->get_title(),
					'description' => ( $this->Settings['WooCommerce']['description'] == 'full' ) ? wp_strip_all_tags( apply_filters( 'the_excerpt', $post->post_content ), true ) : wp_strip_all_tags( apply_filters( 'the_excerpt', $post->post_excerpt ), true ),
					'url' => get_permalink()
				);

				// Product Stock Keeping Unit
				if ($product->get_sku()) {
					$productSchema['sku'] = $product->get_sku();
				}

				// Product Categories
				$categories = version_compare( WC()->version, '3', '>=' ) ? wc_get_product_category_list( get_the_ID() ) : $product->get_categories();
				if ($categories ) {
					$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
					if (preg_match_all("/$regexp/siU", $categories, $matches, PREG_SET_ORDER)) {
						foreach($matches as $match) {
							$productSchema['category'][] = $match[3];
						}
					}
				}

				// Related Products http://schema.org/isRelatedTo
				$relatedProducts = version_compare( WC()->version, '3', '>=' ) ? wc_get_related_products( get_the_ID() ) : $product->get_related(5);
				$relatedProdsSchema = array();
				foreach( $relatedProducts AS $prodId ) {
					$relatedProdsSchema[] = array(
						"@type" => "Product",
						"url" => get_permalink($prodId),
						"name" => get_the_title($prodId)
					);
				}
				if ( count($relatedProdsSchema) > 0 ) {
					$productSchema['isRelatedTo'] = count( $relatedProdsSchema ) == 1 ? $relatedProdsSchema[0] : $relatedProdsSchema;
				}

				// http://schema.org/isSimilarTo
				$similarProducts = version_compare( WC()->version, '3', '>=' ) ? $product->get_upsell_ids() : $product->get_upsells();
				$similarProdsSchema = array();
				foreach( $similarProducts AS $prodId ) {
					$similarProdsSchema[] = array(
						"@type" => "Product",
						"url" => get_permalink($prodId),
						"name" => get_the_title($prodId)
					);
				}
				if ( count($similarProdsSchema) > 0 ) {
					$productSchema['isSimilarTo'] = count( $similarProdsSchema ) == 1 ? $similarProdsSchema[0] : $similarProdsSchema;
				}

				// Product Dimensions
				if ($product->has_dimensions()) {
					// "INH" for inches and
					switch (get_option('woocommerce_dimension_unit')) {
						case "in":
							$unitCode = "INH";
							break;
						case "m":
							$unitCode = "MTR";
							break;
						case "cm":
							$unitCode = "CMT";
							break;
						case "mm":
							$unitCode = "MMT";
							break;
						case "yd":
							$unitCode = "YRD";
							break;
						default:
							break;
					}
					$productSchema['height'] = array(
						"@type" => "QuantitativeValue",
						'unitCode' => $unitCode,
						'value' => $product->get_height()
					);
					$productSchema['width'] = array(
						"@type" => "QuantitativeValue",
						'unitCode' => $unitCode,
						'value' => $product->get_width()
					);
					$productSchema['depth'] = array(
						"@type" => "QuantitativeValue",
						'unitCode' => $unitCode,
						'value' => $product->get_length()
					);
				}
				if ($product->has_weight()) {
					// KGM for kilogram, LBR for pound
					$unitCode = get_option('woocommerce_weight_unit') == "lbs" ? "LBR" : "KGM";
					switch (get_option('woocommerce_weight_unit')) {
						case "lbs":
							$unitCode = "LBR";
							break;
						case "kg":
							$unitCode = "KGM";
							break;
						case "g":
							$unitCode = "LBR";
							break;
						case "oz":
							$unitCode = "LBR";
							break;
						default:
							break;
					}
					$productSchema['weight'] = array(
						'@type' => 'QuantitativeValue',
						'unitCode' => $unitCode,
						'value' => $product->get_weight(),
					);
				}

				// GTIN numbers need product attributes
				$attributes->assignProperty($productSchema, 'gtin8');
				$attributes->assignProperty($productSchema, 'gtin12');
				$attributes->assignProperty($productSchema, 'gtin13');
				$attributes->assignProperty($productSchema, 'gtin14');
				// Attribute Color
				$attributes->assignProperty($productSchema, 'color');
				// Attribute Brand
				$attributes->assignProperty($productSchema, 'brand');


				// Add GTIN from https://wordpress.org/support/plugin/woo-add-gtin/; https://wordpress.org/support/topic/add-gtin-to-schema-markup/#post-11765945
				$gtin = get_post_meta( $post->ID, 'hwp_product_gtin', true );

				if ( ! empty( $gtin ) ) {
					switch ( strlen( $gtin ) ) {
						case 8 :
							$productSchema['gtin8'] = $gtin;
							break;
						case 12 :
							$productSchema['gtin12'] = $gtin;
							break;
						case 13 :
							$productSchema['gtin13'] = $gtin;
							break;
						case 14 :
							$productSchema['gtin14'] = $gtin;
							break;
						default :
							$productSchema['gtin'] = $gtin;
					}
				}


				// Image (Primary image + Gallery)
				if ($product->get_image_id()) {
					$wpimage = wp_get_attachment_image_src($product->get_image_id(), 'single-post-thumbnail');
					$images[] = array(
						"@type" => "ImageObject",
						"url" => $wpimage[0],
						"height" => $wpimage[2],
						"width" => $wpimage[1]
					);
					$gallery = version_compare( WC()->version, '3', '>=' ) ? $product->get_gallery_image_ids() : $product->get_gallery_attachment_ids();
					foreach($gallery AS $index => $imageid) {
						$wpimage = wp_get_attachment_image_src($imageid, 'single-post-thumbnail');
						$images[] = array(
							"@type" => "ImageObject",
							"url" => $wpimage[0],
							"height" => $wpimage[2],
							"width" => $wpimage[1]
						);
					}
					if ( count($images) > 0 ) {
						$productSchema['image'] = count( $images ) == 1 ? $images[0] : $images;
					}
				}


				// Offers
				$productSchema['offers'] = $this->GetSchemaOffer( $product, $attributes );


				// Ratings
				if ($product->get_rating_count() > 0) {
					// Aggregate Rating
					$productSchema['aggregateRating'] = $this->GetSchemaRating( $product );

					// Reviews
					$reviews = array();
					$comments = get_comments(array(
						'post_type' => 'product',
						'post_id' => get_the_ID(),
						'status' => 'approve'
					));
					foreach ($comments AS $comment) {
						$ratingValue = intval(get_comment_meta($comment->comment_ID, 'rating', true));
						$reviewRating = $ratingValue ? array( '@type' => 'Rating', 'ratingValue' => $ratingValue ) : null;
						$reviews[] = array(
							'@type' => 'Review',
							'@id' => get_permalink() . '#li-comment-' . $comment->comment_ID,
							'description' => $comment->comment_content,
							'datePublished' => $comment->comment_date,
							'reviewRating' => $reviewRating,
							'author' => array(
								'@type' => 'Person',
								'name' => $comment->comment_author,
								'url' => $comment->comment_author_url,
							)
						);
					}
					$productSchema['review'] = $reviews;
				} else {
					$productSchema['aggregateRating'] = $this->get_schema_rating_from_widget();
				}


				// Testimonials
				if ( class_exists( 'Woothemes_Testimonials' ) )
				{
					$Testimonials = $woothemes_testimonials->get_testimonials( $args );

					if ( $Testimonials && count( $Testimonials ) )
					{
						foreach ( $Testimonials as $Testimonial )
						{
							$productSchema['review'][] = array
							(
								'@type' => 'Review',
								'@id' => $Testimonial->guid,
								'description' => $Testimonial->post_content,
								'datePublished' => $Testimonial->post_date_gmt,
								'author' => array
								(
									'@type' => 'Person',
									'name' => $Testimonial->byline,
									'url' => $Testimonial->url,
								)
							);
						}
					}
				}


				// Brands
				if ( function_exists( 'get_brands' ) && taxonomy_exists( 'product_brand' ) )
				{
					$Brands = get_the_terms( $post->ID, 'product_brand' );

					if ( $Brands )
					{
						foreach ( $Brands as $Brand )
						{
							$Image = '';
							$ImageId = get_woocommerce_term_meta( $Brand->term_id, 'thumbnail_id', true );

							if ( $ImageId )
							{
								$ImageSrc = wp_get_attachment_image_src( $ImageId, 'thumbnail' );

								if ( $ImageSrc )
								{
									$Image = $ImageSrc[0];
								}
							}

							$productSchema['brand'][] = array
							(
								'@type' => 'Brand',
								'@id' => get_term_link( $Brand ),
								'name' => $Brand->name,
								'description' => $Brand->description,
								'url' => get_term_link( $Brand ),
								'image' => $Image,
							);
						}
					}
				}


				// Map remaining attributes to additionalProperty PropertyValue
				$attributes->assignRemaining($productSchema);

				// Apply Filter for customized markup
				$productSchema = apply_filters('hunch_schema_woocommerce_productschema', $productSchema);

				printf( '<!-- Schema App Advanced --><script type="application/ld+json">%s</script><!-- Schema App Advanced -->' . "\n", $this->markup_encode( $productSchema ) );


				$schema_breadcrumb_position = 1;
				$schema_breadcrumb = array();
				$schema_breadcrumb['@context'] = 'http://schema.org';
				$schema_breadcrumb['@type'] = 'BreadcrumbList';

				$schema_breadcrumb['itemListElement'][] = array
				(
					'@type' => 'ListItem',
					'position' => $schema_breadcrumb_position++,
					'item' => array
					(
						'@id' => get_site_url() . "#breadcrumbitem",
						'name' => get_bloginfo( 'name' ),
					),
				);

				$product_categories = get_the_terms( get_the_ID(), 'product_cat' );

				if ( $product_categories && ! is_wp_error( $product_categories ) )
				{
					foreach( $product_categories as $product_category )
					{
						$schema_breadcrumb['itemListElement'][] = array
						(
							'@type' => 'ListItem',
							'position' => $schema_breadcrumb_position++,
							'item' => array
							(
								'@id' => get_term_link( $product_category, 'product_cat' ) . "#breadcrumbitem",
								'name' => $product_category->name,
							),
						);
					}
				}

				$schema_breadcrumb['itemListElement'][] = array
				(
					'@type' => 'ListItem',
					'position' => $schema_breadcrumb_position++,
					'item' => array
					(
						'@id' => get_permalink() . "#breadcrumbitem",
						'name' => get_the_title(),
					),
				);

				$schema_breadcrumb = apply_filters( 'hunch_schema_woocommerce_breadcrumb', $schema_breadcrumb );

				printf( '<!-- Schema App Advanced Breadcrumb --><script type="application/ld+json">%s</script><!-- Schema App Advanced Breadcrumb -->' . "\n", $this->markup_encode( $schema_breadcrumb ) );
			}


			public function SchemaProductCategory()
			{
				global $post, $wp_query;

				$Items = array();
				$Count = 0;
				$CategoryPermalink = '';

				if ( is_product_category() )
				{
					$CategoryPermalink = get_term_link( get_query_var( 'product_cat' ), 'product_cat' );
				}
				elseif ( is_product_tag() )
				{
					$CategoryPermalink = get_term_link( get_query_var( 'product_tag' ), 'product_tag' );
				}
				elseif ( is_shop() )
				{
					$CategoryPermalink = version_compare( WC()->version, '3', '>=' ) ? get_permalink( wc_get_page_id( 'shop' ) ) : get_permalink( woocommerce_get_page_id( 'shop' ) ); 
				}


				// Only use if absolutely necessary
				//rewind_posts();

				while ( have_posts() )
				{
					the_post();

					$Product = wc_get_product( get_the_ID() );

					if ( ! $Product )
					{
						continue;
					}

					$Attribute = new SchemaAppWooCommerceAttribute( $Product );
					$ProductRating = version_compare( WC()->version, '3', '>=' ) ? $this->GetSchemaRating( $Product ) : null;
					$ProductImage = array();

					$Count++;

					if ( $Product->get_image_id() )
					{
						$Image = wp_get_attachment_image_src( $Product->get_image_id(), 'single-post-thumbnail' );

						$ProductImage[] = array
						(
							'@type' => 'ImageObject',
							'url' => $Image[0],
							'height' => $Image[2],
							'width' => $Image[1]
						);
					}

					$Items[] = array
					(
						'@type' => 'ListItem',
						'position' => $Count,
						'item' => array
						(
							'@type' => 'Product',
							'@id' => get_permalink(),
							'name' => get_the_title(),
							'url' => get_permalink(),
							'image' => $ProductImage,
							'offers' => $this->GetSchemaOffer( $Product, $Attribute, true, $Count ),
							'aggregateRating' => $ProductRating,
						),
					);
				}


				$Schema = array
				(
					'@context' => 'http://schema.org',
					'@type' => 'OfferCatalog',
					'@id' => $CategoryPermalink,
					'name' => single_cat_title( '', false ),
					'description' => category_description(),
					'url' => $CategoryPermalink,
					'itemListOrder' => 'ItemListOrderAscending',
					'numberOfItems' => $wp_query->found_posts,
					'itemListElement' => $Items,
				);


				$Schema = apply_filters( 'hunch_schema_woocommerce_productcategoryschema', $Schema );

				printf( '<!-- Schema App Advanced --><script type="application/ld+json">%s</script><!-- Schema App Advanced -->' . "\n", $this->markup_encode( $Schema ) );
			}


			public function GetSchemaOffer( $Product, $Attribute, $InCategory = false, $Count = 0 )
			{
				if ( $Product->is_type( 'variable' ) )
				{
					$variations = $Product->get_available_variations();

					if ( ! empty( $variations ) )
					{
						if ( $InCategory )
						{
							$VariationCount = 0;
							$VariationPrices = array();

							foreach( $variations as $variation )
							{
								// Offers are only for public products
								if ( $variation['variation_is_visible'] && $variation['variation_is_active'] )
								{
									$VariationCount++;

									$VariationPrices[] = $variation['display_price'];
								}
							}

							if ( count( $VariationPrices ) )
							{
								$offer = array
								(
									'@type' => 'AggregateOffer',
									'lowPrice' => min( $VariationPrices ),
									'highPrice' => max( $VariationPrices ),
									'priceCurrency' => get_woocommerce_currency(),
									'offerCount' => $VariationCount,
								);

								return $offer;
							}
							else
							{
								return;
							}
						}
						else
						{
							$offers = array();

							foreach( $variations as $variation )
							{
								// Offers are only for public products
								if ( $variation['variation_is_visible'] && $variation['variation_is_active'] )
								{
									$availablity = $variation['is_in_stock'] ? 'http://schema.org/InStock' : 'http://schema.org/OutOfStock';

									$offer = array
									(
										'@type' => 'Offer',
										'url' => get_permalink(),
										'name' => implode( ' - ', array_filter( array_map( 'trim', $variation['attributes'] ) ) ),
										'price' => $variation['display_price'],
										'priceCurrency' => get_woocommerce_currency(),
										'availability' => $availablity,
										'itemCondition' => 'http://schema.org/' . $this->Settings['WooCommerce']['itemCondition'], // Default
									);

									if ( $this->GetSchemaSeller() )
									{
										if ( count( $offers ) )
										{
											$offer['seller']['@id'] = site_url();
										}
										else
										{
											$offer['seller'] = $this->GetSchemaSeller();
										}
									}

									// Generate a variation ID
									if ( $variation['sku'] !== '' )
									{
										$offer['sku'] = $variation['sku'];
										$offer['@id'] = get_permalink() . '#' . $variation['sku'];
									}
									else
									{
										$attribute_values = array();

										foreach ( $variation['attributes'] as $key => $value )
										{
											if ( $value !== '' )
											{
												$attribute_values[] = substr( $key, 10 ) . '-' . filter_var( $value, FILTER_SANITIZE_URL );
											}
										}

										$offer['@id'] = get_permalink() . '#' . implode( '-', $attribute_values );
									}

									// Look for itemCondition override by variation
									foreach ( $variation['attributes'] as $key => $value )
									{
										if ( stristr( $key, 'itemCondition' ) )
										{
											$offer['itemCondition'] = $value;
										}
									}


									$product_variation = new WC_Product_Variation( $variation['variation_id'] );

									// Separate offers for sale and regular price
									if ( $product_variation->is_on_sale() ) {
										$offerregular = $offersale = $offer;


										$offerregular['@id'] .= '-regular-price';
										$offerregular['name'] .= ' Regular Price';
										$offerregular['price'] = version_compare( WC()->version, '3', '>=' ) ? wc_get_price_to_display( $Product, array( 'price' => $product_variation->get_regular_price() ) ) : $product_variation->get_regular_price();

										$offers[] = $offerregular;


										$offersale['@id'] .= '-sale-price';
										$offersale['name'] .= ' Sale Price';

										if ( $product_variation->get_date_on_sale_from() ) {
											$offersale['validFrom'] = date( 'Y-m-d', $product_variation->get_date_on_sale_from()->getTimestamp() );
										}

										if ( $product_variation->get_date_on_sale_to() ) {
											$offersale['validThrough'] = date( 'Y-m-d', $product_variation->get_date_on_sale_to()->getTimestamp() );
											$offersale['priceValidUntil'] = date( 'Y-m-d', $product_variation->get_date_on_sale_to()->getTimestamp() );
										}

										if ( $this->GetSchemaSeller() )
										{
											unset( $offersale['seller'] );
											$offersale['seller']['@id'] = site_url();
										}

										$offers[] = $offersale;
									} else {
										$offers[] = $offer;
									}
								}
							}

							return $offers;
						}
					}
				}
				else
				{
					$availablity = $Product->is_in_stock() ? 'http://schema.org/InStock' : 'http://schema.org/OutOfStock';

					$offer = array
					(
						'@type' => 'Offer',
						'url' => get_permalink(),
						'price' => version_compare( WC()->version, '3', '>=' ) ? wc_get_price_to_display( $Product ) : $Product->get_display_price(),
						'priceCurrency' => get_woocommerce_currency(),
						'availability' => $availablity,
					);

					if ( $this->GetSchemaSeller() )
					{
						if ( $InCategory && $Count > 1 )
						{
							$offer['seller']['@id'] = site_url();
						}
						else
						{
							$offer['seller'] = $this->GetSchemaSeller();
						}
					}

					$offer['itemCondition'] = 'http://schema.org/' . $this->Settings['WooCommerce']['itemCondition']; // Default
					$Attribute->assignProperty($offer, 'itemCondition'); // Override


					// Separate offers for sale and regular price
					if ( $Product->is_on_sale() ) {
						$offers = array();
						$offerregular = $offersale = $offer;


						$offerregular['@id'] = get_permalink() . '#regular-price';
						$offerregular['name'] = 'Regular Price';
						$offerregular['price'] = version_compare( WC()->version, '3', '>=' ) ? wc_get_price_to_display( $Product, array( 'price' => $Product->get_regular_price() ) ) : $Product->get_regular_price();

						$offers[] = $offerregular;


						$offersale['@id'] = get_permalink() . '#sale-price';
						$offersale['name'] = 'Sale Price';

						if ( $Product->get_date_on_sale_from() ) {
							$offersale['validFrom'] = date( 'Y-m-d', $Product->get_date_on_sale_from()->getTimestamp() );
						}

						if ( $Product->get_date_on_sale_to() ) {
							$offersale['validThrough'] = date( 'Y-m-d', $Product->get_date_on_sale_to()->getTimestamp() );
							$offersale['priceValidUntil'] = date( 'Y-m-d', $Product->get_date_on_sale_to()->getTimestamp() );
						}

						if ( $this->GetSchemaSeller() )
						{
							unset( $offersale['seller'] );
							$offersale['seller']['@id'] = site_url();
						}

						$offers[] = $offersale;


						return $offers;
					} else {
						return $offer;
					}
				}
			}


			public function GetSchemaRating( $Product )
			{
				if ( $Product->get_rating_count() )
				{
					return array
					(
						'@type' => 'AggregateRating',
						'ratingValue' => $Product->get_average_rating(),
						'ratingCount' => (string) $Product->get_rating_count(),
						'reviewCount' => $Product->get_review_count(),
						'bestRating' => '5',
						'worstRating' => '1'
					);
				}
			}


			public function GetSchemaSeller()
			{
				$Schema = '';

				if ( class_exists( 'HunchSchema_Thing' ) && is_callable( array( 'HunchSchema_Thing', 'getPublisher' ) ) )
				{
					$HunchSchema_Thing = new HunchSchema_Thing;

					$Schema = $HunchSchema_Thing->getPublisher();

					$Schema['url'] = site_url();
					$Schema['@id'] = site_url();
				}

				return $Schema;
			}


			public function get_schema_rating_from_widget() {
				global $post;

				$schema_app_rating = SchemaAppRating::get_instance();

				if ( $schema_app_rating->widget_is_visible() ) {

					$widget_ratings = $schema_app_rating->get_ratings();

					if ( $widget_ratings ) {
						return array (
							'@type' => 'AggregateRating',
							'@id' => get_permalink( $post->ID ) . '#aggregate',
							'url' => get_permalink( $post->ID ),
							'ratingValue' => $widget_ratings['rating_value'],
							'ratingCount' => $widget_ratings['rating_count'],
						);
					}
				}
			}

		}
	}

?>