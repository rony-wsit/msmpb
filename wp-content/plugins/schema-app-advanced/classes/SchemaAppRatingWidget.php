<?php

	if ( ! defined( 'ABSPATH' ) )
	{
		exit;
	}


	if ( ! class_exists( 'SchemaAppRatingWidget' ) )
	{
		class SchemaAppRatingWidget extends WP_Widget
		{
			public function __construct()
			{
				$SchemaAppAdvanced = SchemaAppAdvanced::Instance();

				parent::__construct( 'SchemaAppRatingWidget', "$SchemaAppAdvanced->Name - Rating", array( 'classname' => 'widget-schemaapprating', 'description' => 'Display Rating Widget.' ) );
			}


			public function widget( $Arguments, $Settings )
			{
				$SchemaAppAdvanced = SchemaAppAdvanced::Instance();

				if ( empty( $SchemaAppAdvanced->Settings['LicenseStatus'] ) )
				{
					return;
				}
                                
                                if ( function_exists('is_amp_endpoint') && is_amp_endpoint() ) 
                                {
                                        return;
                                }

				$Title = apply_filters( 'widget_title', empty( $Settings['title'] ) ? '' : $Settings['title'], $Settings, $this->id_base );

				print $Arguments['before_widget'];

				if ( ! empty( $Title ) )
				{
					print $Arguments['before_title'] . $Title . $Arguments['after_title'];
				}

				if ( ! empty( $SchemaAppAdvanced->SettingsSchemaApp['graph_uri'] ) )
				{
					wp_enqueue_script( "$SchemaAppAdvanced->SettingsName-Script-Rating" );

					printf( '<star-injector data-account-id="%s" data-account-url="http://schemaapp.com/db/Ugozi_Edge_Inc/Cutten_Fields"></star-injector>', $SchemaAppAdvanced->SettingsSchemaApp['graph_uri'] );
				}
				else
				{
					print '<p>Required parameter Account Id missing.</p>';
				}

				print $Arguments['after_widget'];
			}


			public function form( $Settings )
			{
				$Settings = wp_parse_args( ( array ) $Settings, array( 'title' => '' ) );
				$Title = strip_tags( $Settings['title'] );

				?>

					<div>

						<p>
							<label for="<?php print $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
							<input id="<?php print $this->get_field_id( 'title' ); ?>" class="regular-text" type="text" name="<?php print $this->get_field_name( 'title' ); ?>" value="<?php print esc_attr( $Title ); ?>" />
						</p>

					</div>

				<?php

			}


			public function update( $SettingsNew, $Settings )
			{
				$Settings['title'] = strip_tags( $SettingsNew['title'] );

				return $Settings;
			}

		}
	}

?>