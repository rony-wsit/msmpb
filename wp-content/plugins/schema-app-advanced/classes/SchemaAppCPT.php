<?php

	if ( ! defined( 'ABSPATH' ) )
	{
		exit;
	}


	if ( ! class_exists( 'SchemaAppCPT' ) )
	{
		class SchemaAppCPT extends SchemaAppAdvanced
		{
			private static $instance;


			public static function get_instance() {
				if ( ! self::$instance ) {
					self::$instance = new self();
				}

				return self::$instance;
			}


			public function __construct()
			{
				if ( self::$instance ) {
					return self::$instance;
				}

				parent::__construct();

				add_action( 'admin_init', array( $this, 'HookAdminInit' ) );

				if ( ! empty( $this->Settings['LicenseStatus'] ) )
				{
					add_filter( 'hunch_schema_thing_post_type', array( $this, 'HookHunchSchemaThingPostType' ) );
					add_action( 'hunch_schema_markup_render', array( $this, 'HookHunchSchemaMarkupRender' ), 10, 5 );
				}
			}


			public function Init()
			{
			}


			public function HookAdminInit()
			{
				add_settings_section( 'SchemaAppCPTSchemaMapping', '', array( $this, 'SettingsSectionRenderSchemaMapping' ), "$this->SettingsName-CPT" );

				$CustomPostTypes = get_post_types( array( 'public' => true, '_builtin' => false ) );

				if ( count( $CustomPostTypes ) )
				{
					foreach ( $CustomPostTypes as $CustomPostType )
					{
						if ( $CustomPostType == 'product' )
						{
							continue;
						}

						add_settings_field( $CustomPostType, $CustomPostType, array( $this, 'SettingsFieldRender' ), "$this->SettingsName-CPT", 'SchemaAppCPTSchemaMapping', array( 'Name' => $CustomPostType, 'Type' => 'Text' ) );
					}
				}


				add_action( 'acf/render_field_settings', array( $this, 'HookACFRenderFieldSettings' ) );
				add_action( 'acf/create_field_options', array( $this, 'HookACFCreateFieldOptions' ) );

				// Pro version check
				if ( function_exists( 'post_type_exists' ) && post_type_exists( 'acf-field' ) )
				{
					add_filter( 'acf/update_field', array( $this, 'HookFilterACFUpdateField' ) );
				}
				else
				{
					add_action( 'acf/update_field', array( $this, 'HookActionACFUpdateField' ), 10, 2 );
				}
			}


			public function SettingsSectionRenderSchemaMapping( $Arguments )
			{

				?>

					<h3>Custom Type Schema Mapping</h3>
					<p>For each Custom Post Type, set the most specific <strong>Schema.org class</strong>. For example, for 'News' use http://schema.org/NewsArticle. We will automatically generate url, name, description fields. <!-- For more specific fields use Advanced Custom Fields plugin and use the field name to associate with the relevant schema property. Refer to EXAMPLES ON SCHEMA APP BLOGPOST --> </p>

				<?php

			}

			public function SettingsSectionRenderACFMapping ( $Arguments )
			{

				?>

					<h3>Advanced Custom Field Schema Mapping</h3>
					<p>For each advanced custom field mapping you can assign a <strong>Schema.org property</strong>. For example, for custom field 'isbn' use http://schema.org/isbn.</p>

				<?php

			}


			public function SettingsFieldRender( $Arguments )
			{
				$this->SettingsFormField( 'CPTSchemaMapping', $Arguments['Name'], $Arguments['Type'], ( ! empty( $Arguments['Options'] ) ? $Arguments['Options'] : array() ), ( ! empty( $Arguments['Description'] ) ? $Arguments['Description'] : '' ) );
			}


			public function HookACFCreateFieldOptions( $Field )
			{

				?>

					<tr class="field_HunchSchemaProperty">
						<td class="label">
							<label>Schema Property</label>
							<p class="description">Schema.org property</p>
						</td>
						<td>
							<?php
								do_action( 'acf/create_field', array
								(
									'type' => 'text',
									'name' => 'fields[' . $Field['name'] . '][HunchSchemaProperty]',
									'value' => ! empty( $Field['HunchSchemaProperty'] ) ? $Field['HunchSchemaProperty'] : '',
									'class' => 'HunchSchemaProperty',
								));
							?>
						</td>
					</tr>


				<?php

			}


			public function HookActionACFUpdateField( $Field, $PostId )
			{
				if ( ! empty( $Field['HunchSchemaProperty'] ) && stripos( $Field['HunchSchemaProperty'], 'http://' ) !== false )
				{
					$Field['HunchSchemaProperty'] = str_ireplace( array( 'http://', 'https://', 'schema.org/' ), '', $Field['HunchSchemaProperty'] );

					$ACFFieldFunctions = new acf_field_functions();

					$ACFFieldFunctions->update_field( $Field, $PostId );
				}
			}


			public function HookACFRenderFieldSettings( $Field )
			{
				acf_render_field_setting( $Field, array
				(
					'label' => 'Schema Property',
					'instructions' => 'Schema.org property',
					'name' => 'HunchSchemaProperty',
					'type' => 'text',
					'class' => 'field-HunchSchemaProperty'
				), true );
			}


			public function HookFilterACFUpdateField( $Field )
			{
				if ( ! empty( $Field['HunchSchemaProperty'] ) && stripos( $Field['HunchSchemaProperty'], 'http://' ) !== false )
				{
					$Field['HunchSchemaProperty'] = str_ireplace( array( 'http://', 'https://', 'schema.org/' ), '', $Field['HunchSchemaProperty'] );
				}

				return $Field;
			}


			public function HookHunchSchemaThingPostType( $PostType )
			{
				$CustomPostTypes = get_post_types( array( 'public' => true, '_builtin' => false ) );

				if ( is_singular() && in_array( $PostType, $CustomPostTypes ) && $PostType != 'product' )
				{
					return 'CustomPostType';
				}

				return $PostType;
			}


			public function HookHunchSchemaMarkupRender( $Markup, $MarkupType, $Post, $PostType, $JSON )
			{
				$CustomPostTypes = get_post_types( array( 'public' => true, '_builtin' => false ) );

				if ( ! empty( $Markup ) && $MarkupType == 'Default' && is_singular() && in_array( $PostType, $CustomPostTypes ) && $PostType != 'product' )
				{
					$Markup = json_decode( $Markup );

					if ( ! in_array( $Markup->{'@type'}, array( 'Article', 'Recipe', 'NewsArticle', 'Report', 'ScholarlyArticle', 'SocialMediaPosting', 'TechArticle', 'BlogPosting', 'DiscussionForumPosting' ) ) && class_exists( 'HunchSchema_Thing' ) && is_callable( array( 'HunchSchema_Thing', 'getVideos' ) ) )
					{
						$HunchSchema_Thing = new HunchSchema_Thing;

						$MarkupVideo = $HunchSchema_Thing->getVideos();

						if ( $MarkupVideo )
						{
							if ( $JSON )
							{
								if ( count( $MarkupVideo ) == 1 )
								{
									$JSONSchemaMarkup = reset( $MarkupVideo );

									print $this->markup_encode( $MarkupVideo );
								}
								else
								{
									print $this->markup_encode( $MarkupVideo );
								}
							}
							else
							{
								printf( '<!-- Schema App Advanced --><script type="application/ld+json">%s</script><!-- Schema App Advanced -->' . "\n", $this->markup_encode( $MarkupVideo ) );
							}
						}
					}
				}
			}

		}
	}

?>