<?php

	if ( ! defined( 'ABSPATH' ) )
	{
		exit;
	}


	if ( ! class_exists( 'SchemaAppLinked' ) )
	{
		class SchemaAppLinked extends SchemaAppAdvanced
		{
			private static $instance;


			public static function get_instance() {
				if ( ! self::$instance ) {
					self::$instance = new self();
				}

				return self::$instance;
			}


			public function __construct()
			{
				if ( self::$instance ) {
					return self::$instance;
				}

				global $wp_version;

				parent::__construct();

				add_action( 'admin_init', array( $this, 'HookAdminInit' ) );
				add_action( 'admin_enqueue_scripts', array( $this, 'HookAdminEnqueueScripts' ), 10, 1 );

				if ( version_compare( $wp_version, '4.4', '>=' ) )
				{
					add_filter( 'manage_edit-category_columns', array( $this, 'HookManageEditCategoryColumns' ) );
					add_filter( 'manage_category_custom_column', array( $this, 'HookManageCategoryCustomColumn' ), 10, 3 );
					add_action( 'category_edit_form_fields', array( $this, 'HookCategoryEditFormFields' ), 10, 2 );
					add_action( 'edited_category', array( $this, 'HookEditedCategory' ), 10, 2 );
					add_action( 'wp_ajax_HunchSchemaWikipediaURLSearch', array( $this, 'HookJAAXHunchSchemaWikipediaURLSearch' ) );
					add_action( 'wp_ajax_HunchSchemaWikiDataURLSearch', array( $this, 'HookJAAXHunchSchemaWikiDataURLSearch' ) );

					add_filter( 'manage_edit-post_tag_columns', array( $this, 'HookManageEditCategoryColumns' ) );
					add_filter( 'manage_post_tag_custom_column', array( $this, 'HookManageCategoryCustomColumn' ), 10, 3 );
					add_action( 'post_tag_edit_form_fields', array( $this, 'HookCategoryEditFormFields' ), 10, 2 );
					add_action( 'edited_post_tag', array( $this, 'HookEditedCategory' ), 10, 2 );
				}

				if ( version_compare( $wp_version, '4.4', '>=' ) && ! empty( $this->Settings['LicenseStatus'] ) )
				{
					add_filter( 'hunch_schema_markup', array( $this, 'HookHunchSchemaMarkup' ), 10, 4 );
				}
			}


			public function Init()
			{
			}


			public function HookAdminInit()
			{
				add_settings_section( 'SchemaAppLinkedGuide', '', array( $this, 'SettingsSectionRenderGuide' ), "$this->SettingsName-Linked" );
				add_settings_section( 'SchemaAppLinkedSummary', '', array( $this, 'SettingsSectionRenderSummary' ), "$this->SettingsName-Linked" );
			}


			public function SettingsSectionRenderGuide( $Arguments )
			{
				print '<h2>Instructions</h2>';
				print '<p>Schema Markup was created so that you could explicitly tell search engines and other data consumers exactly what your content is about. To a machine, relating the content to other things on the web, help them clearly identify a concept.  In Schema App Advanced, we help lookup Category Taxonomy in both Wikipedia and WikiData to explicitly connect your Articles with what they’re actually about. The result is that Search Engines can better match your content with searches.</p>';
				print "<p>To link Categories to Wikipedia and Wikidata:</p><ol>";
				print "<li>Go to the <a href='" . admin_url( 'edit-tags.php?taxonomy=category' ) . "'>Category</a> List and Edit.</li>";
				print '<li>In the Editor we will get the best match automatically, review it, then Save.</li>';
				print '<li>If the Wiki URLs don\'t have the best match, input another search term and pick from the results.</li>';
				print '</ol>';
				print '<p>Once you have updated the Categories, its BlogPosts will include the links in the schema.org/about data item.</p>';
			}


			public function SettingsSectionRenderSummary( $Arguments )
			{
				$Categories = get_categories( array( 'taxonomy' => 'category', 'hide_empty' => 0, 'fields' => 'id=>name' ) );
				$CategoryWikipedia = $CategoryWikiData = 0;

				foreach ( $Categories as $CategoryId => $CategoryName )
				{
					$CategoryWikipediaURL = get_term_meta( $CategoryId, 'HunchSchemaWikipediaURL', true );
					$CategoryWikiDataURL = get_term_meta( $CategoryId, 'HunchSchemaWikiDataURL', true );

					if ( ! empty( $CategoryWikipediaURL ) )
					{
						$CategoryWikipedia++;
					}

					if ( ! empty( $CategoryWikiDataURL ) )
					{
						$CategoryWikiData++;
					}
				}


				$Tags = get_terms( array( 'taxonomy' => 'post_tag', 'hide_empty' => 0, 'fields' => 'id=>name' ) );
				$TagWikipedia = $TagWikiData = 0;

				foreach ( $Tags as $TagId => $TagName )
				{
					$TagWikipediaURL = get_term_meta( $TagId, 'HunchSchemaWikipediaURL', true );
					$TagWikiDataURL = get_term_meta( $TagId, 'HunchSchemaWikiDataURL', true );

					if ( ! empty( $TagWikipediaURL ) )
					{
						$TagWikipedia++;
					}

					if ( ! empty( $TagWikiDataURL ) )
					{
						$TagWikiData++;
					}
				}

				?>

					<h3>Category Summary</h3>

					<p>Total number of Categories: <?php print count( $Categories ); ?></p>
					<p>Number of Categories mapped to Wikipedia: <?php print $CategoryWikipedia; ?></p>
					<p>Number of Categories mapped to Wikidata: <?php print $CategoryWikiData; ?></p>

					<form method="post">
						<button class="button" name="Action" value="SchemaApp-Linked-WikiLookup-Categories">Start Wiki Lookup</button>
					</form>

					<h3>Tag Summary</h3>

					<p>Total number of Tags: <?php print count( $Tags ); ?></p>
					<p>Number of Tags mapped to Wikipedia: <?php print $TagWikipedia; ?></p>
					<p>Number of Tags mapped to Wikidata: <?php print $TagWikiData; ?></p>

					<form method="post">
						<button class="button" name="Action" value="SchemaApp-Linked-WikiLookup-Tags">Start Wiki Lookup</button>
					</form>

				<?php

				if ( ! empty( $_POST['Action'] ) && $_POST['Action'] == 'SchemaApp-Linked-WikiLookup-Categories' )
				{
					print "<script>jQuery( document ).ready( function( $ ) { $( '.wrap .nav-tab-wrapper a.nav-tab[data-tab=\"Linked\"]' ).click(); });</script>";
					print '<br><hr><p><strong>Wiki Lookup Log:</strong></p>';
					print '<ol>';

					ob_implicit_flush( true );
					ob_start();

					set_time_limit( 0 );

					foreach ( $Categories as $CategoryId => $CategoryName )
					{
						$Output = '';
						$CategoryWikipediaURL = get_term_meta( $CategoryId, 'HunchSchemaWikipediaURL', true );
						$CategoryWikiDataURL = get_term_meta( $CategoryId, 'HunchSchemaWikiDataURL', true );

						$Output .= sprintf( '<li>%s &nbsp; <a href="%s">Edit</a>', $CategoryName, admin_url( "term.php?taxonomy=category&tag_ID=$CategoryId" ) );

						if ( empty( $CategoryWikipediaURL ) )
						{
							$Response = json_decode( $this->WikipediaSearch( $CategoryName, 1 ) );

							if ( $Response->Status == 'Ok' )
							{
								$URL = reset( $Response->Data->query->pages )->fullurl;

								update_term_meta( $CategoryId, 'HunchSchemaWikipediaURL', sanitize_text_field( $URL ) );

								$Output .= sprintf( '<br>Wikipedia: <a href="%s" target="_blank">%s</a>', $URL, $URL );
							}
							else
							{
								$Output .= sprintf( '<br>Wikipedia: <span style="color: red;">%s</span>', $Response->Message );
							}
						}

						if ( empty( $CategoryWikiDataURL ) )
						{
							$Response = json_decode( $this->WikiDataSearch( $CategoryName, 1 ) );

							if ( $Response->Status == 'Ok' )
							{
								$URL = reset( $Response->Data->search )->concepturi;

								update_term_meta( $CategoryId, 'HunchSchemaWikiDataURL', sanitize_text_field( $URL ) );

								$Output .= sprintf( '<br>WikiData: <a href="%s" target="_blank">%s</a>', $URL, $URL );
							}
							else
							{
								$Output .= sprintf( '<br>WikiData: <span style="color: red;">%s</span>', $Response->Message );
							}
						}

						$Output .= '</li>';

						print str_pad( $Output, 4096 );

						ob_flush();
						usleep( 100000 );
					}

					ob_end_flush();

					print '</ol>';
				}


				if ( ! empty( $_POST['Action'] ) && $_POST['Action'] == 'SchemaApp-Linked-WikiLookup-Tags' )
				{
					print "<script>jQuery( document ).ready( function( $ ) { $( '.wrap .nav-tab-wrapper a.nav-tab[data-tab=\"Linked\"]' ).click(); });</script>";
					print '<br><hr><p><strong>Wiki Lookup Log:</strong></p>';
					print '<ol>';

					ob_implicit_flush( true );
					ob_start();

					set_time_limit( 0 );

					foreach ( $Tags as $TagId => $TagName )
					{
						$Output = '';
						$TagWikipediaURL = get_term_meta( $TagId, 'HunchSchemaWikipediaURL', true );
						$TagWikiDataURL = get_term_meta( $TagId, 'HunchSchemaWikiDataURL', true );

						$Output .= sprintf( '<li>%s &nbsp; <a href="%s">Edit</a>', $TagName, admin_url( "term.php?taxonomy=post_tag&tag_ID=$TagId" ) );

						if ( empty( $TagWikipediaURL ) )
						{
							$Response = json_decode( $this->WikipediaSearch( $TagName, 1 ) );

							if ( $Response->Status == 'Ok' )
							{
								$URL = reset( $Response->Data->query->pages )->fullurl;

								update_term_meta( $TagId, 'HunchSchemaWikipediaURL', sanitize_text_field( $URL ) );

								$Output .= sprintf( '<br>Wikipedia: <a href="%s" target="_blank">%s</a>', $URL, $URL );
							}
							else
							{
								$Output .= sprintf( '<br>Wikipedia: <span style="color: red;">%s</span>', $Response->Message );
							}
						}

						if ( empty( $TagWikiDataURL ) )
						{
							$Response = json_decode( $this->WikiDataSearch( $TagName, 1 ) );

							if ( $Response->Status == 'Ok' )
							{
								$URL = reset( $Response->Data->search )->concepturi;

								update_term_meta( $TagId, 'HunchSchemaWikiDataURL', sanitize_text_field( $URL ) );

								$Output .= sprintf( '<br>WikiData: <a href="%s" target="_blank">%s</a>', $URL, $URL );
							}
							else
							{
								$Output .= sprintf( '<br>WikiData: <span style="color: red;">%s</span>', $Response->Message );
							}
						}

						$Output .= '</li>';

						print str_pad( $Output, 4096 );

						ob_flush();
						usleep( 100000 );
					}

					ob_end_flush();

					print '</ol>';
				}
			}


			public function HookAdminEnqueueScripts( $Hook )
			{
				if ( $Hook == 'term.php' && ! empty( $_GET['taxonomy'] ) && in_array( $_GET['taxonomy'], array( 'category', 'post_tag' ) ) )
				{
					wp_enqueue_script( "$this->SettingsName-Admin-Taxonomy-Category-Edit", $this->URL . 'js/admin-taxonomy-category-edit.js', array( 'jquery' ), $this->Version );
				}
			}


			public function HookManageEditCategoryColumns( $Columns )
			{
				$Columns['HunchSchemaWiki'] = 'Wiki';

				return $Columns;
			}


			public function HookManageCategoryCustomColumn( $Content, $ColumnName, $TermId )
			{
				if ( $ColumnName = 'HunchSchemaWiki' )
				{
					$WikipediaURL = get_term_meta( $TermId, 'HunchSchemaWikipediaURL', true );
					$WikiDataURL = get_term_meta( $TermId, 'HunchSchemaWikiDataURL', true );
					$Color = 'red';

					if ( ! empty( $WikipediaURL ) && ! empty( $WikiDataURL ) )
					{
						$Color = 'green';
					}
					elseif ( ! empty( $WikipediaURL ) || ! empty( $WikiDataURL ) )
					{
						$Color = 'gold';
					}

					return sprintf( '<span style="color: %s;">&#11044;</span>', $Color );
				}

				return $Content;
			}


			public function HookCategoryEditFormFields( $Tag, $Taxonomy )
			{
				$WikipediaURL = get_term_meta( $Tag->term_id, 'HunchSchemaWikipediaURL', true );
				$WikiDataURL = get_term_meta( $Tag->term_id, 'HunchSchemaWikiDataURL', true );

				wp_nonce_field( $this->SettingsName, "Nonce$this->SettingsName" );

				?>

					<tr class="form-field term-hunch-schema-wikipedia-url-wrap">
						<th scope="row">
							<label for="HunchSchemaWikipediaURL">Same as Wikipedia</label>
						</th>
						<td>
							Search Term: <input class="regular-text" style="width: 20em;" type="text" name="HunchSchemaWikipediaSearch" value="<?php print esc_attr( $Tag->name ); ?>">
							<br>
							<input type="text" name="HunchSchemaWikipediaURL" value="<?php print esc_attr( $WikipediaURL ); ?>" readonly>
							<p class="description"><button class="button search">Search Wikipedia</button> <button class="button clear">Clear</button></p>
							<br>
							<div class="search-result"></div>
						</td>
					</tr>

					<tr class="form-field term-hunch-schema-wikidata-url-wrap">
						<th scope="row">
							<label for="HunchSchemaWikiDataURL">Same as WikiData Concept</label>
						</th>
						<td>
							Search Term: <input class="regular-text" style="width: 20em;" type="text" name="HunchSchemaWikiDataSearch" value="<?php print esc_attr( $Tag->name ); ?>">
							<br>
							<input type="text" name="HunchSchemaWikiDataURL" value="<?php print esc_attr( $WikiDataURL ); ?>" readonly>
							<p class="description"><button class="button search">Search WikiData</button> <button class="button clear">Clear</button></p>
							<br>
							<div class="search-result"></div>
						</td>
					</tr>

				<?php

			}


			public function HookEditedCategory( $TermId, $TermTaxonomyId )
			{
				if ( ! empty( $_POST["Nonce$this->SettingsName"] ) && wp_verify_nonce( $_POST["Nonce$this->SettingsName"], $this->SettingsName ) )
				{
					if ( ! empty( $_POST['HunchSchemaWikipediaURL'] ) )
					{
						update_term_meta( $TermId, 'HunchSchemaWikipediaURL', sanitize_text_field( $_POST['HunchSchemaWikipediaURL'] ) );
					}
					else
					{
						delete_term_meta( $TermId, 'HunchSchemaWikipediaURL' );
					}

					if ( ! empty( $_POST['HunchSchemaWikiDataURL'] ) )
					{
						update_term_meta( $TermId, 'HunchSchemaWikiDataURL', sanitize_text_field( $_POST['HunchSchemaWikiDataURL'] ) );
					}
					else
					{
						delete_term_meta( $TermId, 'HunchSchemaWikiDataURL' );
					}
				}
			}


			public function HookJAAXHunchSchemaWikipediaURLSearch()
			{
				print $this->WikipediaSearch( $_POST['Name'] );

				exit;
			}


			public function HookJAAXHunchSchemaWikiDataURLSearch()
			{
				print $this->WikiDataSearch( $_POST['Name'] );

				exit;
			}


			public function HookHunchSchemaMarkup( $Markup, $MarkupType, $Post, $PostType )
			{
				global $wp_version;

				if ( ! empty( $Markup ) && $MarkupType == 'Default' && ! empty( $Post ) && $PostType == 'post' )
				{
					$About = array();
					$Categories = get_the_category();

					if ( count( $Categories ) && version_compare( $wp_version, '4.4', '>=' ) )
					{
						foreach ( $Categories as $Category )
						{
							$CategoryURLs = array();
							$CategoryURLs[] = get_term_meta( $Category->term_id, 'HunchSchemaWikipediaURL', true );
							$CategoryURLs[] = get_term_meta( $Category->term_id, 'HunchSchemaWikiDataURL', true );
							$CategoryURLs = array_filter( $CategoryURLs, 'trim' );

							if ( ! empty( $CategoryURLs ) )
							{
								$About[] = array
								(
									'@type' => 'Thing',
									'@id' => get_category_link( $Category->term_id ),
									'name' => $Category->name,
									'sameAs' => $CategoryURLs,
								);
							}
							else
							{
								$About[] = $Category->name;
							}
						}

						$Markup = json_decode( $Markup );

						$Markup->about = $About;

						$Markup = $this->markup_encode( $Markup );
					}


					$Mentions = array();
					$Tags = get_the_tags();

					if ( ! is_wp_error( $Tags ) && is_array( $Tags ) && count( $Tags ) && version_compare( $wp_version, '4.4', '>=' ) )
					{
						foreach ( $Tags as $Tag )
						{
							$TagURLs = array();
							$TagURLs[] = get_term_meta( $Tag->term_id, 'HunchSchemaWikipediaURL', true );
							$TagURLs[] = get_term_meta( $Tag->term_id, 'HunchSchemaWikiDataURL', true );
							$TagURLs = array_filter( $TagURLs, 'trim' );

							if ( ! empty( $TagURLs ) )
							{
								$Mentions[] = array
								(
									'@type' => 'Thing',
									'@id' => get_tag_link( $Tag->term_id ),
									'name' => $Tag->name,
									'sameAs' => $TagURLs,
								);
							}
						}

						if ( count( $Mentions ) )
						{
							$Markup = json_decode( $Markup );

							$Markup->mentions = $Mentions;

							$Markup = $this->markup_encode( $Markup );
						}
					}
				}

				return $Markup;
			}


			public function WikipediaSearch( $Search = '', $Limit = 10 )
			{
				if ( ! empty( $Search ) )
				{
					$Response = wp_remote_get( add_query_arg( array( 'action' => 'query', 'generator' => 'search', 'format' => 'json', 'prop' => 'info', 'inprop' => 'url', 'gsrsearch' => sanitize_text_field( $Search ), 'gsrlimit' => $Limit ), 'https://en.wikipedia.org/w/api.php' ) );

					if ( is_wp_error( $Response ) )
					{
						return wp_json_encode( array( 'Status' => 'Error', 'Message' => $Response->get_error_message() ) );
					}
					elseif ( empty( $Response['body'] ) )
					{
						return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'Connection error. Please try again later.' ) );
					}
					else
					{
						$ResponseJson = json_decode( $Response['body'] );

						if ( isset( $ResponseJson->query->pages ) )
						{
							return wp_json_encode( array( 'Status' => 'Ok', 'Data' => $ResponseJson ) );
						}
						elseif ( isset( $ResponseJson->error ) )
						{
							return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'API error.', 'Data' => $ResponseJson ) );
						}
						elseif ( isset( $ResponseJson->warnings ) )
						{
							return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'API error.', 'Data' => $ResponseJson ) );
						}
						else
						{
							return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'No results found, search a more generic term.', 'Data' => $ResponseJson ) );
						}
					}
				}
				else
				{
					return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'Required parameter missing.' ) );
				}
			}


			public function WikiDataSearch( $Search = '', $Limit = 10 )
			{
				if ( ! empty( $Search ) )
				{
					$Response = wp_remote_get( add_query_arg( array( 'action' => 'wbsearchentities', 'format' => 'json', 'language' => 'en', 'search' => sanitize_text_field( $Search ), 'limit' => $Limit ), 'https://www.wikidata.org/w/api.php' ) );

					if ( is_wp_error( $Response ) )
					{
						return wp_json_encode( array( 'Status' => 'Error', 'Message' => $Response->get_error_message() ) );
					}
					elseif ( empty( $Response['body'] ) )
					{
						return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'Connection error. Please try again later.' ) );
					}
					else
					{
						$ResponseJson = json_decode( $Response['body'] );

						if ( count( $ResponseJson->search ) )
						{
							return wp_json_encode( array( 'Status' => 'Ok', 'Data' => $ResponseJson ) );
						}
						elseif ( isset( $ResponseJson->error ) )
						{
							return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'API error.', 'Data' => $ResponseJson ) );
						}
						elseif ( isset( $ResponseJson->warnings ) )
						{
							return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'API error.', 'Data' => $ResponseJson ) );
						}
						else
						{
							return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'No results found, search a more generic term.' ) );
						}
					}
				}
				else
				{
					return wp_json_encode( array( 'Status' => 'Error', 'Message' => 'Required parameter missing.' ) );
				}
			}

		}
	}

?>