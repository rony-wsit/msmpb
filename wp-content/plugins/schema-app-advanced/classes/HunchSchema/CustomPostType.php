<?php
/**
 * Description of SchemaAppCPTMarkup
 *
 * @author mark
 */
class HunchSchema_CustomPostType extends HunchSchema_Thing {

    public $schemaType = '';

	public function __construct()
	{
		$PostType = get_post_type();
		$SchemaAppAdvanced = SchemaAppAdvanced::Instance();

		$this->Settings = $SchemaAppAdvanced->SettingsSchemaApp;

		// Check if the current post type is mapped.
		if ( array_key_exists( $PostType, $SchemaAppAdvanced->Settings['CPTSchemaMapping'] ) && ! empty( $SchemaAppAdvanced->Settings['CPTSchemaMapping'][$PostType] ) )
		{
			$this->schemaType = $SchemaAppAdvanced->Settings['CPTSchemaMapping'][$PostType];

			// Test if the full schema.org Class URL is used
			$this->schemaType = str_ireplace( array( 'http://', 'https://', 'schema.org/' ), '', $this->schemaType );
		}
	}


    public function getResource($pretty = false) {
        if (empty($this->schemaType)) {
            return;
        }


        $this->schema = array(
            '@context' => 'http://schema.org/',
            '@type' => $this->schemaType,
            'name' => get_the_title(),
            'description' => $this->getExcerpt(),
            'image' => $this->getImage(),
            'url' => get_permalink(),
        );

        // Check for CreativeWork type
        if (in_array($this->schemaType, array("Article", "Recipe", "NewsArticle", "Report", "ScholarlyArticle", "SocialMediaPosting", "TechArticle", "BlogPosting", "DiscussionForumPosting"))) {
            $this->schema['datePublished'] = get_the_date('Y-m-d');
            $this->schema['dateModified'] = get_the_modified_date('Y-m-d');
            $this->schema['author'] = $this->getAuthor();
            $this->schema['headline'] = get_the_title();
            $this->schema['publisher'] = $this->getPublisher();
            $this->schema['mainEntityOfPage'] = array
            (
				'@type' => 'WebPage',
				'@id' => get_permalink(),
            );
            $this->schema['video'] = $this->getVideos();
        }


		if ( function_exists( 'get_field_objects' ) )
		{
			$ACFFieldObjects = get_field_objects();

			if ( $ACFFieldObjects )
			{
				$subObjects = array();

				foreach ( $ACFFieldObjects as $Item )
				{
					if ( ! empty( $Item['HunchSchemaProperty'] ) )
					{
						// Check for subobject mapping
						// E.g. http://schema.org/author~http://schema.org/Person~http://schema.org/name
						if ( strstr($Item['HunchSchemaProperty'], "~") ) {

							$path = explode("~", $Item['HunchSchemaProperty']);

							if ( count($path) >= 3 ) {
								$object = array(
									'@type' => $path[1],
									$path[2] => esc_html( sanitize_text_field( wp_specialchars_decode( $Item['value'], ENT_NOQUOTES ) ) ),
								);

								if ( array_key_exists( $path[0], $subObjects) ) {
									$subObjects[$path[0]] = array_merge($subObjects[$path[0]], $object);
								} else {
									$subObjects[$path[0]] = $object;
								}
							}

						} else {
							$this->schema[$Item['HunchSchemaProperty']] = esc_html( sanitize_text_field( wp_specialchars_decode( $Item['value'], ENT_NOQUOTES ) ) );
						}
					}
				}

				if ( count( $subObjects ) >= 1 ) {
					foreach( $subObjects AS $property => $object ) {
						$this->schema[$property] = $object;
					}
				}
			}
		}


        return $this->toJson($this->schema, $pretty);
    }


    public function getBreadcrumb( $pretty = false )
    {
		global $post;


		$post_type = get_post_type();

		if ( ! is_post_type_hierarchical( $post_type ) )
		{
			return;
		}


		$position = 1;
		$this->SchemaBreadcrumb['@context'] = 'http://schema.org';
		$this->SchemaBreadcrumb['@type'] = 'BreadcrumbList';

		$this->SchemaBreadcrumb['itemListElement'][] = array
		(
			'@type' => 'ListItem',
			'position' => $position++,
			'item' => array
			(
				'@id' => get_site_url() . "#breadcrumbitem",
				'name' => get_bloginfo( 'name' ),
			),
		);

		if ( $post->post_parent )
		{
			$ancestors = array_reverse( get_post_ancestors( $post->ID ) );

			foreach( $ancestors as $post_id )
			{
				$this->SchemaBreadcrumb['itemListElement'][] = array
				(
					'@type' => 'ListItem',
					'position' => $position++,
					'item' => array
					(
						'@id' => get_permalink( $post_id ) . "#breadcrumbitem",
						'name' => get_the_title( $post_id ),
					),
				);
			}
		}

		$this->SchemaBreadcrumb['itemListElement'][] = array
		(
			'@type' => 'ListItem',
			'position' => $position++,
			'item' => array
			(
				'@id' => get_permalink() . "#breadcrumbitem",
				'name' => get_the_title(),
			),
		);

        return $this->toJson( $this->SchemaBreadcrumb, $pretty );
    }
}