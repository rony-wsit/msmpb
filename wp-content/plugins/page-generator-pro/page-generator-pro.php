<?php
/**
* Plugin Name: Page Generator Pro
* Plugin URI: http://www.wpzinc.com/plugins/page-generator-pro
* Version: 2.3.8
* Author: WP Zinc
* Author URI: http://www.wpzinc.com
* Description: Generate multiple Pages, Posts and Custom Post Types, using dynamic content selected from a number of sources.
*/

/**
 * Page Generator Pro Class
 * 
 * @package   Page_Generator_Pro
 * @author    Tim Carr
 * @version   1.0.0
 * @copyright WP Zinc
 */
class Page_Generator_Pro {

    /**
     * Holds the class object.
     *
     * @since   1.1.3
     *
     * @var     object
     */
    public static $instance;

    /**
     * Holds the plugin information object.
     *
     * @since   1.0.0
     *
     * @var     object
     */
    public $plugin = '';

    /**
     * Holds the dashboard class object.
     *
     * @since   1.1.6
     *
     * @var     object
     */
    public $dashboard = '';

    /**
     * Holds the licensing class object.
     *
     * @since   1.1.6
     *
     * @var     object
     */
    public $licensing = '';

    /**
     * Classes
     *
     * @since   1.9.8
     *
     * @var     array
     */
    public $classes = '';

    /**
     * Constructor. Acts as a bootstrap to load the rest of the plugin
     * 
     * @since   1.0.0
     */
    public function __construct() {

        // Plugin Details
        $this->plugin                   = new stdClass;
        $this->plugin->name             = 'page-generator-pro';
        $this->plugin->displayName      = 'Page Generator Pro';
        $this->plugin->version          = '2.3.8';
        $this->plugin->buildDate        = '2019-12-12 18:00:00';
        $this->plugin->requires         = 3.6;
        $this->plugin->tested           = '5.3';
        $this->plugin->folder           = plugin_dir_path( __FILE__ );
        $this->plugin->url              = plugin_dir_url( __FILE__ );
        $this->plugin->documentation_url= 'https://www.wpzinc.com/documentation/page-generator-pro';
        $this->plugin->support_url      = 'https://www.wpzinc.com/support';
        $this->plugin->upgrade_url      = 'https://www.wpzinc.com/plugins/page-generator-pro';
        $this->plugin->review_name      = 'page-generator';
        $this->plugin->review_notice    = sprintf( __( 'Thanks for using %s to generate content!', $this->plugin->name ), $this->plugin->displayName );

        // Licensing Submodule
        if ( ! class_exists( 'LicensingUpdateManager' ) ) {
            require_once( $this->plugin->folder . '_modules/licensing/lum.php' );
        }
        $this->licensing = new LicensingUpdateManager( $this->plugin, 'https://www.wpzinc.com/wp-content/plugins/lum' );

        // If whitelabelling is available, do this now
        if ( $this->licensing->has_feature( 'whitelabelling' ) ) {
            if ( defined( strtoupper( $this->plugin->name ) . '_DISPLAY_NAME' ) ) {
                $this->plugin->displayName = constant( strtoupper( $this->plugin->name ) . '_DISPLAY_NAME' );
            }
            if ( defined( strtoupper( $this->plugin->name ) . '_SUPPORT_URL' ) ) {
                $this->plugin->support_url = constant( strtoupper( $this->plugin->name ) . '_SUPPORT_URL' );
            }
            if ( defined( strtoupper( $this->plugin->name ) . '_DOCUMENTATION_URL' ) ) {
                $this->plugin->documentation_url = constant( strtoupper( $this->plugin->name ) . '_DOCUMENTATION_URL' );
            }
        }

        // Dashboard Submodule
        if ( ! class_exists( 'WPZincDashboardWidget' ) ) {
            require_once( $this->plugin->folder . '_modules/dashboard/dashboard.php' );
        }
        $this->dashboard = new WPZincDashboardWidget( $this->plugin, 'https://www.wpzinc.com/wp-content/plugins/lum-deactivation' );

        // Defer loading of Plugin Classes
        add_action( 'admin_init', array( $this, 'deactivate_free_version' ) );
        add_action( 'init', array( $this, 'initialize' ), 1 );
        add_action( 'init', array( $this, 'upgrade' ), 2 );

    }

    /**
     * Detects if the Free version of the Plugin is running, and if so,
     * deactivates it.
     *
     * @since   1.6.7
     */
    public function deactivate_free_version() {
        
        // Bail if the function is not available
        if ( ! function_exists( 'deactivate_plugins' ) ) {
            return;
        }

        // Bail if the Free version is not active
        if ( ! is_plugin_active( 'page-generator/page-generator.php' ) ) {
            return;
        }

        // Deactivate the Free version
        deactivate_plugins( 'page-generator/page-generator.php' );
      
    }

    /**
     * Initializes required and licensed classes
     *
     * @since   1.9.8
     */
    public function initialize() {

        $this->classes = new stdClass;

        // Global Classes
        $this->classes->access      = new Page_Generator_Pro_Access( self::$instance );
        $this->classes->common      = new Page_Generator_Pro_Common( self::$instance );
        $this->classes->geo         = new Page_Generator_Pro_Geo( self::$instance );
        $this->classes->post_type   = new Page_Generator_Pro_PostType( self::$instance );
        $this->classes->settings    = new Page_Generator_Pro_Settings( self::$instance );
        $this->classes->taxonomy    = new Page_Generator_Pro_Taxonomy( self::$instance );
        
        if ( $this->licensing->check_license_key_valid() ) {
            $this->classes->editor          = new Page_Generator_Pro_Editor( self::$instance );
            $this->classes->page_builders   = new Page_Generator_Pro_PageBuilders( self::$instance );
            $this->classes->shortcode       = new Page_Generator_Pro_Shortcode( self::$instance );

            // Load WP-CLI classes, if WP-CLI exists
            if ( class_exists( 'WP_CLI' ) ) {
                // In CLI mode, is_admin() is not called, so we need to require the classes that
                // the CLI commands may use.
                $this->classes->block_spin          = new Page_Generator_Pro_Block_Spin( self::$instance );
                $this->classes->generate            = new Page_Generator_Pro_Generate( self::$instance );
                $this->classes->georocket           = new Page_Generator_Pro_Georocket_API( self::$instance );
                $this->classes->groups              = new Page_Generator_Pro_Groups( self::$instance );
                $this->classes->groups_table        = new Page_Generator_Pro_Groups_Table( self::$instance );
                $this->classes->groups_ui           = new Page_Generator_Pro_Groups_UI( self::$instance );
                $this->classes->groups_terms        = new Page_Generator_Pro_Groups_Terms( self::$instance );
                $this->classes->groups_terms_table  = new Page_Generator_Pro_Groups_Terms_Table( self::$instance );
                $this->classes->groups_terms_ui     = new Page_Generator_Pro_Groups_Terms_UI( self::$instance );
                $this->classes->import              = new Page_Generator_Pro_Import( self::$instance );
                $this->classes->media_library       = new Page_Generator_Pro_Media_Library( self::$instance );
                $this->classes->keywords            = new Page_Generator_Pro_Keywords( self::$instance );
                $this->classes->pexels              = new Page_Generator_Pro_Pexels( self::$instance );
                $this->classes->phone_area_codes    = new Page_Generator_Pro_Phone_Area_Codes( self::$instance );
                $this->classes->pixabay             = new Page_Generator_Pro_Pixabay( self::$instance );
                $this->classes->spintax             = new Page_Generator_Pro_Spintax( self::$instance );
                $this->classes->wikipedia           = new Page_Generator_Pro_Wikipedia( self::$instance );
                $this->classes->youtube             = new Page_Generator_Pro_YouTube( self::$instance );
                $this->classes->yelp                = new Page_Generator_Pro_Yelp( self::$instance );

                // Load the CLI command(s)
                require_once( $this->plugin->folder . 'includes/admin/cli.php' );
            }
        }

        // Admin Classes
        if ( is_admin() || ( defined( 'DOING_CRON' ) && DOING_CRON ) ) {
            $this->classes->admin               = new Page_Generator_Pro_Admin( self::$instance );
            $this->classes->install             = new Page_Generator_Pro_Install( self::$instance );
            $this->classes->groups              = new Page_Generator_Pro_Groups( self::$instance );
            $this->classes->groups_table        = new Page_Generator_Pro_Groups_Table( self::$instance );
            $this->classes->groups_ui           = new Page_Generator_Pro_Groups_UI( self::$instance );
            $this->classes->groups_terms        = new Page_Generator_Pro_Groups_Terms( self::$instance );
            $this->classes->groups_terms_table  = new Page_Generator_Pro_Groups_Terms_Table( self::$instance );
            $this->classes->groups_terms_ui     = new Page_Generator_Pro_Groups_Terms_UI( self::$instance );
            $this->classes->keywords            = new Page_Generator_Pro_Keywords( self::$instance );
            $this->classes->notices             = new Page_Generator_Pro_Notices( self::$instance );
            $this->classes->phone_area_codes    = new Page_Generator_Pro_Phone_Area_Codes( self::$instance );
            $this->classes->screen              = new Page_Generator_Pro_Screen( self::$instance );
             
            // Licensed
            if ( $this->licensing->check_license_key_valid() ) {
                // Initialize licensed classes
                $this->classes->ajax            = new Page_Generator_Pro_AJAX( self::$instance );
                $this->classes->block_spin      = new Page_Generator_Pro_Block_Spin( self::$instance );
                $this->classes->chimprewriter   = new Page_Generator_Pro_ChimpRewriter_API( self::$instance );
                $this->classes->generate        = new Page_Generator_Pro_Generate( self::$instance );
                $this->classes->georocket       = new Page_Generator_Pro_Georocket_API( self::$instance );
                // $this->classes->gutenberg       = new Page_Generator_Pro_Gutenberg( self::$instance ); @TODO Uncomment for Gutenberg shortcodes
                $this->classes->import          = new Page_Generator_Pro_Import( self::$instance );
                $this->classes->media_library   = new Page_Generator_Pro_Media_Library( self::$instance );
                $this->classes->pexels          = new Page_Generator_Pro_Pexels( self::$instance );
                $this->classes->pixabay         = new Page_Generator_Pro_Pixabay( self::$instance );
                $this->classes->spinnerchief    = new Page_Generator_Pro_SpinnerChief_API( self::$instance );
                $this->classes->spin_rewriter   = new Page_Generator_Pro_Spin_Rewriter_API( self::$instance );
                $this->classes->spintax         = new Page_Generator_Pro_Spintax( self::$instance );
                $this->classes->webanalyzer     = new Page_Generator_Pro_Webanalyzer_API( self::$instance );
                $this->classes->wikipedia       = new Page_Generator_Pro_Wikipedia( self::$instance );
                $this->classes->wordai          = new Page_Generator_Pro_WordAI_API( self::$instance );
                $this->classes->youtube         = new Page_Generator_Pro_YouTube( self::$instance );
                $this->classes->yelp            = new Page_Generator_Pro_Yelp( self::$instance );
            }
        }
        
    }

    /**
     * Runs the upgrade routine once the plugin has loaded
     *
     * @since   1.1.7
     */
    public function upgrade() {

        // Bail if we're not in the WordPress Admin
        if ( ! is_admin() ) {
            return;
        }

        // Run upgrade routine
        $this->get_class( 'install' )->upgrade();

    }

    /**
     * Returns the given class
     *
     * @since   1.9.8
     *
     * @param   string  $name   Class Name
     * @return  object          Class Object
     */
    public function get_class( $name ) {

        // If the class hasn't been loaded, throw a WordPress die screen
        // to avoid a PHP fatal error.
        if ( ! isset( $this->classes->{ $name } ) ) {
            // Define the error
            $error = new WP_Error( 'page_generator_pro_get_class', sprintf( __( '%s: Error: Could not load Plugin class <strong>%s</strong>', $this->plugin->name ), $this->plugin->displayName, $name ) );
             
            // Depending on the request, return or display an error
            // Admin UI
            if ( is_admin() ) {  
                wp_die(
                    $error,
                    sprintf( __( '%s: Error', 'page-generator-pro' ), $this->plugin->displayName ),
                    array(
                        'back_link' => true,
                    )
                );
            }

            // Cron / CLI
            return $error;
        }

        // Return the class object
        return $this->classes->{ $name };

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since   1.1.6
     *
     * @return  object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}

/**
 * Define the autoloader for this Plugin
 *
 * @since   1.9.8
 *
 * @param   string  $class_name     The class to load
 */
function Page_Generator_Pro_Autoloader( $class_name ) {

    // Define the required start of the class name
    $class_start_name = array(
        'Page_Generator_Pro',
    );

    // Get the number of parts the class start name has
    $class_parts_count = count( explode( '_', $class_start_name[0] ) );

    // Break the class name into an array
    $class_path = explode( '_', $class_name );

    // Bail if it's not a minimum length
    if ( count( $class_path ) < $class_parts_count ) {
        return;
    }

    // Build the base class path for this class
    $base_class_path = '';
    for ( $i = 0; $i < $class_parts_count; $i++ ) {
        $base_class_path .= $class_path[ $i ] . '_';
    }
    $base_class_path = trim( $base_class_path, '_' );

    // Bail if the first parts don't match what we expect
    if ( ! in_array( $base_class_path, $class_start_name ) ) {
        return;
    }

    // Define the file name we need to include
    $file_name = strtolower( implode( '-', array_slice( $class_path, $class_parts_count ) ) ) . '.php';

    // Define the paths with file name we need to include
    $include_paths = array(
        dirname( __FILE__ ) . '/includes/admin/' . $file_name,
        dirname( __FILE__ ) . '/includes/global/' . $file_name,
    );

    // Iterate through the include paths to find the file
    foreach ( $include_paths as $path_file ) {
        if ( file_exists( $path_file ) ) {
            require_once( $path_file );
            return;
        }
    }

}
spl_autoload_register( 'Page_Generator_Pro_Autoloader' );

// Load Activation, Cron and Deactivation functions
include_once( dirname( __FILE__ ) . '/includes/admin/activation.php' );
include_once( dirname( __FILE__ ) . '/includes/admin/cron.php' );
include_once( dirname( __FILE__ ) . '/includes/admin/deactivation.php' );
register_activation_hook( __FILE__, 'page_generator_pro_activate' );
add_action( 'wpmu_new_blog', 'page_generator_pro_activate_new_site' );
add_action( 'activate_blog', 'page_generator_pro_activate_new_site' );
register_deactivation_hook( __FILE__, 'page_generator_pro_deactivate' );

/**
 * Main function to return Plugin instance.
 *
 * @since   1.9.8
 */
function Page_Generator_Pro() {
    
    return Page_Generator_Pro::get_instance();

}

// Finally, initialize the Plugin.
Page_Generator_Pro();