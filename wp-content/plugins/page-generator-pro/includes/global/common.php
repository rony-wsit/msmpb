<?php
/**
 * Common class
 * 
 * @package Page Generator Pro
 * @author  Tim Carr
 * @version 1.0.0
 */
class Page_Generator_Pro_Common {

    /**
     * Holds the base object.
     *
     * @since   1.9.8
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   1.9.8
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

    }

    /**
     * Helper method to retrieve available spintax providers
     * supported by this Plugin
     *
     * @since   2.2.9
     *
     * @return  array   Spintax Service Providers
     */
    public function get_spintax_providers() {

        $providers = array(
            ''              => __( 'This Plugin', 'page-generator-pro' ),
            'chimprewriter' => __( 'ChimpRewriter', 'page-generator-pro' ),
            'spinnerchief'  => __( 'SpinnerChief', 'page-generator-pro' ),
            'spin_rewriter' => __( 'Spin Rewriter', 'page-generator-pro' ),
            'wordai'        => __( 'WordAi', 'page-generator-pro' ),
        );

        /**
         * Defines the available spintax providers supported by this Plugin
         *
         * @since   2.2.9
         *
         * @param   array   $providers  Spintax Service Providers
         */
        $providers = apply_filters( 'page_generator_pro_common_get_spintax_providers', $providers );

        // Return filtered results
        return $providers;

    }

    /**
     * Helper method to retrieve public Post Types
     *
     * @since   1.1.3
     *
     * @return  array   Public Post Types
     */
    public function get_post_types() {

        // Get public Post Types
        $types = get_post_types( array(
            'public' => true,
        ), 'objects' );

        // Filter out excluded post types
        $excluded_types = $this->get_excluded_post_types();
        if ( is_array( $excluded_types ) ) {
            foreach ( $excluded_types as $excluded_type ) {
                unset( $types[ $excluded_type ] );
            }
        }

        /**
         * Defines the available public Post Type Objects that content can be generated for.
         *
         * @since   1.1.3
         *
         * @param   array   $types  Post Types
         */
        $types = apply_filters( 'page_generator_pro_common_get_post_types', $types );

        // Return filtered results
        return $types;

    }

    /**
     * Helper method to retrieve hierarchical public Post Types
     *
     * @since   1.2.1
     *
     * @return  array   Public Post Types
     */
    public function get_hierarchical_post_types() {

        // Get public hierarchical Post Types
        $types = get_post_types( array(
            'public'        => true,
            'hierarchical'  => true,
        ), 'objects' );

        // Filter out excluded post types
        $excluded_types = $this->get_excluded_post_types();
        if ( is_array( $excluded_types ) ) {
            foreach ( $excluded_types as $excluded_type ) {
                unset( $types[ $excluded_type ] );
            }
        }

        /**
         * Defines the available public hierarchical Post Type Objects that content can be generated for.
         *
         * @since   1.2.1
         *
         * @param   array   $types  Post Types
         */
        $types = apply_filters( 'page_generator_pro_common_get_hierarchical_post_types', $types );

        // Return filtered results
        return $types;

    }

    /**
     * Helper method to retrieve Post Types that have excerpt support
     *
     * @since   1.9.7
     *
     * @return  array   Public Post Types
     */
    public function get_excerpt_post_types() {

        // Get public hierarchical Post Types
        $types = get_post_types( array(
            'public'        => true,
        ), 'objects' );

        // Filter out excluded post types
        $excluded_types = $this->get_excluded_post_types();
        if ( is_array( $excluded_types ) ) {
            foreach ( $excluded_types as $excluded_type ) {
                unset( $types[ $excluded_type ] );
            }
        }

        // Filter out Post Types that don't have excerpt support
        foreach ( $types as $post_type => $post_type_object ) {
            if ( ! post_type_supports( $post_type, 'excerpt' ) ) {
                unset( $types[ $post_type ] );
            }
        }

        /**
         * Defines the available public hierarchical Post Type Objects that content can be generated for.
         *
         * @since   1.9.7
         *
         * @param   array   $types  Post Types
         */
        $types = apply_filters( 'page_generator_pro_common_get_excerpt_post_types', $types );

        // Return filtered results
        return $types;

    }

    /**
     * Helper method to retrieve excluded Post Types
     *
     * @since   1.1.3
     *
     * @return  array   Excluded Post Types
     */
    public function get_excluded_post_types() {

        // Get excluded Post Types
        $types = array(
            $this->base->get_class( 'post_type' )->post_type_name,
            'attachment',
            'revision',
            'nav_menu_item',
        );

        /**
         * Defines the Post Type Objects that content cannot be generated for.
         *
         * @since   1.1.3
         *
         * @param   array   $types  Post Types
         */
        $types = apply_filters( 'page_generator_pro_common_get_excluded_post_types', $types );

        // Return filtered results
        return $types;

    }

    /**
     * Returns any available Templates for each Post Type
     *
     * @since   1.5.8
     *
     * @return  array   Post Types and Templates
     */
    public function get_post_types_templates() {

        // Get Post Types
        $post_types = $this->get_post_types();

        // Bail if no Post Types
        if ( empty( $post_types ) ) {
            return false;
        }

        // Build templates
        $templates = array();
        foreach ( $post_types as $post_type ) {
            // Skip if this Post Type doesn't have any templates
            $post_type_templates = get_page_templates( null, $post_type->name );
            if ( empty( $post_type_templates ) ) {
                continue;
            }
            
            $templates[ $post_type->name ] = $post_type_templates;
        }

        /**
         * Defines available Theme Templates for each Post Type that can have content
         * generated for it.
         *
         * @since   1.5.8
         *
         * @param   array   $templates  Templates by Post Type
         */
        $templates = apply_filters( 'page_generator_pro_common_get_post_type_templates', $templates );

        // Return filtered results
        return $templates;

    }

    /**
     * Helper method to retrieve all Taxonomies
     *
     * @since   1.1.3
     *
     * @param   string  $post_type  Post Type
     * @return  array               Taxonomies
     */
    public function get_taxonomies() {

        // Get all taxonomies
        $taxonomies = get_taxonomies();

        // Get information for each taxonomy
        foreach ( $taxonomies as $index => $taxonomy ) {
            // Ignore our own taxonomy
            if ( $taxonomy == $this->base->get_class( 'taxonomy' )->taxonomy_name ) {
                unset( $taxonomies[ $index ] );
                continue;
            }
            
            $taxonomies[ $index ] = get_taxonomy( $taxonomy );
        }

        // Get excluded taxonomies
        $excluded_taxonomies = $this->get_excluded_taxonomies();

        // Remove excluded taxonomies from the main taxonomies array
        foreach ( $excluded_taxonomies as $excluded_taxonomy ) {
            unset( $taxonomies[ $excluded_taxonomy ] );
        }

        /**
         * Defines available taxonomies.
         *
         * @since   1.1.3
         *
         * @param   array   $taxonomies             Taxonomies
         * @param   array   $excluded_taxonomies    Excluded Taxonomies (these have already been removed from $taxonomies)
         */
        $taxonomies = apply_filters( 'page_generator_pro_common_get_taxonomies', $taxonomies, $excluded_taxonomies );

        // Return filtered results
        return $taxonomies;

    }

    /**
     * Helper method to retrieve all Taxonomies, with a flag for each
     * denoting whether the Taxonomy is hierarchical or not.
     *
     * @since   1.8.2
     *
     * @return  array   Taxonomies
     */
    public function get_taxonomies_hierarchical_status() {

        // Get taxonomies
        $taxonomies = $this->get_taxonomies();
        $taxonomies_hierarchical_status = array();

        // Iterate through taxonomies, defining flag for whether they are hierarchical or not
        foreach ( $taxonomies as $taxonomy ) {
            $taxonomies_hierarchical_status[ $taxonomy->name ] = $taxonomy->hierarchical;
        }

        // Get excluded taxonomies
        $excluded_taxonomies = $this->get_excluded_taxonomies();

        // Remove excluded taxonomies from the main taxonomies array
        foreach ( $excluded_taxonomies as $excluded_taxonomy ) {
            unset( $taxonomies_hierarchical_status[ $excluded_taxonomy ] );
        }

        /**
         * Defines available hierarchical taxonomies.
         *
         * @since   1.8.2
         *
         * @param   array   $taxonomies             Taxonomies
         * @param   array   $excluded_taxonomies    Excluded Taxonomies (these have already been removed from $taxonomies)
         */
        $taxonomies_hierarchical_status = apply_filters( 'page_generator_pro_common_get_taxonomies_hierarchical_status', $taxonomies_hierarchical_status, $excluded_taxonomies );

        // Return filtered results
        return $taxonomies_hierarchical_status;

    }

    /**
     * Helper method to retrieve Taxonomies for the given Post Type
     *
     * @since   1.1.3
     *
     * @param   string  $post_type  Post Type
     * @return  array               Taxonomies
     */
    public function get_post_type_taxonomies( $post_type = '' ) {

        // Get Post Type Taxonomies
        $taxonomies = get_object_taxonomies( $post_type, 'objects' );

        // Get excluded taxonomies
        $excluded_taxonomies = $this->get_excluded_taxonomies();

        // Remove excluded taxonomies from the main taxonomies array
        foreach ( $excluded_taxonomies as $excluded_taxonomy ) {
            unset( $taxonomies[ $excluded_taxonomy ] );
        }

        /**
         * Defines available taxonomies for the given Post Type.
         *
         * @since   1.1.3
         *
         * @param   array   $taxonomies             Taxonomies
         * @param   array   $excluded_taxonomies    Excluded Taxonomies (these have already been removed from $taxonomies)
         * @param   string  $post_type              Post Type
         */
        $taxonomies = apply_filters( 'page_generator_pro_common_get_post_type_taxonomies', $taxonomies, $excluded_taxonomies, $post_type );

        // Return filtered results
        return $taxonomies;

    }

    /**
     * Helper method to retrieve excluded Taxonomies
     *
     * @since   1.1.3
     *
     * @return  array   Taxonomies
     */
    private function get_excluded_taxonomies() {

        // Get excluded Taxonomies
        $excluded_taxonomies = array(
            'nav_menu',
            'link_category',
            'post_format',
        );

        /**
         * Defines taxonomies to exclude from displaying on the Generate screens.
         *
         * @since   1.1.3
         *
         * @param   array   $excluded_taxonomies    Excluded Taxonomies
         */
        $excluded_taxonomies = apply_filters( 'page_generator_pro_common_get_excluded_taxonomies', $excluded_taxonomies );

        // Return filtered results
        return $excluded_taxonomies;

    }

    /**
     * Helper method to retrieve authors
     *
     * @since   1.1.3
     *
     * @return  array   Authors
     */
    public function get_authors() {

        // Get authors
        $authors = get_users( array(
             'orderby' => 'nicename',
        ) );

        /**
         * Defines available authors for the Author dropdown on the Generate Content screen.
         *
         * @since   1.1.3
         *
         * @param   array   $authors    Authors
         */
        $authors = apply_filters( 'page_generator_pro_common_get_authors', $authors );

        // Return filtered results
        return $authors;
        
    }

    /**
     * Helper method to retrieve post statuses
     *
     * @since   1.1.3
     *
     * @return  array   Post Statuses
     */
    public function get_post_statuses() {

        // Get statuses
        $statuses = array(
            'draft'     => __( 'Draft', 'page-generator-pro' ),
            'future'    => __( 'Scheduled', 'page-generator-pro' ),
            'private'   => __( 'Private', 'page-generator-pro' ),
            'publish'   => __( 'Publish', 'page-generator-pro' ),
        );

        /**
         * Defines available Post Statuses for generated content.
         *
         * @since   1.1.3
         *
         * @param   array   $statuses   Statuses
         */
        $statuses = apply_filters( 'page_generator_pro_common_get_post_statuses', $statuses );

        // Return filtered results
        return $statuses;
        
    }

    /**
     * Helper method to retrieve post date options
     *
     * @since   1.1.6
     *
     * @return  array   Date Options
     */
    public function get_date_options() {

        // Get date options
        $date_options = array(
            'now'       => __( 'Now', 'page-generator-pro' ),
            'specific'  => __( 'Specify Date', 'page-generator-pro' ),
            'random'    => __( 'Random Date', 'page-generator-pro' ),
        );

        /**
         * Defines available scheduling options for generated content.
         *
         * @since   1.1.6
         *
         * @param   array   $date_options   Date Options
         */
        $date_options = apply_filters( 'page_generator_pro_common_get_date_options', $date_options );

        // Return filtered results
        return $date_options;

    }

    /**
     * Helper method to retrieve post schedule units
     *
     * @since   1.1.3
     *
     * @return  array   Post Schedule Units
     */
    public function get_schedule_units() {

        // Get units
        $units = array(
            'minutes'   => __( 'Minutes', 'page-generator-pro' ),
            'hours'     => __( 'Hours', 'page-generator-pro' ),
            'days'      => __( 'Days', 'page-generator-pro' ),
            'weeks'     => __( 'Weeks', 'page-generator-pro' ),
            'months'    => __( 'Months', 'page-generator-pro' ),
        );

        /**
         * Defines available scheduling units.
         *
         * @since   1.1.3
         *
         * @param   array   $units  Units
         */
        $units = apply_filters( 'page_generator_pro_common_get_schedule_units', $units );

        // Return filtered results
        return $units;
        
    }

    /**
     * Helper method to retrieve Post/Page generation methods
     *
     * @since   1.1.5
     *
     * @return  array   Generation Methods
     */
    public function get_methods() {

        // Get methods
        $methods = array(
            'all'        => __( 'All', 'page-generator-pro' ),
            'sequential' => __( 'Sequential', 'page-generator-pro' ),
            'random'     => __( 'Random', 'page-generator-pro' ),
        );

        /**
         * Defines available content generation methods.
         *
         * @since   1.1.5
         *
         * @param   array   $methods    Content Generation Methods
         */
        $methods = apply_filters( 'page_generator_pro_common_get_methods', $methods );

        // Return filtered results
        return $methods;

    }

    /**
     * Helper method to retrieve available overwrite generation methods
     *
     * @since   1.5.8
     *
     * @return  array   Overwrite Methods
     */
    public function get_overwrite_methods() {

        // Get methods
        $methods = array(
            0                           => __( 'No', 'page-generator-pro' ),
            'skip_if_exists'            => __( 'No, skip if existing Page generated by this Group', 'page-generator-pro' ),
            'skip_if_exists_any'        => __( 'No, skip if existing Page exists', 'page-generator-pro' ),
            'overwrite'                 => __( 'Yes, if existing Page generated by this Group', 'page-generator-pro' ),
            'overwrite_any'             => __( 'Yes, if existing Page exists', 'page-generator-pro' ),
        );

        /**
         * Defines available content overwrite options.
         *
         * @since   1.5.8
         *
         * @param   array   $methods    Content Overwrite Options
         */
        $methods = apply_filters( 'page_generator_pro_common_get_overwrite_methods', $methods );

        // Return filtered results
        return $methods;

    }

    /**
     * Helper method to retrieve available overwrite sections
     *
     * @since   2.3.5
     *
     * @return  array   Overwrite Sections
     */
    public function get_overwrite_sections() {

        // Get sections
        $sections = array(
            'post_title'            => __( 'Title', 'page-generator-pro' ),
            'post_content'          => __( 'Content', 'page-generator-pro' ),
            'post_excerpt'          => __( 'Excerpt', 'page-generator-pro' ),
            'post_author'           => __( 'Author', 'page-generator-pro' ),
            'post_date'             => __( 'Published Date', 'page-generator-pro' ),
            'comment_status'        => __( 'Allow Comments', 'page-generator-pro' ),
            'ping_status'           => __( 'Allow track / pingbacks', 'page-generator-pro' ),
            'custom_fields'         => __( 'Custom Fields', 'page-generator-pro' ),
            'featured_image'        => __( 'Featured Image', 'page-generator-pro' ),  
       );

        /**
         * Defines available content overwrite sections.
         *
         * @since   2.3.5
         *
         * @param   array   $sections    Content Overwrite Sections
         */
        $sections = apply_filters( 'page_generator_pro_common_get_overwrite_sections', $sections );

        // Return filtered results
        return $sections;

    }

    /**
     * Helper method to retrieve available overwrite generation methods
     * for Terms
     *
     * @since   1.6.1
     *
     * @return  array   Overwrite Methods
     */
    public function get_term_overwrite_methods() {

        // Get overwrite methods
        $methods = $this->get_overwrite_methods();

        // Remove non-applicable methods
        unset( $methods['overwrite_preseve_date'] );

        /**
         * Defines available term overwrite options.
         *
         * @since   1.6.1
         *
         * @param   array   $methods    Term Overwrite Options
         */
        $methods = apply_filters( 'page_generator_pro_common_get_term_overwrite_methods', $methods );

        // Return filtered results
        return $methods;

    }

    /**
     * Helper method to retrieve Featured Image source options
     *
     * @since   1.1.7
     *
     * @return  array   Featured Image Sources
     */
    public function get_featured_image_sources() {

        // Get sources
        $sources = array(
            ''        => __( 'No Feat. Image', 'page-generator-pro' ),
            'id'      => __( 'Media Library Image', 'page-generator-pro' ),
            'url'     => __( 'Image URL', 'page-generator-pro' ),
            'pexels'  => __( 'Pexels', 'page-generator-pro' ),
            'pixabay' => __( 'Pixabay', 'page-generator-pro' ),
        );

        /**
         * Defines available Featured Image sources.
         *
         * @since   1.1.7
         *
         * @param   array   $methods    Featured Image Sources
         */
        $sources = apply_filters( 'page_generator_pro_common_get_featured_image_sources', $sources );

        // Return filtered results
        return $sources;

    }

    /**
     * Helper method to retrieve Order By options
     *
     * @since   1.7.2
     *
     * @return  array   Order By Options
     */
    public function get_order_by_options() {

        // Get order by options
        $order_by = array(
            'none'          => __( 'No Order', 'page-generator-pro' ),
            'ID'            => __( 'Post ID', 'page-generator-pro' ),
            'author'        => __( 'Author', 'page-generator-pro' ),
            'title'         => __( 'Title', 'page-generator-pro' ),
            'name'          => __( 'Name', 'page-generator-pro' ),
            'type'          => __( 'Post Type', 'page-generator-pro' ),
            'date'          => __( 'Published Date', 'page-generator-pro' ),
            'modified'      => __( 'Modified Date', 'page-generator-pro' ),
            'rand'          => __( 'Random', 'page-generator-pro' ),
            'comment_count' => __( 'Number of Comments', 'page-generator-pro' ),
            'relevance'     => __( 'Relevance', 'page-generator-pro' ),
            'distance'      => __( 'Distance', 'page-generator-pro' ),
        );

        /**
         * Defines WP_Query compatible order by options
         *
         * @since   1.7.2
         *
         * @param   array   $order_by   Order By options
         */
        $order_by = apply_filters( 'page_generator_pro_common_get_order_by_options', $order_by );

        // Return filtered results
        return $order_by;

    }

    /**
     * Helper method to retrieve Location Order By options
     *
     * @since   1.7.8
     *
     * @return  array   Order By Options
     */
    public function get_locations_order_by_options() {

        // Get order by options
        $order_by = array(
            'city_name'     => __( 'City Name', 'page-generator-pro' ),
            'county_name'   => __( 'County Name', 'page-generator-pro' ),
            'region_name'   => __( 'Region Name', 'page-generator-pro' ),
            'zipcode'       => __( 'ZIP Code', 'page-generator-pro' ),
        );

        /**
         * Defines GeoRocket API compatible order by options
         *
         * @since   1.7.8
         *
         * @param   array   $order_by   GeoRocket API order by options
         */
        $order_by = apply_filters( 'page_generator_pro_common_get_location_order_by_options', $order_by );

        // Return filtered results
        return $order_by;

    }

    /**
     * Helper method to retrieve Order options
     *
     * @since   1.7.2
     *
     * @return  array   Order Options
     */
    public function get_order_options() {

        // Get order options
        $order = array(
            'asc'          => __( 'Ascending (A-Z)', 'page-generator-pro' ),
            'desc'         => __( 'Descending (Z-A)', 'page-generator-pro' ),
        );

        /**
         * Defines WP_Query compatible order options
         *
         * @since   1.7.2
         *
         * @param   array   $order  Order options
         */
        $order = apply_filters( 'page_generator_pro_common_get_order_options', $order );

        // Return filtered results
        return $order;

    }

    
    /**
     * Helper method to retrieve Comparison operators
     *
     * @since   2.2.8
     *
     * @return  array   Comparison Options
     */
    public function get_comparison_operators() {

        // Get operator options
        $operators = array(
            '='         => __( 'Equals', 'page-generator-pro' ),
            '!='        => __( 'Does Not Equal', 'page-generator-pro' ),
            '>'         => __( 'Greater Than', 'page-generator-pro' ),
            '>='        => __( 'Greater Than or Equal To', 'page-generator-pro' ),
            '<'         => __( 'Less Than', 'page-generator-pro' ),
            '<='        => __( 'Less Than or Equal To', 'page-generator-pro' ),
            'LIKE'      => __( 'Similar To', 'page-generator-pro' ),
            'NOT LIKE'  => __( 'Not Similar To', 'page-generator-pro' ),
        );

        /**
         * Define MySQL compliant operator options
         *
         * @since   2.2.8
         *
         * @param   array   $order  Order options
         */
        $operators = apply_filters( 'page_generator_pro_common_get_comparison_operators', $operators );

        // Return filtered results
        return $operators;

    }

    /**
     * Helper method to retrieve Operator options
     *
     * @since   2.2.2
     *
     * @return  array   Operator Options
     */
    public function get_operator_options() {

        // Get operator options
        $operators = array(
            'AND'   => __( 'All', 'page-generator-pro' ),
            'OR'    => __( 'Any', 'page-generator-pro' ),
        );

        /**
         * Define MySQL compliant operator options
         *
         * @since   2.2.2
         *
         * @param   array   $order  Order options
         */
        $operators = apply_filters( 'page_generator_pro_common_get_operator_options', $operators );

        // Return filtered results
        return $operators;

    }

    /**
     * Helper method to retrieve Keyword Transformation options
     *
     * @since   1.7.8
     *
     * @return  array   Keyword Transformation Options
     */
    public function get_keyword_transformation_options() {

        // Get transformation options
        $transformation_options = array(
            'uppercase_all'                     => __( 'Uppercase all characters', 'page-generator-pro' ),
            'lowercase_all'                     => __( 'Lowercase all characters', 'page-generator-pro' ),
            'uppercase_first_character'         => __( 'Capitalise first letter', 'page-generator-pro' ),
            'uppercase_first_character_words'   => __( 'Capitalise first letter of each word', 'page-generator-pro' ),
            'url'                               => __( 'Convert to Permalink style slug', 'page-generator-pro' ), 
            'all'                               => __( 'All Terms', 'page-generator-pro' ),
        );

        /**
         * Defines available Keyword Transformation options.
         *
         * @since   1.7.8
         *
         * @param   array   $transformation_options  Transformation Options
         */
        $transformation_options = apply_filters( 'page_generator_pro_common_get_keyword_transformation_options', $transformation_options );

        // Return filtered results
        return $transformation_options;

    }

    /**
     * Returns an array of Javascript DOM selectors to enable the keyword
     * autocomplete functionality on.
     *
     * @since   2.0.2
     *
     * @return  array   Javascript DOM Selectors
     */
    public function get_autocomplete_enabled_fields() {

        // Get fields
        $fields = array(
            // Classic Editor
            'input[name=post_title]',
            'input[name=tag-name]',
            'input[name=name]',
            'input[name=slug]',
            'textarea[name=description]',

            // Gutenberg
            'textarea.editor-post-title__input', // Post Title

            // Plugin Fields
            '.wpzinc-autocomplete',
        );

        /**
         * Defines an array of Javascript DOM selectors to enable the keyword
         * autocomplete functionality on.
         *
         * @since   2.0.2
         *
         * @param   array   $fields  Supported Fields
         */
        $fields = apply_filters( 'page_generator_pro_common_get_autocomplete_enabled_fields', $fields );

        // Return filtered results
        return $fields;

    }

    /**
     * Helper method to retrieve available Unsplash Image Size options
     *
     * @since   1.7.9
     *
     * @return  array   Image Size Options
     */
    public function get_unsplash_image_size_options() {

        // Get Unsplash Image Size Options
        $image_sizes = array(
            'raw'       => __( 'Raw', 'page-generator-pro' ),
            'full'      => __( 'Full', 'page-generator-pro' ),
            'regular'   => __( 'Regular', 'page-generator-pro' ),
            'small'     => __( 'Small', 'page-generator-pro' ),
            'thumb'     => __( 'Thumbnail', 'page-generator-pro' ),
        );

        /**
         * Defines available Unsplash API Image Size options.
         *
         * @since   1.7.9
         *
         * @param   array   $image_sizes  Image Sizes
         */
        $image_sizes = apply_filters( 'page_generator_pro_common_get_unsplash_image_size_options', $image_sizes );

        // Return filtered results
        return $image_sizes;

    }

    /**
     * Helper method to retrieve available Unsplash Image Orientation options
     *
     * @since   1.8.1
     *
     * @return  array   Image Size Options
     */
    public function get_unsplash_image_orientation_options() {

        // Get Unsplash Image Size Options
        $image_orientations = array(
            ''              => __( '(any)', 'page-generator-pro' ),
            'landscape'     => __( 'Landscape', 'page-generator-pro' ),
            'portrait'      => __( 'Portrait', 'page-generator-pro' ),
            'squarish'      => __( 'Squarish', 'page-generator-pro' ),
        );

        /**
         * Defines available Unsplash API Image Orientation options.
         *
         * @since   1.8.1
         *
         * @param   array   $image_orientations  Image Orientation options
         */
        $image_orientations = apply_filters( 'page_generator_pro_common_get_unsplash_image_orientation_options', $image_orientations );

        // Return filtered results
        return $image_orientations;

    }

    /**
     * Helper method to retrieve available WordPress Image Size options
     *
     * @since   1.7.9
     *
     * @return  array   Image Size Options
     */
    public function get_media_library_image_size_options() {

        // Get registered image sizes from WordPress
        $image_sizes = get_intermediate_image_sizes();

        /**
         * Defines available registered image sizes in WordPress
         *
         * @since   1.7.9
         *
         * @param   array   $image_sizes  Image Sizes
         */
        $image_sizes = apply_filters( 'page_generator_pro_common_get_media_library_image_size_options', $image_sizes );

        // Return filtered results
        return $image_sizes;

    }

    /**
     * Helper method to return an array of WordPress Role Capabilities that should be disabled
     * when a Content Group is Generating Content
     *
     * @since   1.9.9
     *
     * @return  array   Capabilities
     */
    public function get_capabilities_to_disable_on_group_content_generation() {

        // Get capabilities
        $capabilities = array(
            'delete_post',
            'edit_post',
        );

        /**
         * Defines Role Capabilities that should be disabled when a Content Group is Generating Content.
         *
         * @since   1.9.9
         *
         * @param   array   $capabilities   Capabilities
         */
        $capabilities = apply_filters( 'page_generator_pro_common_get_capabilities_to_disable_on_group_content_generation', $capabilities );

        // Return filtered results
        return $capabilities;

    }

    /**
     * Helper method to return an array of WordPress Role Capabilities that should be disabled
     * when a Term Group is Generating Terms
     *
     * @since   1.9.9
     *
     * @return  array   Capabilities
     */
    public function get_capabilities_to_disable_on_group_term_generation() {

        // Get capabilities
        $capabilities = array(
            'delete_term',
            'edit_term',
        );

        /**
         * Defines Role Capabilities that should be disabled when a Term Group is Generating Terms.
         *
         * @since   1.9.9
         *
         * @param   array   $capabilities   Capabilities
         */
        $capabilities = apply_filters( 'page_generator_pro_common_get_capabilities_to_disable_on_group_term_generation', $capabilities );

        // Return filtered results
        return $capabilities;

    }

    /**
     * Helper method to retrieve country codes and names
     *
     * @since   1.1.7
     *
     * @return  array   Countries
     */
    public function get_countries() {

        // Get countries
        $countries = array (
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        );

        /**
         * Defines available GeoRocket API country codes and names.
         *
         * @since   1.1.7
         *
         * @param   array   $countries  Countries
         */
        $countries = apply_filters( 'page_generator_pro_common_get_countries', $countries );

        // Return filtered results
        return $countries;

    }

    /**
     * Helper method to retrieve output types for Generate Locations
     *
     * @since   1.7.8
     *
     * @return  array      
     */
    public function get_locations_methods() {

        // Define Output Types
        $methods = array(
            'radius'                        => __( 'Radius', 'page-generator-pro' ),
            'area'                          => __( 'Area', 'page-generator-pro' ),
        );

        /**
         * Defines available output types for Generate Locations.
         *
         * @since   1.7.8
         *
         * @param   array   $methods    Methods
         */
        $methods = apply_filters( 'page_generator_pro_common_get_locations_restrictions', $methods );

        // Return filtered results
        return $methods;

    }

    /**
     * Helper method to retrieve location restrictions for Generate Locations
     *
     * @since   1.7.8
     *
     * @return  array      
     */
    public function get_locations_restrictions() {

        // Define Restrictions
        $restrictions = array(
            'radius'                        => __( 'Radius', 'page-generator-pro' ),
            'city'                          => __( 'City', 'page-generator-pro' ),
            'county'                        => __( 'County', 'page-generator-pro' ),
            'region'                        => __( 'Region', 'page-generator-pro' ),
        );

        /**
         * Defines available location restrictions for Generate Locations
         *
         * @since   1.7.8
         *
         * @param   array   $restrictions   Output Types
         */
        $restrictions = apply_filters( 'page_generator_pro_common_get_locations_restrictions', $restrictions );

        // Return filtered results
        return $restrictions;

    }

    /**
     * Helper method to retrieve output types for Generate Locations
     *
     * @since   1.5.0
     *
     * @return  array      
     */
    public function get_locations_output_types() {

        // Define Output Types
        $output_types = array(
            'street_name'                       => __( 'Street Name', 'page-generator-pro' ),

            'zipcode'                           => __( 'ZIP Code', 'page-generator-pro' ),
            'zipcode_latitude'                  => __( 'ZIP Code: Latitude', 'page-generator-pro' ),
            'zipcode_longitude'                 => __( 'ZIP Code: Longitude', 'page-generator-pro' ),
            
            'zipcode_district'                  => __( 'ZIP Code District', 'page-generator-pro' ), 
            
            'city_name'                         => __( 'City', 'page-generator-pro' ),
            'city_latitude'                     => __( 'City: Latitude', 'page-generator-pro' ),
            'city_longitude'                    => __( 'City: Longitude', 'page-generator-pro' ),
            'city_population'                   => __( 'City: Population', 'page-generator-pro' ),
            
            'county_name'                       => __( 'County', 'page-generator-pro' ),
            'county_code'                       => __( 'County: Code', 'page-generator-pro' ),
            'county_wikipedia_url'              => __( 'County: Wikipedia URL', 'page-generator-pro' ),
            'county_wikipedia_summary'          => __( 'County: Wikipedia Summary', 'page-generator-pro' ), 

            'region_name'                       => __( 'Region', 'page-generator-pro' ),
            'region_code'                       => __( 'Region: Code', 'page-generator-pro' ),
            'region_wikipedia_url'              => __( 'Region: Wikipedia URL', 'page-generator-pro' ),
            'region_wikipedia_summary'          => __( 'Region: Wikipedia Summary', 'page-generator-pro' ),
        );

        /**
         * Backward compat. filter.  Defines available output types for Generate Locations.
         *
         * @since   1.5.0
         *
         * @param   array   $output_types   Output Types
         */
        $output_types = apply_filters( 'page_generator_pro_common_get_nearby_cities_zipcodes_output_types', $output_types );

        /**
         * Defines available output types for Generate Locations.
         *
         * @since   1.5.0
         *
         * @param   array   $output_types   Output Types
         */
        $output_types = apply_filters( 'page_generator_pro_common_get_locations_output_types', $output_types );

        // Return filtered results
        return $output_types;

    }

    /**
     * Helper method to retrieve output types for Generate Nearby Cities / ZIP Codes
     *
     * @since   1.5.0
     *
     * @return  array      
     */
    public function get_nearby_cities_zipcodes_output_types() {

        $output_types = array(
            'city'                          => __( 'City', 'page-generator-pro' ),
            'county'                        => __( 'County', 'page-generator-pro' ),
            'zipcode'                       => __( 'ZIP Code', 'page-generator-pro' ),
        );

        /**
         * Defines available output types for Generate Locations.
         *
         * @since   1.5.0
         *
         * @param   array   $output_types   Output Types
         */
        $output_types = apply_filters( 'page_generator_pro_common_get_nearby_cities_zipcodes_output_types', $output_types );

        // Return filtered results
        return $output_types;

    }

    /**
     * Helper method to retrieve output types for Generate Phone Area Codes.
     *
     * @since   1.5.9
     *
     * @return  array      
     */
    public function get_phone_area_code_output_types() {

        // Define Output Types
        $output_types = array(
            'city'                          => __( 'City', 'page-generator-pro' ),
            'area_code'                     => __( 'Phone Area Code', 'page-generator-pro' ),
            'country_code'                  => __( 'Phone Country Code', 'page-generator-pro' ),
        );

        /**
         * Defines available output types for Generate Phone Area Codes.
         *
         * @since   1.5.9
         *
         * @param   array   $output_types   Output Types
         */
        $output_types = apply_filters( 'page_generator_pro_common_get_phone_area_output_types', $output_types );

        // Return filtered results
        return $output_types;

    }

    /**
     * Helper method to retrieve Wikipedia languages
     *
     * @since   1.2.1
     *
     * @return  array    Languages
     */
    public function get_wikipedia_languages() {

        // Keys are Wikipedia subdomains e.g. ab.wikipedia.org
        // Values are the language names in English
        $languages = array(
            'ab' => 'Abkhazian',
            'ace' => 'Acehnese',
            'ady' => 'Adyghe',
            'aa' => 'Afar',
            'af' => 'Afrikaans',
            'ak' => 'Akan',
            'sq' => 'Albanian',
            'als' => 'Alemannic',
            'am' => 'Amharic',
            'ang' => 'Anglo-Saxon',
            'ar' => 'Arabic',
            'an' => 'Aragonese',
            'arc' => 'Aramaic',
            'hy' => 'Armenian',
            'roa-rup' => 'Aromanian',
            'as' => 'Assamese',
            'ast' => 'Asturian',
            'av' => 'Avar',
            'ay' => 'Aymara',
            'az' => 'Azerbaijani',
            'bm' => 'Bambara',
            'bjn' => 'Banjar',
            'map-bms' => 'Banyumasan',
            'ba' => 'Bashkir',
            'eu' => 'Basque',
            'bar' => 'Bavarian',
            'be' => 'Belarusian',
            'be-tarask' => 'Belarusian (Taraškievica)',
            'bn' => 'Bengali',
            'bh' => 'Bihari',
            'bpy' => 'Bishnupriya Manipuri',
            'bi' => 'Bislama',
            'bs' => 'Bosnian',
            'br' => 'Breton',
            'bug' => 'Buginese',
            'bg' => 'Bulgarian',
            'my' => 'Burmese',
            'bxr' => 'Buryat',
            'zh-yue' => 'Cantonese',
            'ca' => 'Catalan',
            'ceb' => 'Cebuano',
            'bcl' => 'Central Bicolano',
            'ch' => 'Chamorro',
            'cbk-zam' => 'Chavacano',
            'ce' => 'Chechen',
            'chr' => 'Cherokee',
            'chy' => 'Cheyenne',
            'ny' => 'Chichewa',
            'zh' => 'Chinese',
            'cho' => 'Choctaw',
            'cv' => 'Chuvash',
            'zh-classical' => 'Classical Chinese',
            'kw' => 'Cornish',
            'co' => 'Corsican',
            'cr' => 'Cree',
            'crh' => 'Crimean Tatar',
            'hr' => 'Croatian',
            'cs' => 'Czech',
            'da' => 'Danish',
            'dv' => 'Divehi',
            'nl' => 'Dutch',
            'nds-nl' => 'Dutch Low Saxon',
            'dz' => 'Dzongkha',
            'arz' => 'Egyptian Arabic',
            'eml' => 'Emilian-Romagnol',
            'en' => 'English',
            'myv' => 'Erzya',
            'eo' => 'Esperanto',
            'et' => 'Estonian',
            'ee' => 'Ewe',
            'ext' => 'Extremaduran',
            'fo' => 'Faroese',
            'hif' => 'Fiji Hindi',
            'fj' => 'Fijian',
            'fi' => 'Finnish',
            'frp' => 'Franco-Provençal',
            'fr' => 'French',
            'fur' => 'Friulian',
            'ff' => 'Fula',
            'gag' => 'Gagauz',
            'gl' => 'Galician',
            'gan' => 'Gan',
            'ka' => 'Georgian',
            'de' => 'German',
            'glk' => 'Gilaki',
            'gom' => 'Goan Konkani',
            'got' => 'Gothic',
            'el' => 'Greek',
            'kl' => 'Greenlandic',
            'gn' => 'Guarani',
            'gu' => 'Gujarati',
            'ht' => 'Haitian',
            'hak' => 'Hakka',
            'ha' => 'Hausa',
            'haw' => 'Hawaiian',
            'he' => 'Hebrew',
            'hz' => 'Herero',
            'mrj' => 'Hill Mari',
            'hi' => 'Hindi',
            'ho' => 'Hiri Motu',
            'hu' => 'Hungarian',
            'is' => 'Icelandic',
            'io' => 'Ido',
            'ig' => 'Igbo',
            'ilo' => 'Ilokano',
            'id' => 'Indonesian',
            'ia' => 'Interlingua',
            'ie' => 'Interlingue',
            'iu' => 'Inuktitut',
            'ik' => 'Inupiak',
            'ga' => 'Irish',
            'it' => 'Italian',
            'jam' => 'Jamaican Patois',
            'ja' => 'Japanese',
            'jv' => 'Javanese',
            'kbd' => 'Kabardian',
            'kab' => 'Kabyle',
            'xal' => 'Kalmyk',
            'kn' => 'Kannada',
            'kr' => 'Kanuri',
            'pam' => 'Kapampangan',
            'krc' => 'Karachay-Balkar',
            'kaa' => 'Karakalpak',
            'ks' => 'Kashmiri',
            'csb' => 'Kashubian',
            'kk' => 'Kazakh',
            'km' => 'Khmer',
            'ki' => 'Kikuyu',
            'rw' => 'Kinyarwanda',
            'ky' => 'Kirghiz',
            'rn' => 'Kirundi',
            'kv' => 'Komi',
            'koi' => 'Komi-Permyak',
            'kg' => 'Kongo',
            'ko' => 'Korean',
            'kj' => 'Kuanyama',
            'ku' => 'Kurdish (Kurmanji)',
            'ckb' => 'Kurdish (Sorani)',
            'lad' => 'Ladino',
            'lbe' => 'Lak',
            'lo' => 'Lao',
            'ltg' => 'Latgalian',
            'la' => 'Latin',
            'lv' => 'Latvian',
            'lez' => 'Lezgian',
            'lij' => 'Ligurian',
            'li' => 'Limburgish',
            'ln' => 'Lingala',
            'lt' => 'Lithuanian',
            'jbo' => 'Lojban',
            'lmo' => 'Lombard',
            'nds' => 'Low Saxon',
            'dsb' => 'Lower Sorbian',
            'lg' => 'Luganda',
            'lb' => 'Luxembourgish',
            'mk' => 'Macedonian',
            'mai' => 'Maithili',
            'mg' => 'Malagasy',
            'ms' => 'Malay',
            'ml' => 'Malayalam',
            'mt' => 'Maltese',
            'gv' => 'Manx',
            'mi' => 'Maori',
            'mr' => 'Marathi',
            'mh' => 'Marshallese',
            'mzn' => 'Mazandarani',
            'mhr' => 'Meadow Mari',
            'cdo' => 'Min Dong',
            'zh-min-nan' => 'Min Nan',
            'min' => 'Minangkabau',
            'xmf' => 'Mingrelian',
            'mwl' => 'Mirandese',
            'mdf' => 'Moksha',
            'mo' => 'Moldovan',
            'mn' => 'Mongolian',
            'mus' => 'Muscogee',
            'nah' => 'Nahuatl',
            'na' => 'Nauruan',
            'nv' => 'Navajo',
            'ng' => 'Ndonga',
            'nap' => 'Neapolitan',
            'ne' => 'Nepali',
            'new' => 'Newar',
            'pih' => 'Norfolk',
            'nrm' => 'Norman',
            'frr' => 'North Frisian',
            'lrc' => 'Northern Luri',
            'se' => 'Northern Sami',
            'nso' => 'Northern Sotho',
            'no' => 'Norwegian (Bokmål)',
            'nn' => 'Norwegian (Nynorsk)',
            'nov' => 'Novial',
            'ii' => 'Nuosu',
            'oc' => 'Occitan',
            'cu' => 'Old Church Slavonic',
            'or' => 'Oriya',
            'om' => 'Oromo',
            'os' => 'Ossetian',
            'pfl' => 'Palatinate German',
            'pi' => 'Pali',
            'pag' => 'Pangasinan',
            'pap' => 'Papiamentu',
            'ps' => 'Pashto',
            'pdc' => 'Pennsylvania German',
            'fa' => 'Persian',
            'pcd' => 'Picard',
            'pms' => 'Piedmontese',
            'pl' => 'Polish',
            'pnt' => 'Pontic',
            'pt' => 'Portuguese',
            'pa' => 'Punjabi',
            'qu' => 'Quechua',
            'ksh' => 'Ripuarian',
            'rmy' => 'Romani',
            'ro' => 'Romanian',
            'rm' => 'Romansh',
            'ru' => 'Russian',
            'rue' => 'Rusyn',
            'sah' => 'Sakha',
            'sm' => 'Samoan',
            'bat-smg' => 'Samogitian',
            'sg' => 'Sango',
            'sa' => 'Sanskrit',
            'sc' => 'Sardinian',
            'stq' => 'Saterland Frisian',
            'sco' => 'Scots',
            'gd' => 'Scottish Gaelic',
            'sr' => 'Serbian',
            'sh' => 'Serbo-Croatian',
            'st' => 'Sesotho',
            'sn' => 'Shona',
            'scn' => 'Sicilian',
            'szl' => 'Silesian',
            'simple' => 'Simple English',
            'sd' => 'Sindhi',
            'si' => 'Sinhalese',
            'sk' => 'Slovak',
            'sl' => 'Slovenian',
            'so' => 'Somali',
            'azb' => 'Southern Azerbaijani',
            'es' => 'Spanish',
            'srn' => 'Sranan',
            'su' => 'Sundanese',
            'sw' => 'Swahili',
            'ss' => 'Swati',
            'sv' => 'Swedish',
            'tl' => 'Tagalog',
            'ty' => 'Tahitian',
            'tg' => 'Tajik',
            'ta' => 'Tamil',
            'roa-tara' => 'Tarantino',
            'tt' => 'Tatar',
            'te' => 'Telugu',
            'tet' => 'Tetum',
            'th' => 'Thai',
            'bo' => 'Tibetan',
            'ti' => 'Tigrinya',
            'tpi' => 'Tok Pisin',
            'to' => 'Tongan',
            'ts' => 'Tsonga',
            'tn' => 'Tswana',
            'tum' => 'Tumbuka',
            'tr' => 'Turkish',
            'tk' => 'Turkmen',
            'tyv' => 'Tuvan',
            'tw' => 'Twi',
            'udm' => 'Udmurt',
            'uk' => 'Ukrainian',
            'hsb' => 'Upper Sorbian',
            'ur' => 'Urdu',
            'ug' => 'Uyghur',
            'uz' => 'Uzbek',
            've' => 'Venda',
            'vec' => 'Venetian',
            'vep' => 'Vepsian',
            'vi' => 'Vietnamese',
            'vo' => 'Volapük',
            'fiu-vro' => 'Võro',
            'wa' => 'Walloon',
            'war' => 'Waray',
            'cy' => 'Welsh',
            'vls' => 'West Flemish',
            'fy' => 'West Frisian',
            'pnb' => 'Western Punjabi',
            'wo' => 'Wolof',
            'wuu' => 'Wu',
            'xh' => 'Xhosa',
            'yi' => 'Yiddish',
            'yo' => 'Yoruba',
            'diq' => 'Zazaki',
            'zea' => 'Zeelandic',
            'za' => 'Zhuang',
            'zu' => 'Zulu',
        );

        /**
         * Defines available Wikipedia languages.
         *
         * @since   1.2.1
         *
         * @param   array   $output_types   Output Types
         */
        $languages = apply_filters( 'page_generator_pro_common_get_wikipedia_languages', $languages );

        // Return filtered results
        return $languages;

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.1.6
     * @deprecated  1.9.8
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'common';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.9.8', 'Page_Generator_Pro()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Page_Generator_Pro()->get_class( $name );

    }

}