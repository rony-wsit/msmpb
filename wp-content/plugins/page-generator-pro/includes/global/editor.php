<?php
/**
 * Editor class
 * 
 * @package Page_Generator_Pro
 * @author  Tim Carr
 * @version 1.0.0
 */
class Page_Generator_Pro_Editor {

    /**
     * Holds the base object.
     *
     * @since 1.2.1
     *
     * @var object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   1.0.0
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Maybe disable the Custom Fields dropdown on the Post Editor
        add_filter( 'postmeta_form_keys', array( $this, 'maybe_remove_custom_fields_meta_box_meta_keys' ), 10, 2 );

        // Maybe change the wp_dropdown_page() select for an input
        add_filter( 'quick_edit_dropdown_pages_args', array( $this, 'maybe_simplify_wp_dropdown_page_query' ), 10 );
        add_filter( 'page_attributes_dropdown_pages_args', array( $this, 'maybe_simplify_wp_dropdown_page_query' ), 10 );
        add_filter( 'wp_dropdown_pages', array( $this, 'maybe_replace_wp_dropdown_page' ), 10, 3 );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ), 99 );
        add_action( 'admin_init', array( $this, 'setup_tinymce_plugins' ) );

    }

    /**
     * Defines the Meta Keys to display in the <select> dropdown for the Custom Fields Meta Box.
     *
     * If null is returned, WordPress will perform a DB query to fetch all unique
     * meta keys from the Post Meta table, which can be a slow and expensive
     * query if the WordPress installations contains a lot of post meta data.
     *
     * @since   0.0.1
     *
     * @param   array   $meta_keys  Meta Keys
     * @param   WP_Post $post       WordPress Post
     * @return  array               Meta Keys
     */
    public function maybe_remove_custom_fields_meta_box_meta_keys( $meta_keys, $post ) {

        // Don't do anything if we are not disabling custom fields
        $disable_custom_fields = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-general', 'disable_custom_fields', '0' );
        if ( ! $disable_custom_fields ) {
            return $meta_keys;
        }

        // Define the meta keys that you want to return
        // At least one key must be specified, otherwise WordPress will query the DB
        $keys = array(
            '_page_generator_pro_group_id',
        );

        /**
         * Defines the Meta Keys to make available in the Custom Fields dropdown.
         *
         * @since   2.0.7
         *
         * @param   array   $keys       Defined Meta Keys to use
         * @param   array   $meta_keys  Original Meta Keys
         * @param   WP_Post $post       WordPress Post
         */
        $keys = apply_filters( 'page_generator_pro_maybe_remove_custom_fields_meta_box_meta_keys', $keys, $meta_keys, $post );
        
        // Return keys
        return $keys;

    }

    /**
     * If the Plugin is configured to replace wp_dropdown_pages() <select> output with an AJAX <select> or
     * input field, simplify the Page query so it isn't slow and expensive.
     *
     * This query is always run when wp_dropdown_pages() is called (even though we might not use it, 
     * so we always need to optimize it.
     *
     * @since   2.1.6
     *
     * @param   array       $args   Arguments
     * @return  array               Arguments
     */
    public function maybe_simplify_wp_dropdown_page_query( $args ) {

        // Don't do anything if we are not 
        $change_page_dropdown_field = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-general', 'restrict_parent_page_depth', '0' );
        if ( ! $change_page_dropdown_field ) {
            return $args;
        }

        // Simplify the query
        $args['depth'] = 1;
        $args['id'] = 1;
        $args['sort_column'] = 'ID';

        // Return
        return $args;

    }

    /**
     * Replaces the wp_dropdown_pages() <select> output with either an AJAX <select>
     * or <input> output for performance.
     *
     * @since   2.1.6
     *
     * @param   string  $output     HTML Output
     * @param   array   $args       Arguments
     * @param   array   $pages      Pages from Query
     * @return  string              HTML Output
     */
    public function maybe_replace_wp_dropdown_page( $output, $args, $pages ) {

        // Don't do anything if we are not 
        $change_page_dropdown_field = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-general', 'restrict_parent_page_depth', '0' );
        if ( ! $change_page_dropdown_field ) {
            return $output;
        }

        // Filter the output, depending on how we're changing the Page Parent Output
        switch ( $change_page_dropdown_field ) {
            /**
             * AJAX Select
             */
            case 'ajax_select':
                // Get AJAX <select> HTML
                $output = $this->change_wp_dropdown_pages_output_to_ajax_select_field( $output, $args, $pages );
                break;

            /**
             * Input
             */
            default:
                // Get <input> HTML
                $output = $this->change_wp_dropdown_pages_output_to_input_field( $output, $args, $pages );
                break;
        }

        // Return
        return $output;

    }

    /**
     * Replaces the wp_dropdown_pages() <select> output with an <input> output,
     * for performance.
     *
     * @since   2.1.6
     *
     * @param   string  $output     HTML Output
     * @param   array   $args       Arguments
     * @param   array   $pages      Pages from Query
     * @return  string              HTML Output
     */
    private function change_wp_dropdown_pages_output_to_input_field( $output, $args, $pages ) {

        // Get CSS class
        $class = '';
        if ( ! empty( $r['class'] ) ) {
            $class = " class='" . esc_attr( $r['class'] ) . "'";
        }

        // Build field
        $output = '<input type="text" name="' . esc_attr( $args['name'] ) . '"' . $class . ' id="' . esc_attr( $args['id'] ) . '" value="' . $args['selected'] . '" size="6" />
                    <br /><small>' . __( 'Enter the Page / Post ID', 'page-generator-pro' ) . '</small>';

        // If a parent is specified, fetch its title
        if ( $args['selected'] ) {
            $output .= '<br /><small>' . get_the_title( $args['selected'] ) . '</small>';
        }

        // Return
        return $output;

    }

    /**
     * Replaces the wp_dropdown_pages() <select> output with an AJAX <select> output,
     * for performance.
     *
     * @since   2.1.8
     *
     * @param   string  $output     HTML Output
     * @param   array   $args       Arguments
     * @param   array   $pages      Pages from Query
     * @return  string              HTML Output
     */
    private function change_wp_dropdown_pages_output_to_ajax_select_field( $output, $args, $pages ) {

        $output = '<select name="' . esc_attr( $args['name'] ) . '" class="wpzinc-selectize-search widefat" data-action="page_generator_pro_search_pages" data-args="' . http_build_query( $args ) . '" data-name-field="post_title" data-value-field="ID" data-method="POST" data-output-fields="post_title">';
        if ( $args['selected'] ) {
            $output .= '<option value="' . $args['selected'] . '" selected>' . get_the_title( $args['selected'] ) . '</option>';
        }
        $output .= '</select>';

        // Return
        return $output;

    }

    /**
     * Enqueues the Modal and QuickTags script for the Text Editor
     *
     * @since   1.5.2
     */
    public function enqueue_scripts() {

        // modal.js
        wp_enqueue_script( 'wpzinc-admin-modal' );

    }

    /**
     * Setup calls to add a button and plugin to the Page Generator Pro WP_Editor
     *
     * @since 1.0.0
     */
    public function setup_tinymce_plugins() {

		// Check if rich editing is enabled for the user
        if ( get_user_option( 'rich_editing' ) != 'true' ) {
        	return;
        }

        // Add filters to register TinyMCE Plugins
		add_filter( 'mce_external_plugins', array( $this, 'register_tinymce_plugins' ) );
        add_filter( 'mce_buttons', array( $this, 'register_tinymce_buttons' ) );

    }

    /**
     * Register JS plugins for the TinyMCE Editor
     *
     * @since   1.0.0
     *
     * @param   array   $plugins    JS Plugins
     * @return  array 		        JS Plugins
     */
    public function register_tinymce_plugins( $plugins ) {

        // Set a flag to denote whether we should register TinyMCE Plugins
        $should_register_tinymce_plugins = false;

        // Check we are on a Plugin screen that contains a wp_editor() instance
        $screen = $this->base->get_class( 'screen' )->get_current_screen();

        switch ( $screen['screen'] ) {
            /**
             * Content: Groups
             */
            case 'content_groups':
                switch ( $screen['section'] ) {
                    /**
                     * Content: Groups: Edit
                     */
                    case 'edit':
                        $should_register_tinymce_plugins = true;
                        break;
                }
                break;

            /**
             * Content: Terms
             */
            case 'content_terms':
                $should_register_tinymce_plugins = true;
                break;
        }

        /**
         * Set a flag to denote whether we should register TinyMCE Plugins
         *
         * @since   2.2.4
         *
         * @param   bool   $should_register_tinymce_plugins    Should Register TinyMCE Plugins
         */
        $should_register_tinymce_plugins = apply_filters( 'page_generator_pro_editor_should_register_tinymce_plugins', $should_register_tinymce_plugins );

        // Bail if we shouldn't register TinyMCE Plugins
        if ( ! $should_register_tinymce_plugins ) {
            return $plugins;
        }

        // Register TinyMCE Plugins
        $plugins['page_generator_pro_autocomplete']    = $this->base->plugin->url . 'assets/js/min/tinymce-autocomplete-min.js';
    	$plugins['page_generator_pro_google_maps']     = $this->base->plugin->url . 'assets/js/min/tinymce-google-map-min.js';
        $plugins['page_generator_pro_media_library']   = $this->base->plugin->url . 'assets/js/min/tinymce-media-library-min.js';
        $plugins['page_generator_pro_open_street_map'] = $this->base->plugin->url . 'assets/js/min/tinymce-open-street-map-min.js';
        $plugins['page_generator_pro_pexels']          = $this->base->plugin->url . 'assets/js/min/tinymce-pexels-min.js';
        $plugins['page_generator_pro_pixabay']         = $this->base->plugin->url . 'assets/js/min/tinymce-pixabay-min.js';
        $plugins['page_generator_pro_related_links']   = $this->base->plugin->url . 'assets/js/min/tinymce-related-links-min.js';
	    $plugins['page_generator_pro_spintax_generate']= $this->base->plugin->url . 'assets/js/min/tinymce-spintax-generate-min.js';
        $plugins['page_generator_pro_wikipedia']       = $this->base->plugin->url . 'assets/js/min/tinymce-wikipedia-min.js';
	    $plugins['page_generator_pro_yelp']            = $this->base->plugin->url . 'assets/js/min/tinymce-yelp-min.js';
	    $plugins['page_generator_pro_youtube']         = $this->base->plugin->url . 'assets/js/min/tinymce-youtube-min.js';

        /**
         * Defines the TinyMCE Plugins to register
         *
         * @since   1.0.0
         *
         * @param   array   $plugins    TinyMCE Plugins
         */
	    $plugins = apply_filters( 'page_generator_pro_editor_register_tinymce_plugins', $plugins );

        // Return filtered results
        return $plugins;

    }

    /**
     * Registers buttons in the TinyMCE Editor
     *
     * @since   1.0.0
     *
     * @param   array   $buttons    Buttons
     * @return  array 		        Buttons
     */
    public function register_tinymce_buttons( $buttons ) {

        // Check we are on the Groups screen
        $screen = get_current_screen();
        if ( $screen->post_type != $this->base->plugin->name ) {
            return $buttons;
        }

    	array_push( 
            $buttons,
            "|",
            'page_generator_pro_google_maps', 
            'page_generator_pro_media_library',
            'page_generator_pro_open_street_map', 
            'page_generator_pro_pexels',
            'page_generator_pro_pixabay',
            'page_generator_pro_related_links',
            'page_generator_pro_spintax_generate',
            'page_generator_pro_wikipedia', 
            'page_generator_pro_yelp', 
            'page_generator_pro_youtube'
        );

        /**
         * Defines the TinyMCE Buttons to register
         *
         * @since   1.0.0
         *
         * @param   array   $plugins    TinyMCE Plugins
         */
    	$buttons = apply_filters( 'page_generator_pro_editor_register_tinymce_buttons', $buttons );

        // Return filtered results
        return $buttons;

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.1.6
     * @deprecated  1.9.8
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'editor';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.9.8', 'Page_Generator_Pro()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Page_Generator_Pro()->get_class( $name );

    }

}