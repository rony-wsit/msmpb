<?php
/**
 * Shortcode class
 * 
 * @package Page_Generator_Pro
 * @author  Tim Carr
 * @version 1.0.0
 */
class Page_Generator_Pro_Shortcode {

    /**
     * Holds the base object.
     *
     * @since   1.2.1
     *
     * @var     object
     */
    public $base;

    /**
     * Flag denoting if we've output the required Yelp logo
     * if a Yelp shortcode was used.
     *
     * This prevents outputting it multiple times when
     * the shortcode is used more than once
     *
     * @since   2.2.0
     *
     * @var     bool
     */
    public $yelp_logo_output = false;

    /**
     * Constructor
     * 
     * @since   1.0.0
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Hooks and Filters
        add_action( 'wp_head', array( $this, 'maybe_load_js' ) );
        add_filter( 'wp_get_custom_css', array( $this, 'maybe_load_css' ), 10, 2 );
        add_filter( 'the_content', array( $this, 'maybe_change_css_prefix_content' ) );

        // Register shortcodes
        $this->add_shortcodes( false );

    }

    /**
     * Registers the shortcodes used by this plugin, depending on whether we're running
     * content generation for a Group or not.
     *
     * @since   1.2.0
     *
     * @param   bool    $generating_group   Generating Group
     */
    public function add_shortcodes( $generating_group = false ) {

        // Get shortcodes
        $shortcodes = $this->get_shortcodes();

        // Bail if no shortcodes are available
        if ( ! is_array( $shortcodes ) || count( $shortcodes ) == 0 ) {
            return;
        }

        // Get CSS Prefix
        $css_prefix = $this->get_css_prefix();

        // Iterate through shortcodes, registering them
        foreach ( $shortcodes as $shortcode => $properties ) {
           
            // Skip if this shortcode should only be registered WHEN generating content
            if ( ! $generating_group && $properties['register_on_generation_only'] ) {
                continue;
            }

            // Skip if this shortcode should only be registered when NOT generating content
            if ( $generating_group && ! $properties['register_on_generation_only'] ) {
                continue;
            }

            // Register the shortcode
            add_shortcode( $this->base->plugin->name . '-' . $shortcode, array( $this, $properties['render_callback'] ) );

            // If a CSS Prefix is specified, and this is the Related Links shortcode, register the actual CSS prefix
            // as an additional shortcode
            if ( $css_prefix == $this->base->plugin->name ) {
                continue;
            }
            if ( $shortcode != 'related-links' ) {
                continue;
            }
            add_shortcode( $css_prefix . '-' . $shortcode, array( $this, $properties['render_callback'] ) );
               
        }

        /**
         * Add any shortcodes here, using add_shortcode(), that should be processed using do_shortcode()
         * when a Page is Generated.  Do NOT register shortcodes that you do NOT want processing into HTML output
         * on the resulting Pages.
         *
         * @since   1.9.8
         *
         * @param   bool    $generating_group   Generating Pages from Group (true|false)
         */
        do_action( 'page_generator_pro_shortcode_add_shortcodes', $generating_group );

    }

    /**
     * Outputs the contents of this Plugin's frontend.js file to the WordPress
     * header as an inline script, if OpenStreetMap markup is present. 
     *
     * We do this instead of enqueueing JS to avoid what people believe is
     * the 'footprint' problem.
     *
     * @since   2.3.4
     */
    public function maybe_load_js() {

        global $post;

        // Bail if in the admin interface
        if ( function_exists( 'is_admin' ) && is_admin() ) {
            return;
        }

        // Bail if Post object is empty
        if ( is_null( $post ) || ! isset( $post->post_content ) ) {
            return;
        }

        // Get shortcodes requiring JS
        $shortcodes_requiring_js = $this->get_shortcodes_requiring_js();

        // If no shortcodes require JS,bail
        if ( ! $shortcodes_requiring_js ) {
            return;
        }

        // Iterate through shortcodes, returning frontend JS
        // if a shortcode or shortcode HTML is found
        foreach ( $shortcodes_requiring_js as $shortcode_name ) {
            if ( strpos( $post->post_content, $shortcode_name ) ) {
                // Fetch Frontend JS
                $plugin_js = file_get_contents( $this->base->plugin->folder . '/assets/js/min/frontend-min.js' );

                // Bail if none found
                if ( empty( $plugin_js ) || ! $plugin_js ) {
                    return;
                }

                // Output
                echo '<script>' . $plugin_js . '</script>';
                return;
            }
        }

        // If here, we don't need to load any frontend JS
        return;

    }

    /**
     * Appends the contents of this Plugin's frontend.css file to the WordPress
     * Theme Customizer Additional CSS.
     *
     * We do this instead of enqueueing CSS to avoid what people believe is
     * the 'footprint' problem.
     *
     * @since   2.0.4
     *
     * @param   string  $customizer_css     Customizer CSS
     * @param   string  $stylesheet         Stylesheet URI
     * @return  string                      Customizer CSS
     */
    public function maybe_load_css( $customizer_css, $stylesheet ) {

        global $post;

        // Bail if in the admin interface
        if ( function_exists( 'is_admin' ) && is_admin() ) {
            return $customizer_css;
        }

        // Bail if Post object is empty
        if ( is_null( $post ) || ! isset( $post->post_content ) ) {
            return $customizer_css;
        }

        // Get shortcodes requiring CSS
        $shortcodes_requiring_css = $this->get_shortcodes_requiring_css();

        // If no shortcodes require CSS, just return the customizer CSS
        if ( ! $shortcodes_requiring_css ) {
            return $customizer_css;
        }

        // Iterate through shortcodes, returning frontend CSS with customizer CSS
        // if a shortcode or shortcode HTML is found
        foreach ( $shortcodes_requiring_css as $shortcode_name ) {
            if ( strpos( $post->post_content, $shortcode_name ) ) {
                return $this->append_css_to_customizer_css( $customizer_css );
            }
        }

        // If here, we don't need to load any frontend CSS
        // Just return the customizer CSS
        return $customizer_css;
    
    }

    /**
     * Appends this Plugin's CSS to the Theme Customizer CSS, changing
     * the CSS Prefix if necessary
     *
     * @since   2.3.4
     *
     * @param   string  $customizer_css     Customizer CSS
     * @return  string                      Customizer CSS
     */
    private function append_css_to_customizer_css( $customizer_css ) {

        // Fetch Frontend CSS
        $plugin_css = file_get_contents( $this->base->plugin->folder . '/assets/css/frontend.css' );

        // Change prefixes, if required
        $plugin_css = $this->change_css_prefix( $plugin_css );

        // If the Customizer CSS already contains the Frontend CSS, bail
        if ( strpos( $customizer_css, $plugin_css ) ) {
            return $customizer_css;
        }

        // Append CSS
        return $customizer_css . "\n" . $plugin_css;

    }

    /**
     * Replaces CSS prefix in the content
     *
     * @since   2.0.3
     *
     * @param   string  $content    Content
     * @return  string              Content
     */
    public function maybe_change_css_prefix_content( $content ) {

        // Change CSS prefix
        $content = $this->change_css_prefix( $content );

        // Return
        return $content;

    }

    /**
     * Returns an array comprising of plugin specific Shortcodes,
     * and their attributes.
     *
     * This is used by both TinyMCE and Gutenberg, so that Shortcodes
     * and Blocks are registered.
     *
     * @since   2.0.5
     */
    public function get_shortcodes() {

        $shortcodes = array(
            /**
             * Google Map
             */
            'google-map' => array(
                // Gutenberg Block Details
                'title'     => __( 'Google Map', $this->base->plugin->name ),
                'icon'      => '',
                'category'  => 'layout',
                'keywords'  => array(
                    __( 'Google Map', $this->base->plugin->name ),
                    __( 'Google', $this->base->plugin->name ),
                    __( 'Map', $this->base->plugin->name ),
                ),

                // Attributes
                'attributes'    => array(
                    'maptype'       => array(
                        'label'         => __( 'Map Type', $this->base->plugin->name ),
                        'type'          => 'text',
                        'description'   => '',
                    ),
                    'location'       => array(
                        'label'         => __( 'Location', $this->base->plugin->name ),
                        'type'          => 'text',
                        'description'   => '',
                        'choices'       => false,
                    ),
                    'destination'   => array(
                        'label'         => __( 'Destination', $this->base->plugin->name ),
                        'type'          => 'text',
                        'description'   => '',
                        'choices'       => false,
                    ),
                    'country_code'  => array(
                        'label'         => __( 'Country Code', $this->base->plugin->name ),
                        'type'          => 'text',
                        'description'   => '',
                        'choices'       => false,
                    ),
                    'term'          => array(
                        'label'         => __( 'Term', $this->base->plugin->name ),
                        'type'          => 'text',
                        'description'   => '',
                        'choices'       => false,
                    ),
                    'height'        => array(
                        'label'         => __( 'Height (px)', $this->base->plugin->name ),
                        'type'          => 'text',
                        'description'   => '',
                        'choices'       => false,
                    ),
                    'zoom'          => array(
                        'label'         => __( 'Zoom Level', $this->base->plugin->name ),
                        'type'          => 'text',
                        'description'   => '',
                        'choices'       => false,
                    ),
                ),

                // Default Values
                'default_values' => array(
                    'maptype'       => 'roadmap',
                    'location'      => '',
                    'destination'   => '',
                    'country_code'  => '',
                    'term'          => '',
                    'height'        => 250,
                    'zoom'          => 14,
                ),

                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => true,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'google_map',
            ),

            /**
             * Media Library
             */
            'media-library' => array(
                // Gutenberg Block Details
                'title'     => __( 'Media Library', $this->base->plugin->name ),

                // Default Values
                'default_values' => array(
                    'title'         => false,
                    'caption'       => false,
                    'alt'           => false,
                    'description'   => false,
                    'operator'      => 'AND',
                    'ids'           => false,
                    'min_id'        => false,
                    'max_id'        => false,
                    'size'          => 'medium',
                    'alt_tag'       => false,
                ),

                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => false,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'media_library',
            ),

            /**
             * OpenStreetMap
             */
            'open-street-map' => array(
                // Gutenberg Block Details
                'title'     => __( 'OpenStreetMap', $this->base->plugin->name ),
                'icon'      => '',
                'category'  => 'layout',
                'keywords'  => array(
                    __( 'OpenStreetMap', $this->base->plugin->name ),
                    __( 'Open', $this->base->plugin->name ),
                    __( 'Street', $this->base->plugin->name ),
                    __( 'Map', $this->base->plugin->name ),
                ),

                // Default Values
                'default_values' => array(
                    //'maptype'       => 'roadmap',
                    'location'      => '',
                    //'destination'   => '',
                    'country_code'  => '',
                    //'term'          => '',
                    'height'        => 250,
                    'zoom'          => 14,
                ),

                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => true,
                'requires_js' => true,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'open_street_map',
            ),

            /**
             * Pexels
             */
            'pexels' => array(
                // Gutenberg Block Details
                'title'     => __( 'Pexels', $this->base->plugin->name ),

                // Default Values
                'default_values' => array(
                    // Search Params
                    'term'          => false,
                    'size'          => 'full',
                    'orientation'   => false,

                    // Output
                    'title'         => false,
                    'caption'       => false,
                    'alt_tag'       => false,
                    'description'   => false,
                    'filename'      => false,
                ),
                
                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => false,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'pexels',
            ),

            /**
             * Pixabay
             */
            'pixabay' => array(
                // Gutenberg Block Details
                'title'     => __( 'Pixabay', $this->base->plugin->name ),

                // Default Values
                'default_values' => array(
                    // Search Params
                    'term'          => false,
                    'language'      => 'en',
                    'image_type'    => 'all',
                    'orientation'   => 'all',
                    'category'      => false,
                    'color'         => false,
                    'safe_search'   => false,

                    // Output
                    'title'         => false,
                    'caption'       => false,
                    'alt_tag'       => false,
                    'description'   => false,
                    'filename'      => false,
                ),
                
                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => false,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'pixabay',
            ),

            /**
             * Related Links
             */
            'related-links' => array(
                // Gutenberg Block Details
                'title'     => __( 'Related Links', $this->base->plugin->name ),
                
                // Register when Generation is running only
                'register_on_generation_only' => false,

                // Requires CSS and/or JS for output
                'requires_css' => true,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'related_links',
            ),

            /**
             * Wikipedia
             */
            'wikipedia' => array(
                // Gutenberg Block Details
                'title'     => __( 'Wikipedia', $this->base->plugin->name ),

                // Default Values
                'default_values' => array(
                    'term'              => '',
                    'use_similar_page'  => 0,
                    'sections'          => '',
                    'paragraphs'        => 0,
                    'apply_synonyms'    => 0, 
                    'language'          => 'en',
                ),
                
                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => false,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'wikipedia',
            ),

            /**
             * Yelp
             */
            'yelp' => array(
                // Gutenberg Block Details
                'title'     => __( 'Yelp', $this->base->plugin->name ),

                // Default Values
                'default_values' => array(
                    'term'          => '',
                    'location'      => '',
                    'radius'        => 0,
                    'minimum_rating'=> 0,
                    'language'      => get_locale(),
                    'price'         => 0,
                    'limit'         => 5,
                    'sort_by'       => '',

                    'image'         => 1,
                    'rating'        => 1,
                    'categories'    => 1,
                    'phone'         => 1,
                    'address'       => 1,
                ),

                'api_keys' => array(
                    'term',
                    'location',
                    'radius',
                    'minimum_rating',
                    'language',
                    'price',
                    'limit',
                    'sort_by',
                ),
                
                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => true,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'yelp',
            ),

            /**
             * YouTube
             */
            'youtube' => array(
                // Gutenberg Block Details
                'title'     => __( 'YouTube', $this->base->plugin->name ),

                // Default Values
                'default_values' => array(
                    'term'          => '',
                    'location'      => 0,
                ),
                
                // Register when Generation is running only
                'register_on_generation_only' => true,

                // Requires CSS and/or JS for output
                'requires_css' => true,
                'requires_js' => false,

                // Function to call when rendering the shortcode on the frontend
                'render_callback' => 'youtube',
            ),
        );

        /**
         * Defines an array comprising of plugin specific Shortcodes,
         * and their attributes.
         *
         * This is used by both TinyMCE and Gutenberg, so that Shortcodes
         * and Blocks are registered.
         *
         * @since   2.0.5
         *
         * @param   array   $shortcodes     Registered Shortcodes
         */
        $shortcodes = apply_filters( 'page_generator_pro_shortcode_get_shortcodes', $shortcodes );

        // Return
        return $shortcodes;

    }

    /**
     * Helper method to fetch the specified shortcode from get_shortcodes()
     *
     * @since   2.0.6
     *
     * @param   string  $shortcode_name     Shortcode Name
     * @return  mixed                       WP_Error | array
     */
    public function get_shortcode( $shortcode_name ) {

        // Fetch all shortcodes
        $shortcodes = $this->get_shortcodes();

        // Bail if the given shortcode name has not been registered
        if ( ! is_array( $shortcodes ) || count( $shortcodes ) == 0 || ! isset( $shortcodes[ $shortcode_name ] ) ) {
            return new WP_Error( 
                'page_generator_pro_shortcode_get_shortcode', 
                sprintf(
                    __( 'The %s shortcode could not be found, as it is not registered in get_shortcodes()', 'page-generator-pro' ),
                    $shortcode_name
                )
            );
        }  

        // Return the shortcode
        return $shortcodes[ $shortcode_name ]; 

    }

    /**
     * Returns Shortcode Names requiring CSS.
     *
     * This can be used to determine if we need to load frontend CSS for a given Page.
     *
     * @since   2.3.4
     *
     * @return  array   Shortcode Names
     */
    private function get_shortcodes_requiring_css() {

        // Fetch all shortcodes
        $shortcodes = $this->get_shortcodes();

        // Bail if the given shortcode name has not been registered
        if ( ! is_array( $shortcodes ) || count( $shortcodes ) == 0 ) {
            return false;
        }  

        $shortcodes_requiring_css = array();
        foreach ( $shortcodes as $shortcode_name => $shortcode ) {
            if ( ! $shortcode['requires_css'] ) {
                continue;
            }

            $shortcodes_requiring_css[] = $shortcode_name;
        }

        // If no shortcodes, return false
        if ( ! count( $shortcodes_requiring_css ) ) {
            return false;
        }

        // Return shortcode names requiring CSS
        return $shortcodes_requiring_css;

    }

    /**
     * Returns Shortcode Names requiring JS.
     *
     * This can be used to determine if we need to load frontend JS for a given Page.
     *
     * @since   2.3.4
     *
     * @return  array   Shortcode Names
     */
    public function get_shortcodes_requiring_js() {

        // Fetch all shortcodes
        $shortcodes = $this->get_shortcodes();

        // Bail if the given shortcode name has not been registered
        if ( ! is_array( $shortcodes ) || count( $shortcodes ) == 0 ) {
            return false;
        }  

        $shortcodes_requiring_js = array();
        foreach ( $shortcodes as $shortcode_name => $shortcode ) {
            if ( ! $shortcode['requires_js'] ) {
                continue;
            }

            $shortcodes_requiring_js[] = $shortcode_name;
        }

        // If no shortcodes, return false
        if ( ! count( $shortcodes_requiring_js ) ) {
            return false;
        }

        // Return shortcode names requiring JS
        return $shortcodes_requiring_js;

    }

    /**
     * Google Maps Shortcode
     *
     * @since  1.0.0
     *
     * @param  array   $atts   Shortcode Attributes
     * @return string          Output
     */
    public function google_map( $atts ) {

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'google-map' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Google Maps Shortcode Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-google-map' );

        // Build iframe arguments and determine endpoint
        $args = false;
        switch ( $atts['maptype'] ) {

            /**
             * Street View
             */
            case 'streetview':
                // Get latitude and longitude
                $lat_lng = false;
                $result = $this->base->get_class( 'georocket' )->get_geocode( $atts['location'] . ', ' . $atts['country_code'], $this->base->licensing->get_license_key() );

                // Bail if errors occured
                if ( is_wp_error( $result ) ) {
                    if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                        return sprintf( __( 'Google Maps Shortcode Error: %s', 'page-generator-pro' ), $result->get_error_message() );
                    }

                    return '';
                }

                if ( ! $result->data ) {
                    if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                        return sprintf( __( 'Google Maps Shortcode Error: %s', 'page-generator-pro' ), 'No Data in Geocode Response' );
                    }

                    return '';
                }

                // If here, we have a latitude and longitude
                $endpoint = $atts['maptype'];
                $args = array(
                    'location' => $result->data->latitude . ',' . $result->data->longitude,
                );
                break;

            /**
             * Directions
             */
            case 'directions':
                $endpoint = $atts['maptype'];
                $args = array(
                    'origin'        => $atts['location'],
                    'destination'   => $atts['destination'],
                );
                break;

            /**
             * Road Map or Satellite
             */
            default:
                $endpoint = 'place';
                $args = array(
                    'q'         => ( ! empty( $atts['term'] ) ? $atts['term'] . ' in ' . $atts['location'] : $atts['location'] ),
                    'zoom'      => $atts['zoom'],
                    'maptype'   => $atts['maptype'],
                );
                break;

        }

        // Fetch API Key
        $google_maps_api_key = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-google', 'google_maps_api_key' );
        if ( empty( $google_maps_api_key ) ) {
            $google_maps_api_key = 'AIzaSyCNTEOso0tZG6YMSJFoaJEY5Th1stEWrJI';
        }
        $args['key'] = $google_maps_api_key;

        // Build URL
        $url = 'https://www.google.com/maps/embed/v1/' . $endpoint . '?' . http_build_query( $args );

        /**
         * Filter the Google Maps iFrame URL, before output.
         *
         * @since   2.0.4
         *
         * @param   string  $url        URL with Arguments
         * @param   array   $atts       Shortcode Attributes
         * @param   array   $args       URL Arguments
         * @param   string  $endpoint   URL Endpoint
         */
        $url = apply_filters( 'page_generator_pro_shortcode_google_maps_url', $url, $atts, $args, $endpoint );

        // Build HTML using the URL
        $html = '<iframe class="page-generator-pro-map" width="100%" height="' . $atts['height'] . '" frameborder="0" style="border:0" src="' . $url . '" allowfullscreen></iframe>';

        /**
         * Filter the Google Maps HTML output, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $html   HTML Output
         * @param   array   $atts   Shortcode Attributes
         */
        $html = apply_filters( 'page_generator_pro_shortcode_google_maps', $html, $atts );

        // Return
        return $html;

    }

    /**
     * Media Library Shortcode
     *
     * @since   1.7.9
     *
     * @param   array   $atts   Shortcode Attributes
     * @return  string          Output
     */
    public function media_library( $atts ) {

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'media-library' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Media Library Shortcode Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-media-library' );

        // Get Image HTML
        $html = $this->base->get_class( 'media_library' )->get_random_image_html_tag( $atts );

        /**
         * Filter the Media Library HTML output, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $html   HTML Output
         * @param   array   $atts   Shortcode Attributes
         */
        $html = apply_filters( 'page_generator_pro_shortcode_media_library', $html, $atts );

        // Return
        return $html;
        
    }

    /**
     * Open Street Map Shortcode
     *
     * @since  2.2.6
     *
     * @param  array   $atts   Shortcode Attributes
     * @return string          Output
     */
    public function open_street_map( $atts ) {

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'open-street-map' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Open Street Maps Shortcode Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-open-street-map' );

        // Get latitude and longitude
        $lat_lng = false;
        $result = $this->base->get_class( 'georocket' )->get_geocode( $atts['location'] . ', ' . $atts['country_code'], $this->base->licensing->get_license_key() );

        // Bail if errors occured
        if ( is_wp_error( $result ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Open Street Maps Shortcode Error: %s', 'page-generator-pro' ), $result->get_error_message() );
            }

            return '';
        }

        // Generate random ID for the map
        $map_id = md5( rand() );

        // Build HTML
        $html = '<div id="page-generator-pro-open-street-map-' . $map_id . '" class="page-generator-pro-map" style="height:' . $atts['height'] . 'px;"></div>';
        $html .= '<script type="text/javascript">
var map = L.map(\'page-generator-pro-open-street-map-' . $map_id . '\').setView([' . $result->data->latitude . ', ' . $result->data->longitude . '], ' . $atts['zoom'] . ');
L.tileLayer(\'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png\', {
    attribution: \'' . sprintf( __( 'Map data &copy; %s', 'page-generator-pro' ), '<a href="https://www.openstreetmap.org/">OpenStreetMap</a>' ) . '\'
}).addTo(map);
</script>';

        /**
         * Filter the Open Street Maps HTML output, before returning.
         *
         * @since   2.2.6
         *
         * @param   string  $html   HTML Output
         * @param   array   $atts   Shortcode Attributes
         */
        $html = apply_filters( 'page_generator_pro_shortcode_open_street_map', $html, $atts );

        // Return
        return $html;

    }

    /**
     * Pexels Shortcode
     *
     * @since   2.2.9
     *
     * @param   array   $atts   Shortcode Attributes
     * @return  string          Output
     */
    public function pexels( $atts ) {

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'pexels' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Pexels Shortcode Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-pexels' );

        // Get Post ID
        $post_id = $this->get_post_or_group_id();

        // If a Pexels API Key has been specified, use it instead of the class default.
        $api_key = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-pexels', 'api_key' );
        if ( ! empty( $api_key ) ) {
            $this->base->get_class( 'pexels' )->set_api_key( $api_key );
        }

        // Run images query
        $images = $this->base->get_class( 'pexels' )->photos_search( $atts['term'], $atts['size'], $atts['orientation'] );
        if ( is_wp_error( $images ) || ! is_array( $images ) || count( $images ) == 0 ) {
            // Couldn't fetch an image, so don't show an image at all.
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Pexels Shortcode Error: %s', 'page-generator-pro' ), $images->get_error_message() );
            }

            return '';
        }

        // Pick an image at random from the resultset
        if ( count( $images ) == 1 ) {
            $image_index = 0;
        } else {
            $image_index = rand( 0, ( count( $images ) - 1 ) );
        }   

        // Import the image
        $image_id = $this->base->get_class( 'import' )->import_remote_image( 
            $images[ $image_index ]['url'], 
            $post_id, 
            $atts['filename'],
            ( ! $atts['title'] ? $images[ $image_index ]['title'] : $atts['title'] ), // title
            ( ! $atts['caption'] ? $images[ $image_index ]['title'] : $atts['caption'] ), // caption
            ( ! $atts['alt_tag'] ? $images[ $image_index ]['title'] : $atts['alt_tag'] ), // alt_tag
            ( ! $atts['description'] ? $images[ $image_index ]['title'] : $atts['description'] ) // description
        );

        // Bail if an error occured
        if ( is_wp_error( $image_id ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Pexels Shortcode Error: %s', 'page-generator-pro' ), $image_id->get_error_message() );
            }

            return '';
        }

        // Get img tag
        $html = wp_get_attachment_image( $image_id, 'full' );

        /**
         * Filter the Pexels HTML output, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $html       HTML Output
         * @param   array   $atts       Shortcode Attributes
         * @param   int     $image_id   WordPress Media Library Image ID
         * @param   array   $images     Pexels Image Results
         */
        $html = apply_filters( 'page_generator_pro_shortcode_pexels', $html, $atts, $image_id, $images );

        // Return
        return $html;

    }

    /**
     * Pixabay Shortcode
     *
     * @since   2.2.9
     *
     * @param   array   $atts   Shortcode Attributes
     * @return  string          Output
     */
    public function pixabay( $atts ) {

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'pixabay' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Pixabay Shortcode Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-pixabay' );

        // Get Post ID
        $post_id = $this->get_post_or_group_id();

        // If a Pexels API Key has been specified, use it instead of the class default.
        $api_key = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-pixabay', 'api_key' );
        if ( ! empty( $api_key ) ) {
            $this->base->get_class( 'pixabay' )->set_api_key( $api_key );
        }

        // Run images query
        $images = $this->base->get_class( 'pixabay' )->photos_search( 
            $atts['term'],
            $atts['language'],
            $atts['image_type'],
            $atts['orientation'],
            $atts['category'],
            0,
            0,
            $atts['color'],
            $atts['safe_search']
        );

        if ( is_wp_error( $images ) || ! is_array( $images ) || count( $images ) == 0 ) {
            // Couldn't fetch an image, so don't show an image at all.
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Pixabay Shortcode Error: %s', 'page-generator-pro' ), $images->get_error_message() );
            }

            return '';
        }

        // Pick an image at random from the resultset
        if ( count( $images ) == 1 ) {
            $image_index = 0;
        } else {
            $image_index = rand( 0, ( count( $images ) - 1 ) );
        }   

        // Import the image
        $image_id = $this->base->get_class( 'import' )->import_remote_image( 
            $images[ $image_index ]['url'], 
            $post_id, 
            $atts['filename'],
            ( ! $atts['title'] ? $images[ $image_index ]['title'] : $atts['title'] ), // title
            ( ! $atts['caption'] ? $images[ $image_index ]['title'] : $atts['caption'] ), // caption
            ( ! $atts['alt_tag'] ? $images[ $image_index ]['title'] : $atts['alt_tag'] ), // alt_tag
            ( ! $atts['description'] ? $images[ $image_index ]['title'] : $atts['description'] ) // description
        );

        // Bail if an error occured
        if ( is_wp_error( $image_id ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Pixabay Shortcode Error: %s', 'page-generator-pro' ), $image_id->get_error_message() );
            }

            return '';
        }

        // Get img tag
        $html = wp_get_attachment_image( $image_id, 'full' );

        /**
         * Filter the Pexels HTML output, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $html       HTML Output
         * @param   array   $atts       Shortcode Attributes
         * @param   int     $image_id   WordPress Media Library Image ID
         * @param   array   $images     Pixabay Image Results
         */
        $html = apply_filters( 'page_generator_pro_shortcode_pixabay', $html, $atts, $image_id, $images );

        // Return
        return $html;

    }


    /**
     * Related Links Shortcode
     *
     * @since   1.5.9
     *
     * @param   array   $atts   Shortcode Attributes
     * @return  string          Output
     */
    public function related_links( $atts ) {

        global $post;

        // Bail if we're in the WordPress Admin
        if ( is_admin() ) {
            return '';
        }

        // Get the Group ID which generated this content
        $group_id = absint( get_post_meta( $post->ID, '_page_generator_pro_group', true ) );

        // Define default shortcode attributes
        $defaults = array(
            // Output and Group ID
            'group_id'                  => $group_id,
            'output_type'               => '',

            // Output Type: List of Links
            'limit'                     => -1,
            'columns'                   => 1,
            'link_title'                => '',

            // Output Type: Parent, Previous and Next
            'parent_title'              => '',
            'next_title'                => '',
            'prev_title'                => '',

            // Post
            'post_type'                 => $post->post_type,
            'post_status'               => 'publish',
            'post_parent'               => 0,
            'post_name'                 => '',

            // Author
            'author'                    => '',

            // Order
            'orderby'                   => 'rand',
            'order'                     => 'ASC',

            // Radius
            'radius'                    => '',
        );

        // Define default taxonomy shortcode attributes
        foreach ( $this->base->get_class( 'common' )->get_taxonomies() as $taxonomy ) {
            $defaults[ $taxonomy->name ] = '';
        }

        // Define default custom field shortcode attributes
        $custom_field_atts = $this->get_shortcode_atts_by_partial_key( 'custom_field_', $atts );
        if ( $custom_field_atts != false ) {
            foreach ( $custom_field_atts as $att => $value ) {
                $defaults[ $att ] = '';
            }
        }

        /**
         * Filter the Related Links Shortcode Default Attributes.
         *
         * @since   1.0.0
         *
         * @param   array       $defaults   Default Attributes
         * @param   array       $atts       Shortcode Attributes
         * @param   WP_Post     $post       WordPress Post
         */
        $defaults = apply_filters( 'page_generator_pro_shortcode_related_links_defaults', $defaults, $atts, $post );

        // Parse shortcode attributes, using defaults if required
        $atts = shortcode_atts( $defaults, $atts, $this->base->plugin->name . '-related-links' );

        // If certain attributes are empty, but require a value, set them now
        if ( empty( $atts['group_id'] ) ) {
            $atts['group_id'] = $group_id;
        }
        if ( empty( $atts['post_type'] ) ) {
            $atts['post_type'] = $post->post_type;
        }

        // Convert Group ID to an array, so we support multiple Group IDs
        $atts['group_id'] = explode( ',', $atts['group_id'] );
        foreach ( $atts['group_id'] as $index => $group_id ) {
            $atts['group_id'][ $index ] = absint( $group_id );
        }

        // Start building the WP_Query arguments
        $args = array(
            'post_type'     => $atts['post_type'],
            'post_status'   => $atts['post_status'],
            'posts_per_page'=> -1,
            'orderby'       => $atts['orderby'],
            'order'         => $atts['order'],
            'meta_query'    => array(
                array(
                    'key'       => '_page_generator_pro_group',
                    'value'     => $atts['group_id'],
                    'compare'   => 'IN',
                ),
            ),

            // For performance, just return the Post ID and don't update meta or term caches
            'fields'                => 'ids',
            'cache_results'         => false,
            'update_post_meta_cache'=> false,
            'update_post_term_cache'=> false,
        );

        // Add Parent constraint to WP_Query, if defined
        if ( $atts['post_parent'] != false ) {
            // If Post Parent is a string, fetch Post Parent ID by Slug
            if ( ! is_numeric( $atts['post_parent'] ) ) {
                $parent = get_page_by_path( $atts['post_parent'], OBJECT, $atts['post_type'] );
                if ( isset( $parent->ID ) ) {
                    $args['post_parent'] = absint( $parent->ID );
                }
            } else {
                $args['post_parent'] = absint( $atts['post_parent'] );
            }
        }

        // Add Name constraint to WP_Query, if defined
        if ( ! empty( $atts['post_name'] ) ) {
            $args['post_name__in'] = explode( ',', $atts['post_name'] );
        }

        // Add Author constraint to WP_Query, if defined
        if ( ! empty( $atts['author'] ) ) {
            $args['author'] = absint( $atts['author'] );
        }

        // Add Taxonomy Term constraints to WP_Query, if defined
        // Fetch Taxonomies for this Post Type
        $taxonomies = $this->base->get_class( 'common' )->get_post_type_taxonomies( $args['post_type'] );
        if ( is_array( $taxonomies ) && count( $taxonomies ) > 0 ) {
            $args['tax_query'] = array();
            foreach ( $taxonomies as $taxonomy ) {
                // Skip if no constraints defined for this Taxonomy
                if ( ! isset( $atts[ $taxonomy->name ] ) ) {
                    continue;
                }
                if ( empty( $atts[ $taxonomy->name ] ) ) {
                    continue;
                }

                // Build array of Terms, checking if they're numeric (i.e. all Term IDs)
                $terms = array_map( 'trim', explode( ',', $atts[ $taxonomy->name ] ) );
                $terms_are_numeric = true;
                foreach ( $terms as $term ) {
                    if ( ! is_numeric( $term ) ) {
                        $terms_are_numeric = false;
                        break;
                    }
                }

                // Add constraint
                $args['tax_query'][] = array(
                    'taxonomy'  => $taxonomy->name,
                    'field'     => ( $terms_are_numeric ? 'term_id' : 'name' ),
                    'terms'     => $terms,
                );
            }
        }

        // Add Custom Field constraints to WP_Query, if defined
        // Define default custom field shortcode attributes
        if ( $custom_field_atts != false ) {
            if ( ! isset( $args['meta_query'] ) ) {
                $args['meta_query'] = array();
            }

            foreach ( $custom_field_atts as $custom_field => $meta_value ) {
                // Remove custom_field_ prefix, so we're left with the Meta Key
                $meta_key = str_replace( 'custom_field_', '', $custom_field );

                // Add to WP_Query args
                $args['meta_query'][] = array(
                    'key'   => $meta_key,
                    'value' => $meta_value,
                );
            }
        }

        // Add Radius constraints to WP_Query, if defined
        if ( $atts['radius'] ) {
            $post_ids_within_radius = $this->base->get_class( 'geo' )->get_post_ids( 
                $post->ID,
                absint( $atts['radius'] ),
                ( ( $atts['orderby'] == 'distance' ) ? $atts['order'] : false ),
                $atts['group_id']
            );

            // Only add constraint if results were found
            if ( is_array( $post_ids_within_radius ) && count( $post_ids_within_radius ) > 0 ) {
                // Add Post IDs to fetch
                $args['post__in'] = array_keys( $post_ids_within_radius );
            }
        }

        // If the Order By is distance, use post__in as Post IDs are already sorted by distance
        if ( $args['orderby'] == 'distance' ) {
            $args['orderby'] = 'post__in';
            unset( $args['order'] ); // Must be done to ensure orderby => post__in works!
        }

        // Fetch other Pages / Posts generated in this Group based on the supplied arguments
        $posts = new WP_Query( $args );

        // If no other Pages / Posts found, bail
        if ( count( $posts->posts ) == 0 ) {
            return '';
        }

        // Depending on the output type, build HTML now
        $html = '<ul class="page-generator-pro-related-links page-generator-pro-related-links-columns-' . $atts['columns'] . '">';

        switch ( $atts['output_type'] ) {
            case 'prev_next':
                // Get current Post index from the WP_Query results
                $current_post_index = array_search( $post->ID, $posts->posts );
                
                // Add Parent Post Link
                if ( ! empty( $atts['parent_title'] ) ) {
                    $parent_post_id = wp_get_post_parent_id( $post->ID );
                    if ( $parent_post_id ) {
                        $distance = ( isset( $post_ids_within_radius[ $parent_post_id ] ) ? $post_ids_within_radius[ $parent_post_id ] : '' );
                        $html .= '<li class="parent">
                            <a href="' . get_permalink( $parent_post_id ) . '" title="' . get_the_title( $parent_post_id ) . '">' . 
                                $this->replace_post_variables( $atts['parent_title'], $parent_post_id, $distance ) . '
                            </a>
                        </li>';
                    }
                }

                // Add Previous Post Link
                if ( ! empty( $atts['prev_title'] ) && $current_post_index - 1 >= 0 ) {
                    $previous_post_id = $posts->posts[ $current_post_index - 1 ];
                    if ( $previous_post_id ) {
                        $distance = ( isset( $post_ids_within_radius[ $previous_post_id ] ) ? $post_ids_within_radius[ $previous_post_id ] : '' );
                        $html .= '<li class="prev">
                            <a href="' . get_permalink( $previous_post_id ) . '" title="' . get_the_title( $previous_post_id ) . '">' . 
                                $this->replace_post_variables( $atts['prev_title'], $previous_post_id, $distance ) . '
                            </a>
                        </li>';
                    }
                }

                // Add Next Post Link
                if ( ! empty( $atts['next_title'] ) && $current_post_index + 1 <= count( $posts->posts ) - 1 ) {
                    $next_post_id = $posts->posts[ $current_post_index + 1 ];
                    if ( $next_post_id ) {
                        $distance = ( isset( $post_ids_within_radius[ $next_post_id ] ) ? $post_ids_within_radius[ $next_post_id ] : '' );
                        $html .= '<li class="next">
                            <a href="' . get_permalink( $next_post_id ) . '" title="' . get_the_title( $next_post_id ) . '">' . 
                                $this->replace_post_variables( $atts['next_title'], $next_post_id, $distance ) . '
                            </a>
                        </li>';
                    }
                }
                break;

            default:
                foreach ( $posts->posts as $index => $post_id ) {
                    // Skip if this Post is the current Post
                    if ( $post_id == $post->ID ) {
                        continue;
                    }

                    // Get Distance, if available
                    $distance = ( isset( $post_ids_within_radius[ $post_id ] ) ? $post_ids_within_radius[ $post_id ] : '' );

                    // If a Link Title is specified, use it now
                    $title = ( ! empty( $atts['link_title'] ) ? $this->replace_post_variables( $atts['link_title'], $post_id, $distance ) : get_the_title( $post_id ) );

                    // Append link to HTML
                    $html .= '<li><a href="' . get_permalink( $post_id ) . '" title="' . get_the_title( $post_id ) . '">' . $title . '</a></li>';
                    
                    // Exit loop if a link limit exists and has been reached
                    if ( $atts['limit'] > 0 && $index + 1 == $atts['limit'] ) {
                        break;
                    }
                }
                break;
        }

        // Build link list
        $html .= '</ul>';

        /**
         * Filter the Related Links Shortcode HTML output, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $html   HTML Output
         * @param   array   $atts   Shortcode Attributes
         * @param   array   $posts  Related Posts
         * @param   WP_Post $post   WordPress Post the shortcode is used on
         */
        $html = apply_filters( 'page_generator_pro_shortcode_related_links', $html, $atts, $posts, $post );

        // Maybe change the CSS Prefix
        // We do this here as this shortcode is rendered when viewing a Page, not when generating a Page
        $html = $this->maybe_change_css_prefix_content( $html );

        // Return
        return $html;

    }

    /**
     * Replaces Post variables with the Post's data.
     *
     * @since   2.2.1
     *
     * @param   string  $text       Text
     * @param   int     $post_id    Post ID
     * @param   string  $distance   Distance
     * @return  string              Text
     */
    private function replace_post_variables( $text, $post_id, $distance = '' ) {

        // Replace %title with the Post's Title
        $text = str_replace( '%title%', get_the_title( $post_id ), $text );
        $text = str_replace( '%title', get_the_title( $post_id ), $text );

        // Replace any %custom_field_NAME with Post's Custom Field Value
        if ( preg_match_all( '/%custom_field_(.*?)%/', $text, $matches ) ) {
            foreach ( $matches[1] as $index => $custom_field ) {
                $text = str_replace( $matches[0][ $index ], get_post_meta( $post_id, $custom_field, true ), $text );
            }
        }

        // Replace %parent_title with the Post's Parent Title
        $post_parent_id = wp_get_post_parent_id( $post_id );
        $text = str_replace( '%parent_title%', get_the_title( $post_parent_id ), $text );
        $text = str_replace( '%parent_title', get_the_title( $post_parent_id ), $text );

        // Replace any %parent_custom_field_NAME with Post's Parent Custom Field Value
        if ( preg_match_all( '/%parent_custom_field_(.*?)%/', $text, $matches ) ) {
            foreach ( $matches[1] as $index => $custom_field ) {
                $text = str_replace( $matches[0][ $index ], get_post_meta( $post_parent_id, $custom_field, true ), $text );
            }
        }

        // Replace Distance
        $text = str_replace( '%distance%', round( $distance, 2 ), $text );
        $text = str_replace( '%distance', round( $distance, 2 ), $text );

        // Return
        return $text;

    }

    /**
     * Wikipedia Shortcode
     *
     * @since  1.0.0
     *
     * @param  array   $atts   Shortcode Attributes
     * @return string          Output
     */
    public function wikipedia( $atts ) {

        // Check DOMDocument installed
        if ( ! class_exists( 'DOMDocument' ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return __( 'Wikipedia Shortcode Error: PHP DOMDocument Extension is not installed. This is required to parse Wikipedia content.', 'page-generator-pro' );
            }

            return '';
        }

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'wikipedia' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Wikipedia Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-wikipedia' );
        $atts['term'] = explode( ';', $atts['term'] );
        $atts['sections'] = ( empty( $atts['sections'] ) ? false : explode( ';', $atts['sections'] ) );

        // Iterate through terms until we find a page
        $errors = array();
        foreach ( $atts['term'] as $term ) {
            // Check if a Wikipedia Page exists for the given Term and Language
            $page_exists = $this->base->get_class( 'wikipedia' )->page_exists( $term, $atts['language'] );
            
            // Collect errors
            if ( is_wp_error( $page_exists ) ) {
                if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                    $errors[] = sprintf( 
                        __( 'Term: %s: Response: %s', 'page-generator-pro' ),
                        $term,
                        $page_exists->get_error_message()
                    );
                }

                continue;
            }

            // Get paragraphs from Wikipedia Article
            $paragraphs = $this->base->get_class( 'wikipedia' )->get_page_sections( 
                $term,
                $atts['use_similar_page'], 
                $atts['sections'],
                $atts['language']
            );

            // Collect errors
            if ( is_wp_error( $paragraphs ) ) {
                if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                    $errors[] = sprintf( 
                        __( 'Term: %s: Response: %s', 'page-generator-pro' ),
                        $term,
                        $paragraphs->get_error_message()
                    );
                }

                continue;
            }

            // If here, we managed to fetch paragraphs
            // Unset errors and break the loop
            unset( $errors );
            break;
        }

        // If errors exist, bail
        if ( isset( $errors ) && count( $errors ) > 0 ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Wikipedia Shortcode Error(s):<br />%s', 'page-generator-pro' ), implode( '<br />', $errors ) );
            }

            return '';
        }

        // If a paragraph limit has been specified, apply it now
        if ( isset( $atts['paragraphs'] ) && is_numeric( $atts['paragraphs'] ) && $atts['paragraphs'] > 0 ) {
            $paragraphs = array_slice( $paragraphs, 0, absint( $atts['paragraphs'] ) );
        }

        // Convert paragraphs into HTML
        $content = '<p>' . implode( '</p><p>', $paragraphs ) . '</p>';
        
        // Apply synonyms for spintax, if enabled
        if ( $atts['apply_synonyms'] ) {
            $content = $this->base->get_class( 'spintax' )->add_spintax( $content );
            $content = $this->base->get_class( 'spintax' )->process( $content );
        }

        // Build HTML
        $html = '<div class="' . $this->base->plugin->name . '-wikipedia">' . $content . '</div>';

        /**
         * Filter the Wikipedia Shortcode HTML output, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $html   HTML Output
         * @param   array   $atts   Shortcode Attributes
         * @param   string  $build  Wikipedia Content
         */
        $html = apply_filters( 'page_generator_pro_shortcode_wikipedia', $html, $atts, $content, $paragraphs );

        // Return
        return $html;

    }

    /**
     * Yelp Shortcode
     *
     * @since 1.0.0
     *
     * @param  array   $atts   Shortcode Attributes
     * @return string          Output
     */
    public function yelp( $atts ) {

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'yelp' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Yelp Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-yelp' );

        // Copy shortcode attributes to Yelp API arguments, removing some unused keys
        $args = array();
        foreach ( $shortcode['api_keys'] as $api_key ) {
            if ( ! isset( $atts[ $api_key ] ) ) {
                continue;
            }
            if ( ! $atts[ $api_key ] ) {
                continue;
            }

            $args[ $api_key ] = $atts[ $api_key ];
        }

        // Send request to Yelp API
        $results = $this->base->get_class( 'yelp' )->businesses_search( $args );

        // Check for errors from the Yelp API
        if ( is_wp_error( $results ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'Yelp Shortcode Error: %s', 'page-generator-pro' ), $results->get_error_message() );
            }

            // Just return a blank string
            return '';
        }

        // Check if any businesses were found
        if ( ! $results ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return __( 'Yelp Shortcode Error: No businesses found', 'page-generator-pro' );
            }

            return '';
        }

        // Iterate through results, building HTML
        $html = '<div class="' . $this->base->plugin->name . '-yelp">';
        foreach ( $results as $count => $business ) {

            $html .= '
            <div class="business">
                <div class="name">' . $business->name . '</div>';

            // Display Image, if enabled
            if ( $atts['image'] == 1 ) {
                $html .= '
                    <div class="image">
                        <img src="' . $business->image_url . '" />
                    </div>';
            }

            // Display Rating, if enabled
            if ( $atts['rating'] == 1 ) {
                $html .= '
                    <div class="rating">
                        <div class="rating-stars rating-stars-' . str_replace( '.', '-', $business->rating ) . '"></div>
                        ' . $business->review_count . ' ' . ( $business->review_count == 1 ? __( 'review', 'page-generator-pro' ) : __( 'reviews', 'page-generator-pro' ) ) . '
                    </div>';
            }

            // Display Categories, if enabled
            if ( $atts['categories'] == 1 ) {
                $html .= '<div class="categories">';
                
                $total_categories = count( $business->categories );
                foreach ( $business->categories as $cCount => $category ) {
                    $html .= $category->title;
                    if ( ( $cCount + 1 ) != $total_categories ) {
                        $html .= ', ';
                    }
                }
                    
                $html .= '</div>';
            }

            // Display Phone Number, if enabled
            if ( $atts['phone'] == 1 ) {
                $html .= '<div class="phone">' . $business->phone . '</div>';
            }

            // Display Address, if enabled
            if ( $atts['address'] == 1 ) {
                $html .= '<div class="address">';
            
                // Address
                $total_address_lines = count( $business->location->display_address );
                foreach ( $business->location->display_address as $aCount => $address ) {
                    $html .= $address;
                    if ( ( $aCount + 1 ) != $total_address_lines ) {
                        $html .= ', ';
                    }
                }
                
                $html .= '</div>'; 
            }
            
            $html .= '</div>';
            
            // Check if limit reached
            if ( ( $count + 1 ) == $atts['limit'] ) {
                break;
            }
        }
        
        /**
         * Filter the Yelp Shortcode HTML output, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $html   HTML Output
         * @param   array   $atts   Shortcode Attributes
         */
        $html = apply_filters( 'page_generator_pro_shortcode_yelp', $html, $atts );

        // Add Yelp logo, if we haven't yet output it.
        // This is required to meet the display requirements below, which is why this is done after filtering
        // http://www.yelp.co.uk/developers/getting_started/display_requirements
        if ( ! $this->yelp_logo_output ) {
            $html .= '<a href="https://www.yelp.com" target="_blank"><img src="https://s3-media1.ak.yelpcdn.com/assets/2/www/img/55e2efe681ed/developers/yelp_logo_50x25.png" /></a>';
            $this->yelp_logo_output = true;
        }

        $html .= '</div>';

        // Return
        return $html;

    }

    /**
     * YouTube Shortcode
     *
     * @since   1.2.0
     *
     * @param   array   $atts   Shortcode Attributes
     * @return  string          Output
     */
    public function youtube( $atts ) {

        // Fetch shortcode
        $shortcode = $this->get_shortcode( 'youtube' );

        // Bail if an error occured
        if ( is_wp_error( $shortcode ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'YouTube Error: %s', 'page-generator-pro' ), $shortcode->get_error_message() );
            }
            return '';
        }

        // Parse shortcode attributes, defining fallback defaults if required
        $atts = shortcode_atts( $shortcode['default_values'], $atts, $this->base->plugin->name . '-youtube' );

        // If our term is a location, get its latitude and longitude now
        $lat_lng = false;
        if ( $atts['location'] ) {
            $result = $this->base->get_class( 'georocket' )->get_geocode( $atts['location'], $this->base->licensing->get_license_key() );
            
            if ( ! is_wp_error( $result ) && $result->success && $result->data != false ) {
                $lat_lng = array(
                    'latitude'  => $result->data->latitude,
                    'longitude' => $result->data->longitude,
                );
            }
        }

        // Setup YouTube instance
        $youtube = $this->base->get_class( 'youtube' );
        
        // If a YouTube Data API key has been specified, use it instead of the class default.
        $youtube_data_api_key = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-google', 'youtube_data_api_key' );
        if ( ! empty( $youtube_data_api_key ) ) {
            $youtube->api_key = $youtube_data_api_key;
        }

        // Run query
        $videos = $youtube->search( $atts['term'], $lat_lng );
        if ( is_wp_error( $videos ) || ! is_array( $videos ) || count( $videos ) == 0 ) {
            // Couldn't fetch videos from YouTube
            // If a location was specified, remove it and try again
            if ( $lat_lng != false ) {
                $videos = $youtube->search( $atts['term'], false );
            }
        }

        // Bail if an error occured
        if ( is_wp_error( $videos ) ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return sprintf( __( 'YouTube Shortcode Error: %s', 'page-generator-pro' ), $videos->get_error_message() );
            }

            return '';
        }

        // Bail if no videos found
        if ( ! is_array( $videos ) || count( $videos ) == 0 ) {
            if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
                return __( 'YouTube Shortcode Error: No videos found', 'page-generator-pro' );
            }

            return '';
        }

        // Pick a video at random from the resultset
        if ( count( $videos ) == 1 ) {
            $video_index = 0;
        } else {
            $video_index = rand( 0, ( count( $videos ) - 1 ) );
        }

        // Get video URL
        $url = $videos[ $video_index ]['url'];

        /**
         * Filter the YouTube Shortcode URL, before returning.
         *
         * @since   1.0.0
         *
         * @param   string  $url            YouTube URL
         * @param   array   $atts           Shortcode Attributes
         * @param   array   $videos         Video Results
         * @param   int     $video_index    Video Index
         */
        $url = apply_filters( 'page_generator_pro_shortcode_youtube', $url, $atts, $videos, $video_index );

        // Return
        return $url;

    }

    /**
     * Helper function to try and fetch either the Generated Post ID
     * or Group ID (if no Generated Post ID is available).
     *
     * This is then used by Shortcodes to store Attachments against
     * the given Post ID / Group ID.
     *
     * @since   1.9.7
     *
     * @return  int     Post or Group ID
     */
    private function get_post_or_group_id() {

        global $post, $page_generator_pro_group_id;

        if ( ! empty( $post ) ) {
            return $post->ID;
        }

        if ( ! empty( $page_generator_pro_group_id ) ) {
            return $page_generator_pro_group_id;
        }

        return 0;

    }

    /**
     * Returns the CSS prefix to use.
     *
     * @since   2.0.3
     *
     * @return  string  CSS Prefix
     */
    private function get_css_prefix() {

        // Get prefix
        $css_prefix = trim( $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-general', 'css_prefix' ) );
        
        // Fallback to plugin name if no prefix specified
        if ( empty( $css_prefix ) ) {
            $css_prefix = $this->base->plugin->name;
        }

        /**
         * Returns the CSS prefix to use.
         *
         * @since   2.0.3
         *
         * @param   string    $css_prefix   CSS Prefix to use
         */
        $css_prefix = apply_filters( 'page_generator_pro_shortcode_get_css_prefix', $css_prefix );

        // Return
        return $css_prefix;

    }

    /**
     * Changes the default Plugin CSS Prefix for the one specified in the Plugin Settings
     *
     * @since   2.0.3
     *
     * @param   string  $content    Content
     * @return  string              Amended Content
     */
    private function change_css_prefix( $content ) {

        // Get CSS Prefix
        $css_prefix = $this->get_css_prefix();

        // Bail if it matches the Plugin Name
        if ( $css_prefix == $this->base->plugin->name ) {
            return $content;
        }

        // Replace prefix
        $content = str_replace( $this->base->plugin->name, $css_prefix, $content );

        // Return
        return $content;

    }

    /**
     * Returns an array of shortcode attributes and values where the given key is contained
     * in the shortcode attribute key.
     *
     * @since   2.2.8
     *
     * @param   string  $key    Key to search
     * @param   array   $atts   Shortcode Attributes
     * @return  mixed           false | array
     */
    private function get_shortcode_atts_by_partial_key( $key, $atts ) {

        $found_atts = array();
        foreach ( $atts as $att => $value ) {
            if ( strpos( $att, $key ) === false ) {
                continue;
            }

            $found_atts[ $att ] = $value;
        }

        // If no attributes found, bail
        if ( count( $found_atts ) == 0 ) {
            return false;
        }

        return $found_atts;

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.1.6
     * @deprecated  1.9.8
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'shortcode';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.9.8', 'Page_Generator_Pro()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Page_Generator_Pro()->get_class( $name );

    }

}