<?php
/**
 * AJAX class
 * 
 * @package Page Generator Pro
 * @author  Tim Carr
 * @version 1.0.0
 */
class Page_Generator_Pro_AJAX {

    /**
     * Holds the base object.
     *
     * @since   1.9.8
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     *
     * @since   1.0.0
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Georocket API
        add_action( 'wp_ajax_page_generator_pro_georocket', array( $this, 'georocket' ) );

        // Keywords: Generate Locations
        add_action( 'wp_ajax_page_generator_pro_keywords_generate_locations', array( $this, 'keywords_generate_locations' ) );

        // Page Attributes: Parent
        add_action( 'wp_ajax_page_generator_pro_search_pages', array( $this, 'search_pages' ) );

        // Generate: Authors
        add_action( 'wp_ajax_page_generator_pro_search_authors', array( $this, 'search_authors' ) );

        // TinyMCE
        add_action( 'wp_ajax_page_generator_pro_tinymce_spintax_generate', array( $this, 'spintax_generate' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_google_maps_modal', array( $this, 'output_tinymce_google_maps_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_media_library_modal', array( $this, 'output_tinymce_media_library_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_open_street_map_modal', array( $this, 'output_tinymce_open_street_map_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_pexels_modal', array( $this, 'output_tinymce_pexels_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_pixabay_modal', array( $this, 'output_tinymce_pixabay_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_related_links_modal', array( $this, 'output_tinymce_related_links_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_wikipedia_modal', array( $this, 'output_tinymce_wikipedia_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_yelp_modal', array( $this, 'output_tinymce_yelp_modal' ) );
        add_action( 'wp_ajax_page_generator_pro_output_tinymce_youtube_modal', array( $this, 'output_tinymce_youtube_modal' ) );

        // Generate: Content
        add_action( 'wp_ajax_page_generator_pro_generate', array( $this, 'generate' ) ); // Backward compat < 1.6.0
        add_action( 'wp_ajax_page_generator_pro_generate_content', array( $this, 'generate_content' ) );
        add_action( 'wp_ajax_page_generator_pro_generate_content_trash_generated_content', array( $this, 'trash_generated_content' ) );
        add_action( 'wp_ajax_page_generator_pro_generate_content_delete_generated_content', array( $this, 'delete_generated_content' ) );
        add_action( 'wp_ajax_page_generator_pro_generate_content_finished', array( $this, 'finished_generated_content' ) );

        // Generate: Terms
        add_action( 'wp_ajax_page_generator_pro_generate_term', array( $this, 'generate_term' ) );
        add_action( 'wp_ajax_page_generator_pro_generate_term_delete_generated_term', array( $this, 'delete_generated_terms' ) );
        add_action( 'wp_ajax_page_generator_pro_generate_term_finished', array( $this, 'finished_generated_terms' ) );

    }

    /**
     * Calls the Georocket API, returning results that are compatible
     * with selectize.js
     *
     * @since   1.7.8
     */
    public function georocket() {

        // If any required parameters are missing, bail
        $required_params = array(
            'api_call',
            'country_code',
        );
        foreach ( $required_params as $required_param ) {
            if ( ! isset( $_POST[ $required_param ] ) ) {
                wp_send_json_error( array(
                    'message' => sprintf( __( 'The %s parameter is missing from the POST data.', 'page-generator-pro' ), $required_param ),
                    'args'    => false,
                    'request' => $_POST,
                ) );
            }
        }

        // Get sanitized data
        $api_call = sanitize_text_field( $_POST['api_call'] );

        // Define an array of all possible arguments
        $args = array(
            // Plugin License Key
            'license_key'   => $this->base->licensing->get_license_key(),

            'country_code'  => sanitize_text_field( $_POST['country_code'] ),
        );

        // If a search field and query is specified, add it now
        // e.g. city_name: birmingham
        if ( isset( $_POST['api_search_field'] ) && isset( $_POST['query'] ) ) {
            $args[ sanitize_text_field( $_POST['api_search_field'] ) ] = sanitize_text_field( $_POST['query'] );
        }

        // If region IDs are specified, add them now
        if ( isset( $_POST['region_id'] ) ) {
            $args['region_id'] = $_POST['region_id'];
        }

        // If county IDs are specified, add them now
        if ( isset( $_POST['county_id'] ) ) {
            $args['county_id'] = $_POST['county_id'];
        }

        // Depending on the search key, run API call
        switch ( $api_call ) {
            case 'get_cities':
                // API call to cities endpoint
                $terms = $this->base->get_class( 'georocket' )->get_cities( $args );
                break;

            case 'get_counties':
                // API call to counties endpoint
                $terms = $this->base->get_class( 'georocket' )->get_counties( $args );
                break;

            case 'get_regions':
                // API call to regions endpoint
                $terms = $this->base->get_class( 'georocket' )->get_regions( $args );
                break;

            default:
                // Invalid key
                wp_send_json_error( array(
                    'message' => __( 'api_call parameter is invalid, and should match one of get_cities, get_counties or get_regions', 'page-generator-pro' ),
                    'args'      => $args,
                    'request'   => $_POST,
                ) );
                break;
        }
       
        // Bail if an error occured
        if ( is_wp_error( $terms ) ) {
            wp_send_json_error( array(
                'message'   => $terms->get_error_message(),
                'args'      => $args,
                'request'   => $_POST,
            ) );
        }

        // Bail if no results were found
        if ( ! is_array( $terms->data ) || count( $terms->data ) == 0 ) {
            wp_send_json_error( array(
                'message'   => __( 'No results were found for the given criteria.', 'page-generator-pro' ),
                'args'      => $args,
                'request'   => $_POST,
            ) );
        }

        // Send results
        wp_send_json_success( array(
            'data'      => $terms->data,
            'args'      => $args,
            'request'   => $_POST,
        ) );

    }

    /**
     * Passes on the Keywords: Generate Locations form submission request
     * to the GeoRocket API
     *
     * @since   1.8.2
     */
    public function keywords_generate_locations() {

        // Check that a keyword ID or name was supplied
        if ( ( ! isset( $_POST['keyword_id'] ) || empty( $_POST['keyword_id'] ) ) && ( ! isset( $_POST['keyword'] ) || empty( $_POST['keyword'] ) ) ) {
            wp_send_json_error( __( 'Please specify a keyword.', 'page-generator-pro' ) );
        }

        // If a Keyword ID has been specified, store it now for later use
        $keyword_id = false;
        if ( isset( $_POST['keyword_id'] ) && ! empty( $_POST['keyword_id'] ) ) {
            $keyword_id = absint( $_POST['keyword_id'] );
        }

        // If no Keyword ID is specified, check that the new keyword we want to create doesn't already exist
        if ( ! $keyword_id ) {
            // Check if the keyword already exists
            $keyword_exists = $this->base->get_class( 'keywords' )->exists( $_POST['keyword'] );
            if ( $keyword_exists ) {
                wp_send_json_error( __( 'The keyword already exists.  Please specify a different keyword name.', 'page-generator-pro' ) );
            }
        }

        // Setup Georocket, and define an array of all possible arguments
        $args = array(
            // Plugin License Key
            'license_key'   => $this->base->licensing->get_license_key(),
            
            // Location and Radius
            'location'          => false,
            'radius'            => false,

            // Street Name Restraints
            'street_name'       => false,
            'street_name_not'   => false,

            // Zipcode District Restraints
            'zipcode_district'      => false,
            'zipcode_district_not'  => false,

            // City Restraints
            'population_min'    => false,
            'population_max'    => false,
            'city_id'           => false,
            'city_name'         => false,
            'city_name_not'     => false,
            
            // County Restraints
            'county_id'         => false,
            'county_name'       => false,
            'county_name_not'   => false,
            
            // Region Restraints
            'region_id'         => false,
            'region_name'       => false,
            'region_name_not'   => false,

            // Country Restraints
            'country_id'        => false,
            'country_code'      => sanitize_text_field( $_POST['country_code'] ),
            'country_name'      => false,

            // Pagination
            'per_page'          => 10000,
            'page'              => ( isset( $_POST['page'] ) ? absint( $_POST['page'] ) : false ),                      // For non-zipcode requests
            'start_id'          => ( isset( $_POST['start_id'] ) ? sanitize_text_field( $_POST['start_id'] ) : false ), // For zipcode requests
        );

        // Arguments will be either location/radius or city/county/region/country
        switch ( sanitize_text_field( $_POST['method'] ) ) {
            case 'radius':
                $args['location'] = sanitize_text_field( $_POST['location'] ) . ', ' . sanitize_text_field( $_POST['country_code'] );
                
                // Build arguments
                $keys = array(
                    'radius',
                    'population_min',
                    'population_max',
                );
                $args = $this->keywords_generate_locations_build_args( $args, $keys, $_POST );
                break;

            case 'area':
                $keys = array(
                    'population_min',
                    'population_max',

                    'city_name',
                    'city_id',

                    'county_name',
                    'county_id',

                    'region_name',
                    'region_id',
                );
                $args = $this->keywords_generate_locations_build_args( $args, $keys, $_POST );
                break;
        }

        // Define exclusions now, if they exist
        $exclusions = false;
        if ( isset( $_POST['exclusions'] ) && ! empty( $_POST['exclusions'] ) ) {
            $exclusions = explode( ',', sanitize_text_field( $_POST['exclusions'] ) );
        }

        // Make Georocket API call, depending on the level of detail required for the output
        if ( count( array_intersect( array( 'street_name' ), $_POST['output_type'] ) ) > 0 ) {
            // API call to street names endpoint
            if ( $exclusions != false ) {
                $args['street_name_not'] = $exclusions;
            }
            $result = $this->base->get_class( 'georocket' )->get_street_names( $args );
        } elseif ( count( array_intersect( array( 'zipcode_district' ), $_POST['output_type'] ) ) > 0 ) {
            // API call to zipcode district endpoint
            if ( $exclusions != false ) {
                $args['zipcode_district_not'] = $exclusions;
            }
            $result = $this->base->get_class( 'georocket' )->get_zipcode_districts( $args );
        } elseif ( count( array_intersect( array( 'zipcode', 'zipcode_latitude', 'zipcode_longitude' ), $_POST['output_type'] ) ) > 0 ) {
            // API call to zipcodes endpoint
            if ( $exclusions != false ) {
                $args['city_name_not'] = $exclusions;
            }
            $result = $this->base->get_class( 'georocket' )->get_zipcodes( $args );
        } elseif ( count( array_intersect( array( 'city_name', 'city_latitude', 'city_longitude', 'city_population', 'city_wikipedia_url', 'city_wikipedia_summary', 'city_wikipedia_summary_local' ), $_POST['output_type'] ) ) > 0 ) {
            // API call to cities endpoint
            if ( $exclusions != false ) {
                $args['city_name_not'] = $exclusions;
            }
            $result = $this->base->get_class( 'georocket' )->get_cities( $args );
        } elseif ( count( array_intersect( array( 'county_name', 'county_code', 'county_wikipedia_url', 'county_wikipedia_summary' ), $_POST['output_type'] ) ) > 0 ) {
            // API call to counties endpoint
            if ( $exclusions != false ) {
                $args['county_name_not'] = $exclusions;
            }
            $result = $this->base->get_class( 'georocket' )->get_counties( $args );
        } elseif ( count( array_intersect( array( 'region_name', 'region_code', 'region_wikipedia_url', 'region_wikipedia_summary' ), $_POST['output_type'] ) ) > 0 ) {
            // API call to regions endpoint
            if ( $exclusions != false ) {
                $args['region_name_not'] = $exclusions;
            }
            $result = $this->base->get_class( 'georocket' )->get_regions( $args );
        }

        // Bail if an error occured
        if ( is_wp_error( $result ) ) {
            wp_send_json_error( $result->get_error_message() );
        }

        // Bail if no results were found
        if ( ! is_array( $result->data ) || count( $result->data ) == 0 ) {
            wp_send_json_error( __( 'No results were found for the given criteria.', 'page-generator-pro' ) );
        }

        // Build single Keyword
        $keyword = array(
            'keyword'   => sanitize_text_field( $_POST['keyword'] ),
            'data'      => '',
            'delimiter' => ( count( $_POST['output_type'] ) > 1 ? ',' : '' ),
            'columns'   => ( count( $_POST['output_type'] ) > 1 ? implode( ',', $_POST['output_type'] ) : '' ),
        );

        // Build the keyword data based on the output type formatting
        $formatted_terms = array();
        foreach ( $result->data as $i => $term ) {
            // Define array to build output order for this term
            $formatted_terms[ $i ] = array();

            // Build array
            foreach ( $_POST['output_type'] as $output_type ) {
                if ( isset( $term->{ $output_type } ) && ! empty( $term->{ $output_type } ) ) {
                    // Depending on the output type, we might need to use quotes and convert newlines to <br />
                    switch ( $output_type ) {
                        case 'city_wikipedia_summary':
                        case 'city_wikipedia_summary_local':
                        case 'county_wikipedia_summary':
                        case 'region_wikipedia_summary':
                            $formatted_terms[ $i ][] = '\"' . trim( str_replace( "\n", '<br />', $term->{ $output_type } ) ) . '\"';
                            break;

                        default:
                            $formatted_terms[ $i ][] = $term->{ $output_type };
                            break;
                    }
                }
            }

            // Remove any empty array values, and implode into a string
            $formatted_terms[ $i ] = implode( ', ', array_filter( $formatted_terms[ $i ] ) );
        }

        // Remove duplicates
        // This should never occur, but it's a good fallback just in case
        $formatted_terms = array_values( array_unique( $formatted_terms ) );

        // Add Terms to keyword data
        $keyword['data'] = implode( "\n", $formatted_terms ); 

        // Save Keyword, returning Keyword ID or WP_Error
        $keyword_result = $this->base->get_class( 'keywords' )->save( $keyword, $keyword_id, true );

        // Bail if an error occured
        if ( is_wp_error( $keyword_result ) ) {
            wp_send_json_error( $keyword_result->get_error_message() );
        }

        // Return the Keyword ID along with the GeoRocket Links and Meta
        wp_send_json_success( array(
            'keyword_id'    => $keyword_result,
            'keyword_url'   => admin_url( 'admin.php?page=page-generator-pro-keywords&cmd=form&id=' . $keyword_result ),
            'links'         => $result->links,
            'meta'          => $result->meta,
        ) );
    }

    /**
     * Appends the given Post Data to the arguments array, based on the
     * specific Post Data Keys to add.
     *
     * If a Post Data Key doesn't exist, is empty or false, we don't add
     * it to the arguments
     *
     * @since   2.2.3
     *
     * @param   array   $args       Arguments
     * @param   array   $keys       Argument Keys to possibly add to $args
     * @param   array   $post_data  POST Data
     * @return  array               Arguments
     */
    private function keywords_generate_locations_build_args( $args, $keys, $post_data ) {

        foreach ( $keys as $key ) {
            if ( ! isset( $post_data[ $key ] ) ) {
                continue;
            }
            if ( empty( $post_data[ $key ] ) ) {
                continue;
            }
            if ( is_array( $post_data[ $key ] ) && count( $post_data[ $key ] ) == 0 ) {
                continue;
            }
            if ( ! is_array( $post_data[ $key ] ) && ! $post_data[ $key ] ) {
                continue;
            }

            if ( is_array( $post_data[ $key ] ) ) {
                if ( count( $post_data[ $key ] ) == 1 ) {
                    $args[ $key ] = sanitize_text_field( $post_data[ $key ][0] );   
                } else {
                    $args[ $key ] = $_POST[ $key ];
                }
            } else {
                $args[ $key ] = sanitize_text_field( $post_data[ $key ] ); 
            }
        }

        return $args;

    }

    /**
     * Searches for Pages, Posts or Custom Post Types for the given freeform text
     *
     * @since   2.1.8
     */
    public function search_pages() {

        // Parse args
        parse_str( sanitize_text_field( $_REQUEST['args'] ), $args );

        // Build WP_Query args
        $query = array(
            'post_status'   => 'publish',
            'post_type'     => ( isset( $args['post_type'] ) ? $args['post_type'] : 'page' ),
            's'             => sanitize_text_field( $_REQUEST['query'] ),
            'order'         => 'ASC',
            'orderby'       => 'relevance',

            // Performance
            'posts_per_page'        => 10,
            'update_post_term_cache'=> false,
            'update_post_meta_cache'=> false,
            'fields'                => 'id=>parent',
        );

        // Add Exclusion
        if ( isset( $args['exclude_tree'] ) ) {
            $query['post__not_in'] = array( absint( $args['exclude_tree'] ) );
        }

        // Get results
        $posts = new WP_Query( $query );

        // If an error occured, bail
        if ( is_wp_error( $posts ) ) {
            return wp_send_json_error( $posts->get_error_message() );
        }

        // Build array
        $posts_array = array();
        if ( ! empty( $posts->posts ) ) {
            foreach ( $posts->posts as $post ) {
                $posts_array[] = array(
                    'ID'            => $post->ID,
                    'post_title'    => ( $post->post_parent > 0 ? get_the_title( $post->post_parent ) . ': ' : '' ) . get_the_title( $post->ID ),
                );
            }
        }

        // Done
        wp_send_json_success( $posts_array );

    }

    /**
     * Searches for Authors for the given freeform text
     *
     * @since   1.8.3
     */
    public function search_authors() {

        // Get vars
        $query = sanitize_text_field( $_REQUEST['query'] );

        // Get results
        $users = new WP_User_Query( array(
            'search' => '*' . $query . '*',
        ) );

        // If an error occured, bail
        if ( is_wp_error( $users ) ) {
            return wp_send_json_error( $users->get_error_message() );
        }

        // Build array
        $users_array = array();
        $results = $users->get_results();
        if ( ! empty( $results ) ) {
            foreach ( $results as $user ) {
                $users_array[] = array(
                    'id'        => $user->ID,
                    'user_login'=> $user->user_login,
                );
            }
        }

        // Done
        wp_send_json_success( $users_array );

    }

    /**
     * Adds spintax to words in the given content.
     *
     * @since   1.7.9
     */
    public function spintax_generate() {

        // Bail if no content
        if ( ! isset( $_REQUEST['content'] ) ) {
            wp_send_json_error( __( 'No content was selected.', 'page-generator-pro' ) );
        }

        $content = trim( $_REQUEST['content'] );
        if ( empty( $content ) ) {
            wp_send_json_error( __( 'No content was selected.', 'page-generator-pro' ) );
        }

        // Add spintax to content
        $content = $this->base->get_class( 'spintax' )->add_spintax( $content, true );

        // Return success or error
        if ( is_wp_error( $content ) ) {
            wp_send_json_error( $content->get_error_message() );
        }

        // Return content
        wp_send_json_success( $content );

    }
    
    /**
     * Loads the view for the Google Maps TinyMCE modal.
     *
     * @since   1.5.2
     */
    public function output_tinymce_google_maps_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
            
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-google-maps.php' ); 
        die();

    }

    /**
     * Loads the view for the Media Library TinyMCE modal.
     *
     * @since   1.7.9
     */
    public function output_tinymce_media_library_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
        
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-media-library.php' ); 
        die();

    }

    /**
     * Loads the view for the OpenStreetMaps TinyMCE modal.
     *
     * @since   2.2.6
     */
    public function output_tinymce_open_street_map_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
            
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-open-street-map.php' ); 
        die();

    }

    /**
     * Loads the view for the Pexels TinyMCE modal.
     *
     * @since   2.2.9
     */
    public function output_tinymce_pexels_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
        
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-pexels.php' ); 
        die();

    }

    /**
     * Loads the view for the Pixabay TinyMCE modal.
     *
     * @since   2.2.9
     */
    public function output_tinymce_pixabay_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
        
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-pixabay.php' ); 
        die();

    }

    /**
     * Loads the view for the Related Links modal.
     *
     * @since   1.5.9
     */
    public function output_tinymce_related_links_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
        $taxonomies =$this->base->get_class( 'common' )->get_taxonomies();
        $comparison_operators = $this->base->get_class( 'common' )->get_comparison_operators();
        
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-related-links.php' ); 
        die();

    }

    /**
     * Loads the view for the Wikipedia TinyMCE modal.
     *
     * @since   1.5.2
     */
    public function output_tinymce_wikipedia_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
        
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-wikipedia.php' ); 
        die();

    }

    /**
     * Loads the view for the Yelp TinyMCE modal.
     *
     * @since   1.5.2
     */
    public function output_tinymce_yelp_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();

        // Get Yelp Locales
        $locales = $this->base->get_class( 'yelp' )->get_locales();

        // Get WordPress Locale
        $current_locale = get_locale();
        
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-yelp.php' ); 
        die();

    }

    /**
     * Loads the view for the YouTube TinyMCE modal.
     *
     * @since   1.5.2
     */
    public function output_tinymce_youtube_modal() {

        // Fetch keywords
        $keywords = $this->base->get_class( 'keywords' )->get_keywords_and_columns();
        
        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/tinymce-youtube.php' ); 
        die();

    }

    /**
     * Backward compat function for Page Generator Pro 1.6.0 and older.
     *
     * Passes the request to generate_content()
     *
     * @since   1.0.0
     */
    public function generate() {

        $this->generate_content();

    }

    /**
     * Generates a Page, Post or CPT
     *
     * @since   1.6.1
     */
    public function generate_content() {

        // Validate
        $group = $this->generate_validation();

        /**
         * Runs any actions before Generate Content has started.
         *
         * @since   1.9.9
         *
         * @param   int     $group_id   Group ID
         * @param   bool    $test_mode  Test Mode
         */
        do_action( 'page_generator_pro_generate_content_before', $group['group_id'], $group['test_mode'] );

        // Run
        $start  = microtime( true );
        $result = $this->base->get_class( 'generate' )->generate_content( $group['group_id'], $group['current_index'], $group['test_mode'] );
        $end    = microtime( true );

        /**
         * Runs any actions once Generate Content has finished.
         *
         * @since   1.9.3
         *
         * @param   int     $group_id   Group ID
         * @param   bool    $test_mode  Test Mode
         */
        do_action( 'page_generator_pro_generate_content_after', $group['group_id'], $group['test_mode'] );

        // Return
        $this->generate_return( $result, $start, $end );

    } 

    /**
     * Generates a Term
     *
     * @since   1.6.1
     */
    public function generate_term() {

        // Validate
        $group = $this->generate_validation();

        /**
         * Runs any actions before Generate Terms has started.
         *
         * @since   1.9.9
         *
         * @param   int     $group_id   Group ID
         * @param   bool    $test_mode  Test Mode
         */
        do_action( 'page_generator_pro_generate_terms_before', $group['group_id'], $group['test_mode'] );

        // Run
        $start  = microtime( true );
        $result = $this->base->get_class( 'generate' )->generate_term( $group['group_id'], $group['current_index'], $group['test_mode'] );
        $end    = microtime( true );

        /**
         * Runs any actions once Generate Terms has finished.
         *
         * @since   1.9.3
         *
         * @param   int     $group_id   Group ID
         * @param   bool    $test_mode  Test Mode
         */
        do_action( 'page_generator_pro_generate_terms_after', $group['group_id'], $group['test_mode'] );

        // Return
        $this->generate_return( $result, $start, $end );

    }

    /**
     * Trashes all Generated Content
     *
     * @since   1.9.1
     */
    public function trash_generated_content() {

        // Validate
        $group = $this->generate_validation();

        // Run
        $start  = microtime( true );
        $result = $this->base->get_class( 'generate' )->trash_content( $group['group_id'] );
        $end    = microtime( true );

        // Return
        $this->generate_return( $result, $start, $end );

    }

    /**
     * Deletes all Generated Content
     *
     * @since   1.8.4
     */
    public function delete_generated_content() {

        // Validate
        $group = $this->generate_validation();

        // Run
        $start  = microtime( true );
        $result = $this->base->get_class( 'generate' )->delete_content( $group['group_id'] );
        $end    = microtime( true );

        // Return
        $this->generate_return( $result, $start, $end );

    }

    /**
     * Removes the generating flag on the Group, as Generation has finished.
     *
     * @since   1.9.9
     */
    public function finished_generated_content() {

        // Validate
        $group = $this->generate_validation();

        // Run
        $start  = microtime( true );
        $result = $this->base->get_class( 'groups' )->stop_generation( $group['group_id'] );
        $end    = microtime( true );

        // Return
        $this->generate_return( $result, $start, $end );

    }

    /**
     * Deletes all Generated Terms
     *
     * @since   1.9.5
     */
    public function delete_generated_terms() {

        // Validate
        $group = $this->generate_validation();

        // Run
        $start  = microtime( true );
        $result = $this->base->get_class( 'generate' )->delete_terms( $group['group_id'] );
        $end    = microtime( true );

        // Return
        $this->generate_return( $result, $start, $end );

    }

    /**
     * Removes the generating flag on the Group, as Generation has finished.
     *
     * @since   1.9.9
     */
    public function finished_generated_terms() {

        // Validate
        $group = $this->generate_validation();

        // Run
        $start  = microtime( true );
        $result = $this->base->get_class( 'groups_terms' )->stop_generation( $group['group_id'] );
        $end    = microtime( true );

        // Return
        $this->generate_return( $result, $start, $end );

    }

    /**
     * Runs validation when AJAX calls are made to generate content or terms,
     * returning the Group ID and Current Index.
     *
     * @since   1.6.1
     *
     * @return  array   Group ID and Current Index
     */
    private function generate_validation() {

        // Sanitize inputs
        if ( ! isset( $_POST['id'] ) ) {
            wp_send_json_error( __( 'No group ID was specified!', 'page-generator-pro' ) );
            die();
        }
        
        return array(
            'group_id'      => absint( $_POST['id'] ),
            'current_index' => ( isset( $_POST['current_index'] ) ? absint( $_POST['current_index'] ) : 0 ),
            'test_mode'     => ( isset( $_POST['test_mode'] ) ? true : false ),
        );

    }

    /**
     * Returns the generation result as a JSON error or success
     *
     * @since   1.6.1
     *
     * @param   mixed   $result     WP_Error | string URL
     * @param   float   $start      Start Execution Time
     * @param   float   $end        End Execution Time
     */
    private function generate_return( $result, $start, $end ) {

        // Return error or success JSON
        if ( is_wp_error( $result ) ) {
            wp_send_json_error( $result->get_error_code() . ': ' . $result->get_error_message() );
        }

        // If here, run routine worked
        wp_send_json_success( array(
            'url'               => $result['url'],
            'keywords_terms'    => $result['keywords_terms'],
            'generated'         => $result['generated'],
            'message'           => $result['message'],

            // Performance
            'start'             => $start,
            'end'               => $end,
            'duration'          => round ( ( $end - $start ), 2 ),
            'memory_usage'      => round( memory_get_usage() / 1024 / 1024 ),
            'memory_peak_usage' => round( memory_get_peak_usage() / 1024 / 1024 ),
        ) );

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.1.6
     * @deprecated  1.9.8
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'ajax';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.9.8', 'Page_Generator_Pro()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Page_Generator_Pro()->get_class( $name );

    }

}