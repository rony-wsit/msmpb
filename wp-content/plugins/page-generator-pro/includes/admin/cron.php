<?php
/**
 * Called by WordPress CRON to Generate Content or Terms
 *
 * @since   1.9.9
 * 
 * @param   int     $group_id   Group ID
 */
function page_generator_pro_generate_cron( $group_id, $type = 'content' ) {

    // Initialise Plugin
    $plugin = Page_Generator_Pro::get_instance();

    // Get Groups or Groups Term Instance
    $groups = ( ( $type == 'term' ) ? $plugin->get_class( 'groups_terms' ) : $plugin->get_class( 'groups' ) );

	// If this Group has a request to cancel generation, silently clear the status, system and cancel
    // flags before performing further checks on whether we should generate
    if ( $groups->cancel_generation_requested( $group_id ) ) {
        $groups->stop_generation( $group_id );
    }

    // If the group is already generating, bail
    if ( $groups->is_generating( $group_id ) ) {
    	$error = new WP_Error( 
    		'page_generator_pro_generate_cron',
            sprintf(
                __( 'Group ID #%s: Generation is already running via %s', 'page-generator-pro' ),
                $group_id,
                $groups->get_system( $group_id )
            )
        );

        error_log( 'page_generator_pro_generate_cron(): Error: ' . $error->get_error_message() );
        $groups->stop_generation( $group_id );
        return;
    }

    // Get group
    $group = $groups->get_settings( $group_id );
    if ( ! $group ) {
        $error = new WP_Error(
        	'page_generator_pro_generate_cron',
            sprintf(
                __( 'Group ID #%s: Could not get settings', 'page-generator-pro' ),
                $group_id
            )
        );

        error_log( 'page_generator_pro_generate_cron(): Error: ' . $error->get_error_message() );
        $groups->stop_generation( $group_id );
        return;
    }

    // Calculate how many pages could be generated
    $number_of_pages_to_generate = $plugin->get_class( 'generate' )->get_max_number_of_pages( $group );
    if ( is_wp_error( $number_of_pages_to_generate ) ) {
        $error = new WP_Error( 
        	'page_generator_pro_generate_cron',
            sprintf(
                __( 'Group ID #%s: %s', 'page-generator-pro' ),
                $group_id,
                $result->get_error_message()
            )
        );

        error_log( 'page_generator_pro_generate_cron(): Error: ' . $error->get_error_message() );
        $groups->stop_generation( $group_id );
        return;
    }

    // Add Plugin Settings
    $group['stop_on_error'] = (int) $plugin->get_class( 'settings' )->get_setting( $plugin->plugin->name . '-generate', 'stop_on_error', '1' );

    // Set a flag to denote that this Group is generating content via the CLI
    $groups->start_generation( $group_id, 'generating', 'cron' );

    // Run a loop to generate each page
    for ( $i = $group['resumeIndex']; $i < ( $group['numberOfPosts'] + $group['resumeIndex'] ); $i++ ) {
        // If cancel generation was requested, exit now
        if ( $groups->cancel_generation_requested( $group_id ) ) {
            $groups->stop_generation( $group_id );
            $error = new WP_Error( 'page_generator_pro_generate_cron', 'Group ID #' . $group_id . ': Generation cancelled by User' );

            error_log( 'page_generator_pro_generate_cron(): Error: ' . $error->get_error_message() );
            return;
        }

        // Run
        $start  = microtime( true );
        switch ( $type ) {
            case 'term':
                $result = $plugin->get_class( 'generate' )->generate_term( $group_id, $i, false );
                break;

            default:
                $result = $plugin->get_class( 'generate' )->generate( $group_id, $i, false );
                break;    
        }
        $end    = microtime( true );

        // Bail if something went wrong
        if ( is_wp_error( $result ) ) {
            error_log( 'page_generator_pro_generate_cron(): Error: ' . $result->get_error_message() );

            // If we're stopping on error, remove generating flag and exit
            if ( $group['stop_on_error'] ) {
                $groups->stop_generation( $group_id );
                return; 
            }

            // If here, we're not stopping on error, so continue the loop
            continue;
        }

        // Output success result and performance
        $data = array(
            'url'               => $result['url'],
            'keywords_terms'    => $result['keywords_terms'],
            'start'             => $start,
            'end'               => $end,
            'duration'          => round ( ( $end - $start ), 2 ),
            'memory_usage'      => round( memory_get_usage() / 1024 / 1024 ),
            'memory_peak_usage' => round( memory_get_peak_usage() / 1024 / 1024 ),
        );
    
        // Build message and output
        $message = array(
            sprintf( 
                __( 'Group ID #%s: %s/%s: Created %s in %s seconds.  Memory Usage / Peak: %s/%sMB', 'page-generator-pro' ),
                $group_id,
                ( $i + 1 ),
                ( $group['numberOfPosts'] + $group['resumeIndex'] ),
                $data['url'],
                $data['duration'],
                $data['memory_usage'],
                $data['memory_peak_usage']
            ),
        );
        foreach ( $data['keywords_terms'] as $keyword => $term ) {
            $message[] = '{' . $keyword . '}: ' . ( strlen( $term ) > 50 ? substr( $term, 0, 50 ) . '...' : $term );
        }
        $message[] = '--';

        // Output log
        //error_log( 'page_generator_pro_generate_cron(): Success: ' . implode( "\n", $message ) );
    }

    // Stop generation
    $groups->stop_generation( $group_id ); 

}
add_action( 'page_generator_pro_generate_cron', 'page_generator_pro_generate_cron', 10, 2 );