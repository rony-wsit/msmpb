<?php
/**
 * Webanalyzer API class
 * 
 * @package Page_Generator_Pro
 * @author  Tim Carr
 * @version 1.7.1
 */
class Page_Generator_Pro_Webanalyzer_API extends Page_Generator_Pro_WPZinc_API {

    /**
     * Holds the API endpoint
     *
     * @since   1.7.1
     *
     * @var     string
     */
    public $api_endpoint = 'https://www.wpzinc.com/?webanalyzer_api=1';

    /**
     * Returns synonyms for the given keyword
     *
     * @since   1.7.1
     *
     * @param   string  $keyword    Keyword
     * @return  mixed               WP_Error | array
     */
	public function synonyms( $keyword ) {

        return $this->post( 'synonyms', array(
            'keyword' => $keyword,
        ) );

	}

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.1.6
     * @deprecated  1.9.8
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'webanalyzer';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.9.8', 'Page_Generator_Pro()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Page_Generator_Pro()->get_class( $name );

    }

}