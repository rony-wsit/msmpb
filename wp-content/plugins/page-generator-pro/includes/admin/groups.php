<?php
/**
 * Handles creating, editing, deleting and calling the generate routine
 * for the Generate Content section of the Plugin.
 * 
 * @package  Page_Generator_Pro
 * @author   Tim Carr
 * @version  2.0.2
 */
class Page_Generator_Pro_Groups {

    /**
     * Holds the base class object.
     *
     * @since   2.0.2
     *
     * @var     object
     */
    public $base;

    /**
     * Stores the current Group the settings are defined for.
     *
     * @since   2.0.2
     *
     * @var     int
     */
    public $group_id = 0;

    /**
     * Stores a Group's settings
     *
     * @since   2.0.2
     *
     * @var     array
     */
    public $settings = array();

    /**
     * Constructor.
     *
     * @since   1.2.3
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

    }

    /**
     * Creates a single Group, if none exist, when the Plugin is activated.
     *
     * @since   1.3.8
     *
     * @global  $wpdb   WordPress DB Object
     */
    public function activate() {

        // Bail if we already have at least one Group
        $number_of_groups = $this->get_count();
        if ( $number_of_groups > 0 ) {
            return;
        }

        // Create Group
        wp_insert_post( array(
            'post_type'     => $this->base->get_class( 'post_type' )->post_type_name,
            'post_status'   => 'publish',
            'post_title'    => __( 'Title' ),
            'post_content'  => __( 'Edit this content, replacing it with the content you want to generate. You can use {keywords} here too.  Need help? Visit <a href="https://www.wpzinc.com/documentation/page-generator-pro/generate/" target="_blank">https://www.wpzinc.com/documentation/page-generator-pro/generate/</a>' ),
        ) );

    }

    /**
     * Defines a default settings structure when creating a new group
     *
     * @since   1.2.0
     *
     * @return  array   Group
     */
    public function get_defaults() {

        // Get Defaults
        $defaults = array(
            'group_type'    => 'content',
            'title'         => '',
            'description'   => '',
            'permalink'     => '',
            'content'       => '',
            'excerpt'       => '',
            'latitude'      => '',
            'longitude'     => '',
            'store_keywords'=> 0,
            'meta'          => array(),
            'rotateAuthors' => 0,
            'author'        => '',
            'comments'      => 0,
            'trackbacks'    => 0,
            'type'          => 'page',
            'status'        => 'publish',
            'date_option'   => 'now',
            'date_specific' => date( 'Y-m-d' ),
            'date_min'      => date( 'Y-m-d', strtotime( '-1 week' ) ),
            'date_max'      => date( 'Y-m-d' ),
            'schedule'      => 1,
            'scheduleUnit'  => 'hours',
            'method'        => 'all',
            'apply_synonyms'=> 0,
            'overwrite'     => 0,
            'overwrite_sections' => array(
                'post_title'            => 1,
                'post_content'          => 1,
                'post_excerpt'          => 1,
                'post_status'           => 1,
                'post_author'           => 1,
                'post_date'             => 1,
                'comment_status'        => 1,
                'ping_status'           => 1,
                'custom_fields'         => 1,
                'featured_image'        => 1,
            ),
            'numberOfPosts' => 0,
            'resumeIndex'   => 0,
            'pageParent'    => '',
            'pageTemplate'  => '',
            'tax'           => '',
            'featured_image_source'                     => '', // id (media library), url, pexels, pixabay

            // Featured Image: Media Library
            'featured_image_media_library_title'        => '',
            'featured_image_media_library_caption'      => '',
            'featured_image_media_library_description'  => '',
            'featured_image_media_library_alt'          => '',
            'featured_image_media_library_operator'     => '',
            'featured_image_media_library_ids'          => '',
            'featured_image_media_library_min_id'       => '',
            'featured_image_media_library_max_id'       => '',

            // Featured Image: Third Party Source (URL, Pexels, Pixabay)
            'featured_image'                            => '', // url, term
            'featured_image_title'                      => '',
            'featured_image_alt'                        => '', // alt tag
            'featured_image_caption'                    => '',
            'featured_image_description'                => '',
            'featured_image_filename'                   => '',
        );

        /**
         * Defines the default settings structure when a new Content Group is created.
         *
         * @since   1.2.0
         *
         * @param   array   $defaults   Default Settings
         */
        $defaults = apply_filters( 'page_generator_pro_groups_get_defaults', $defaults );

        // Return.
        return $defaults;
        
    }

    /**
     * Returns a Group's Settings by the given Group ID
     *
     * @since   1.2.1
     *
     * @param   int     $id     ID
     * @return  mixed           false | array
     */
    public function get_settings( $id ) {

        // Get settings
        $settings = get_post_meta( $id, '_page_generator_pro_settings', true );

        // If the result isn't an array, we're getting settings for a new Group, so just use the defaults
        if ( ! is_array( $settings ) ) {
            $settings = $this->get_defaults();
        } else {
            // Store the Post's Title and Content in the settings, for backward compat
            $post               = get_post( $id );
            $settings['title']  = $post->post_title;
            $settings['content']= $post->post_content;

            // Merge with defaults, so keys are always set
            $settings = array_merge( $this->get_defaults(), $settings );  
        }

        // Fetch all Metadata stored against the Group ID, and add that to the settings array
        $settings['post_meta'] = $this->get_post_meta( $id );

        // Add the generated pages count and last generated index
        $settings['generated_pages_count'] = $this->get_generated_count_by_id( $id );
        $settings['last_index_generated'] = $this->get_last_index_generated( $id );
        
        // Return settings
        return $settings;

    }

    /**
     * Returns all Post Metadata for the given Group ID, excluding some specific keys.
     *
     * This ensures that Page Builder data, ACF data etc. is included in the Group
     * settings and subsequently copied to the generated Page.
     *
     * @since   1.4.4
     *
     * @param   int     $id     Group ID
     * @return  array           Metadata
     */
    private function get_post_meta( $id ) {

        // Fetch all metadata
        $meta = get_post_meta( $id );

        // Bail if no metadata was returned
        if ( empty( $meta ) ) {
            return false;
        }

        // Define the metadata to ignore
        $ignored_keys = array(
            '_edit_lock',
            '_edit_last',
            '_page_generator_pro_last_index_generated',
            '_page_generator_pro_settings',
            '_page_generator_pro_status',
            '_page_generator_pro_system',
            '_yoast_wpseo_content_score',
        );

        /**
         * Defines Post Meta Keys in a Content Group to ignore and not copy to generated Posts / Groups.
         *
         * @since   1.4.4
         *
         * @param   array   $ignored_keys   Ignored Keys
         * @param   int     $id             Group ID
         */
        $ignored_keys = apply_filters( 'page_generator_pro_groups_get_post_meta_ignored_keys', $ignored_keys, $id );

        // Iterate through the metadata, removing items we don't want
        foreach ( $meta as $meta_key => $meta_value ) {
            // Remove ignored keys
            if ( in_array( $meta_key, $ignored_keys ) ) {
                unset( $meta[ $meta_key ] );
                continue;
            }

            // Fetch the single value
            $meta[ $meta_key ] = get_post_meta( $id, $meta_key, true );

            // Depending on the key, we might need to unserialize or decode the JSON
            switch ( $meta_key ) {
                case '_elementor_data':
                    // Decode JSON
                    if ( is_string( $meta[ $meta_key ] ) && ! empty( $meta[ $meta_key ] ) ) {
                        $meta[ $meta_key ] = json_decode( $meta[ $meta_key ], true );
                    }
                    if ( empty( $meta[ $meta_key ] ) ) {
                        $meta[ $meta_key ] = array();
                    }
                    break;
            }
        }

        /**
         * Filters the Group Metadata to return.
         *
         * @since   1.4.4
         *
         * @param   array   $meta   Metadata
         * @param   int     $id     Group ID
         */
        $meta = apply_filters( 'page_generator_pro_groups_get_post_meta', $meta, $id );

        // Return filtered metadata
        return $meta;

    }

    /**
     * Returns an array of all Groups with their Settings
     *
     * @since   1.2.3
     *
     * @return  array   Groups
     */
    public function get_all() {

        // Groups
        $groups = new WP_Query( array(
            'post_type'     => $this->base->get_class( 'post_type' )->post_type_name,
            'post_status'   => array(
                'publish',
                'pending',
                'draft',
                'future',
                'private',
            ),
            'posts_per_page'=> -1,
        ) );

        if ( count( $groups->posts ) == 0 ) {
            return false;
        }

        // Build array
        $groups_arr = array();
        foreach ( $groups->posts as $group ) {
            // Get settings
            $groups_arr[ $group->ID ] = $this->get_settings( $group->ID );
        }

        /**
         * Filters the Groups to return.
         *
         * @since   1.2.3
         *
         * @param   array       $groups_arr Groups
         * @param   WP_Query    $groups     Groups Query
         */
        $groups_arr = apply_filters( 'page_generator_pro_groups_get_all', $groups_arr, $groups );

        // Return filtered results
        return $groups_arr;

    }

    /**
     * Returns an array of all Group IDs with their names
     *
     * @since   1.2.3
     *
     * @return  array   Groups
     */
    public function get_all_ids_names() {

        // Groups
        $groups = new WP_Query( array(
            'post_type'             => $this->base->get_class( 'post_type' )->post_type_name,
            'post_status'           => array( 'publish', 'draft' ),
            'posts_per_page'        => -1,
            'update_post_term_cache'=> false,
            'update_post_meta_cache'=> false,
            'fields'                => 'ids',
        ) );

        if ( count( $groups->posts ) == 0 ) {
            return false;
        }

        // Build array
        $groups_arr = array();
        foreach ( $groups->posts as $group_id ) {
            $groups_arr[ $group_id ] = get_the_title( $group_id );
        }

        /**
         * Filters the Groups to return.
         *
         * @since   1.2.3
         *
         * @param   array       $groups_arr Groups
         * @param   WP_Query    $groups     Groups Query
         */
        $groups_arr = apply_filters( 'page_generator_pro_groups_get_all_ids_names', $groups_arr, $groups );

        return $groups_arr;

    }

    /**
     * Get the number of Groups
     *
     * @since   1.3.8
     *
     * @return  int             Number of Generated Pages / Posts / CPTs
     */
    public function get_count() {

        $posts = new WP_Query( array(
            'post_type'             => $this->base->get_class( 'post_type' )->post_type_name,
            'post_status'           => 'publish',
            'posts_per_page'        => 1,
            'update_post_term_cache'=> false,
            'update_post_meta_cache'=> false,
            'fields'                => 'ids',
        ) );

        return count( $posts->posts );

    }

    /**
     * Get the number of Pages / Posts / CPTs generated by the given Group ID
     *
     * @since   1.2.3
     *
     * @param   int     $id     Group ID
     * @return  int             Number of Generated Pages / Posts / CPTs
     */
    public function get_generated_count_by_id( $id ) {

        // Fetch valid Post Statuses that can be used when generating content
        $statuses = array_keys( $this->base->get_class( 'common' )->get_post_statuses() );

        $posts = new WP_Query( array (
            'post_type'     => 'any',
            'post_status'   => $statuses,
            'posts_per_page'=> -1,
            'meta_query'    => array(
                array(
                    'key'   => '_page_generator_pro_group',
                    'value' => absint( $id ),
                ),
            ),
            'update_post_term_cache'=> false,
            'update_post_meta_cache'=> false,
            'fields'                => 'ids',
        ) );

        return count( $posts->posts );

    }

    /**
     * Runs an action on a Group
     *
     * Called by both row actions and edit actions
     *
     * @since   1.9.5
     *
     * @param   string  $action     Action
     * @param   int     $id         Group ID
     * @param   bool    $redirect   Redirct on success / error
     */
    public function run_action( $action, $id, $redirect = false ) {

        switch ( $action ) {

            /**
             * Generate
             */
            case 'generate':
                // Validate group before passing this request through
                $result = $this->validate( $id );
                if ( $result ) {
                    wp_redirect( 'admin.php?page=' . $this->base->plugin->name . '-generate&id=' . $id . '&type=content' );
                    die();
                }
                break;

            /**
             * Generate via Cron
             */
            case 'generate_cron':
                $result = $this->schedule_generation( $id );
                break;

            /**
             * Duplicate
             */
            case 'duplicate':
                $result = $this->duplicate( $id );
                break;

            /**
             * Test
             */
            case 'test':
                $result = $this->test( $id );
                break;

            /**
             * Trash Generated Content
             */
            case 'trash_generated_content':
                $result = $this->trash_generated_content( $id );
                break;

            /**
             * Delete Generated Content
             */
            case 'delete_generated_content':
                $result = $this->delete_generated_content( $id );
                break;

            /**
             * Cancel Generation
             */
            case 'cancel_generation':
                $result = $this->cancel_generation( $id );
                break;

            default:
                /**
                 * Run a custom row action on a Group.
                 *
                 * @since   1.9.5
                 *
                 * @param   mixed   $result     Result (WP_Error | bool | string)
                 * @param   string  $action     Action
                 * @param   int     $id         Group ID
                 */
                $result = false;
                $result = apply_filters( 'page_generator_pro_groups_run_row_actions', $result, $action, $id );
                break;

        }

        // If there is no result from the action, nothing happened
        if ( ! isset( $result ) || $result == false ) {
            return;
        }

        // Setup notices class, enabling persistent storage
        $this->base->get_class( 'notices' )->enable_store();
        $this->base->get_class( 'notices' )->set_key_prefix( 'page_generator_pro_' . wp_get_current_user()->ID );

        // Depending on the result of the action, store a notification and redirect
        if ( is_wp_error( $result ) ) {
            $this->base->get_class( 'notices' )->add_error_notice( $result->get_error_message() );
            
            if ( $redirect ) {
                // Redirect to the Generate Content WP_List_Table
                wp_redirect( 'edit.php?post_type=' . $this->base->get_class( 'post_type' )->post_type_name );
                die();
            }
        }

        // Build success notice, if an error didn't occur
        $message = false;
        if ( ! is_wp_error( $result ) ) {
            switch ( $action ) {

                /**
                 * Test
                 */
                case 'test':
                    $message = __( 'Test Page Generated in ' . $result['duration'] . ' seconds at ', 'page-generator-pro' ) . ': ' . '<a href="' . $result['url'] . '" target="_blank">' . $result['url'] . '</a>';
                    foreach ( $result['keywords_terms'] as $keyword => $term ) {
                        $message .= '<br />{' . $keyword . '}: ' . ( strlen( $term ) > 50 ? substr( $term, 0, 50 ) . '...' : $term );
                    }
                    break;

                default:
                    // Get message
                    $message = $this->base->get_class( 'groups_ui' )->get_message( $action . '_success' );
            
                    /**
                     * Define an optional success message based on the result of a custom row action on a Group.
                     *
                     * @since   1.9.5
                     *
                     * @param   mixed   $message    Success Message (false | string)
                     * @param   mixed   $result     Result (WP_Error | bool | string)
                     * @param   string  $action     Action
                     * @param   int     $id         Group ID
                     */
                    $message = apply_filters( 'page_generator_pro_groups_run_row_actions_success_message', $message, $result, $action, $id );
                    break;

            }
        }

        // Store success notice
        if ( $message != false ) {
            $this->base->get_class( 'notices' )->add_success_notice( $message );
        }

        // Redirect to the Generate Content WP_List_Table
        if ( $redirect ) {
            wp_redirect( 'edit.php?post_type=' . $this->base->get_class( 'post_type' )->post_type_name );
            die();
        }

    }

    /**
     * Adds or edits a record, based on the given settings array.
     *
     * @since   1.2.1
     * 
     * @param   array   $settings   Array of settings to save
     * @param   int     $id         Group ID
     */
    public function save( $settings, $group_id ) {

        // Merge with defaults, so keys are always set
        $settings = array_merge( $this->get_defaults(), $settings );

        // Clear out blank meta
        if ( isset( $settings['meta'] ) && is_array( $settings['meta'] ) && count( $settings['meta'] ) > 0 ) {
            foreach ( $settings['meta']['key'] as $index => $value ) {
                if ( empty( $value ) ) {
                    unset( $settings['meta']['key'][ $index ] );
                    unset( $settings['meta']['value'][ $index ] );
                }
            }
        }

        // Ensure some keys have a value, in case the user blanked out the values
        // This prevents errors later on when trying to generate content from a Group
        if ( empty( $settings['resumeIndex'] ) ) {
            $settings['resumeIndex'] = 0;
        }

        // Sanitize the Permalink setting
        if ( ! empty( $settings['permalink'] ) ) {
            $settings['permalink'] = preg_replace( "/[^a-z0-9-_{}\(\):]+/i", "", str_replace( ' ', '-', trim( $settings['permalink'] ) ) );
        }

        // Trim top level settings
        foreach ( $settings as $key => $value ) {
            if ( is_array( $value ) ) {
                continue;
            }

            $settings[ $key ] = trim( $value );
        }
        $settings['pageParent'] = array_map( 'trim', $settings['pageParent'] );

        // Update Post Meta
        update_post_meta( $group_id, '_page_generator_pro_settings', $settings );

        // Validate the Group, adding error notices as necessary
        $validated = $this->validate( $group_id );
        if ( is_wp_error( $validated ) ) {
            $this->base->get_class( 'notices' )->enable_store();
            $this->base->get_class( 'notices' )->set_key_prefix( 'page_generator_pro_' . wp_get_current_user()->ID );
            $this->base->get_class( 'notices' )->add_error_notice( $validated->get_error_message() );
        }

    }

    /**
     * Performs several validations on the given Group Settings, to ensure that
     * content generation will function successfully.
     *
     * @since   2.0.1
     *
     * @param   int     $id     Group ID
     * @return  mixed           WP_Error | bool
     */
    public function validate( $id ) {

        // Fetch group settings
        $settings = $this->get_settings( $id );
        if ( ! $settings ) {
            return new WP_Error( 'page_generator_pro_groups_validate', sprintf( __( 'Group ID %s does not exist!', 'page-generator-pro' ), $id ) );
        }

        // If the Permalink isn't empty, check it has at least one keyword specified
        // so that unique permalinks are produced
        if ( ! empty( $settings['permalink'] ) ) {
            preg_match_all( "|{(.+?)}|", $settings['permalink'], $matches );

            if ( ! is_array( $matches ) || count( $matches[1] ) == 0 ) {
                return new WP_Error( 
                    'page_generator_pro_groups_validate_permalink', 
                    __( 'The Permalink must either be blank or contain one or more keywords, so that a unique Permalink is produced for each generated page.  Defining a "static" Permalink will result in a single generated page, regardless of any other settings.', $this->base->plugin->name )
                );
            }
        }

        // If the Group is not published, generation might fail in Gutenberg stating that no keywords could be found
        // in the Content.
        $post_status = get_post_status( $id );
        if ( ! in_array( $post_status, array( 'publish', 'private' ) ) ) {
            return new WP_Error( 
                'page_generator_pro_groups_validate_save', 
                __( 'The Group must be saved (either Published or Private) for Generation to function correctly.', $this->base->plugin->name )
            );
        }

        $result = true;

        /**
         * Performs several validations on the given Group Settings, to ensure that
         * content generation will function successfully.
         *
         * @since   2.0.1
         *
         * @param   mixed   $rest       Validation Result (WP_Error | bool)
         * @param   array   $settings   Group Settings
         * @param   int     $id         Group ID
         */
        $result = apply_filters( 'page_generator_pro_groups_validate', $result, $settings, $id );

        // Return result
        return $result;

    }

    /**
     * Fetches the last index generated for the given Group.
     *
     * @since   2.2.6
     *
     * @param   int     $id     Group ID
     */
    public function get_last_index_generated( $id ) {

        return absint( get_post_meta( $id, '_page_generator_pro_last_index_generated', true ) );

    }

    /**
     * Stores the given index as the last generated index for the given
     * Group.
     *
     * @since   2.2.6
     *
     * @param   int     $id     Group ID
     * @param   int     $index  Last Index Generated
     */
    public function update_last_index_generated( $id, $index ) {

        update_post_meta( $id, '_page_generator_pro_last_index_generated', $index );

    }

    /**
     * Schedules content generation via WordPress' Cron
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     * @return  mixed           WP_Error | true
     */
    public function schedule_generation( $id ) {

        // Bail if the group is already scheduled
        if ( $this->is_scheduled( $id ) ) {
            return new WP_Error( 'page_generator_pro_groups_schedule_generation', sprintf( __( 'Group ID %s is already scheduled to generate content!', 'page-generator-pro' ), $id ) );
        }

        // Bail if the group is already generating content
        if ( $this->is_generating( $id ) ) {
            return new WP_Error( 'page_generator_pro_groups_schedule_generation', sprintf( __( 'Group ID %s is already generating content!', 'page-generator-pro' ), $id ) );
        }

        // Validate the group prior to scheduling
        $validated = $this->validate( $id );
        if ( is_wp_error( $validated ) ) {
            return $validated;
        }

        // If here, we're OK to schedule
        wp_schedule_single_event( time() + 10, 'page_generator_pro_generate_cron', array(
            $id,
            'content'
        ) );

        // Mark group as scheduled
        $this->start_generation( $id, 'scheduled', 'cron' );

        // Done
        return true;

    }

    /**
     * Duplicates a group
     *
     * @since   1.2.3
     *
     * @param   int     $id     ID
     * @return  mixed           WP_Error | true
     */
    public function duplicate( $id ) {

        // Fetch group settings
        $settings = $this->get_settings( $id );

        // Validate group
        $validated = $this->validate( $id );
        if ( is_wp_error( $validated ) ) {
            return $validated;
        }

        // Create new Post
        $duplicate_post_id = wp_insert_post( array(
            'post_type'     => $this->base->get_class( 'post_type' )->post_type_name,
            'post_title'    => $settings['title'] . __( ' - Copy', 'page-generator-pro' ),
            'post_content'  => $settings['content'],
            'post_status'   => get_post_status( $id ),
        ) );

        // Bail if an error occured
        if ( is_wp_error( $duplicate_post_id ) ) {
            return $duplicate_post_id;
        }

        // Update Group Settings
        $this->save( $settings, $duplicate_post_id );

        // Duplicate Group Metadata
        $this->copy_post_meta( $duplicate_post_id, $settings['post_meta'] );

        // Return result
        return true;

    }

    /**
     * Tests content for the given Group ID
     *
     * @since   1.8.0
     *
     * @param   int     $id     Group ID
     * @return  mixed           WP_Error | array
     */
    public function test( $id ) {

        // Fetch group
        $post = get_post( $id );
        if ( ! $post ) {
            return new WP_Error( sprintf( __( 'Group ID %s does not exist!', 'page-generator-pro' ), $id ) );
        }

        // Fetch group settings
        $settings = $this->get_settings( $id );

        // Validate group
        $validated = $this->validate( $id );
        if ( is_wp_error( $validated ) ) {
            return $validated;
        }

        // Run test
        $result = $this->base->get_class( 'generate' )->generate_content( $id, $settings['resumeIndex'], true );

        // Define success / error notice based on the test result
        if ( is_wp_error( $result ) ) {
            return $result;
        }

        /**
         * Runs any actions once Generate Content has finished.
         *
         * @since   1.9.3
         *
         * @param   int     $group_id   Group ID
         * @param   bool    $test_mode  Test Mode
         */
        do_action( 'page_generator_pro_generate_content_after', $id, true );

        // Return result
        return $result;
        
    }

    /**
     * Trashes Generated Content for the given Group ID
     *
     * @since   1.9.1
     *
     * @param   int     $id     Group ID
     * @return  mixed           WP_Error | array
     */
    public function trash_generated_content( $id ) {

        // Trash Generated Content now
        return $this->base->get_class( 'generate' )->trash_content( $id );
        
    }

    /**
     * Deletes Generated Content for the given Group ID
     *
     * @since   1.8.0
     *
     * @param   int     $id     Group ID
     * @return  mixed           WP_Error | array
     */
    public function delete_generated_content( $id ) {

        // Delete Generated Content now
        return $this->base->get_class( 'generate' )->delete_content( $id );
        
    }

    /**
     * Returns a flag denoting whether the given Group ID has generated content
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     * @return          bool    Has Generated Content
     */
    public function has_generated_content( $id ) {

        // Get number of generated pages
        $generated_pages_count = $this->get_generated_count_by_id( $id );

        if ( $generated_pages_count > 0 ) {
            return true;
        }

        return false;

    }

    /**
     * Returns a flag denoting whether the given Group ID is idle i.e. not generating
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     * @return  bool            Is Idle (not generating)
     */
    public function is_idle( $id ) {

        $status = $this->get_status( $id );

        if ( $status == 'idle' || empty( $status ) ) {
            return true;
        }

        return false;

    }

    /**
     * Returns a flag denoting whether the given Group ID is scheduled to generate
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     * @return  bool            Is Scheduled to Generate
     */
    public function is_scheduled( $id ) {

        $status = $this->get_status( $id );

        if ( $status == 'scheduled' ) {
            return true;
        }

        return false;

    }

    /**
     * Returns a flag denoting whether the given Group ID is generating
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     * @return  bool            Is Generating
     */
    public function is_generating( $id ) {

        $status = $this->get_status( $id );

        if ( $status == 'generating' ) {
            return true;
        }

        return false;

    }

    /**
     * Gets the status of the given Group ID (idle, scheduled, generating)
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     * @return  string          Status
     */
    public function get_status( $id ) {

        return get_post_meta( $id, '_page_generator_pro_status', true );

    }

    /**
     * Gets the given Group ID's system being used for generation
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     */ 
    public function get_system( $id ) {

        return get_post_meta( $id, '_page_generator_pro_system', true );

    }

    /**
     * Starts generation for the given Group ID by:
     * - Defining the status flag
     * - Defining the system flag
     * - Deleting the cancel flag
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     */ 
    public function start_generation( $id, $status, $system ) {

        update_post_meta( $id, '_page_generator_pro_status', $status );
        update_post_meta( $id, '_page_generator_pro_system', $system );
        delete_post_meta( $id, '_page_generator_pro_cancel' );

        return true;

    }

    /**
     * Cancels generation for the given Group ID by:
     * - Deleting the status flag
     * - Deleting the system flag
     * - Adding a cancel flag, so that if the generation process is running async, it'll stop
     * on the next iteration.
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     */ 
    public function cancel_generation( $id ) {

        // Get status and system used
        $system = $this->get_system( $id );

        // If we're using WordPress CRON, clear the scheduled hook.
        if ( $system == 'cron' ) {
            wp_clear_scheduled_hook( 'page_generator_pro_generate_cron', array(
                $id,
                'content'
            ) );
        }

        delete_post_meta( $id, '_page_generator_pro_status' );
        delete_post_meta( $id, '_page_generator_pro_system' );
        update_post_meta( $id, '_page_generator_pro_cancel', 1 );

        return true;

    }

    /**
     * Returns a flag denoting whether the given Group ID has a request to cancel generation.
     *
     * @since   1.9.9
     *
     * @param   int $id     Group ID
     * @return  bool        Stop Generation
     */
    public function cancel_generation_requested( $id ) {

        global $wpdb;

        // Read value directly from the DB, so that a cached meta value is not returned
        // This ensures that cron, cli will perform a fresh read for each generated
        // item to ensure generation is cancelled if the flag has been set through the browser
        // through the cancel command
        $result = $wpdb->get_var( " SELECT meta_value FROM " . $wpdb->postmeta . "
                                    WHERE post_id = " . absint( $id ) . "
                                    AND meta_key = '_page_generator_pro_cancel'
                                    LIMIT 1" );
        
        return (bool) $result;

    }

    /**
     * Stops generation for the given Group ID by:
     * - Deleting the status flag
     * - Deleting the system flag
     * - Deleting the cancellation flag
     *
     * @since   1.9.9
     *
     * @param   int     $id     Group ID
     */ 
    public function stop_generation( $id ) {

        // Get status and system used
        $system = $this->get_system( $id );

        // If we're using WordPress CRON, clear the scheduled hook.
        if ( $system == 'cron' ) {
            wp_clear_scheduled_hook( 'page_generator_pro_generate_cron', array(
                $id
            ) );
        }

        delete_post_meta( $id, '_page_generator_pro_status' );
        delete_post_meta( $id, '_page_generator_pro_system' );
        delete_post_meta( $id, '_page_generator_pro_cancel' );

        return true;

    }

    /**
     * Copies all metadata from the source Group ID to the destination Group ID
     *
     * @since   1.4.4
     *
     * @param   int     $destination_group_id   Destination Group ID
     * @param   array   $meta_data              Group Metadata
     */
    private function copy_post_meta( $destination_group_id, $meta_data ) {

        // Bail if no data
        if ( empty( $meta_data ) ) {
            return;
        }

        // Iterate through the metadata, storing it in the new Group
        foreach ( $meta_data as $meta_key => $meta_value ) {
            update_post_meta( $destination_group_id, $meta_key, $meta_value );
        }
        
    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.1.6
     * @deprecated  1.9.8
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'groups';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.9.8', 'Page_Generator_Pro()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Page_Generator_Pro()->get_class( $name );

    }

}