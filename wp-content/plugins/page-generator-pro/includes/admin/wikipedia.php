<?php
/**
 * Wikipedia class
 * 
 * @package Page_Generator_Pro
 * @author  Tim Carr
 * @version 2.2.7
 */
class Page_Generator_Pro_Wikipedia {

    /**
     * Holds the base object.
     *
     * @since   2.2.7
     *
     * @var     object
     */
    public $base;

    /**
     * Holds fetched Wikipedia articles in single request cycle
     *
     * @since   2.2.7
     * 
     * @var     array
     */
    private $cache = array();

    /**
     * Checks if a Page exists for the given Term and Language
     *
     * @since   2.2.7
     *
     * @param   string  $term       Term / Page
     * @param   string  $language   Language
     * @return  mixed               WP_Error | true
     */
    public function page_exists( $term, $language ) {

        // If the page exists in cache, it exists
        if ( isset( $this->cache[ $term . '-' . $language ] ) ) {
            return true;
        }

        // Query API to check if page exists on Wikipedia
        $url = 'http://' . $language . '.wikipedia.org/w/api.php?section=0&action=parse&page=' . $term . '&format=json&prop=text&redirects';
        $response = wp_remote_get( $url );

        // Bail if an error occured
        if ( is_wp_error( $response ) ) {
            return $response;
        }

        // Bail if HTTP response code isn't valid
        if ( $response['response']['code'] != 200 ) {
            return new WP_Error( 'page_generator_pro_wikipedia_page_exists_http_error', $response['response']['code'] );
        }

        // Check response
        $body = wp_remote_retrieve_body( $response );
        $check = json_decode( $body );

        // Bail if an error was received from Wikipedia
        if ( isset( $check->error ) ) {
            return new WP_Error( 'page_generator_pro_wikipedia_page_exists_error', $check->error->code . ': ' . $check->error->info );
        }

        return true;

    }

    /**
     * Checks if a Page exists for the given Term and Language
     *
     * @since   2.2.7
     *
     * @param   string  $term                   Term / Page
     * @param   bool    $use_similar_page       Use a similar Page if the Term's Page cannot be found
     * @param   array   $sections               Section(s) to fetch
     * @param   string  $language               Language
     * @return  mixed                           WP_Error | string
     */
    public function get_page_sections( $term, $use_similar_page = false, $sections = false, $language = 'en' ) {

        // If the page doesn't exist in cache, fetch it now
        if ( ! isset( $this->cache[ $term . '-' . $language ] ) ) {
            // Get entire Wikipedia Page
            $page = $this->get_page( $term, $language );
            if ( is_wp_error( $page ) ) {
                return $page;
            }

            // If the term is ambiguous, and could refer to one of several articles on Wikipedia, either fetch
            // that article or bail, depending on the $use_similar_page setting
            if ( $this->is_disambiguation_page( $page ) ) {
                if ( ! $use_similar_page ) {
                    return new WP_Error( 
                        'page_generator_pro_get_page_sections_ambiguous_term', 
                        sprintf(
                            __( 'The Term "%s" is ambiguous and could relate to one of several articles available on Wikipedia.  To use one of these similar articles, set use_similar_page=1 in your shortcode.', 'page-generator-pro' ),
                            $term
                        )
                    );
                }

                // Get similar term that has a full Wikipedia Page
                $term = $this->get_similar_term( $page );

                // Get entire similar Page
                $page = $this->get_page( $term, $language );
                if ( is_wp_error( $page ) ) {
                    return $page;
                }
            }

            // Get content
            $content = $this->get_content( $page );
            if ( is_wp_error( $content ) ) {
                return $content;
            }

            // Get table of contents headings and their keys
            $headings = $this->get_headings( $page );
            if ( is_wp_error( $headings ) ) {
                return $headings;
            }
            $headings_keys = array_keys( $headings );

            // Store in cache
            $this->cache[ $term . '-' . $language ] = array(
                'content'       => $content,
                'headings'      => $headings,
                'headings_keys' => $headings_keys,
            );

            // Cleanup unused vars
            unset( $content, $headings, $headings_keys );
        }

        // If no sections are specified, return the summary
        if ( ! $sections || is_array( $sections ) && count( $sections ) == 0 ) {
            return $this->get_paragraphs( $this->cache[ $term . '-' . $language ]['content'], $term, false, 'toc' );
        }

        // Iterate through each section, fetching paragraphs
        $paragraphs = array();
        foreach ( $sections as $section ) {
            unset( $result );

            switch ( $section ) {
                case 'summary':
                    $result = $this->get_paragraphs( $this->cache[ $term . '-' . $language ]['content'], $term, false, 'toc' );
                    break;

                default:
                    // Get index of this section from the array of headings
                    $index = $this->get_heading_index( $this->cache[ $term . '-' . $language ]['headings'], $section );

                    // If no index could be found, skip this section
                    if ( $index === false ) {
                        break;
                    }

                    // Based on the index of this heading, define the start and end heading keys (IDs)
                    $start_heading = $this->cache[ $term . '-' . $language ]['headings_keys'][ $index ];
                    $end_heading = $this->cache[ $term . '-' . $language ]['headings_keys'][ $index + 1 ];

                    // Extract paragraphs
                    $result = $this->get_paragraphs( $this->cache[ $term . '-' . $language ]['content'], $term, $start_heading, $end_heading );
                    break;
            }

            // Skip if no content found
            if ( ! isset( $result ) || count( $result ) == 0 ) {
                continue;
            }

            // Add the results (paragraphs) to the main array
            $paragraphs = array_merge( $paragraphs, $result );
        }

        // If no paragraphs found, bail
        if ( count( $paragraphs ) == 0 ) {
            return new WP_Error( 
                'page_generator_pro_wikipedia_get_page_sections_no_paragraphs_found',
                sprintf( 
                    __( 'No paragraphs could be found in the sections %s', 'page-generator-pro' ),
                    implode( ', ', $sections )
                )
            );
        }

        // Return paragraphs
        return $paragraphs;

    }

    /**
     * Returns a DOMDocument representing a Wikipedia Article for the given Term
     *
     * @since   2.2.7
     *
     * @param   string  $term       Term
     * @param   string  $language   Language
     * @return  mixed               WP_Error | DOMDocument (Wikipedia Page)
     */
    private function get_page( $term, $language = 'en' ) {

        // Get data from Wikipedia
        $url = 'http://' . $language . '.wikipedia.org/wiki/' . str_replace(' ', '_', $term );
        $response = wp_remote_get( $url );

        // Bail if an error occured
        if ( is_wp_error( $response ) ) {
            return $response;
        }

        // Bail if HTTP response code isn't valid
        if ( $response['response']['code'] != 200 ) {
            return new WP_Error( 'page_generator_pro_wikipedia_get_page_http_error', $response['response']['code'] );
        }

        // Get body
        $body = wp_remote_retrieve_body( $response );
        if ( empty( $body ) ) {
            return new WP_Error( 'page_generator_pro_wikipedia_get_page_empty', __( 'Wikipedia Shortcode Error: No Wikipedia content found', 'page-generator-pro' ) );
        }

        // Parse into DOMDocument
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false; 

        // Convert encoding to UTF-8 if php-mbstring is installed
        if ( function_exists( 'mb_convert_encoding' ) ) {
            @$dom->loadHTML( mb_convert_encoding( $body, 'HTML-ENTITIES', 'UTF-8' ) );
        } else {
            // Cannot guarantee this works as mb_convert_encoding is not available
            @$dom->loadHTML( $body );
        }

        return $dom;

    }

    /**
     * Flag to denote if the page is a disambiguation page, meaning that the term given
     * is too ambiguous to determine which article to fetch
     *
     * @since   2.2.8
     *
     * @param   DOMDocument     $dom    Wikipedia Page DOM
     * @return  bool                    Is Disambiguation Page
     */
    private function is_disambiguation_page( $dom ) {

        // Check if a disambiguation element exists
        $disambigbox = $dom->getElementById( 'disambigbox' );
        $disambig = $dom->getElementById( 'disambig' );
        if ( is_null( $disambigbox ) && is_null( $disambig ) ) {
            return false;
        }

        return true;

    }

    /**
     * Returns the first similar term from the list of "may refer to" links
     * where the given DOM represents a Wikipedia Disambiguation Page
     *
     * @since   2.2.8
     *
     * @param   DOMDocument     $dom        Wikipedia Page DOM
     * @param   string          $language   Language
     * @return  mixed                       WP_Error | string
     */
    private function get_similar_term( $dom, $language = 'en' ) {

        // Sanity check that a disambiguation element exists
        if ( ! $this->is_disambiguation_page( $dom ) ) {
            return new WP_Error( 
                'page_generator_pro_wikipedia_get_similar_page_not_disambiguation', 
                __( 'The given Page is not a disambiguation page, therefore no similar page can be fetched.', 'page-generator-pro' )
            );
        }

        // Get content
        $content = $this->get_content( $dom );
        if ( is_wp_error( $content ) ) {
            return $content;
        }

        // Get terms listed in the 'may refer to' part
        $links = $this->get_similar_page_terms( $content );
        if ( is_wp_error( $links ) ) {
            return $links;
        }

        // Return first link's term as a DOMDocument
        return $links[0];

    }

    /**
     * Returns an array of all Terms found in the Wikipedia article's
     * 'may refer to' links.
     *
     * @since   2.2.8
     *
     * @param   DOMElement  $content    Wikipedia Page Content
     * @return  mixed                   WP_Error | array
     */
    private function get_similar_page_terms( $content ) {

        // Fetch list items in content
        $similar_pages = $content->getElementsByTagName( 'li' );

        // Bail if no table of contents could be found
        if ( ! $similar_pages->length ) {
            return new WP_Error( 
                'page_generator_pro_wikipedia_get_similar_page_similar_pages_empty', 
                __( 'There are no similar page links on this disambiguation page.', 'page-generator-pro' )
            );
        }

        // Extract link names and anchors
        $terms = array();
        foreach ( $similar_pages as $similar_page ) {
            foreach ( $similar_page->childNodes as $link ) {
                // Skip if not a link
                if ( $link->nodeName != 'a' ) {
                    continue;
                }

                // Skip if the class name contains 'new' - there's no published article available
                if ( strpos( $link->getAttribute( 'class' ), 'new' ) !== false ) {
                    continue;
                }

                $terms[] = $link->nodeValue;
            }
        }       

        // Bail if no links found
        if ( count( $terms ) == 0 ) {
            return new WP_Error( 
                'page_generator_pro_wikipedia_get_similar_page_similar_pages_empty', 
                __( 'There are no similar page terms on this disambiguation page.', 'page-generator-pro' )
            );
        }     

        // Return terms
        return $terms;

    }

    /**
     * Returns the main content of the Wikipedia article
     *
     * @since   2.2.7
     *
     * @param   DOMDocument     $dom    Wikipedia Page DOM
     * @return  mixed                   WP_Error | DOMElement (Article Content)
     */
    private function get_content( $dom ) {

        // Get content
        $content = $dom->getElementById( 'mw-content-text' );

        // Bail if the table of contents element could not be found
        if ( is_null( $content ) ) {
            return new WP_Error( 'page_generator_pro_wikipedia_get_headings_content_element_not_found', __( 'No Content element could be found.', 'page-generator-pro' ) );
        }

        // Iterate through content until we find the .mw-parser-output element
        foreach ( $content->childNodes as $node ) {
            if ( $node->getAttribute( 'class' ) == 'mw-parser-output' ) {
                return $node; 
            }
        }

        // If here, we couldn't find the .mw-parser-output element
        return new WP_Error( 'page_generator_pro_wikipedia_get_content_mw_parser_output_class_missing', __( 'The mw-parser-output CSS class could not be found on the Wikipedia Page', 'page-generator-pro' ) );

    }

    /**
     * Returns an array of all headings found in the Wikipedia article's
     * Table of Contents
     *
     * @since   2.2.7
     *
     * @param   DOMDocument     $dom    Wikipedia DOM
     * @return  mixed                   WP_Error | array
     */
    private function get_headings( $dom ) {

        // Get table of contents
        $toc_element = $dom->getElementById( 'toc' );

        // Bail if the table of contents element could not be found
        if ( is_null( $toc_element ) ) {
            return new WP_Error( 'page_generator_pro_wikipedia_get_headings_no_toc', __( 'No Table of Contents could be found.', 'page-generator-pro' ) );
        }

        // Get table of contents
        $table_of_contents = $toc_element->getElementsByTagName( 'li' );

        // Bail if no table of contents could be found
        if ( ! $table_of_contents->length ) {
            return new WP_Error( 'page_generator_pro_wikipedia_get_table_of_contents_empty', __( 'No table of contents could be found.', 'page-generator-pro' ) );
        }

        // Extract heading names and anchors
        $headings = array();
        foreach ( $table_of_contents as $heading ) {
            // Skip if this is not a top level heading
            if ( strpos( $heading->getAttribute( 'class' ), 'toclevel-1' ) === false ) {
                continue;
            }

            foreach ( $heading->childNodes as $link ) {
                // Skip if not a link
                if ( $link->nodeName != 'a' ) {
                    continue;
                }

                // Get heading text parts, so we just get the text, not the number
                $heading_parts = $link->getElementsByTagName( 'span' );

                // If no heading parts found, just use the node as the text
                if ( ! $heading_parts->length ) {
                    $headings[ str_replace( '#', '', $link->getAttribute( 'href' ) ) ] = $link->nodeValue;
                    continue;
                }

                // Iterate through heading parts
                foreach ( $heading_parts as $heading_part ) {
                    $class = $heading_part->getAttribute( 'class' );
                    if ( $class != 'toctext' ) {
                        continue;
                    }

                    // We found the heading text
                    $headings[ str_replace( '#', '', $link->getAttribute( 'href' ) ) ] = $heading_part->nodeValue;
                    continue;
                }
            }
        }            

        // Return headings
        return $headings;

    }

    /**
     * Searches both keys and values for the given array of headings to find a heading
     *
     * @since   2.2.7
     *
     * @param   array   $headings   Headings
     * @param   string  $search     Heading to search for
     * @return  mixed               false | index
     */
    private function get_heading_index( $headings, $search ) {

        $search = strtolower( $search );

        $i = 0;
        foreach ( $headings as $heading => $label ) {
            if ( strtolower( $heading ) == $search ) {
                return $i;
            }

            if ( strtolower( $label ) == $search ) {
                return $i;
            }

            $i++;
        }

        return false;

    }

    /**
     * Returns an array of all paragraphs between the given start and end element
     *
     * @since   2.2.7
     *
     * @param   DOMElement  $content        Article Content Node
     * @param   string      $term           Term
     * @param   mixed       $start_element  false | string
     * @param   mixed       $end_element    false | string
     * @return  array                       Paragraphs
     */
    private function get_paragraphs( $content, $term, $start_element = false, $end_element = false ) {

        // Define array to store paragraphs in
        $paragraphs = array();

        // Flag to denote whether we should start collecting paragraphs
        $collect_paragraphs = ( ! $start_element ? true : false );

        foreach ( $content->childNodes as $node ) {
            if ( ! $node instanceof DOMElement ) {
                continue;
            }

            // Start collecting paragraphs if we've not yet started and this element matches our start element selector
            if ( $start_element != false && $this->is_element( $node, $start_element ) ) {
                $collect_paragraphs = true;
            }

            // Stop collecting paragraphs if we've reached the end element
            if ( $end_element != false && $this->is_element( $node, $end_element ) ) {
                $collect_paragraphs = false;
                break;
            }

            // Skip if we're not yet collecting paragraphs
            if ( ! $collect_paragraphs ) {
                continue;
            }

            // Skip if not a paragraph
            if ( $node->tagName != 'p' ) {
                continue;
            }

            // Get text
            $text = trim( $node->nodeValue );

            // Skip if empty
            if ( empty( $text ) ) {
                continue;
            }

            // Skip if this entire paragraph's nodeValue matches the keyword
            if ( strpos( $term, $text ) !== false ) {
                continue;
            }

            // Skip if the paragraph starts with certain characters
            if ( strpos( $text, '[[' ) !== false ) {
                continue;
            }

            // Remove footnotes
            $text = preg_replace( "/\[[a-z0-9]+\]/", null, $text );

            // Add paragraph to array
            $paragraphs[] = $text;
        }

        return $paragraphs;

    }

    /**
     * Recursively iterates through the node to see if it, or any descendents,
     * have an ID or class attribute matching the given search
     *
     * @since   2.2.7
     */
    private function is_element( $node, $search ) {

        // Return true if the element's ID matches our search term
        if ( $node->getAttribute( 'id' ) == $search ) {
            return true;
        }

        // Return true if a class name matches our search term
        $classes = explode( ' ', $node->getAttribute( 'class' ) );
        if ( in_array( $search, $classes ) ) {
            return true;
        }

        // If children exist, iterate them now
        if ( $node->childNodes ) {
            foreach( $node->childNodes as $child_node ) {
                if ( ! $child_node instanceof DOMElement ) {
                    continue;
                }

                if ( $this->is_element( $child_node, $search ) ) {
                    return true;
                }
            }
        }

        return false;

    }

}