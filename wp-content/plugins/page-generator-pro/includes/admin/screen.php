<?php
/**
 * Determines which Plugin Screen the User is on
 * 
 * @package Page Generator Pro
 * @author  Tim Carr
 * @version 2.2.4
 */
class Page_Generator_Pro_Screen {

    /**
     * Holds the base object.
     *
     * @since   2.2.4
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   2.2.4
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

    }

    /**
     * Returns an array comprising of a simplified screen and section that we are viewing
     * within the WordPress Administration interface.
     *
     * Non-Plugin screens will be returned if we need to hook into them.
     *
     * For example:
     * [
     *  'screen' => 'keywords',
     *  'section' => 'generate_locations',
     * ]
     *
     * Returns false if we're not on a screen that the Plugin needs to interact with.
     *
     * @since   2.2.4
     *
     * @return  array   Screen and Section (if false, we're not on this Plugin's screens)
     */
    public function get_current_screen() {

        // Assume we're not on a plugin screen
        $result = array(
            'screen'  => false,
            'section' => false,
        );

        // Bail if we can't determine this
        if ( ! function_exists( 'get_current_screen' ) ) {
            return $result;
        }

        // Get screen
        $screen = get_current_screen();

        // Get screen ID without Plugin Display Name, which can be edited by whitelabelling
        $screen_id = str_replace( array(
            'toplevel_page_', // licensing = page-generator-pro
            sanitize_title( $this->base->plugin->displayName ) . '_page_',
        ), '',  $screen->id ); // was $screen->id

        switch ( $screen_id ) {

            /**
             * Settings
             */
            case $this->base->plugin->name . '-settings':
                $result = array(
                    'screen'    => 'settings',
                    'section'   => ( isset( $_REQUEST['tab'] ) ? str_replace( $this->base->plugin->name . '-', '', sanitize_text_field( $_REQUEST['tab'] ) ) : 'general' ),
                );
                break;

            /**
             * Keywords
             */
            case $this->base->plugin->name . '-keywords':
                $cmd = ( isset( $_REQUEST['cmd'] ) ? sanitize_text_field( $_REQUEST['cmd'] ) : false );
                switch ( $cmd ) {
                    // Keywords WP_List_Table
                    case false:
                        $result = array(
                            'screen'    => 'keywords',
                            'section'   => 'wp_list_table',
                        );
                        break;

                    // Add/Edit
                    case 'form':
                        $result = array(
                            'screen'    => 'keywords',
                            'section'   => 'edit',
                        );
                        break;

                    // Import CSV
                    case 'form-import-csv':
                        $result = array(
                            'screen'    => 'keywords',
                            'section'   => 'import_csv',
                        );
                        break;

                    // Generate Locations
                    case 'form-locations':
                        $result = array(
                            'screen'    => 'keywords',
                            'section'   => 'generate_locations',
                        );
                        break;

                    // Generate Phone Area Codes
                    case 'form-phone':
                        $result = array(
                            'screen'    => 'keywords',
                            'section'   => 'generate_phone_area_codes',
                        );
                        break;
                }
                break;

            /**
             * Content: Groups: Table
             */
            case 'edit-' . $this->base->plugin->name:
                switch ( $screen->action ) {
                    // WP_List_Table
                    case '':
                        $result = array(
                            'screen'    => 'content_groups',
                            'section'   => 'wp_list_table',
                        );
                        break;
                }
                break;

            /**
             * Licensing
             * Content: Groups: Add/Edit
             */
            case $this->base->plugin->name:
            case 'page-generator-pro':
                switch ( $screen->base ) {
                    case 'toplevel_page_' . $this->base->plugin->name:
                        $result = array(
                            'screen'    => 'licensing',
                            'section'   => 'licensing',
                        );
                        break;

                    case 'post':
                        $result = array(
                            'screen'    => 'content_groups',
                            'section'   => 'edit',
                        );
                        break;
                }
                break;

            /**
             * Content: Terms: Add/Edit
             */
            case 'edit-page-generator-tax':
                switch ( $screen->base ) {
                    // WP_List_Table
                    case 'edit-tags':
                        $result = array(
                            'screen'    => 'content_terms',
                            'section'   => 'wp_list_table',
                        );
                        break;

                    // Edit
                    case 'term':
                        $result = array(
                            'screen'    => 'content_terms',
                            'section'   => 'edit',
                        );
                        break;
                }
                break;

            /**
             * Content: Generate
             */
            case $this->base->plugin->name . '-generate':
                $result = array(
                    'screen'    => 'generate',
                    'section'   => 'generate',
                );
                break;

            /**
             * Posts, Pages
             */
            case 'edit-post':
            case 'edit-page':
                $result = array(
                    'screen'    => 'post',
                    'section'   => 'wp_list_table',
                );
                break;

            case 'post':
            case 'page':
                $result = array(
                    'screen'    => 'post',
                    'section'   => 'edit',
                );
                break;

            /**
             * Appearance > Customize
             */
            case 'customize':
                $result = array(
                    'screen'    => 'appearance',
                    'section'   => 'customize',
                );
                break;

            /**
             * Settings > Reading
             */
            case 'options-reading':
                $result = array(
                    'screen'    => 'options',
                    'section'   => 'reading',
                );
                break;

        }

        // If here, we couldn't determine the screen
        return $result;

    }

}