<?php
/**
 * Gutenberg class. Registers shortcodes as blocks.
 * 
 * @package Page_Generator_Pro
 * @author  Tim Carr
 * @version 2.0.5
 */
class Page_Generator_Pro_Gutenberg {

    /**
     * Holds the base object.
     *
     * @since   2.0.5
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   2.0.5
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Register Gutenberg Blocks
        add_action( 'init', array( $this, 'add_blocks' ) );

    }

    /**
     * Registers Plugin Shortcodes as Blocks, so that they can be used in the Gutenberg Editor
     *
     * @since   2.0.5
     */
    public function add_blocks() {

        // Bail if Gutenberg isn't available
        if ( ! function_exists( 'register_block_type' ) ) {
            return;
        }

        // Get shortcodes
        $shortcodes = $this->base->get_class( 'shortcode' )->get_shortcodes();

        // Bail if no shortcodes are available
        if ( ! is_array( $shortcodes ) || count( $shortcodes ) == 0 ) {
            return;
        }

        // Iterate through shortcodes, registering them
        foreach ( $shortcodes as $shortcode => $properties ) {
            register_block_type( $this->base->plugin->name . '/' . $shortcode, array(
                'editor_script' => $this->base->plugin->name . '-gutenberg',
            ) );
        }

        /**
         * Add any blocks here, using register_block_type(), that should appear in the Gutenberg Editor.
         *
         * Shortcodes registered through the `page_generator_pro_shortcode_get_shortcodes` filter will
         * have already been registered above, so they do not need to be manually registered here.
         *
         * @since   2.0.5
         *
         * @param   array    $shortcodes   Shortcodes already registered as Gutenberg Blocks
         */
        do_action( 'page_generator_pro_gutenberg_add_blocks', $shortcodes );

    }

}