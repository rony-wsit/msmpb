<?php
/**
 * Access Control class
 * 
 * @package Page Generator Pro
 * @author  Tim Carr
 * @version 2.1.7
 */
class Page_Generator_Pro_Access {

    /**
     * Holds the base object.
     *
     * @since   2.1.7
     *
     * @var     object
     */
    public $base;

    /**
     * Holds an array of permitted users, if any
     *
     * Populated by the plugin-name__PERMITTED_USERS constant.
     *
     * @since   2.1.7
     *
     * @var     mixed   false | array
     */
    private $permitted_users = false;

    /**
     * Holds the current logged in User
     *
     * @since   2.1.7
     *
     * @var     mixed   false | WP_User
     */
    private $current_user = false;

    /**
     * Constructor
     * 
     * @since   2.1.7
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

    }

    /**
     * Determines whether the logged in WordPress User has access to a particular
     * feature, by:
     * - checking if the license key has access control options,
     * - the feature is defined in the wp-config file,
     * - the value in the wp-config file permits or denies access
     *
     * This function assumes access until a condition revokes it.
     *
     * @since   2.1.7
     *
     * @param   string  $feature    Feature the user is attempting to access
     * @return  bool                User can access feature
     */
    public function can_access( $feature ) {

        // If access control isn't available, permit access
        if ( ! $this->base->licensing->has_feature( 'access_control' ) ) {
            return true;
        }

        // If the logged in user is permitted, permit access
        if ( $this->is_logged_in_user_permitted() ) {
            return true;
        }

        // Define constant name
        $constant_name = strtoupper( $this->base->plugin->name ) . '_' . strtoupper( $feature );

        // If the feature isn't defined as a constant, permit access
        if ( ! defined( $constant_name ) ) {
            return true;
        }

        // Return value
        return (bool) constant( $constant_name );

    }

    /**
     * Determines if the logged in User is permitted to access the Plugin
     *
     * @since   2.1.7
     *
     * @return  bool    User is permitted
     */
    private function is_logged_in_user_permitted() {

        // Return false if no permitted users exist
        if ( ! defined( strtoupper( $this->base->plugin->name ) . '_PERMITTED_USERS' ) ) {
            return false;
        }

        // Get permitted users
        if ( ! $this->permitted_users ) {
            $this->permitted_users = explode( ',', constant( strtoupper( $this->base->plugin->name ) . '_PERMITTED_USERS' ) );
        }

        // Return false if the array is empty
        if ( empty( $this->permitted_users ) ) {
            return false;
        }

        // Fetch the logged in User
        if ( ! $this->current_user ) {
            $this->current_user = wp_get_current_user();
        }

        // Check if the logged in User is a permitted User
        foreach ( $this->permitted_users as $permitted_user ) {
            // Permitted user can be a User ID, username or email address
            if ( $this->current_user->user_login == $permitted_user ) {
                return true;
            }
            if ( $this->current_user->user_email == $permitted_user ) {
                return true;
            }
            if ( $this->current_user->ID == $permitted_user ) {
                return true;
            }
        }

        // If here, the user is not permitted
        return false;

    }

}