<?php
/**
 * Generate class. Handles generating Pages, Posts, Custom Post Types
 * and Taxonomy Terms.
 * 
 * @package Page_Generator_Pro
 * @author  Tim Carr
 * @version 1.0.0
 */
class Page_Generator_Pro_Generate {

    /**
     * Holds the base object.
     *
     * @since   1.9.8
     *
     * @var     object
     */
    public $base;

    /**
     * Holds an array comprising of every keyword detected in the Group.
     * Each Keyword holds an array comprising of every single Term for that Keyword.
     *
     * @since   1.9.8
     *
     * @var     array
     */
    public $keywords = array();

    /**
     * Holds an array comprising of every keyword detected in the Group.
     * Each Keyword holds the nth Term that will be used to replace the Keyword.
     *
     * @since   1.9.8
     *
     * @var     array
     */
    public $keywords_terms = array();

    /**
     * Holds the array of found keywords across all settings.
     *
     * @since   1.2.0
     *
     * @var     array
     */
    public $required_keywords = array();

    /**
     * Holds the array of found keywords that require a fixed term across all settings.
     *
     * @since   1.2.0
     *
     * @var     array
     */
    public $required_keywords_fixed = array();

    /**
     * Holds the array of found keywords that require a sub term to be extracted by a given column name.
     *
     * @since   1.7.8
     *
     * @var     array
     */
    public $required_keywords_columns = array();

    /**
     * Holds the array of keywords to replace e.g. {city}
     *
     * @since   1.3.1
     *
     * @var     array
     */
    public $searches = array();

    /**
     * Holds the array of keyword values to replace e.g. Birmingham
     *
     * @since   1.3.1
     *
     * @var     array
     */
    public $replacements = array();

    /**
     * Holds a flag to denote whether Page Generator Pro shortcodes
     * should be processed on the main Post Content
     *
     * @since   1.9.5
     *
     * @var     bool
     */
    public $process_shortcodes_on_post_content = false;

    /**
     * Constructor.
     *
     * @since   1.9.3
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Register Action Hooks
        add_action( 'page_generator_pro_generate_content_after', array( $this, 'generate_content_finished' ), 10, 2 );
        add_action( 'page_generator_pro_generate_terms_after', array( $this, 'generate_terms_finished' ), 10, 2 );

        // Delete Geodata on Post Deletion
        add_action( 'delete_post', array( $this, 'delete_latitude_longitude_by_post_id' ) );

        // Bail if WP-CLI is not available
        if ( ! class_exists( 'WP_CLI' ) ) {
            return;
        }
         
        // Register WP-CLI Hooks
        WP_CLI::add_hook( 'page_generator_pro_generate_content_after', array( $this, 'generate_content_finished' ), 10, 2 );
        WP_CLI::add_hook( 'page_generator_pro_generate_terms_after', array( $this, 'generate_terms_finished' ), 10, 2 );

    }

    /**
     * Calculates the maximum number of items that will be generated based
     * on the settings.
     *
     * @since   1.1.5
     *
     * @param   array   $settings   Group Settings (either a Content or Term Group)
     * @return  mixed               WP_Error | integer
     */
    public function get_max_number_of_pages( $settings ) {

        // Build a class array of required keywords that need replacing with data
        $this->find_keywords_in_settings( $settings );

        // Bail if no keywords were found
        if ( count( $this->required_keywords ) == 0 ) {
            return 0;
        }

        // Get the terms for each required keyword
        $this->keywords = $this->get_keywords_terms_columns_delimiters( $this->required_keywords );

        // Bail if no keywords were found
        if ( empty( $this->keywords['terms'] ) ) {
            return 0;
        }

        // Depending on the generation method chosen, for each keyword, define the term
        // that will replace it.
        switch ( $settings['method'] ) {

            /**
             * All
             * Random
             * - Generates all possible term combinations across keywords
             */
            case 'all':
            case 'random':
                $total = 1;
                foreach ( $this->keywords['terms'] as $keyword => $terms ) {
                    $total = ( $total * count( $terms ) );
                }

                return $total;
                break;

            /**
             * Sequential
             * - Generates term combinations across keywords matched by index
             */
            case 'sequential':
                $total = 0;
                foreach ( $this->keywords['terms'] as $keyword => $terms ) {
                    if ( count( $terms ) > 0 && ( count( $terms ) < $total || $total == 0 ) ) {
                        $total = count( $terms );
                    }
                }

                return $total;
                break;

        }

    }

    /**
     * Deprecated function for generating Posts, Pages and Custom Post Types
     * from a given Group.
     *
     * Use generate_content() instead.
     *
     * @since   1.0.0
     *
     * @param   int     $group_id   Group ID
     * @param   int     $index      Keyword Index
     * @param   bool    $test_mode  Test Mode
     * @return  mixed               WP_Error | array
     */
    public function generate( $group_id, $index = 0, $test_mode = false ) {

        return $this->generate_content( $group_id, $index, $test_mode );

    }
    
    /**
     * Main function to generate a Page, Post, Custom Post Type or Taxonomy Term
     *
     * @since   1.6.1
     *
     * @param   int     $group_id   Group ID
     * @param   int     $index      Keyword Index
     * @param   bool    $test_mode  Test Mode
     * @return  mixed               WP_Error | URL
     */
    public function generate_content( $group_id, $index = 0, $test_mode = false ) {

        // Performance debugging
        $start  = microtime( true );

        // Define the Group ID as a global, so it can be picked up by shortcode processing
        global $page_generator_pro_group_id;
        $page_generator_pro_group_id = $group_id;

        // If test mode is enabled, set the debug constant
        if ( $test_mode && ! defined( 'PAGE_GENERATOR_PRO_DEBUG' ) ) {
            define( 'PAGE_GENERATOR_PRO_DEBUG', true );
        }

        // If this Group has a request to cancel generation, exit
        if ( ! $test_mode ) {
            if ( $this->base->get_class( 'groups' )->cancel_generation_requested( $group_id ) ) {
                $this->base->get_class( 'groups' )->stop_generation( $group_id );
                return new WP_Error( 'generation_error', __( 'A request to cancel generation was made by the User. Exiting...', 'page-generator-pro' ) );
            }
        }
        
        // Get group settings
        $settings = $this->base->get_class( 'groups' )->get_settings( $group_id );

        // Validate group
        $validated = $this->base->get_class( 'groups' )->validate( $group_id );
        if ( is_wp_error( $validated ) ) {
            return $validated;
        }

        // Build a class array of required keywords that need replacing with data
        $this->find_keywords_in_settings( $settings );
        if ( count( $this->required_keywords ) == 0 ) {
            return new WP_Error( 'keyword_error', __( 'No keywords were specified in the Group.', 'page-generator-pro' ) );
        }

        // Build a keywords array comprising of terms, columns and delimiters for each of the required keywords
        $this->keywords = $this->get_keywords_terms_columns_delimiters( $this->required_keywords );
        if ( count( $this->keywords['terms'] ) == 0 ) {
            return new WP_Error( 'keyword_error', __( 'Keywords were specified in the Group, but no keywords exist in either the Keywords section of the Plugin or as a Taxonomy.', 'page-generator-pro' ) );
        }

        // Build array of keyword --> term key/value pairs to use for this generation
        $this->keywords_terms = $this->get_keywords_terms( $settings['method'], $index );
        if ( is_wp_error( $this->keywords_terms ) ) {
            return $this->keywords_terms;
        }

        // Rotate Author
        if ( isset( $settings['rotateAuthors'] ) ) {
            $authors = $this->base->get_class( 'common' )->get_authors();
            $user_index = ( $index % count( $authors ) );
        }

        // If 'Apply Synonyms' has been enabled, add spintax to the content now
        if ( isset( $settings['apply_synonyms'] ) && $settings['apply_synonyms'] ) {
            $settings['content'] = $this->base->get_class( 'spintax' )->add_spintax( $settings['content'] );
        }

        // Define whether we'll process shortcodes on the Post Content
        // Some Page Builders will mean we won't do this, such as Elementor, which don't use
        // the Post Content for output.
        $this->process_shortcodes_on_post_content = $this->should_process_shortcodes_on_post_content( $settings );

        // Remove all shortcode processors, so we don't process any shortcodes. This ensures page builders, galleries etc
        // will work as their shortcodes will be processed when the generated page is viewed.
        remove_all_shortcodes();

        // Add Page Generator Pro's shortcodes, so they're processed now (true = we want to register shortcodes that need processing into HTML)
        $this->base->get_class( 'shortcode' )->add_shortcodes( true );

        // Iterate through each keyword and term key/value pair
        $settings = $this->replace_keywords( $settings );

        // Define Post Name / Slug
        // If no Permalink exists, use the Post Title
        if ( ! empty( $settings['permalink'] ) ) {
            $post_name = sanitize_title( $settings['permalink'] );
        } else {
            $post_name = sanitize_title( $settings['title'] );
        }

        // Determine the Post Parent
        $post_parent = ( ( isset( $settings['pageParent'] ) && isset( $settings['pageParent'][ $settings['type'] ] ) && ! empty( $settings['pageParent'][ $settings['type'] ] ) ) ? $settings['pageParent'][ $settings['type'] ] : 0 );
        if ( ! is_numeric( $post_parent ) ) {
            // Convert Post Parent to slug, retaining forwardslashes
            // This also converts special accented characters to non-accented versions
            $post_parent = $this->sanitize_slug( $post_parent );

            // Find the Post ID based on the given name
            $parent = get_page_by_path( $post_parent, OBJECT, $settings['type'] );

            if ( ! $parent ) {
                $post_parent = 0;
            } else {
                $post_parent = $parent->ID;
            }
        }

        // Depending on the Ovewrite setting, check if an existing Post exists
        switch ( $settings['overwrite'] ) {
            /**
             * No, skip if existing Page generated by this Group
             */
            case 'skip_if_exists':
                // Find existing Post by Permalink generated by this Group
                $existing_post_id = $this->post_exists( $group_id, $settings['type'], $post_parent, $post_name );

                // Bail if a Post is found, as we're skipping Generation
                if ( $existing_post_id > 0 ) {
                    return $this->generate_return( 
                        $existing_post_id, 
                        $settings['type'],
                        false,
                        sprintf(
                            __( 'Skipped, as %s with Permalink already generated by this Group', 'page-generator-pro' ),
                            $settings['type']
                        ),
                        $start,
                        $test_mode
                    );
                }
                break;

            /**
             * No, skip if existing Page exists
             */
            case 'skip_if_exists_any':
                // Find existing Post by Permalink, regardless of Group
                $existing_post_id = $this->post_exists( 0, $settings['type'], $post_parent, $post_name );
                
                // Bail if a Post is found, as we're skipping Generation
                if ( $existing_post_id > 0 ) {
                    return $this->generate_return( 
                        $existing_post_id, 
                        $settings['type'],
                        false,
                        sprintf(
                            __( 'Skipped, as %s with Permalink already exists in WordPress', 'page-generator-pro' ),
                            $settings['type']
                        ),
                        $start,
                        $test_mode
                    );
                }
                break;

            /**
             * Yes, if existing Page generated by this Group
             * Yes, if existing Page generated by this Group, preserving original Publish date
             */
            case 'overwrite':
            case 'overwrite_preseve_date':
                // Try to find existing post
                $existing_post_id = $this->post_exists( $group_id, $settings['type'], $post_parent, $post_name );
                break;

            /**
             * Yes, if existing Page exists
             * Yes, if existing Page exists, preserving original Publish date
             */
            case 'overwrite_any':
            case 'overwrite_any_preseve_date':
                // Try to find existing post
                $existing_post_id = $this->post_exists( 0, $settings['type'], $post_parent, $post_name );
                break;
        }

        // Process Shortcodes
        array_walk_recursive( $settings, array( $this, 'process_shortcodes_in_array' ) );

        // Build Post args
        $post_args = array(
            'post_type'     => $settings['type'],
            'post_title'    => $settings['title'],
            'post_content'  => $settings['content'],
            'post_status'   => ( $test_mode ? 'draft' : $settings['status'] ),
            'post_author'   => ( ( isset( $settings['rotateAuthors'] ) && $settings['rotateAuthors'] == 1 ) ? $authors[ $user_index ]->ID : $settings['author'] ), // ID
            'comment_status'=> ( ( isset( $settings['comments'] ) && $settings['comments'] == 1 ) ? 'open' : 'closed' ),
            'ping_status'   => ( ( isset( $settings['trackbacks'] ) && $settings['trackbacks'] == 1 ) ? 'open' : 'closed' ),
            'post_parent'   => $post_parent,
            'post_name'     => $post_name,
        );

        // Define Post Excerpt, if the Post Type supports it
        if ( post_type_supports( $settings['type'], 'excerpt' ) ) {
            $post_args['post_excerpt'] = $settings['excerpt'];
        }

        // Define the Post Date
        switch ( $settings['date_option'] ) {

            /**
             * Now
             */
            case 'now':
                if ( $settings['status'] == 'future' ) {
                    // Increment the current date by the schedule hours and unit
                    $post_args['post_date'] = date_i18n( 'Y-m-d H:i:s', strtotime( '+' . ( $settings['schedule'] * ( $index + 1 ) ) . ' ' . $settings['scheduleUnit'] ) );
                } else {
                    $post_args['post_date'] = date_i18n( 'Y-m-d H:i:s' );
                }
                break;

            /**
             * Specific Date
             */
            case 'specific':
                if ( $settings['status'] == 'future' ) {
                    // Increment the specific date by the schedule hours and unit
                    $post_args['post_date'] = date_i18n( 'Y-m-d H:i:s', strtotime( $settings['date_specific'] . ' +' . ( $settings['schedule'] * ( $index + 1 ) ) . ' ' . $settings['scheduleUnit'] ) );
                } else {
                    $post_args['post_date'] = $settings['date_specific'];
                }
                break;

            /**
             * Random
             */
            case 'random':
                $min = strtotime( $settings['date_min'] );
                $max = strtotime( $settings['date_max'] );
                $post_args['post_date'] = date_i18n( 'Y-m-d H:i:s', rand( $min, $max ) );
                break;

        }

        /**
         * Filters arguments used for creating or updating a Post when running
         * content generation.
         *
         * @since   1.6.1
         *
         * @param   array   $post_args  wp_insert_post() / wp_update_post() compatible arguments
         * @param   array   $settings   Content Group Settings
         */
        $post_args = apply_filters( 'page_generator_pro_generate_post_args', $post_args, $settings );

        // If overwrite is enabled, attempt to find an existing Post generated by this Group
        // with the same Permalink
        switch ( $settings['overwrite'] ) {

            /**
             * Overwrite
             */
            case 'overwrite':
                // If a Post was found, update it
                if ( $existing_post_id > 0 ) {
                    // Define the Post ID to update
                    $post_args['ID'] = $existing_post_id;

                    // Remove Post Args that we're not overwriting
                    $post_args = $this->restrict_by_overwrite_sections( array_keys( $settings['overwrite_sections'] ), $post_args );

                    // Update Page, Post or CPT
                    $post_id = wp_update_post( $post_args, true );

                    // Define return message
                    $log = sprintf( 
                        __( 'Updated, as %s with Permalink already generated by this Group', 'page-generator-pro' ), 
                        $settings['type']
                    );
                } else {
                    // Create Page, Post or CPT
                    $post_id = wp_insert_post( $post_args, true );

                    // Define return message
                    $log = sprintf( 
                        __( 'Created, as %s with Permalink has not yet been generated by this Group', 'page-generator-pro' ), 
                        $settings['type']
                    );
                }
                break;

            /**
             * Overwrite Any
             */
            case 'overwrite_any':
                // If a Post was found, update it
                if ( $existing_post_id > 0 ) {
                    // Define the Post ID to update
                    $post_args['ID'] = $existing_post_id;

                    // Remove Post Args that we're not overwriting
                    $post_args = $this->restrict_by_overwrite_sections( array_keys( $settings['overwrite_sections'] ), $post_args );

                    // Update Page, Post or CPT
                    $post_id = wp_update_post( $post_args, true );

                    // Define return message
                    $log = sprintf( 
                        __( 'Updated, as %s with Permalink already exists in WordPress', 'page-generator-pro' ), 
                        $settings['type']
                    );
                } else {
                    // Create Page, Post or CPT
                    $post_id = wp_insert_post( $post_args, true );

                    // Define return message
                    $log = sprintf( 
                        __( 'Created, as %s with Permalink does not exist in WordPress', 'page-generator-pro' ), 
                        $settings['type']
                    );
                }
                break;

            /**
             * Don't Overwrite
             */
            default:
                // Create Page, Post or CPT
                $post_id = wp_insert_post( $post_args, true );

                // Define return message
                $log = sprintf( 
                    __( 'Created', 'page-generator-pro' ), 
                    $settings['type']
                );
                break;

        }

        // Check Post creation / update worked
        if ( is_wp_error( $post_id ) ) {
            // Fetch error codes when trying to insert / update the Post
            $error_codes = $post_id->get_error_codes();

            // Ignore invalid_page_template errors.  wp_update_post() adds the existing page_template
            // parameter to $post_args before passing onto wp_insert_post(); however the template
            // might belong to a Page Builder Template that has / will not register the template with
            // the active Theme.
            // We manually assign _wp_page_template later on in this process, so we can safely ignore
            // this error.
            if ( count( $error_codes ) == 1 && $error_codes[0] == 'invalid_page_template' ) {
                // The Post ID will be the existing Post ID we just updated
                $post_id = $existing_post_id;
            } else {
                // UTF-8 encode the Title, Excerpt and Content
                $post_args['post_title'] = utf8_encode( $post_args['post_title'] );
                $post_args['post_content'] = utf8_encode( $post_args['post_content'] );
                if ( post_type_supports( $settings['type'], 'excerpt' ) ) {
                    $post_args['post_excerpt'] = utf8_encode( $post_args['post_excerpt'] );
                }
                
                // Try again
                if ( isset( $post_args['ID'] ) ) {
                    // Remove Post Args that we're not overwriting
                    $post_args = $this->restrict_by_overwrite_sections( array_keys( $settings['overwrite_sections'] ), $post_args );

                    // Update Page, Post or CPT
                    $post_id = wp_update_post( $post_args, true ); 
                } else {
                    // Create Page, Post or CPT
                    $post_id = wp_insert_post( $post_args, true ); 
                }

                // If Post creation / update still didn't work, bail
                if ( is_wp_error( $post_id ) ) {
                    $post_id->add_data( $post_args, $post_id->get_error_code() );
                    return $post_id;
                }
            }
        }

        // Store this Generation ID in the Post's meta, so we can edit/delete the generated Post(s) in the future
        update_post_meta( $post_id, '_page_generator_pro_group', $group_id );

        // Post / Page Template
        // Backward compat for Free
        if ( ! empty( $settings['pageTemplate'] ) && ! is_array( $settings['pageTemplate'] ) ) {
            update_post_meta( $post_id, '_wp_page_template', $settings['pageTemplate'] );
        }
        if ( ! empty( $settings['pageTemplate'][ $settings['type'] ] ) ) {
            update_post_meta( $post_id, '_wp_page_template', $settings['pageTemplate'][ $settings['type'] ] );
        }

        // Featured Image
        $image_id = $this->featured_image( $post_id, $settings, $post_args );
        if ( is_wp_error( $image_id ) && defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
            return $image_id;
        }

        // Store Custom Fields as Post Meta on the Generated Post
        $this->custom_fields( $post_id, $settings, $post_args );
        
        // Store Post Meta (ACF, Yoast, Page Builder data etc) on the Generated Post
        $this->post_meta( $post_id, $settings, $post_args );

        // Store Latitude and Longitude
        $result = $this->latitude_longitude( $post_id, $group_id, $settings );
        if ( is_wp_error( $result ) && defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG === true ) {
            return $result;
        }

        // Assign Taxonomy Terms to the Generated Post
        $result = $this->assign_taxonomy_terms_to_post( $settings['tax'], $settings['type'], $post_id );
        if ( is_wp_error( $result ) ) {
            return $result;
        }

        // Request that the user review the plugin. Notification displayed later,
        // can be called multiple times and won't re-display the notification if dismissed.
        if ( ! $test_mode ) {
            $this->base->dashboard->request_review();
        
            // Store current index as the last index generated for this Group, if we're not in test mode
            $this->base->get_class( 'groups' )->update_last_index_generated( $group_id, $index );
        }

        // Return success data
        return $this->generate_return( $post_id, $settings['type'], true, $log, $start, $test_mode );

    }

    /**
     * Main function to generate a Page, Post, Custom Post Type or Taxonomy Term
     *
     * @since   1.0.0
     *
     * @param   int     $group_id   Group ID
     * @param   int     $index      Keyword Index
     * @param   bool    $test_mode  Test Mode
     * @return  mixed               WP_Error | URL
     */
    public function generate_term( $group_id, $index, $test_mode = false ) {

        // Performance debugging
        $start = microtime( true );

        // Get group settings
        $settings = $this->base->get_class( 'groups_terms' )->get_settings( $group_id );

        // Validate group
        $validated = $this->base->get_class( 'groups_terms' )->validate( $group_id );
        if ( is_wp_error( $validated ) ) {
            return $validated;
        }

        // Build a class array of required keywords that need replacing with data
        $this->find_keywords_in_settings( $settings );
        if ( count( $this->required_keywords ) == 0 ) {
            return new WP_Error( 'page_generator_pro_generate_generate_term_keyword_error', __( 'No keywords were specified in the Group.', 'page-generator-pro' ) );
        }

        // Build a keywords array comprising of terms, columns and delimiters for each of the required keywords
        $this->keywords = $this->get_keywords_terms_columns_delimiters( $this->required_keywords );
        if ( count( $this->keywords['terms'] ) == 0 ) {
            return new WP_Error( 'page_generator_pro_generate_generate_term_keyword_error', __( 'Keywords were specified in the Group, but no keywords exist in either the Keywords section of the Plugin or as a Taxonomy.', 'page-generator-pro' ) );
        }

        // Build array of keyword --> term key/value pairs to use for this generation
        $this->keywords_terms = $this->get_keywords_terms( $settings['method'], $index );
        if ( is_wp_error( $this->keywords_terms ) ) {
            return $this->keywords_terms;
        }

        // Iterate through each keyword and term key/value pair
        $settings = $this->replace_keywords( $settings );

        // If 'Apply Synonyms' has been enabled, add spintax to the excerpt now
        if ( isset( $settings['apply_synonyms'] ) && $settings['apply_synonyms'] ) {
            $settings['excerpt'] = $this->base->get_class( 'spintax' )->add_spintax( $settings['excerpt'] );
        }

        // Build Term args
        $term_args = array(
            'description'   => $settings['excerpt'],
        );

        // Define Slug
        // If no Permalink exists, use the Title
        if ( ! empty( $settings['permalink'] ) ) {
            $term_args['slug'] = sanitize_title( $settings['permalink'] );
        }

        // If the taxonomy is hierarhical, and a parent term has been specified, attempt to find it now
        if ( is_taxonomy_hierarchical( $settings['taxonomy'] ) && ! empty( $settings['parent_term'] ) ) {
            $existing_parent_terms = new WP_Term_Query( array(
                'taxonomy'      => array( $settings['taxonomy'] ),
                'name'          => array( $settings['parent_term'] ),
                'hide_empty'    => false,
                
                // For performance, just return the Post ID and don't update meta or term caches
                'fields'                => 'ids',
                'update_term_meta_cache'=> false,
            ) );

            // If an existing Parent Term was found, use that
            if ( ! is_null( $existing_parent_terms->terms ) && count( $existing_parent_terms->terms ) > 0 ) {
                $parent_term_id = $existing_parent_terms->terms[0];
            } else {
                $parent_term = wp_insert_term( $settings['parent_term'], $settings['taxonomy'] ); 
                $parent_term_id = $parent_term['term_id'];
            }

            // Assign the parent term ID now
            $term_args['parent'] = $parent_term_id;
        }

        /**
         * Filters arguments used for creating or updating a Term when running
         * content generation.
         *
         * @since   1.6.1
         *
         * @param   array   $term_args  wp_insert_term() / wp_update_term() compatible arguments
         * @param   array   $settings   Content Group Settings
         */
        $term_args = apply_filters( 'page_generator_pro_generate_term_args', $term_args, $settings );

        // Build query to check if this Term already exists
        $existing_terms_args = array(
            'taxonomy'      => array( $settings['taxonomy'] ),
            'name'          => array( $settings['title'] ),
            'hide_empty'    => false,
            
            // For performance, just return the Post ID and don't update meta or term caches
            'fields'                => 'ids',
            'update_term_meta_cache'=> false,
        );

        // If a Parent Term ID exists, restrict the above query to only check for existing Terms generated
        // that belong to the Parent
        if ( isset( $parent_term_id ) ) {
            $existing_terms_args['child_of'] = absint( $parent_term_id );
        }

        // Run term query, which we'll use below
        $existing_terms = new WP_Term_Query( $existing_terms_args );

        // If overwrite is enabled, attempt to find an existing Term with the same Permalink
        switch ( $settings['overwrite'] ) {

            /**
             * Overwrite
             */
            case 'overwrite':
                // If a Term was found, update it
                if ( ! is_null( $existing_terms->terms ) && count( $existing_terms->terms ) > 0 ) {
                    // Update Term
                    $term = wp_update_term( $existing_terms->terms[0], $settings['taxonomy'], $term_args );
                } else {
                    // Create Term
                    $term = wp_insert_term( $settings['title'], $settings['taxonomy'], $term_args ); 
                }
                break;

            /**
             * Don't Overwrite
             */
            default:
                // If a Term was found, just return it, not overwriting it
                if ( ! is_null( $existing_terms->terms ) && count( $existing_terms->terms ) > 0 ) {
                    // Return existing URL
                    return array(
                        'url'           => get_term_link( $existing_terms->terms[0], $settings['taxonomy'] ),
                        'keywords_terms'=> $this->keywords_terms,
                    );
                } else {
                    // Create Term
                    $term = wp_insert_term( $settings['title'], $settings['taxonomy'], $term_args ); 
                }
                break;

        }

        // Check Term creation / update worked
        if ( is_wp_error( $term ) ) {
            $term->add_data( $term_args, $term->get_error_code() );
            return $term;
        }

        // Store this Generation ID in the Post's meta, so we can edit/delete the generated Post(s) in the future
        update_term_meta( $term['term_id'], '_page_generator_pro_group', $group_id );

        if ( ! $test_mode ) {
            // Request that the user review the plugin. Notification displayed later,
            // can be called multiple times and won't re-display the notification if dismissed.
            $this->base->dashboard->request_review();

            // Store current index as the last index generated for this Group, if we're not in test mode
            $this->base->get_class( 'groups_terms' )->update_last_index_generated( $group_id, $index );
        }

        // Return the URL and keyword / term replacements used
        return $this->generate_return( $term['term_id'], $settings['taxonomy'], true, 'msg', $start, $test_mode );

    }

    /**
     * Helper method to iterate through the keyword / term key/value pairs,
     * building search and replacement arrays (comprising of keyword transformations etc),
     * before recursively iterating through the supplied settings, replacing the keywords
     * and their transformations with the terms.
     *
     * @since   1.9.8
     *
     * @param   array   $settings   Group Settings
     * @return  array               Group Settings
     */
    public function replace_keywords( $settings ) {

        foreach ( $this->keywords_terms as $keyword => $term ) {
            // Build search and replacement arrays, including keyword transformations
            $this->build_search_replace_arrays( $keyword, $term );

            // Iterate through Group Settings, replacing keywords with terms, as well as 
            // performing spintax and shortcode processing
            array_walk_recursive( $settings, array( $this, 'replace_keywords_in_array' ) );
        }

        return $settings;

    }

    /**
     * Adds keywords and their replacements to the class $searches and $replacements arrays,
     * based on the given $keyword (search) and $term (replacement).
     *
     * Also includes columns, nth terms and transformations (e.g. uppercase_all, lowercase_all etc)
     *
     * @since   1.7.8
     *
     * @param   string  $keyword    Keyword (search)
     * @param   string  $term       Term (replace)
     */
    private function build_search_replace_arrays( $keyword, $term ) {

        // Reset search and replacement arrays for this keyword
        $this->searches     = array(
            '{' . $keyword . '}',
        );
        $this->replacements = array(
            $term,
        );

        // Add keyword transformations
        $this->build_search_replace_arrays_transformations( $keyword, $term, $keyword );

        // Add nth terms
        // Includes nth terms with keyword transformations
        $this->build_search_replace_arrays_nth_terms( $keyword, $keyword );

        // Add keywords with column names
        // Includes keyword transformations with column names
        // Includes keyword transformations with column names and nth terms
        $this->build_search_replace_arrays_column_names( $keyword, $term );

    }

    /**
     * Adds column name keyword search and replacements
     *
     * @since   2.0.2
     *
     * @param   string  $keyword    Keyword
     * @param   string  $term       Term
     * @param   string  $search     Search Keyword
     */
    private function build_search_replace_arrays_column_names( $keyword, $term ) {

        // Add keyword columns
        // Bail if keyword columns don't exist
        if ( count( $this->required_keywords_columns ) == 0 ) {
            return;
        }
        if ( ! isset( $this->required_keywords_columns[ $keyword ] ) ) {
            return;
        }
        if ( count( $this->required_keywords_columns[ $keyword ] ) == 0 ) {
            return;
        }

        foreach ( $this->required_keywords_columns[ $keyword ] as $column ) {
            // Split the term
            $term_parts = str_getcsv( $term, $this->keywords['delimiters'][ $keyword ] );

            // Fetch the column index
            $column_index = array_search( $column, $this->keywords['columns'][ $keyword ] );

            // Skip if no column index could be found
            if ( $column_index === FALSE ) {
                continue;
            }

            // Add to the search and replace arrays
            $this->searches[] = '{' . $keyword . '(' . $column . ')}';
            $this->replacements[] = trim( $term_parts[ $column_index ] );

            // Add keyword transformations
            $this->build_search_replace_arrays_transformations( $keyword . '(' . $column . ')', trim( $term_parts[ $column_index ] ), $keyword, $column );

            // Add nth terms
            // Includes nth terms with keyword transformations
            $this->build_search_replace_arrays_nth_terms( $keyword . '(' . $column . ')', $keyword, $column );
        }
 
    }

    /**
     * Adds transformation keyword search and replacements
     *
     * @since   2.0.2
     *
     * @param   string  $keyword    Keyword
     * @param   string  $term       Term
     * @param   string  $search     Search Keyword
     * @param   string  $column     Keyword Column
     */
    private function build_search_replace_arrays_transformations( $search, $replacement, $keyword, $column = false ) {

        // Get Keyword Transformations
        $keyword_transformations = $this->base->get_class( 'common' )->get_keyword_transformation_options();

        // Bail if no keyword transformations
        if ( ! is_array( $keyword_transformations ) ) {
            return;
        }

        // Build array of every possible keyword transformation
        $all_keyword_transformation_combinations = $this->generate_unique_combinations_by_size(
            array_keys( $keyword_transformations ),
            2
        );

        // Iterate through each keyword transformation, building array of replacements
        foreach ( $keyword_transformations as $keyword_transformation => $label ) {
            $this->searches[] = '{' . $search . ':' . $keyword_transformation . '}';
            $this->replacements[] = $this->apply_keyword_transformation( $keyword_transformation, $replacement, $keyword, $column );
        }

        // Iterate through each keyword transformation combination, building array of replacements
        foreach ( $all_keyword_transformation_combinations as $keyword_transformation_combination ) {
            $this->searches[] = '{' . $search . ':' . $keyword_transformation_combination . '}';

            // Build replacement
            $keyword_transformation_combination_parts = explode( ':', $keyword_transformation_combination );
            $replacement_transformed = $replacement;
            foreach ( $keyword_transformation_combination_parts as $keyword_transformation ) {
                $replacement_transformed = $this->apply_keyword_transformation( $keyword_transformation, $replacement_transformed, $keyword, $column );
            }

            // Add transformed replacement to array
            $this->replacements[] = $replacement_transformed;
        }

    }

    /**
     * Applies the given keyword transformation to the given string (term)
     *
     * @since   2.2.3
     *
     * @param   string  $keyword_transformation     Keyword Transformation
     * @param   string  $term                       Term
     * @param   string  $keyword                    Keyword
     * @param   mixed   $column                     Keyword Column
     * @return  string                              Transformed Term
     */
    private function apply_keyword_transformation( $keyword_transformation, $term, $keyword, $column = false ) {

        switch ( $keyword_transformation ) {
            case 'uppercase_all':
                return strtoupper( $term );
                break;

            case 'lowercase_all':
                return strtolower( $term );
                break;

            case 'uppercase_first_character':
                return ucfirst( $term );
                break;

            case 'uppercase_first_character_words':
                return ucwords( $term );
                break;

            case 'url':
                return sanitize_title( $term );
                break;

            case 'all':
                if ( $column ) {
                    $terms = array();
                    foreach ( $this->keywords['terms'][ $keyword ] as $term ) {
                        // Split the term
                        $term_parts = str_getcsv( $term, $this->keywords['delimiters'][ $keyword ] );

                        // Fetch the column index
                        $column_index = array_search( $column, $this->keywords['columns'][ $keyword ] );

                        // Skip if no column index could be found
                        if ( $column_index === FALSE ) {
                            continue;
                        }

                        $terms[] = trim( $term_parts[ $column_index ] );
                    }

                    // Remove duplicates
                    $terms = array_values( array_unique( $terms ) );

                    // Return All Column Terms for the Keyword
                    return implode( ', ', $terms );
                }
                
                // Return all Terms for the Keyword
                return implode( ', ', $this->keywords['terms'][ $keyword ] );
                break;

            default:
                /**
                 * Filter to perform non-standard keyword transformation.
                 *
                 * @since   1.7.8
                 *
                 * @param   string  $term               Term
                 * @param   string  $transformation     Keyword Transformation
                 * @param   string  $keyword            Keyword
                 * @param   mixed   $column             Keyword Column
                 */
                $term = apply_filters( 'page_generator_pro_generate_generate_content_apply_keyword_transformation', $term, $keyword_transformation, $keyword, $column );
                
                return $term;
                break;
        } 
 
    }

    /**
     * Adds nth term keyword search and replacements
     *
     * @since   2.0.2
     *
     * @param   string  $keyword    Keyword
     * @param   string  $term       Term
     * @param   string  $search     Search Keyword
     */
    private function build_search_replace_arrays_nth_terms( $search, $keyword, $column = false ) {

        // Bail if no nth term keywords exist
        if ( count( $this->required_keywords_fixed ) == 0 ) {
            return;
        }

        if ( ! isset( $this->required_keywords_fixed[ $keyword ] ) ) {
            return;
        }

        if ( count( $this->required_keywords_fixed[ $keyword ] ) == 0 ) {
            return;
        }

        foreach ( $this->required_keywords_fixed[ $keyword ] as $keyword_term_index ) {
            // If the nth child is out of the keyword term array's bounds, ignore it
            if ( ! isset( $this->keywords['terms'][ $keyword ][ $keyword_term_index - 1 ] ) ) {
                continue;
            }

            // Define term
            $term = $this->keywords['terms'][ $keyword ][ $keyword_term_index - 1 ];

            // If a column is specified, fetch the term subset
            if ( $column != false ) {
                // Split the term
                $term_parts = str_getcsv( $term, $this->keywords['delimiters'][ $keyword ] );

                // Fetch the column index
                $column_index = array_search( $column, $this->keywords['columns'][ $keyword ] );

                // If the column exists in the term, use it as the term
                if ( $column_index !== FALSE ) {
                    $term = trim( $term_parts[ $column_index ] );
                }
            }

            $this->searches[] = '{' . $search . ':' . $keyword_term_index . '}';
            $this->replacements[] = $term;

            // Add keyword transformations for this nth term
            $this->build_search_replace_arrays_transformations( 
                $search . ':' . $keyword_term_index,
                $term,
                $keyword
            );
        }

    }

    /**
     * Returns an array comprising of all keywords and their term replacements,
     * including keywords with column names in the format keyword_column.
     *
     * Does not include transformations or nth terms
     *
     * Used to store basic keyword/term data in the generated Page's Post Meta
     * if Store Keywords is enabled
     *
     * @since   2.2.8
     *
     * @return  array   Keyword / Term Key/Value Pairs
     */
    private function get_keywords_terms_array_with_columns() {

        $store_keywords = array();
        
        foreach ( $this->keywords_terms as $keyword => $term ) {
            // Add keyword/term pair
            $store_keywords[ $keyword ] = $term;

            // Add keyword column/term pairs
            if ( count( $this->required_keywords_columns ) == 0 ) {
                continue;
            }
            if ( ! isset( $this->required_keywords_columns[ $keyword ] ) ) {
                continue;
            }
            if ( count( $this->required_keywords_columns[ $keyword ] ) == 0 ) {
                continue;
            }

            foreach ( $this->required_keywords_columns[ $keyword ] as $column ) {
                // Split the term
                $term_parts = str_getcsv( $term, $this->keywords['delimiters'][ $keyword ] );

                // Fetch the column index
                $column_index = array_search( $column, $this->keywords['columns'][ $keyword ] );

                // Skip if no column index could be found
                if ( $column_index === FALSE ) {
                    continue;
                }

                // Add to the search and replace arrays
                $store_keywords[ $keyword . '_' . $column ] = trim( $term_parts[ $column_index ] );
            }
        }

        // Bail if no keywords
        if ( count( $store_keywords ) == 0 ) {
            return false;
        }

        return $store_keywords;
    }

    /**
     * Returns an array comprising of keywords, with each keyword having a replacement value, based
     * on the index requested.
     *
     * Returns an array comprising of all possible value combinations, for the given keywords and terms
     *
     * @since   1.5.1
     *
     * @param   array   $input  Multidimensional array of Keyword Names (keys) => Terms (values)
     * @return  array           Single dimensional array, zero indexed, of keyword names (keys) => term (value)
     */
    private function generate_all_array_combinations( $input ) {

        // Setup vars
        $input  = array_filter( $input );
        $result = array( array() );

        // Iterate through each keyword
        foreach ( $input as $keyword => $terms ) {
            $append = array();

            // Iterate through master array of results
            foreach ( $result as $product ) {
                // Iterate through this keyword's terms
                foreach( $terms as $term ) {
                    $product[ $keyword ] = $term;
                    $append[] = $product;
                }
            }

            // Append the list of Terms to the master array of results
            $result = $append;
        }

        return $result;

    }

    /**
     * Returns an array comprising of all possible unique combinations for the given input.
     *
     * Size will limit the number of elements to combine from the array.
     *
     * @since   2.2.3
     *
     * @param   array   $input          Single dimensional array of values
     * @param   int     $size           Maximum number of elements to combine from the array
     * @param   array   $combinations   Existing combinations from the last recursion
     * @return  array                   Single dimensional array, zero indexed, of unique combinations
     */
    private function generate_unique_combinations_by_size( $input, $size = false, $combinations = array() ) {

        // If size isn't set, it'll be the number of input elements
        if ( ! $size ) {
            $size = count( $input );
        }

        // First iteration will match the given input
        if ( empty( $combinations ) ) {
            $combinations = $input;
        }

        // If size is 1, we've finished building the unique combinations
        if ( $size == 1 ) {
            return $combinations;
        }

        // Define array to store new values
        $new_combinations = array();

        // Loop through existing combinations and input to create strings
        foreach ( $combinations as $combination ) {
            foreach ( $input as $value ) {
                // Skip if the combination and value are the same
                if ( $combination == $value ) {
                    continue;
                }

                $new_combinations[] = $combination . ':' . $value;
            }
        }

        // Call the function for the next iteration, down one size
        return $this->generate_unique_combinations_by_size( $input, $size - 1, $new_combinations );

    }

    /**
     * A faster method for fetching all keyword combinations for PHP 5.5+
     *
     * @since   1.5.1
     *
     * @param   array   $input  Multidimensional array of Keyword Names (keys) => Terms (values)
     * @return  \Generator      Generator
     */
    private function generate_all_combinations( $input ) {

        // Load class
        require_once( $this->base->plugin->folder . '/includes/admin/cartesian-product.php' );

        // Return
        return new Page_Generator_Pro_Cartesian_Product( $input );

    }

    /**
     * Recursively goes through the settings array, finding any {keywords}
     * specified, to build up an array of keywords we need to fetch.
     *
     * @since   1.0.0
     *
     * @param   array   $settings   Settings
     */
    private function find_keywords_in_settings( $settings ) {

        // Recursively walk through all settings to find all keywords
        array_walk_recursive( $settings, array( $this, 'find_keywords_in_string' ) );

        // The class arrays will now store keywords, keywords requiring transformations and keywords with column names
        // Itereate through each to remove any duplicates
        if ( count( $this->required_keywords_fixed ) > 0 ) {
            foreach ( $this->required_keywords_fixed as $keyword => $columns ) {
                $this->required_keywords_fixed[ $keyword ] = array_values( array_unique( $columns ) );
            }
        }
        if ( count( $this->required_keywords_columns ) > 0 ) {
            foreach ( $this->required_keywords_columns as $keyword => $columns ) {
                $this->required_keywords_columns[ $keyword ] = array_values( array_unique( $columns ) );
            }
        }

    }

    /**
     * For the given array of keywords, only returns keywords with terms, column names and delimiters
     * where each keywords have terms.
     *
     * @since   1.6.5
     *
     * @param   array   $required_keywords  Required Keywords
     * @return  array                       Keywords with Terms, Columns and Delimiters
     */
    private function get_keywords_terms_columns_delimiters( $required_keywords ) {

        // Define blank array for keywords with terms and keywords with columns
        $results = array(
            'terms'     => array(),
            'columns'   => array(),
            'delimiters'=> array(),
        );

        foreach ( $required_keywords as $key => $keyword ) {

            // Get terms for this keyword
            // If this keyword starts with 'taxonomy_', try to fetch the terms for the Taxonomy
            if ( strpos( $keyword, 'taxonomy_') !== false && strpos( $keyword, 'taxonomy_' ) == 0 ) {
                $result = get_terms( array(
                    'taxonomy'              => str_replace( 'taxonomy_', '', $keyword ),
                    'hide_empty'            => false,
                    'fields'                => 'names',
                    'update_term_meta_cache'=> false,
                ) );

                // Skip if no results
                if ( ! is_array( $result ) ) {
                    continue;
                }
                if ( count( $result ) == 0 ) {
                    continue;
                }

                $results['terms'][ $keyword ] = $result;
            } else {
                $result = $this->base->get_class( 'keywords' )->get_by( 'keyword', $keyword );

                // Skip if no results
                if ( ! is_array( $result ) ) {
                    continue;
                }
                if ( count( $result ) == 0 ) {
                    continue;
                }

                $results['terms'][ $keyword ]       = $result['dataArr'];
                $results['columns'][ $keyword ]     = $result['columnsArr'];
                $results['delimiters'][ $keyword ]  = $result['delimiter'];
            }
        }

        // Return results
        return $results;

    }

    /**
     * Returns an array of keyword and term key / value pairs.
     * 
     * @since   1.0.0
     *
     * @param   string  $method     Generation Method
     * @param   int     $index      Generation Index
     * @return  mixed               WP_Error | array
     * 
     */
    private function get_keywords_terms( $method, $index ) {

         switch ( $method ) {

            /**
             * All
             * - Generates all possible term combinations across keywords
             */
            case 'all':
                // If we're on PHP 5.5+, use our Cartesian Product class, which implements a Generator
                // to allow iteration of data without needing to build an array in memory.
                // See: http://php.net/manual/en/language.generators.overview.php
                if ( version_compare( phpversion(), '5.5.0', '>=' ) ) {
                    // Use PHP 5.5+ Generator
                    $combinations = $this->generate_all_combinations( $this->keywords['terms'] );

                    // If the current index exceeds the total number of combinations, we've exhausted all
                    // options and don't want to generate any more Pages (otherwise we end up with duplicates)
                    if ( $index > ( $combinations->count() - 1 ) ) {
                        // If the combinations count is a negative number, we exceeded the floating point for an integer
                        // Tell the user to upgrade PHP and/or reduce the number of keyword terms
                        if ( $combinations->count() < 0 ) {
                            $message = __( 'The total possible number of unique keyword term combinations exceeds the maximum number value that can be stored by your version of PHP.  Please consider upgrading to a 64 bit PHP 7.0+ build and/or reducing the number of keyword terms that you are using.', 'page-generator-pro' );
                        } else {
                            $message = __( 'All possible keyword term combinations have been generated. Generating more Pages/Posts would result in duplicate content.', 'page-generator-pro' );
                        }

                        return new WP_Error( 'page_generator_pro_generate_content_keywords_exhausted', $message );
                    }

                    // Iterate through the combinations until we reach the one matching the index
                    // @TODO Can we optimize this?
                    foreach ( $combinations as $c_index => $combination ) {
                        // Skip if not the index we want
                        if ( $c_index != $index ) {
                            continue;
                        }

                        // Define the keyword => term key/value pairs to use based on the current index
                        $keywords_terms = $combination;
                        break;
                    }
                } else {
                    // Use older method, which will hit memory errors
                    $combinations = $this->generate_all_array_combinations( $this->keywords['terms'] );  

                    // If the current index exceeds the total number of combinations, we've exhausted all
                    // options and don't want to generate any more Pages (otherwise we end up with duplicates)
                    if ( $index > ( count( $combinations ) - 1 ) ) {
                        return new WP_Error( 'keywords_exhausted', __( 'All possible keyword term combinations have been generated. Generating more Pages/Posts would result in duplicate content.', 'page-generator-pro' ) );
                    }

                    // Define the keyword => term key/value pairs to use based on the current index
                    $keywords_terms = $combinations[ $index ];
                    break;
                }
                break;

            /**
             * Sequential
             * - Generates term combinations across keywords matched by index
             */
            case 'sequential':
                $keywords_terms = array();
                foreach ( $this->keywords['terms'] as $keyword => $terms ) {
                    // Use modulo to get the term index for this keyword
                    $term_index = ( $index % count( $terms ) );   

                    // Build the keyword => term key/value pairs
                    $keywords_terms[ $keyword ] = $terms[ $term_index ];
                }
                break;

            /**
             * Random
             * - Gets a random term for each keyword
             */
            case 'random':
                $keywords_terms = array();
                foreach ( $this->keywords['terms'] as $keyword => $terms ) {
                    $term_index = rand( 0, ( count( $terms ) - 1 ) );  

                    // Build the keyword => term key/value pairs
                    $keywords_terms[ $keyword ] = $terms[ $term_index ];
                }
                break;

            /**
             * Invalid method
             */
            default:    
                return new WP_Error( 'page_generator_pro_generate_get_keywords_terms_invalid_method', __( 'The method given is invalid.', 'page-generator-pro' ) );
                break;
        }

        // Cleanup the terms
        foreach ( $keywords_terms as $key => $term ) {
            $keywords_terms[ $key ] = trim( html_entity_decode( $term ) );
        }

        return $keywords_terms;

    }

    /**
     * Performs a search on the given string to find any {keywords}
     *
     * @since 1.2.0
     *
     * @param   string  $content    Array Value (string to search)
     * @param   string  $key        Array Key
     */
    private function find_keywords_in_string( $content, $key ) {

        // If $content is an object, iterate this call
        if ( is_object( $content ) ) {
            return array_walk_recursive( $content, array( $this, 'find_keywords_in_string' ) );
        }

        // Get keywords and spins
        preg_match_all( "|{(.+?)}|", $content, $matches );

        // Continue if no matches found
        if ( ! is_array( $matches ) ) {
            return;
        }
        if ( count( $matches[1] ) == 0 ) {
            return;
        }

        // Iterate through matches
        foreach ( $matches[1] as $m_key => $keyword ) {
            // Unset some vars
            unset( $transformation, $column );

            // Ignore spins
            if ( strpos( $keyword, "|" ) !== false ) {
                continue;
            }

            // If a keyword is within spintax at the start of the string (e.g. {{service}|{service2}} ),
            // we get an additional leading curly brace for some reason.  Remove it
            $keyword = str_replace( '{', '', $keyword );
            $keyword = str_replace( '}', '', $keyword );

            // Reset the transformation and nth term modifiers
            $transformation = false;
            $nth_term = false;

            // If there's a transformation flag applied to the keyword, extract and remove it
            if ( strpos( $keyword, ':' ) !== false ) {
                $keyword_transformations = explode( ':', $keyword );

                foreach ( $keyword_transformations as $keyword_transformation_index => $keyword_transformation ) {
                    // First entry is the keyword
                    if ( $keyword_transformation_index == 0 ) {
                        $keyword = $keyword_transformation;
                        continue;
                    }

                    // nth Term
                    if ( is_numeric( $keyword_transformation ) ) {
                        $nth_term = absint( $keyword_transformation );
                        continue;
                    }

                    // Standard transformation
                    $transformation = trim( $keyword_transformation );
                }
            }

            // If there's a column name applied to the keyword, extract and remove it
            $keyword_column_start_bracket_position = strpos( $keyword, '(' );
            $keyword_column_end_bracket_position = strpos( $keyword, ')', $keyword_column_start_bracket_position );
            if ( $keyword_column_start_bracket_position !== false && $keyword_column_end_bracket_position !== false ) {
                // Extract
                $column = substr( $keyword, ( $keyword_column_start_bracket_position + 1 ), ( $keyword_column_end_bracket_position - $keyword_column_start_bracket_position - 1 ) );

                // Remove
                $keyword = preg_replace( '/\(.*?\)/', '', $keyword );
            }

            // Lowercase keyword, to avoid duplicates e.g. {City} and {city}
            $keyword = strtolower( $keyword );

            // If this keyword is not in our required_keywords array, add it
            if ( ! in_array( $keyword, $this->required_keywords ) ) {
                $this->required_keywords[ $keyword ] = $keyword;
            }

            // If the nth_term was specified in the keyword, add this keyword to the required keywords fixed array
            if ( $nth_term != false ) {
                if ( ! isset( $this->required_keywords_fixed[ $keyword ] ) || ! is_array( $this->required_keywords_fixed[ $keyword ] ) ) {
                    $this->required_keywords_fixed[ $keyword ] = array(
                        $nth_term,
                    );
                } else {
                    $this->required_keywords_fixed[ $keyword ][] = $nth_term;
                }
            }

            // If the column is specified, add this keyword to the required keywords columns array

            if ( isset( $column ) && ! empty( $column ) ) {
                if ( ! isset( $this->required_keywords_columns[ $keyword ] ) || ! is_array( $this->required_keywords_columns[ $keyword ] ) ) {
                    $this->required_keywords_columns[ $keyword ] = array(
                        $column,
                    );
                } else {
                    $this->required_keywords_columns[ $keyword ][] = $column;
                }
            } 
        }

    }

    /**
     * array_walk_recursive callback, which finds $this->searches, replacing with
     * $this->replacements in $item.
     *
     * Also performs spintax.
     *
     * @since   1.3.1
     *
     * @param   mixed   $item   Item (array, object, string)
     * @param   string  $key    Key
     */
    private function replace_keywords_in_array( &$item, $key ) {

        // If the settings key's value is an array, walk through it recursively to search/replace
        // Otherwise do a standard search/replace on the string
        if ( is_array( $item ) ) {
            // Array
            array_walk_recursive( $item, array( $this, 'replace_keywords_in_array' ) );
        } elseif( is_object( $item ) ) {
            // Object
            array_walk_recursive( $item, array( $this, 'replace_keywords_in_array' ) );
        } elseif ( is_string( $item ) ) {
            // If here, we have a string
            // Perform keyword replacement, spintax and shortcode processing now
            $item = str_ireplace( $this->searches, $this->replacements, $item ); 

            // Process Spintax
            $result = $this->base->get_class( 'spintax' )->process( $item );
            if ( is_wp_error( $result ) ) {
                if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG ) {
                    // Store the error in the item
                    $item = $result->get_error_message();
                    return;
                }
            }

            // Spintax OK - assign to item
            $item = $result;

            // Process Block Spinning
            $result = $this->base->get_class( 'block_spin' )->process( $item );
            if ( is_wp_error( $result ) ) {
                if ( defined( 'PAGE_GENERATOR_PRO_DEBUG' ) && PAGE_GENERATOR_PRO_DEBUG ) {
                    // Store the error in the item
                    $item = $result->get_error_message();
                    return;
                }
            }

            // Block Spinning OK - assign to item
            $item = $result;

            /**
             * Perform any other keyword replacements or string processing.
             *
             * @since   1.9.8
             *
             * @param   string  $item   Group Setting String (this can be Post Meta, Custom Fields, Permalink, Title, Content etc)
             * @param   string  $key    Group Setting Key
             */
            $item = apply_filters( 'page_generator_pro_generate_replace_keywords_in_array', $item, $key );
        }

    }

    /**
     * Determines if a Post already exists that was generated by the given Group ID for the
     * given Post Type, Parent and Slug
     *
     * @since   2.1.8
     *
     * @param   int     $group_id       Group ID (0 = any Group)
     * @param   string  $post_type      Generated Post Type
     * @param   int     $post_parent    Post Parent (0 = none)
     * @param   string  $post_name      Post Name
     * @return  mixed                   false | int
     */
    private function post_exists( $group_id, $post_type, $post_parent, $post_name ) {

        // Build query arguments
        $args = array(
            'post_type'         => $post_type,
            'post_status'       => array( 'publish', 'pending', 'draft', 'future', 'private' ),
            'post_parent'       => $post_parent, 
            'post_name__in'     => array( $post_name ),
            
            // For performance, just return the Post ID and don't update meta or term caches
            'fields'                => 'ids',
            'cache_results'         => false,
            'update_post_meta_cache'=> false,
            'update_post_term_cache'=> false,
        );

        // If the Group ID isn't zero, add the Group clause to the query
        if ( $group_id > 0 ) {
            $args['meta_query'] = array(
                array(
                    'key'   => '_page_generator_pro_group',
                    'value' => absint( $group_id ),
                ),
            );
        }

        // Try to find existing post
        $existing_post = new WP_Query( $args );

        if ( count( $existing_post->posts ) == 0 ) {
            return false;
        }

        // Return existing Post's ID
        return $existing_post->posts[0];

    }

    /**
     * Sanitizes the given string to a slug, retaining forwardslashes.
     *
     * Special accented characters are converted to non-accented versions.
     *
     * @since   2.2.6
     *
     * @param   string  $slug   Slug to sanitize
     * @return  string          Sanitized Slug
     */
    private function sanitize_slug( $slug ) {
        
        // Split by forwardslash
        $slug_parts = explode( '/', $slug );

        // Sanitize each part
        foreach ( $slug_parts as $index => $slug ) {
            $slug_parts[ $index ] = sanitize_title( $slug );
        }

        // Convert array back to string
        $slug = implode( '/', $slug_parts );

        // Return
        return $slug;

    }

    /**
     * array_walk_recursive callback, which processes shortcodes.
     *
     * @since   1.9.7
     *
     * @param   mixed   $item   Item (array, object, string)
     * @param   string  $key    Key
     */
    private function process_shortcodes_in_array( &$item, $key ) {

        // If the settings key's value is an array, walk through it recursively to search/replace
        // Otherwise do a standard search/replace on the string
        if ( is_array( $item ) ) {
            // Array
            array_walk_recursive( $item, array( $this, 'process_shortcodes_in_array' ) );
        } elseif( is_object( $item ) ) {
            // Object
            array_walk_recursive( $item, array( $this, 'process_shortcodes_in_array' ) );
        } elseif ( is_string( $item ) ) {
            // If here, we have a string
            // Perform shortcode processing
            // Some Page Builders don't use the main Post Content for output, and instead use their own post meta to build the output
            // Therefore, processing shortcodes on the Post Content would duplicate effort
            switch ( $key ) {
                case 'content':
                    if ( $this->process_shortcodes_on_post_content ) {
                        $item = do_shortcode( $item );
                    }
                    break;

                default:
                    $item = do_shortcode( $item );
                    break;
            }

            /**
             * Filter to allow registering and processing shortcodes on a string.
             *
             * @since   1.9.8
             *
             * @param   string  $item   Group Setting String (this can be Post Meta, Custom Fields, Permalink, Title, Content etc)
             * @param   string  $key    Group Setting Key
             */
            $item = apply_filters( 'page_generator_pro_generate_process_shortcodes_in_array', $item, $key );
        }

    }

    /**
     * Defines the Featured Image for the given generated Post ID, if
     * the Group Settings specify a Featured Image and (if overwriting)
     * the Featured Image should be overwritten
     *
     * @since   2.3.5
     *
     * @param   int     $post_id    Generated Post ID
     * @param   array   $settings   Group Settings
     * @param   array   $post_args  wp_insert_post() / wp_update_post() arguments
     * @return  mixed               WP_Error | Image ID
     */
    private function featured_image( $post_id, $settings, $post_args ) {

        // Bail if no Featured Image source defined
        if ( empty( $settings['featured_image_source'] ) ) {
            return false;
        }

        // Bail if we're overwriting an existing Post and don't want to overwrite the Featured Image
        if ( isset( $post_args['ID'] ) && ! array_key_exists( 'featured_image', $settings['overwrite_sections'] ) ) {
            return false;
        }

        switch ( $settings['featured_image_source'] ) {
            /**
             * Media Library ID
             */
            case 'id':
                // Build Featured Image Arguments
                $args = array(
                    'title'         => ( ! empty( $settings['featured_image_media_library_title'] ) ? $settings['featured_image_media_library_title'] : false ),
                    'caption'       => ( ! empty( $settings['featured_image_media_library_caption'] ) ? $settings['featured_image_media_library_caption'] : false ),
                    'alt'           => ( ! empty( $settings['featured_image_media_library_alt'] ) ? $settings['featured_image_media_library_alt'] : false ),
                    'description'   => ( ! empty( $settings['featured_image_media_library_description'] ) ? $settings['featured_image_media_library_description'] : false ),
                    'operator'      => ( ! empty( $settings['featured_image_media_library_operator'] ) ? $settings['featured_image_media_library_operator'] : 'AND' ),       
                    'ids'           => ( ! empty( $settings['featured_image_media_library_ids'] ) ? $settings['featured_image_media_library_ids'] : false ),
                    'min_id'        => ( ! empty( $settings['featured_image_media_library_min_id'] ) ? $settings['featured_image_media_library_min_id'] : false ),
                    'max_id'        => ( ! empty( $settings['featured_image_media_library_max_id'] ) ? $settings['featured_image_media_library_max_id'] : false ),
                );

                // Get Image ID
                $image_id = $this->base->get_class( 'media_library' )->get_random_image_id( $args );
                break;

             /**
             * Image URL
             */
            case 'url':
                // Bail if no Featured Image URL specified
                if ( empty( $settings['featured_image'] ) ) {
                    return new WP_Error( 'page_generator_pro_generate', __( 'No Featured Image URL was specified.', 'page-generator-pro' ) );
                }

                // Import
                $image_id = $this->base->get_class( 'import' )->import_remote_image( 
                    $settings['featured_image'],
                    $post_id,
                    $settings['featured_image_filename'],
                    $settings['featured_image_title'],
                    $settings['featured_image_caption'],
                    $settings['featured_image_alt'],
                    $settings['featured_image_description']
                );
                break;

            /**
             * Pexels
             */
            case 'pexels':
            case 'pixabay':
                // Bail if no Featured Image Term specified
                if ( empty( $settings['featured_image'] ) ) {
                    return new WP_Error( 'page_generator_pro_generate', __( 'No Featured Image Term was specified.', 'page-generator-pro' ) );
                }

                if ( $settings['featured_image_source'] == 'pexels' ) {
                    // If a Pexels API Key has been specified, use it instead of the class default.
                    $api_key = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-pexels', 'api_key' );
                    if ( ! empty( $api_key ) ) {
                        $this->base->get_class( 'pexels' )->set_api_key( $api_key );
                    }

                    // Run images query
                    $images = $this->base->get_class( 'pexels' )->photos_search( $settings['featured_image'] );
                } elseif ( $settings['featured_image_source'] == 'pixabay' ) {
                    // If a Pixabay API Key has been specified, use it instead of the class default.
                    $api_key = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-pixabay', 'api_key' );
                    if ( ! empty( $api_key ) ) {
                        $this->base->get_class( 'pixabay' )->set_api_key( $api_key );
                    }

                    // Run images query
                    $images = $this->base->get_class( 'pixabay' )->photos_search( $settings['featured_image'] );
                }

                // Bail if an error occured
                if ( is_wp_error( $images ) || ! is_array( $images ) || count( $images ) == 0 ) {
                    return $images;
                }

                // Pick an image at random from the resultset
                if ( count( $images ) == 1 ) {
                    $image_index = 0;
                } else {
                    $image_index = rand( 0, ( count( $images ) - 1 ) );
                } 

                // Import the image
                $image_id = $this->base->get_class( 'import' )->import_remote_image( 
                    $images[ $image_index ]['url'], 
                    $post_id, 
                    $settings['featured_image_filename'],
                    $settings['featured_image_title'],
                    $settings['featured_image_caption'],
                    $settings['featured_image_alt'],
                    $settings['featured_image_description']
                );
                break;

        }

        // Bail if no Image ID was defined
        if ( ! isset( $image_id ) ) {
            return false;
        }

        // Return the error if a WP_Error
        if ( is_wp_error( $image_id ) ) {
            return $image_id;
        }

        // Update Featured Image
        update_post_meta( $post_id, '_thumbnail_id', $image_id );

        return $image_id;

    }

    /**
     * Copies Custom Fields to the Generated Post ID, if
     * the Group Settings specify Custo Field data and (if overwriting)
     * whether the Custom Fields data should be overwritten.
     *
     * @since   2.3.5
     *
     * @param   int     $post_id    Generated Post ID
     * @param   array   $settings   Group Settings
     * @param   array   $post_args  wp_insert_post() / wp_update_post() arguments
     * @return  bool                Updated Custom Fields on Generated Post ID
     */
    private function custom_fields( $post_id, $settings, $post_args ) {

        // Bail if we're overwriting an existing Post and don't want to overwrite the Custom Fields
        if ( isset( $post_args['ID'] ) && ! array_key_exists( 'custom_fields', $settings['overwrite_sections'] ) ) {
            return false;
        }

        // Custom Fields
        if ( isset( $settings['meta'] )  && ! empty( $settings['meta'] ) ) {
            foreach ( $settings['meta']['key'] as $meta_index => $meta_key ) {
                update_post_meta( $post_id, $meta_key, $settings['meta']['value'][ $meta_index ] );
            }
        }

        // Store Keywords
        if ( $settings['store_keywords'] ) {
            $store_keywords = $this->get_keywords_terms_array_with_columns();

            if ( $store_keywords ) {
                foreach ( $store_keywords as $meta_key => $meta_value ) {
                    update_post_meta( $post_id, $meta_key, $meta_value );
                }
            }
        }

        return true;

    }

    /**
     * Copies the Content Group's Post Meta to the Generated Post ID,
     * including Page Builder / ACF data.
     *
     * @since   2.3.5
     *
     * @param   int     $post_id    Generated Post ID
     * @param   array   $settings   Group Settings
     * @param   array   $post_args  wp_insert_post() / wp_update_post() arguments
     * @return  bool                Updated Post Meta on Generated Post ID
     */
    private function post_meta( $post_id, $settings, $post_args ) {

        // Determine if we want to create/replace Page Builder Post Meta
        $copy_content_post_meta = ( isset( $post_args['ID'] ) && ! array_key_exists( 'post_content', $settings['overwrite_sections'] ) ? false : true ); 

        // Bail if no Post Meta to copy to the generated Post
        if ( ! isset( $settings['post_meta'] ) ) {
            return false;
        }

        // Iterate through Post Meta
        foreach ( $settings['post_meta'] as $meta_key => $meta_value ) {
            // Some meta keys need to be handled differently, or not copied if we're not overwriting the Content
            switch ( $meta_key ) {
                /**
                 * Elementor
                 */
                case '_elementor_data':
                    // Don't copy if we're not overwriting content
                    if ( ! $copy_content_post_meta ) {
                        break;
                    }

                    // Encode with slashes, just how Elementor does
                    $meta_value = wp_slash( wp_json_encode( $meta_value ) );
                    
                    // Store using update_metadata, just how Elementor does
                    update_metadata( 'post', $post_id, $meta_key, $meta_value );
                    break;

                /**
                 * Themify Builder
                 */
                case '_themify_builder_settings_json':
                    // Don't copy if we're not overwriting content
                    if ( ! $copy_content_post_meta ) {
                        break;
                    }

                    update_post_meta( $post_id, $meta_key, $meta_value );
                    break;

                /**
                 * Page Template
                 */
                case '_wp_page_template':
                    // Skip this, as it will overwrite the template that we define in the group settings
                    break;

                default:
                    update_post_meta( $post_id, $meta_key, $meta_value );
                    break;
            }
        }

        return true;
        
    }

    /**
     * Stores the Latitude and Longitude in the Geo table against the generated Post ID and Group ID
     * if the Latitude and Longitude exist and are valid values
     *
     * @since   2.3.6
     *
     * @param   int     $post_id    Generated Post ID
     * @param   int     $group_id   Group ID
     * @param   array   $settings   Group Settings
     * @return  mixed               WP_Error | bool
     */
    private function latitude_longitude( $post_id, $group_id, $settings ) {

        // Bail if we don't have a latitude or longitude
        if ( empty( $settings['latitude'] ) || empty( $settings['longitude'] ) ) {
            return false;
        }
        if ( ! $this->base->get_class( 'geo' )->is_latitude( $settings['latitude'] ) ) {
            return false;
        }
        if ( ! $this->base->get_class( 'geo' )->is_longitude( $settings['longitude'] ) ) {
            return false;
        }

        // Insert / Update Latitude and Longitude against this Post ID and Group ID
        return $this->base->get_class( 'geo' )->update(
            $post_id,
            $group_id,
            $settings['latitude'],
            $settings['longitude']
        );

    }

    /**
     * Returns an array of Taxonomies and Terms to assign to the Post
     *
     * @since   1.9.5
     *
     * @param   array   $taxonomy_terms     Taxonomies and Terms to assign to the Post
     * @param   string  $post_type          Post Type
     * @param   int     $post_id            Post ID
     * @return  mixed                       WP_Error | bool
     */
    private function assign_taxonomy_terms_to_post( $taxonomy_terms, $post_type, $post_id ) {

        // Get Post Type Taxonomies
        $taxonomies = $this->base->get_class( 'common' )->get_post_type_taxonomies( $post_type );

        // Bail if no Taxonomies exist
        if ( count( $taxonomies ) == 0 ) {
            return true;
        }

        // Iterate through Taxonomies
        foreach ( $taxonomies as $taxonomy ) {
            // Cleanup from last iteration
            unset( $terms );

            // Bail if no Terms exist for this Taxonomy
            if ( ! isset( $taxonomy_terms[ $taxonomy->name ] ) ) {
                continue;
            }
            if ( empty( $taxonomy_terms[ $taxonomy->name ] ) ) {
                continue;
            }
            if ( is_array( $taxonomy_terms[ $taxonomy->name ] ) && count( $taxonomy_terms[ $taxonomy->name ] ) == 0 ) {
                continue;
            }

            // Build Terms, depending on whether the Taxonomy is hierarchical or not
            switch ( $taxonomy->hierarchical ) { 
                case true:
                    foreach ( $taxonomy_terms[ $taxonomy->name ] as $tax_id => $terms_string ) {
                        // If Tax ID is not zero, the Term already exists in the Taxonomy
                        // Just add it to the Terms array
                        if ( $tax_id != 0 ) {
                            $terms[] = (int) $tax_id;
                            continue;
                        }

                        // Convert string to array
                        $terms_arr = str_getcsv( $terms_string, ',' );
                        foreach ( $terms_arr as $new_term ) {
                            // Check if this named term already exists in the taxonomy
                            $result = term_exists( $new_term, $taxonomy->name );
                            if ( $result !== 0 && $result !== null ) {
                                $terms[] = (int) $result['term_id'];
                                continue;
                            }

                            // Skip if the term is empty
                            if ( empty( $new_term ) ) {
                                continue; 
                            }

                            // Term does not exist in the taxonomy - create it
                            $result = wp_insert_term( $new_term, $taxonomy->name );
                            
                            // Skip if something went wrong
                            if ( is_wp_error( $result ) ) {
                                return $result;
                            }
                            
                            // Add to term IDs
                            $terms[] = (int) $result['term_id'];
                        }
                    }
                    break;

                case false:
                default:
                    $terms = $taxonomy_terms[ $taxonomy->name ];
                    break;
            }

            // If terms are not set or empty for this Taxonomy, continue
            if ( ! isset( $terms ) || empty( $terms ) ) {
                continue;
            }

            // Assign Terms to Post
            $result = wp_set_post_terms( $post_id, $terms, $taxonomy->name, false );

            // Bail if an error occured
            if ( is_wp_error( $result ) ) {
                return $result;
            }
        }

        // Terms assigned to Post successfully
        return true;

    }

    /**
     * Checks the given Group settings to see whether we need to process shortcodes on
     * the main Post Content.
     *
     * @since   1.9.5
     * 
     * @param   array   $settings   Group Settings
     * @return  bool                Process Shortcodes
     */
    private function should_process_shortcodes_on_post_content( $settings ) {

        // Assume that we will process shortcodes on the Post Content
        $process = true;

        // If the Group uses Elementor, don't process shortcodes
        if ( isset( $settings['post_meta'] ) && isset( $settings['post_meta']['_elementor_edit_mode'] ) && $settings['post_meta']['_elementor_edit_mode'] == 'builder' ) {
            return false;
        }

        // Return result
        return $process;

    }

    /**
     * Runs any actions once content generation has completed.
     *
     * @since   1.9.3
     *
     * @param   int     $group_id   Group ID
     * @param   bool    $test_mode  Test Mode
     */
    public function generate_content_finished( $group_id, $test_mode ) {

        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        // Elementor: Clear Cache
        if ( is_plugin_active( 'elementor/elementor.php' ) ) {
            Elementor\Plugin::$instance->files_manager->clear_cache();
        }

    }

    /**
     * Runs any actions once content generation has completed.
     *
     * @since   1.9.3
     *
     * @param   int     $group_id   Group ID
     * @param   bool    $test_mode  Test Mode
     */
    public function generate_terms_finished( $group_id, $test_mode ) {


    }

    /**
     * Main function to trash previously generated Contents
     * for the given Group ID
     *
     * @since   1.2.3
     *
     * @param   int     $group_id   Group ID
     * @return  mixed               WP_Error | Success
     */
    public function trash_content( $group_id ) {

        // Get all Post IDs generated by this Group
        $post_ids = $this->get_generated_content_post_ids( $group_id );

        // Bail if an error occured
        if ( is_wp_error( $post_ids ) ) {
            return $post_ids;
        }

        // Delete Posts by their IDs
        foreach ( $post_ids as $post_id ) {
            $result = wp_trash_post( $post_id );
            if ( ! $result ) {
                return new WP_Error( 'page_generator_pro_generate_trash_content', __( 'Unable to trash generated content with ID = ' . $post_id, 'page-generator-pro' ) );
            }
        }

        // Done
        return true;

    }

    /**
     * Main function to delete previously generated Contents
     * for the given Group ID
     *
     * @since   1.2.3
     *
     * @param   int     $group_id   Group ID
     * @return  mixed               WP_Error | Success
     */
    public function delete_content( $group_id ) {

        // Get all Post IDs generated by this Group
        $post_ids = $this->get_generated_content_post_ids( $group_id );

        // Bail if an error occured
        if ( is_wp_error( $post_ids ) ) {
            return $post_ids;
        }

        // Check if any Attachments belong to the given Post IDs, and if so, delete them
        // Attachments are given a Post ID when they're defined as a Featured Image
        $attachment_ids = $this->get_attachments_by_post_ids( $post_ids );
        if ( $attachment_ids !== false ) {
            foreach ( $attachment_ids as $attachment_id ) {
                wp_delete_attachment( $attachment_id );
            }
        }

        // Check if any Attachments belong to the given Group ID, and if so, delete them
        // Attachments are given a Group ID when they're within the content e.g. Unsplash shortcode
        $attachment_ids = $this->get_attachments_by_post_ids( array( $group_id ) );
        if ( $attachment_ids !== false ) {
            foreach ( $attachment_ids as $attachment_id ) {
                wp_delete_attachment( $attachment_id );
            }
        }

        // Delete Posts by their IDs
        foreach ( $post_ids as $post_id ) {
            $result = wp_delete_post( $post_id, true );
            if ( ! $result ) {
                return new WP_Error( 'page_generator_pro_generate_delete_content', __( 'Unable to delete generated content with ID = ' . $post_id, 'page-generator-pro' ) );
            }
        }

        return true;

    }

    /**
     * Deletes latitude and longitude from the Geo table when a Post is deleted.
     * 
     * Trashed Posts are unaffected.
     *
     * @since   2.3.6
     *
     * @param   int     $post_id    Post ID
     */
    public function delete_latitude_longitude_by_post_id( $post_id ) {

        $this->base->get_class( 'geo' )->delete( $post_id );

    }

    /**
     * Returns all Post IDs generated by the given Group ID
     *
     * @since   1.9.1
     *
     * @param   int     $group_id   Group ID
     * @return  mixed               WP_Error | array
     */
    private function get_generated_content_post_ids( $group_id ) {

        // Fetch valid Post Statuses that can be used when generating content
        $statuses = array_keys( $this->base->get_class( 'common' )->get_post_statuses() );

        // Get all Posts
        $posts = new WP_Query( array (
            'post_type'     => 'any',
            'post_status'   => $statuses,
            'posts_per_page'=> -1,
            'meta_query'    => array(
                array(
                    'key'   => '_page_generator_pro_group',
                    'value' => absint( $group_id ),
                ),
            ),
            'fields'        => 'ids',
        ) );

        // If no Posts found, return an error
        if ( count( $posts->posts ) == 0 ) {
            return new WP_Error( 'page_generator_pro_generate_get_generated_content_post_ids', __( 'No content has been generated by this group.', 'page-generator-pro' ) );
        }

        // Return Post IDs
        return $posts->posts;

    }

    /**
     * Returns all Attachment IDs belonging to the given Post IDs
     *
     * @since   1.9.7
     *
     * @param   array   $post_ids   Post IDs
     * @return  mixed               false | array
     */
    private function get_attachments_by_post_ids( $post_ids ) {

        // Get all Attachments belonging to the given Post IDs
        $attachments = new WP_Query( array(
            'post_type'         => 'attachment',
            'post_status'       => 'any',
            'posts_per_page'    => -1,
            'post_parent__in'   => $post_ids,
            'fields'            => 'ids',
        ) );

        // If no Attachments found, return false, as there's nothing to delete
        if ( count( $attachments->posts ) == 0 ) {
            return false;
        }

        // Return Attachment IDs
        return $attachments->posts;

    }

    /**
     * Main function to delete previously generated Terms
     * for the given Group ID
     *
     * @since   1.6.1
     *
     * @param   int     $group_id   Group ID
     * @return  mixed               WP_Error | Success
     */
    public function delete_terms( $group_id ) {

        // Get Settings
        $settings = $this->base->get_class( 'groups_terms' )->get_settings( $group_id );

        // Get all Terms
        $terms = new WP_Term_Query( array(
            'taxonomy'      => $settings['taxonomy'],
            'meta_query'    => array(
                array(
                    'key'   => '_page_generator_pro_group',
                    'value' => absint( $group_id ),
                ),
            ),
            'hide_empty'    => false,
            
            // For performance, just return the Post ID and don't update meta or term caches
            'fields'                => 'ids',
            'update_term_meta_cache'=> false,
        ) );

        // If no Terms found, return false, as there's nothing to delete
        if ( count( $terms->terms ) == 0 ) {
            return new WP_Error( 'page_generator_pro_generate_delete_terms', __( 'No Terms have been generated by this group, so there are no Terms to delete.', 'page-generator-pro' ) );
        }

        // Delete Terms by their IDs
        foreach ( $terms->terms as $term_id ) {
            $result = wp_delete_term( $term_id, $settings['taxonomy'] );
            if ( ! $result ) {
                return new WP_Error( 'page_generator_pro_generate_delete_terms', __( 'Unable to delete generated Term with ID = ' . $term_id, 'page-generator-pro' ) );
            }
        }

        // Done
        return true;

    }

    /**
     * Removes wp_update_post() $post_args that are not selected for overwriting
     *
     * @since   2.3.5
     *
     * @param   array   $overwrite_sections     Sections to Overwrite
     * @param   array   $post_args              wp_update_post() compatible Post Arguments
     * @return  array                           wp_update_post() compatible Post Arguments
     */
    private function restrict_by_overwrite_sections( $overwrite_sections, $post_args ) {

        // Fetch all available overwrite sections
        $all_possible_overwrite_sections = array_keys( $this->base->get_class( 'common' )->get_overwrite_sections() );
        $overwrite_sections_to_ignore = array_diff( $all_possible_overwrite_sections, $overwrite_sections );

        // If all overwrite sections are selected (i.e. no overwrite sections to ignore / skip), just return the post args
        if ( empty( $overwrite_sections_to_ignore ) ) {
            return $post_args;
        }

        // For each overwrite section to ignore / skip, remove it from the Post Args
        foreach ( $overwrite_sections_to_ignore as $overwrite_section_to_ignore ) {
            unset( $post_args[ $overwrite_section_to_ignore ] );
        }

        return $post_args;

    }

    /**
     * Returns an array of data relating to the successfully generated
     * Post or Term
     *
     * @since   2.1.8
     *
     * @param   int     $post_or_term_id        Post or Term ID
     * @param   string  $post_type_or_taxonomy  Post Type or Taxonomy
     * @param   bool    $generated              Post Generated (false = skipped)
     * @param   string  $message                Message to return (created, updated, skipped etc.)
     * @param   int     $start                  Start Time
     * @param   bool    $test_mode              Test Mode
     * @return                                  Success Data
     */
    private function generate_return( $post_or_term_id, $post_type_or_taxonomy, $generated, $message, $start, $test_mode ) {

        // Determine if we're returning data for a generated Post or Term
        if ( post_type_exists( $post_type_or_taxonomy ) ) {
            // Post
            if ( $test_mode ) {
                $url = get_bloginfo( 'url' ) . '?page_id=' . $post_or_term_id . '&preview=true';
            } else {
                $url = get_permalink( $post_or_term_id );
            }
        } else {
            // Term
            $url = get_term_link( $post_or_term_id, $post_type_or_taxonomy );
        }

        // Performance debugging
        $end = microtime( true );

        // Return the URL and keyword / term replacements used
        return array(
            'url'               => $url,
            'keywords_terms'    => $this->keywords_terms,
            'generated'         => $generated,
            'message'           => $message,

            // Performance data
            'start'             => $start,
            'end'               => $end,
            'duration'          => round ( ( $end - $start ), 2 ),
            'memory_usage'      => round( memory_get_usage() / 1024 / 1024 ),
            'memory_peak_usage' => round( memory_get_peak_usage() / 1024 / 1024 ),
        );

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.1.6
     * @deprecated  1.9.8
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'generate';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.9.8', 'Page_Generator_Pro()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Page_Generator_Pro()->get_class( $name );

    }

}