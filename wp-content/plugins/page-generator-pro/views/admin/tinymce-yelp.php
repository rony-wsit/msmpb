<form class="wpzinc-tinymce-popup">
    <div class="option">
        <div class="left">
            <strong><?php _e( 'Term', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_term' ); ?>
        </div>
        <div class="full">
            <input type="text" name="term" id="tinymce_unsplash_term" data-shortcode="term" placeholder="<?php _e( 'e.g. restaurants', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" required />
        </div>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Location', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_location' ); ?>
        </div>
        <div class="full">
            <input type="text" name="location" id="tinymce_unsplash_location" data-shortcode="location" placeholder="<?php _e( 'e.g. Birmingham, UK', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" required />
        </div>
    </div>

    <div class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-3">
        <div>
            <div class="full">
                <strong><?php _e( 'Radius', 'page-generator-pro' ); ?></strong>
            </div>
            <input type="number" min="0" max="20" step="1" name="radius" id="tinymce_radius" data-shortcode="radius" class="widefat" />
            <p class="description">
                <?php _e( 'The maximum radius, in miles, from the Location to search Business Listings for.', 'page-generator-pro' ); ?>
            </p>
        </div>

        <div>
            <div class="full">
                <strong><?php _e( 'Minimum Rating', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="minimum_rating" data-shortcode="minimum_rating" size="1">
                <option value="0"><?php _e( 'No Minimum Rating', 'page-generator-pro' ); ?></option>
                <?php
                for ( $i = 0.5; $i < 5; $i = $i + 0.5 ) {
                    ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
            <p class="description">
                <?php _e( 'The minimum rating a business listing must have to be displayed.', 'page-generator-pro' ); ?>
            </p>
        </div>

        <div>
            <div class="full">
                <strong><?php _e( 'Language', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="locale" data-shortcode="locale" size="1">
                <?php
                foreach ( $locales as $locale => $label ) {
                    ?>
                    <option value="<?php echo $locale; ?>"<?php selected( $locale, $current_locale ); ?>><?php echo $label; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>

    <div class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-3">
        <div>
            <div class="full">
                <strong><?php _e( 'Price', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="price" data-shortcode="price" size="1">
                <option value="0"><?php _e( 'Any Price Level', 'page-generator-pro' ); ?></option>
                <option value="1"><?php _e( '$', 'page-generator-pro' ); ?></option>
                <option value="2"><?php _e( '$$', 'page-generator-pro' ); ?></option>
                <option value="3"><?php _e( '$$$', 'page-generator-pro' ); ?></option>
                <option value="4"><?php _e( '$$$$', 'page-generator-pro' ); ?></option>
            </select>
        </div>

        <div>
            <div class="full">
                <strong><?php _e( 'Number of Listings', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="limit" data-shortcode="limit" size="1">
                <?php
                for ( $i = 1; $i <= 50; $i++ ) {
                    ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div>
            <div class="full">
                <strong><?php _e( 'Sort Listings', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="sort_by" data-shortcode="sort_by" size="1">
                <option value="best_match"><?php _e( 'Best Match', 'page-generator-pro' ); ?></option>
                <option value="rating"><?php _e( 'Rating', 'page-generator-pro' ); ?></option>
                <option value="review_count"><?php _e( 'Review Count', 'page-generator-pro' ); ?></option>
                <option value="description"><?php _e( 'Description', 'page-generator-pro' ); ?></option>
            </select>
        </div>
    </div>

    <div class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-5">
        <div>
            <div class="full">
                <strong><?php _e( 'Image', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="image" data-shortcode="image" size="1">
                <option value="1" selected><?php _e( 'Show', 'page-generator-pro' ); ?></option>
                <option value="0"><?php _e( 'Hide', 'page-generator-pro' ); ?></option>
            </select>
        </div>

        <div>
            <div class="full">
                <strong><?php _e( 'Rating', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="rating" data-shortcode="rating" size="1">
                <option value="1" selected><?php _e( 'Show', 'page-generator-pro' ); ?></option>
                <option value="0"><?php _e( 'Hide', 'page-generator-pro' ); ?></option>
            </select>
        </div>

        <div>
            <div class="full">
                <strong><?php _e( 'Categories', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="categories" data-shortcode="categories" size="1">
                <option value="1" selected><?php _e( 'Show', 'page-generator-pro' ); ?></option>
                <option value="0"><?php _e( 'Hide', 'page-generator-pro' ); ?></option>
            </select>
        </div>

        <div>
            <div class="full">
                <strong><?php _e( 'Phone Number', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="phone" data-shortcode="phone" size="1">
                <option value="1" selected><?php _e( 'Show', 'page-generator-pro' ); ?></option>
                <option value="0"><?php _e( 'Hide', 'page-generator-pro' ); ?></option>
            </select>
        </div>
    
        <div>
            <div class="full">
                <strong><?php _e( 'Address', 'page-generator-pro' ); ?></strong>
            </div>
            <select name="address" data-shortcode="address" size="1">
                <option value="1" selected><?php _e( 'Show', 'page-generator-pro' ); ?></option>
                <option value="0"><?php _e( 'Hide', 'page-generator-pro' ); ?></option>
            </select>
        </div>
    </div>

    <div class="option buttons">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-yelp" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>