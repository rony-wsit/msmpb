<div class="postbox">
    <h3 class="hndle"><?php _e( 'Generate', 'page-generator-pro' ); ?></h3>

    <div class="option">
        <p class="description">
            <?php _e( 'Specifies default behaviour when Generating Content and Terms.', 'page-generator-pro' ); ?>
        </p>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Stop on Error', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php
            $setting = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-generate', 'stop_on_error', '1' );
            ?>
            <select name="<?php echo $this->base->plugin->name; ?>-generate[stop_on_error]" size="1">
                <option value="1"<?php selected( $setting, '1' ); ?>><?php _e( 'Yes', 'page-generator-pro' ); ?></option>
                <option value="0"<?php selected( $setting, '0' ); ?>><?php _e( 'No', 'page-generator-pro' ); ?></option>
            </select>
        
            <p class="description">
                <?php _e( 'Whether to stop Content / Term Generation when an error occurs.', 'page-generator-pro' ); ?>
            </p>
        </div>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Use Performance Addon?', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php
            $setting = $this->base->get_class( 'settings' )->get_setting( $this->base->plugin->name . '-generate', 'use_mu_plugin', '0' );
            ?>
            <select name="<?php echo $this->base->plugin->name; ?>-generate[use_mu_plugin]" size="1">
                <option value="1"<?php selected( $setting, '1' ); ?>><?php _e( 'Yes', 'page-generator-pro' ); ?></option>
                <option value="0"<?php selected( $setting, '0' ); ?>><?php _e( 'No', 'page-generator-pro' ); ?></option>
            </select>
        
            <p class="description">
                <?php _e( 'Experimental: If enabled, uses the Performance Addon Must-Use Plugin.  This can improve generation times and reduce memory usage on sites with several Plugins.', 'page-generator-pro' ); ?>
            </p>
        </div>
    </div>
</div>