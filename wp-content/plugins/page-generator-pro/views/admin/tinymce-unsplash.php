<form class="wpzinc-tinymce-popup">
    <div class="option">
        <div class="left">
            <strong><?php _e( 'Term', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_term' ); ?>
        </div>
        <div class="full">
            <input type="text" name="term" id="tinymce_unsplash_term" data-shortcode="term" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
            <p class="description">
                <?php _e( 'The search term to use.  For example, "laptop" would return an image of a laptop.', 'page-generator-pro' ); ?>
            </p>
        </div>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Image Size', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <select name="size" data-shortcode="size" size="1">
                <?php
                $image_sizes = $this->base->get_class( 'common' )->get_unsplash_image_size_options();
                foreach ( $image_sizes as $image_size => $label ) {
                    ?>
                    <option value="<?php echo $image_size; ?>"><?php echo $label; ?></option>
                    <?php
                }
                ?>
            </select>
            <p class="description">
                <?php _e( 'The image size to output.', 'page-generator-pro' ); ?>
            </p>
        </div>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Image Orientation', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <select name="size" data-shortcode="orientation" size="1">
                <?php
                $image_orientations = $this->base->get_class( 'common' )->get_unsplash_image_orientation_options();
                foreach ( $image_orientations as $image_orientation => $label ) {
                    ?>
                    <option value="<?php echo $image_orientation; ?>"><?php echo $label; ?></option>
                    <?php
                }
                ?>
            </select>
            <p class="description">
                <?php _e( 'The image orientation to output.', 'page-generator-pro' ); ?>
            </p>
        </div>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Title', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_title' ); ?>
        </div>
        <div class="full">
            <input type="text" name="title" id="tinymce_unsplash_title" data-shortcode="title" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
        </div>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Caption', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_caption' ); ?>
        </div>
        <div class="full">
            <input type="text" name="caption" id="tinymce_unsplash_caption" data-shortcode="caption" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
        </div>
    </div>

    <div class="option">
        <div class="left">
            <strong><?php _e( 'Alt Tag', 'page-generator-pro' ); ?></strong>
        </div>
        <div class="right">
            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_alt_tag' ); ?>
        </div>
        <div class="full">
            <input type="text" name="alt_tag" id="tinymce_unsplash_alt_tag" data-shortcode="alt_tag" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
        </div>
    </div>

    <div class="option buttons">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-unsplash" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>