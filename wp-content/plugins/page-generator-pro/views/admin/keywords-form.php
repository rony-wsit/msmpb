<div class="wrap">
    <h1 class="wp-heading-inline">
        <?php echo $this->base->plugin->displayName; ?>

        <span>
        	<?php
        	if ( isset( $keyword ) && isset( $keyword['keywordID'] ) ) {
        		_e( 'Edit Keyword', 'page-generator-pro' );
        	} else {
        		_e( 'Add New Keyword', 'page-generator-pro' );
        	}
        	?>
        </span>
    </h1>

    <?php
    // Button Links
    require_once( 'keywords-links.php' );

    // Output Success and/or Error Notices, if any exist
    $this->base->get_class( 'notices' )->output_notices();
    ?>
    
    <div class="wrap-inner">
	    <div id="poststuff">
	    	<div id="post-body" class="metabox-holder columns-1">
	    		<!-- Content -->
	    		<div id="post-body-content">
	    			<!-- Form Start -->
	    			<form class="<?php echo $this->base->plugin->name; ?>" name="post" method="post" action="admin.php?page=<?php echo $page; ?>&amp;cmd=form<?php echo ( isset( $_GET['id'] ) ? '&id=' . absint( $_GET['id'] ) : '' ); ?>" enctype="multipart/form-data">		
		    	    	<div id="normal-sortables" class="meta-box-sortables ui-sortable">                        
			                <div id="keyword-panel" class="postbox">
			                    <h3 class="hndle"><?php _e( 'Keyword', 'page-generator-pro' ); ?></h3>
			                    <input type="hidden" name="id" id="id" value="<?php echo ( ( isset( $keyword ) && isset( $keyword['keywordID'] ) ) ? $keyword['keywordID'] : '' ); ?>" />
		
			                    <div class="option">
			                    	<div class="left">
			                    		<strong><?php _e( 'Keyword', 'page-generator-pro' ); ?></strong>
			                    	</div>
			                    	<div class="right">
			                    		<input type="text" name="keyword" value="<?php echo ( isset( $keyword['keyword'] ) ? $keyword['keyword'] : '' ); ?>" class="widefat" />
			                    	
				                    	<p class="description">
				                    		<?php _e( 'A unique template tag name, which can then be used when generating content.', 'page-generator-pro' ); ?>
				                    	</p>
			                    	</div>
			                    </div>

			                    <div class="option">
			                    	<div class="left">
			                    		<strong><?php _e( 'Terms', 'page-generator-pro' ); ?></strong>
			                    	</div>
			                    	<div class="right">
			                    		<textarea name="data" rows="10" class="widefat" style="height:300px"><?php echo ( isset( $keyword['data'] ) ? $keyword['data'] : '' ); ?></textarea>
			                    	
				                    	<p class="description">
				                    		<?php _e( 'Word(s) or phrase(s) which will be cycled through when generating content using the above keyword template tag.', 'page-generator-pro' ); ?>
				                    		<br />
				                    		<?php _e( 'One word / phase per line.', 'page-generator-pro' ); ?>
				                    		<br />
				                    		<?php _e( 'If no Terms are entered, the plugin will try to automatically determine a list of similar terms based on the supplied keyword when you click Save.', 'page-generator-pro' ); ?>
				                    	</p>
			                    	</div>
			                    </div>

			                    <div class="option">
			                    	<div class="left">
			                    		<strong><?php _e( 'Delimiter', 'page-generator-pro' ); ?></strong>
			                    	</div>
			                    	<div class="right">
			                    		<input type="text" name="delimiter" value="<?php echo ( isset( $keyword['delimiter'] ) ? $keyword['delimiter'] : '' ); ?>" class="widefat" />
			                    	
				                    	<p class="description">
				                    		<?php _e( 'Optional: If each Keyword Term comprises of two or more words, and you wish to access individual word(s) within each Term when using this Keyword in the Generate Content / Terms screens, define the seperating delimiter here.', 'page-generator-pro' ); ?><br />
				                    		<?php _e( 'For example, if your Keyword Terms are in the format City, County, ZIP Code, the delimiter would be a comma.', 'page-generator-pro' ); ?><br />
				                    	</p>
			                    	</div>
			                    </div>

			                    <div class="option">
			                    	<div class="left">
			                    		<strong><?php _e( 'Columns', 'page-generator-pro' ); ?></strong>
			                    	</div>
			                    	<div class="right">
			                    		<input type="text" name="columns" value="<?php echo ( isset( $keyword['columns'] ) ? $keyword['columns'] : '' ); ?>" class="widefat" />
			                    	
				                    	<p class="description">
				                    		<?php _e( 'Optional: If each Keyword Term comprises of two or more words, and you wish to access individual word(s) within each Term when using this Keyword in the Generate Content / Terms screens, define each column name here.', 'page-generator-pro' ); ?><br />
				                    		<?php _e( 'For example, if your Keyword Terms are in the format City, County, ZIP Code, you might enter <code>city,county,zipcode</code> here.', 'page-generator-pro' ); ?><br />
				                    		<?php _e( 'When generating content, you can then use e.g. {keyword(city)} for each City.', 'page-generator-pro' ); ?>
				                    	</p>
			                    	</div>
			                    </div>
			                    
			                    <div class="option">
			                    	<div class="left">
			                    		<strong><?php _e( 'Data Import', 'page-generator-pro' ); ?></strong>
			                    	</div>
			                    	<div class="right">
			                    		<input type="file" name="file" />
			                    	
				                    	<p class="description">
				                    		<?php _e( 'To mass import data, upload either a CSV file (format word1,word2,word3) or TXT file (one word / phrase per line).', 'page-generator-pro' ); ?>
											<br />
											<?php _e( 'This will append the imported words / phrases to the above Keyword Data.', 'page-generator-pro' ); ?>
				                    	</p>
			                    	</div>
			                    </div>
			                    
			                    <div class="option">
		                    		<?php wp_nonce_field( 'save_keyword', $this->base->plugin->name . '_nonce' ); ?>
		                			<input type="submit" name="submit" value="<?php _e( 'Save', 'page-generator-pro' ); ?>" class="button button-primary" />
			                    </div>
			                </div>
						</div>
						<!-- /normal-sortables -->
				    </form>
				    <!-- /form end -->
	    		</div>
	    		<!-- /post-body-content -->
	    	</div>
		</div>  
	</div>     
</div>