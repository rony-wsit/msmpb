<div class="option">
	<div class="full">
		<strong><?php _e( 'Image Source', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="full">
		<select name="<?php echo $this->base->plugin->name; ?>[featured_image_source]" size="1">
			<?php
			foreach ( $featured_image_sources as $featured_image_source => $label ) {
				?>
				<option value="<?php echo $featured_image_source; ?>"<?php selected( $this->settings['featured_image_source'], $featured_image_source ); ?>><?php echo $label; ?></option>
				<?php
			}
			?>
		</select>
	</div>
</div>

<!-- Media Library Image Options -->
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Title', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_title]" value="<?php echo $this->settings['featured_image_media_library_title']; ?>" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
    </div>

    <p class="description id">
        <?php _e( 'Fetch an image at random with a partial or full match to the given Title.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Caption', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_caption]" value="<?php echo $this->settings['featured_image_media_library_caption']; ?>" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
    </div>
    <p class="description id">
        <?php _e( 'Fetch an image at random with a partial or full match to the given Caption.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Alt Text', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_alt]" value="<?php echo $this->settings['featured_image_media_library_alt']; ?>" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
    </div>
    <p class="description id">
        <?php _e( 'Fetch an image at random with a partial or full match to the given Alt Text.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Description', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_description]" value="<?php echo $this->settings['featured_image_media_library_description']; ?>" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
    </div>
    <p class="description id">
        <?php _e( 'Fetch an image at random with a partial or full match to the given Description.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Operator', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <select name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_operator]" size="1">
            <?php
            foreach ( $operators as $operator => $label ) {
                ?>
                <option value="<?php echo $operator; ?>"<?php selected( $this->settings['featured_image_media_library_operator'], $operator ); ?>><?php echo $label; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <p class="description id">
        <?php _e( 'Determines whether images should match <b>all</b> or <b>any</b> of the Title, Caption, Alt Text and Descriptions specified above.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Image IDs', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_ids]" value="<?php echo $this->settings['featured_image_media_library_ids']; ?>" placeholder="<?php _e( 'e.g. 100, 150, 200', 'page-generator-pro' ); ?>" class="widefat" />
    </div>
    <p class="description id">
        <?php _e( 'Comma separated list of Media Library Image ID(s) to use.  If multiple image IDs are specified, one will be chosen at random for each generated Page', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Min. Image ID', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="number" name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_min_id]" value="<?php echo $this->settings['featured_image_media_library_min_id']; ?>" min="0" max="999999999" step="1" placeholder="<?php _e( 'e.g. 100', 'page-generator-pro' ); ?>" class="widefat" />
    </div>
    <p class="description id">
        <?php _e( 'Fetch an image whose ID matches or is greater than the given ID.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image id">
    <div class="left">
        <strong><?php _e( 'Max. Image ID', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="number" name="<?php echo $this->base->plugin->name; ?>[featured_image_media_library_max_id]" value="<?php echo $this->settings['featured_image_media_library_max_id']; ?>" min="0" max="999999999" step="1" placeholder="<?php _e( 'e.g. 200', 'page-generator-pro' ); ?>" class="widefat" />
    </div>
    <p class="description id">
        <?php _e( 'Fetch an image whose ID matches or is less than the given ID.', 'page-generator-pro' ); ?>
    </p>
</div>

<!-- Third Party Image Options -->
<div class="option featured_image url pexels pixabay">
	<div class="left">
		<strong class="featured_image url"><?php _e( 'URL', 'page-generator-pro' ); ?></strong>
        <strong class="featured_image pexels pixabay"><?php _e( 'Term', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="right">
		<input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image]" value="<?php echo $this->settings['featured_image']; ?>" class="widefat wpzinc-autocomplete" />
	</div>

	<p class="featured_image description url">
		<?php _e( 'Enter an image URL. This can be a dynamic image URL; the contents will be saved in the WordPress Media Library as an image.', 'page-generator-pro' ); ?>
	</p>
	<p class="featured_image description pexels pixabay">
		<?php _e( 'Enter a term, which the image should comprise of - for example, "building" or "animal". Each geneated page will use a different image based on this tag. You can use keyword tags and spintax here.', 'page-generator-pro' ); ?>
	</p>
</div>
<div class="option featured_image url pexels pixabay">
    <div class="left">
        <strong><?php _e( 'Title', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_title]" value="<?php echo $this->settings['featured_image_title']; ?>" class="widefat wpzinc-autocomplete" />
    </div>

    <p class="description">
        <?php _e( 'The title to assign to the image.  Note: it is up to your Theme to output this when it outputs your Featured Image.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image url pexels pixabay">
	<div class="left">
		<strong><?php _e( 'Alt Tag', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="right">
		<input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_alt]" value="<?php echo $this->settings['featured_image_alt']; ?>" class="widefat wpzinc-autocomplete" />
	</div>

	<p class="description">
		<?php _e( 'The alt tag to assign to the image.  Note: it is up to your Theme to output this when it outputs your Featured Image.', 'page-generator-pro' ); ?>
	</p>
</div>
<div class="option featured_image url pexels pixabay">
    <div class="left">
        <strong><?php _e( 'Caption', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_caption]" value="<?php echo $this->settings['featured_image_caption']; ?>" class="widefat wpzinc-autocomplete" />
    </div>

    <p class="description">
        <?php _e( 'The caption to assign to the image.  Note: it is up to your Theme to output this when it outputs your Featured Image.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image url pexels pixabay">
    <div class="left">
        <strong><?php _e( 'Description', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_description]" value="<?php echo $this->settings['featured_image_description']; ?>" class="widefat wpzinc-autocomplete" />
    </div>

    <p class="description">
        <?php _e( 'The description to assign to the image.  Note: it is up to your Theme to output this when it outputs your Featured Image.', 'page-generator-pro' ); ?>
    </p>
</div>
<div class="option featured_image url pexels pixabay">
    <div class="left">
        <strong><?php _e( 'Filename', 'page-generator-pro' ); ?></strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[featured_image_filename]" value="<?php echo $this->settings['featured_image_filename']; ?>" class="widefat wpzinc-autocomplete" />
    </div>

    <p class="description">
        <?php _e( 'Define the filename for the image, excluding the extension.', 'page-generator-pro' ); ?>
    </p>
</div>