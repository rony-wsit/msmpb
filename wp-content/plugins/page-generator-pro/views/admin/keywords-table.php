<div class="wrap">
    <h1 class="wp-heading-inline">
        <?php echo $this->base->plugin->displayName; ?>

        <span>
            <?php _e( 'Keywords', 'page-generator-pro' ); ?>
        </span>
    </h1>

    <?php
    // Button Links
    require_once( 'keywords-links.php' );

    // Search Subtitle
    if ( isset( $_REQUEST['s'] ) && ! empty( $_REQUEST['s'] ) ) {
        ?>
        <span class="subtitle left"><?php _e( 'Search results for', 'page-generator-pro' ); ?> &#8220;<?php echo urldecode( $_REQUEST['s'] ); ?>&#8221;</span>
        <?php
    }

    // Output Success and/or Error Notices, if any exist
    ?>

	<form action="admin.php?page=<?php echo $page; ?>" method="post" id="posts-filter">
		<p class="search-box">
	    	<label class="screen-reader-text" for="post-search-input"><?php _e(' Search Keywords', 'page-generator-pro' ); ?>:</label>
	    	<input type="text" id="field-search-input" name="s" value="<?php echo ( isset( $_REQUEST['s'] ) ? $_REQUEST['s'] : ''); ?>" />
	    	<input type="submit" name="search" class="button" value="<?php _e( 'Search Keywords', 'page-generator-pro' ); ?>" />
	    </p>
	    
		<?php   
		// Output WP_List_Table
		$keywords_table = new Page_Generator_Pro_Keywords_Table( $this->base );
		$keywords_table->prepare_items();
		$keywords_table->display(); 
		?>	
	</form>

</div><!-- /.wrap -->