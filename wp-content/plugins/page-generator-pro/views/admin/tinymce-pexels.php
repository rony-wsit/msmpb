<form class="wpzinc-tinymce-popup">
    <!-- Vertical Tabbed UI -->
    <div class="wpzinc-vertical-tabbed-ui">
        <!-- Tabs -->
        <ul class="wpzinc-nav-tabs wpzinc-js-tabs" data-panels-container="#pexels-container" data-panel=".pexels" data-active="wpzinc-nav-tab-vertical-active" data-match-height="#page-generator-pro-modal-body-body">
            <li class="wpzinc-nav-tab link">
                <a href="#pexels-search-parameters" class="wpzinc-nav-tab-vertical-active">
                    <?php _e( 'Search Parameters', $this->base->plugin->name ); ?>
                </a>
            </li>
            <li class="wpzinc-nav-tab tag">
                <a href="#pexels-output">
                    <?php _e( 'Output', $this->base->plugin->name ); ?>
                </a>
            </li>
        </ul>

        <!-- Content -->
        <div id="pexels-container" class="wpzinc-nav-tabs-content no-padding">

            <!-- Search Parameters -->
            <div id="pexels-search-parameters" class="pexels">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Search Parameters', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php _e( 'Defines search query parameters to fetch an image at random from Pexels.', 'page-generator-pro' ); ?>
                        </p>
                    </header>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Term', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pexels_term' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="term" id="tinymce_pexels_term" data-shortcode="term" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'The search term to use.  For example, "laptop" would return an image of a laptop.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Image Size', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="size" data-shortcode="size" size="1">
                                <?php
                                $image_sizes = $this->base->get_class( 'pexels' )->get_image_sizes();
                                foreach ( $image_sizes as $image_size => $label ) {
                                    ?>
                                    <option value="<?php echo $image_size; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'The image size to output.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Image Orientation', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="orientation" data-shortcode="orientation" size="1">
                                <?php
                                $image_orientations = $this->base->get_class( 'pexels' )->get_image_orientations();
                                foreach ( $image_orientations as $image_orientation => $label ) {
                                    ?>
                                    <option value="<?php echo $image_orientation; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'The image orientation to output.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.postbox -->
            </div>
            <!-- /Search Parameters -->

            <!-- Output -->
            <div id="pexels-output" class="pexels">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Output', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php _e( 'Defines output parameters for the Pexel image.', 'page-generator-pro' ); ?>
                        </p>
                    </header>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Title', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pexels_title' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="title" id="tinymce_pexels_title" data-shortcode="title" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the title for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Caption', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pexels_caption' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="caption" id="tinymce_pexels_caption" data-shortcode="caption" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the caption for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>
                    
                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Alt Tag', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pexels_alt_tag' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="alt_tag" id="tinymce_pexels_alt_tag" data-shortcode="alt_tag" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the alt text for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Description', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pexels_description' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="description" id="tinymce_pexels_description" data-shortcode="description" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the description for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Filename', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pexels_filename' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="filename" id="tinymce_pexels_filename" data-shortcode="filename" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the filename for the image, excluding the extension.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.postbox -->
            </div>
            <!-- /Output -->

        </div>
        <!-- /Content -->

    </div>

    <div class="option buttons">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-pexels" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>     