<form class="wpzinc-tinymce-popup">
    <!-- Vertical Tabbed UI -->
    <div class="wpzinc-vertical-tabbed-ui">
        <!-- Tabs -->
        <ul class="wpzinc-nav-tabs wpzinc-js-tabs" data-panels-container="#pixabay-container" data-panel=".pixabay" data-active="wpzinc-nav-tab-vertical-active" data-match-height="#page-generator-pro-modal-body-body">
            <li class="wpzinc-nav-tab link">
                <a href="#pixabay-search-parameters" class="wpzinc-nav-tab-vertical-active">
                    <?php _e( 'Search Parameters', $this->base->plugin->name ); ?>
                </a>
            </li>
            <li class="wpzinc-nav-tab tag">
                <a href="#pixabay-output">
                    <?php _e( 'Output', $this->base->plugin->name ); ?>
                </a>
            </li>
        </ul>

        <!-- Content -->
        <div id="pixabay-container" class="wpzinc-nav-tabs-content no-padding">

            <!-- Search Parameters -->
            <div id="pixabay-search-parameters" class="pixabay">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Search Parameters', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php _e( 'Defines search query parameters to fetch an image at random from pixabay.', 'page-generator-pro' ); ?>
                        </p>
                    </header>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Term', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pixabay_term' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="term" id="tinymce_pixabay_term" data-shortcode="term" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'The search term to use.  For example, "laptop" would return an image of a laptop.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Image Orientation', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="orientation" data-shortcode="orientation" size="1">
                                <?php
                                $image_orientations = $this->base->get_class( 'pixabay' )->get_image_orientations();
                                foreach ( $image_orientations as $image_orientation => $label ) {
                                    ?>
                                    <option value="<?php echo $image_orientation; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'The image orientation to output.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Language', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="language" data-shortcode="language" size="1">
                                <?php
                                $languages = $this->base->get_class( 'pixabay' )->get_languages();
                                foreach ( $languages as $language => $label ) {
                                    ?>
                                    <option value="<?php echo $language; ?>"<?php selected( $language, 'en' ); ?>><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'The language the search term is in.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Image Type', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="image_type" data-shortcode="image_type" size="1">
                                <?php
                                $image_types = $this->base->get_class( 'pixabay' )->get_image_types();
                                foreach ( $image_types as $image_type => $label ) {
                                    ?>
                                    <option value="<?php echo $image_type; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'The image type to search.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Image Category', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="category" data-shortcode="category" size="1">
                                <?php
                                $categories = $this->base->get_class( 'pixabay' )->get_categories();
                                foreach ( $categories as $category => $label ) {
                                    ?>
                                    <option value="<?php echo $category; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'The image category to search.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Image Color', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="color" data-shortcode="color" size="1">
                                <?php
                                $colors = $this->base->get_class( 'pixabay' )->get_colors();
                                foreach ( $colors as $color => $label ) {
                                    ?>
                                    <option value="<?php echo $color; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'Returns an image primarily comprising of the given color.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.postbox -->
            </div>
            <!-- /Search Parameters -->

            <!-- Output -->
            <div id="pixabay-output" class="pixabay">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Output', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php _e( 'Defines output parameters for the Pexel image.', 'page-generator-pro' ); ?>
                        </p>
                    </header>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Title', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pixabay_title' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="title" id="tinymce_pixabay_title" data-shortcode="title" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the title for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Caption', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pixabay_caption' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="caption" id="tinymce_pixabay_caption" data-shortcode="caption" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the caption for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>
                    
                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Alt Tag', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pixabay_alt_tag' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="alt_tag" id="tinymce_pixabay_alt_tag" data-shortcode="alt_tag" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the alt text for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Description', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pixabay_description' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="description" id="tinymce_pixabay_description" data-shortcode="description" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the description for the image.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Filename', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_pixabay_filename' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="filename" id="tinymce_pixabay_filename" data-shortcode="filename" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Define the filename for the image, excluding the extension.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.postbox -->
            </div>
            <!-- /Output -->

        </div>
        <!-- /Content -->

    </div>

    <div class="option buttons">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-pixabay" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>     