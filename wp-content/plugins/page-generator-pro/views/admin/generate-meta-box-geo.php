<div class="option">    
    <div class="left">
        <strong>
            <?php _e( 'Latitude', 'page-generator-pro' ); ?>
        </strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[latitude]" value="<?php echo $this->settings['latitude']; ?>" placeholder="<?php _e( 'Latitude', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
        
        <p class="description">
            <?php _e( 'Enter the Keyword that stores the Latitude.  This can be used by the Related Links Shortcode for displaying Related Links by Radius.', 'page-generator-pro' ); ?>
        </p>
    </div>
</div>

<div class="option">    
    <div class="left">
        <strong>
            <?php _e( 'Longitude', 'page-generator-pro' ); ?>
        </strong>
    </div>
    <div class="right">
        <input type="text" name="<?php echo $this->base->plugin->name; ?>[longitude]" value="<?php echo $this->settings['longitude']; ?>" placeholder="<?php _e( 'Longitude', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
    
        <p class="description">
            <?php _e( 'Enter the Keyword that stores the Longitude.  This can be used by the Related Links Shortcode for displaying Related Links by Radius.', 'page-generator-pro' ); ?>
        </p>
    </div>
</div>