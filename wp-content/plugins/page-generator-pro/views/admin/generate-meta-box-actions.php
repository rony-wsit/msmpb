<div class="option">
	<div class="full">
		<?php 
		// Nonce field
		wp_nonce_field( 'save_generate', $this->base->plugin->name . '_nonce' ); 
		?>


		<!-- 
		#submitpost is required, so WordPress can unload the beforeunload.edit-post JS event.
		If we didn't do this, the user would always get a JS alert asking them if they want to navigate
		away from the page as they may lose their changes
		-->
		<div class="submitbox" id="submitpost">
			<div id="publishing-action">
				<?php
				// Save
				if ( isset( $post ) && ( ! in_array( $post->post_status, array( 'publish', 'future', 'private' ) ) || 0 == $post->ID ) ) {
					// Publish
					?>
					<input name="original_publish" type="hidden" id="original_publish" value="<?php esc_attr_e( 'Publish' ) ?>" />
					<?php submit_button( __( 'Save' ), 'primary button-large', 'publish', false ); ?>
					<?php
				} else {
					// Update
					?>
					<input name="original_publish" type="hidden" id="original_publish" value="<?php esc_attr_e( 'Update' ) ?>" />
					<?php submit_button( __( 'Save' ), 'primary button-large', 'publish', false ); ?>
					<?php
				}

				// Test & Generate
				?>

				<?php submit_button( __( 'Test' ), 'primary button-large', 'test', false ); ?>

				<?php submit_button( __( 'Generate' ), 'primary button-large', 'generate', false ); ?>
			</div>
		</div>
	</div>
</div>
<?php
// Delete Generated Content, if any exist
if ( $this->settings['generated_pages_count'] > 0 ) {
	?>
	<div class="option">
		<div class="full">
			<?php
			if ( $this->settings['group_type'] == 'content' ) {
				submit_button( __( 'Trash Generated Content', 'page-generator-pro' ), 'trash-generated-content button-red', 'trash_generated_content', false );
			}

			submit_button( __( 'Delete Generated Content', 'page-generator-pro' ), 'delete-generated-content button-red', 'delete_generated_content', false );
			?>
		</div>
	</div>
	<?php	
}