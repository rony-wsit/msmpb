<form class="wpzinc-tinymce-popup">
    <!-- Vertical Tabbed UI -->
    <div class="wpzinc-vertical-tabbed-ui">
        <!-- Tabs -->
        <ul class="wpzinc-nav-tabs wpzinc-js-tabs" data-panels-container="#related-links-container" data-panel=".related-links" data-active="wpzinc-nav-tab-vertical-active" data-match-height="#page-generator-pro-modal-body-body">
            <li class="wpzinc-nav-tab link">
                <a href="#related-links-general" class="wpzinc-nav-tab-vertical-active">
                    <?php _e( 'General', $this->base->plugin->name ); ?>
                </a>
            </li>
            <li class="wpzinc-nav-tab tag">
                <a href="#related-links-taxonomy-conditions">
                    <?php _e( 'Taxonomies', $this->base->plugin->name ); ?>
                </a>
            </li>
            <li class="wpzinc-nav-tab database">
                <a href="#related-links-custom-field-conditions">
                    <?php _e( 'Custom Fields', $this->base->plugin->name ); ?>
                </a>
            </li>
            <li class="wpzinc-nav-tab circle">
                <a href="#related-links-radius">
                    <?php _e( 'Radius', $this->base->plugin->name ); ?>
                </a>
            </li>
        </ul>

        <!-- Content -->
        <div id="related-links-container" class="wpzinc-nav-tabs-content no-padding">
            
            <!-- General -->
            <div id="related-links-general" class="related-links">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'General', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php _e( 'Defines general settings for the output of Related Links.', 'page-generator-pro' ); ?>
                        </p>
                    </header>
                    
                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Groups', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="group_id[]" data-shortcode="group_id" size="1" multiple="multiple" class="widefat wpzinc-selectize-drag-drop">
                                <option value=""><?php _e( 'This Group', 'page-generator-pro' ); ?></option>
                                <?php
                                foreach ( $this->base->get_class( 'groups' )->get_all_ids_names() as $group_id => $group_name ) {
                                    ?>
                                    <option value="<?php echo $group_id; ?>"><?php echo $group_name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Output Type', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <select name="output_type" data-shortcode="output_type" size="1">
                                <option value="list_links"><?php _e( 'List of Links', 'page-generator-pro' ); ?></option>
                                <option value="prev_next"><?php _e( 'Parent, Next and Previous Links', 'page-generator-pro' ); ?></option>
                            </select>
                        </div>
                    </div>

                    <div id="list_links" class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-3">
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Link Title', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="text" name="link_title" data-shortcode="link_title" value="%title" class="widefat" />
                        </div>
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Number of Links', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="number" name="limit" data-shortcode="limit" value="5" min="0" max="9999999" step="1" placeholder="<?php _e( 'e.g. 5', 'page-generator-pro' ); ?>" class="widefat" />
                        </div>
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Number of Columns', 'page-generator-pro' ); ?></strong>
                            </div>
                            <select name="columns" data-shortcode="columns" size="1" class="widefat">
                                <?php
                                for ( $i = 1; $i <= 4; $i++ ) {
                                    ?>
                                    <option value="<?php echo $i; ?>">
                                        <?php
                                        if ( $i == 1 ) {
                                            _e( '1 Column', 'page-generator-pro' );
                                        } else {
                                            echo sprintf( __( '%s Columns', 'page-generator-pro' ), $i );
                                        }
                                        ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <p class="description full-width">
                            <?php _e( '%title will be replaced by the Post Title. %parent_title will be replaced by the Post\'s Parent Title.', 'page-generator-pro' ); ?>
                        </p>
                    </div>

                    <div class="option" style="display:none;"></div>

                    <div id="prev_next" class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-3">
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Parent Link: Title', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="text" name="parent_title" data-shortcode="parent_title" value="%title" class="widefat" />
                        </div>

                        <div>
                            <div class="full">
                                <strong><?php _e( 'Next Link: Title', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="text" name="next_title" data-shortcode="next_title" value="%title &raquo;" class="widefat" />
                        </div>

                        <div>
                            <div class="full">
                                <strong><?php _e( 'Previous Link: Title', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="text" name="prev_title" data-shortcode="prev_title" value="&laquo; %title" class="widefat" />
                        </div>

                        <p class="description full-width">
                            <?php _e( '%title will be replaced by the Parent Post Title.  Leave blank to not display a Parent Link.', 'page-generator-pro' ); ?>
                        </p>
                    </div>

                    <div class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-3">
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Post Type', 'page-generator-pro' ); ?></strong>
                            </div>
                            <select name="post_type" data-shortcode="post_type" size="1">
                                <option value=""><?php _e( '(all)', 'page-generator-pro' ); ?></option>
                                <?php
                                foreach ( $this->base->get_class( 'common' )->get_post_types() as $post_type ) {
                                    ?>
                                    <option value="<?php echo $post_type->name; ?>"><?php echo $post_type->label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <div class="full">
                                <strong><?php _e( 'Post Status', 'page-generator-pro' ); ?></strong>
                            </div>
                            <select name="post_status" data-shortcode="post_status" size="1">
                                <?php
                                foreach ( $this->base->get_class( 'common' )->get_post_statuses() as $post_status => $post_status_label ) {
                                    ?>
                                    <option value="<?php echo $post_status; ?>" <?php selected( $post_status, 'publish' ); ?>><?php echo $post_status_label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div>
                            <div class="full">
                                <strong><?php _e( 'Post Author ID', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="number" name="author" data-shortcode="author" min="1" max="999999" step="1" />
                        </div>
                    </div>

                    <div class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-2">
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Post Parent', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="text" name="post_parent" data-shortcode="post_parent" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'If specified, generated content that is a child of the given Parent will be displayed.  Supports ID, Slug or Keyword Slug', 'page-generator-pro' ); ?>
                            </p>
                        </div>

                        <div>
                            <div class="full">
                                <strong><?php _e( 'Post Name / Slug', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="text" name="post_name" data-shortcode="post_name" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'If specified, generated content that fully matches the given Post Name / Slug will be displayed.  Supports multiple Post Names separated by a comma.  Supports Keyword Slugs.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-2">
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Order Links By', 'page-generator-pro' ); ?></strong>
                            </div>
                            <select name="orderby" data-shortcode="orderby" size="1">
                                <?php
                                foreach ( $this->base->get_class( 'common' )->get_order_by_options() as $key => $label ) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'Distance requires Geolocation Data be completed in both this Content Group and the Group(s) defined above.', 'page-generator-pro' ); ?>
                            </p>
                        </div>

                        <div>
                            <div class="full">
                                <strong><?php _e( 'Order', 'page-generator-pro' ); ?></strong>
                            </div>
                            <select name="order" data-shortcode="order" size="1">
                                <?php
                                foreach ( $this->base->get_class( 'common' )->get_order_options() as $key => $label ) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $label; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.postbox -->
            </div>
            <!-- /General -->

            <!-- Taxonomy Conditions -->
            <div id="related-links-taxonomy-conditions" class="related-links">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Taxonomy Conditions', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php 
                            _e( 'When specified, links will only be displayed for Posts that belong to the given Taxonomy Term(s).<br />
                                Separate Terms with commas to specify multiple Terms.<br />
                                Term Names, IDs and Keywords are supported.', 'page-generator-pro' );
                            ?>
                        </p>
                    </header>

                    <!-- Taxonomies -->
                    <div id="related-links-taxonomies" class="is-sortable"></div>

                    <!-- Hidden Option -->
                    <div id="related-links-taxonomies-row" class="option hidden">  
                        <div class="left">
                            <select name="taxonomy[]" size="1" class="widefat taxonomy">
                                <?php
                                foreach ( $taxonomies as $taxonomy ) {
                                    ?>
                                    <option value="<?php echo $taxonomy->name; ?>"><?php echo $taxonomy->labels->name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="right">
                            <input type="text" name="taxonomy_term[]" data-shortcode="{.taxonomy}" placeholder="<?php _e( 'Term(s)', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                        </div>
                        <div>
                            <a href="#" class="delete-row">
                                <?php _e( 'Delete', 'page-generator-pro' ); ?>
                            </a>
                        </div>
                    </div>

                    <!-- Add -->  
                    <div class="option"> 
                        <button class="button add-row" data-container="#related-links-taxonomies" data-row="#related-links-taxonomies-row">
                            <?php _e( 'Add Taxonomy Condition', 'page-generator-pro' ); ?>
                        </button>
                    </div>
                </div>
                <!-- ./postbox -->
            </div>

            <!-- Custom Field Conditions -->
            <div id="related-links-custom-field-conditions" class="related-links">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Custom Field Conditions', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php 
                            _e( 'When specified, links will only be displayed for Posts that match the given Custom Field Key/Value pairings.<br />
                                Keywords are supported.', 'page-generator-pro' );
                            ?>
                        </p>
                    </header>

                    <!-- Custom Fields -->
                    <div id="related-links-custom-fields" class="is-sortable"></div>

                    <!-- Hidden Option -->
                    <div id="related-links-custom-fields-row" class="option hidden">  
                        <div class="left">
                            <input type="text" name="custom_field[]" placeholder="<?php _e( 'Meta Key', 'page-generator-pro' ); ?>" class="widefat custom-field" />
                        </div>
                        <div class="right">
                            <input type="text" name="custom_field_value[]" data-shortcode="{.custom-field}" data-shortcode-prepend="custom_field_" placeholder="<?php _e( 'Meta Value', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                        </div>
                        <div>
                            <a href="#" class="delete-row">
                                <?php _e( 'Delete', 'page-generator-pro' ); ?>
                            </a>
                        </div>
                    </div>

                    <!-- Add -->
                    <div class="option">
                        <button class="button add-row" data-container="#related-links-custom-fields" data-row="#related-links-custom-fields-row">
                            <?php _e( 'Add Custom Field Condition', 'page-generator-pro' ); ?>
                        </button>
                    </div>
                </div>
                <!-- ./postbox -->
            </div>

            <!-- Radius -->
            <div id="related-links-radius" class="related-links">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Radius', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php 
                            _e( 'When specified, links will only be displayed for Posts that have a Latitude and Longitude falling within the given distance.<br />
                                This Content Group must have Latitude and Longitude values specified in the Geolocation Data section.<br />
                                The target Content Group(s) must have Latitude and Longitude values specified in the Geolocation Data section.', 'page-generator-pro' );
                            ?>
                        </p>
                    </header>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Radius (miles)', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <input type="number" name="radius" data-shortcode="radius" value="" min="0" max="9999999" step="1" placeholder="<?php _e( 'e.g. 5', 'page-generator-pro' ); ?>" class="widefat" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Content -->
    </div>
    <!-- /Vertical Tabbed UI -->

    <div class="option buttons has-wpzinc-vertical-tabbed-ui">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-related-links" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>