<!-- Permalink -->
<div class="option">
	<div class="left">
		<strong><?php _e( 'Permalink', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="right">
		<?php $this->base->get_class( 'keywords' )->output_dropdown( $this->keywords, 'permalink' ); ?>
	</div>
	<div class="full">
		<input type="text" id="permalink" name="<?php echo $this->base->plugin->name; ?>[permalink]" value="<?php echo $this->settings['permalink']; ?>" class="widefat wpzinc-autocomplete" />
	
    	<p class="description">
    		<?php _e( 'Letters, numbers, underscores and dashes only.', 'page-generator-pro' ); ?>
    	</p>
	</div>
</div>