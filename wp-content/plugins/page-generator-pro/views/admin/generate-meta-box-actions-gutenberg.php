<div class="option">
	<div class="full">
		<?php 
		// Nonce field
		wp_nonce_field( 'save_generate', $this->base->plugin->name . '_nonce' ); 
		?>
		
		<span class="test">
			<a href="<?php echo admin_url( 'edit.php?post_type=' . $this->base->get_class( 'post_type' )->post_type_name . '&' . $this->base->plugin->name . '-action=test&id=' . $post->ID . '&type=' . $this->settings['group_type'] ); ?>" class="button button-primary"><?php _e( 'Test' ); ?></a>
		</span>

		<span class="generate">
			<a href="<?php echo admin_url( 'admin.php?page=' . $this->base->plugin->name . '-generate&id=' . $post->ID . '&type=' . $this->settings['group_type'] ); ?>" class="button button-primary"><?php _e( 'Generate' ); ?></a>
		</span>
	</div>
</div>
<?php
// Delete Generated Content, if any exist
if ( $this->settings['generated_pages_count'] > 0 ) {
	?>
	<div class="option">
		<div class="full">
			<?php
			if ( $this->settings['group_type'] == 'content' ) {
				?>
				<span class="trash_generated_content">
					<a href="<?php echo admin_url( 'edit.php?post_type=' . $this->base->get_class( 'post_type' )->post_type_name . '&' . $this->base->plugin->name . '-action=trash-generated-content&id=' . $post->ID . '&type=' . $this->settings['group_type'] ); ?>" class="button button-red trash-generated-content"><?php _e( 'Trash Generated Content' ); ?></a>
				</span>
				<?php
			}
			?>
			<span class="delete_generated_content">
				<a href="<?php echo admin_url( 'edit.php?post_type=' . $this->base->get_class( 'post_type' )->post_type_name . '&' . $this->base->plugin->name . '-action=delete-generated-content&id=' . $post->ID . '&type=' . $this->settings['group_type'] ); ?>" class="button button-red delete-generated-content"><?php _e( 'Delete Generated Content' ); ?></a>
			</span>
		</div>
	</div>
	<?php	
}

// JS Overlay
require_once( 'js-overlay.php' );