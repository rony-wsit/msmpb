<form class="wpzinc-tinymce-popup">
    <div>
        <div class="option">
            <div class="left">
                <strong><?php _e( 'Location', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_open_street_map_location' ); ?>
            </div>
            <div class="full">
                <input type="text" name="location" id="tinymce_open_street_map_location" data-shortcode="location" value="" placeholder="<?php _e( 'e.g. Birmingham', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" required />
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Country', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <select name="country_code" data-shortcode="country_code" size="1">
                    <option value=""></option>
                    <?php
                    $countries = $this->base->get_class( 'common' )->get_countries();
                    foreach ( $countries as $country_code => $country_name ) {
                        ?>
                        <option value="<?php echo $country_code; ?>"><?php echo $country_name; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Height', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <input type="number" name="height" data-shortcode="height" min="1" max="9999" step="1" value="250" required />px
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Zoom Level', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <select name="zoom" data-shortcode="zoom" size="1">
                    <?php
                    for ( $i = 1; $i <= 20; $i++ ) {
                        ?>
                        <option value="<?php echo $i; ?>"<?php selected( 14, $i, true ); ?>><?php echo $i; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <p class="description">
                    <?php _e( 'A higher number means a higher zoom level, showing more detail. As a guide, 1 = World; 20 = Buildings', 'page-generator-pro' ); ?>
                </p>
            </div>
        </div>

        <div class="option buttons">
            <div class="left">
                <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
            </div>
            <div class="right">
                <input type="hidden" name="shortcode" value="page-generator-pro-open-street-map" />
                <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
            </div>
        </div>
    </div>
</form>