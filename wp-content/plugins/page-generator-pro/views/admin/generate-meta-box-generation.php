 <div class="option">
	<div class="left">
		<strong><?php _e( 'Method', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="right">
		<select name="<?php echo $this->base->plugin->name; ?>[method]" size="1">
			<?php
			if ( is_array( $methods ) && count( $methods ) > 0 ) {
				foreach ( $methods as $method => $label ) {
					?>
					<option value="<?php echo $method; ?>"<?php selected( $this->settings['method'], $method ); ?>>
						<?php echo $label; ?>
					</option>
					<?php
				}
			}
			?>
		</select>
	</div>
	<p class="description">
		<?php echo sprintf( __( '<strong>All:</strong> Generates %s for all possible combinations of terms across all keywords used.', 'page-generator-pro' ), $labels['plural'] ); ?>
	</p>
	<p class="description">
		<?php _e( '<strong>Sequential:</strong> Honors the order of terms in each keyword used. Once all terms have been used in a keyword, the generator stops.', 'page-generator-pro' ); ?>
	</p>
	<p class="description">
		<?php echo sprintf( __( '<strong>Random:</strong> For each %s generated, selects a term at random from each keyword used.', 'page-generator-pro' ), $labels['singular'] ); ?>
	</p>
</div>

<div class="option">
	<div class="left">
		<strong><?php _e( 'Spin Content?', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="right">
		<select name="<?php echo $this->base->plugin->name; ?>[apply_synonyms]" size="1">
			<option value="1"<?php selected( $this->settings['apply_synonyms'], 1 ); ?>><?php _e( 'Yes', 'page-generator-pro' ); ?></option>
			<option value="0"<?php selected( $this->settings['apply_synonyms'], 0 ); ?>><?php _e( 'No', 'page-generator-pro' ); ?></option>
		</select>
	</div>

	<p class="description">
		<?php echo sprintf( __( 'If enabled, the Visual Editor\'s content will be spun for each generated %s. If you have already defined spintax, do not use this option.', 'page-generator-pro' ), $labels['singular'] ); ?>
	</p>
</div>

<div class="option">
	<div class="left">
		<strong><?php _e( 'Overwrite', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="right">
		<select name="<?php echo $this->base->plugin->name; ?>[overwrite]" size="1">
			<?php
			if ( is_array( $overwrite_methods ) && count( $overwrite_methods ) > 0 ) {
				foreach ( $overwrite_methods as $method => $label ) {
					?>
					<option value="<?php echo $method; ?>"<?php selected( $this->settings['overwrite'], $method ); ?>>
						<?php echo $label; ?>
					</option>
					<?php
				}
			}
			?>
		</select>
	</div>
	<p class="description">
		<?php 
		echo sprintf( 
			__( 'See the %s to understand each available option.', 'page-generator-pro' ),
			'<a href="' . $this->base->plugin->documentation_url . '/generate-content/#fields--generate">Documentation</a>'
		);
		?>
	</p>
</div>

<div class="option overwrite-sections overwrite overwrite_any overwrite_preserve_date overwrite_any_preserve_date">
	<div class="full">
		<strong><?php _e( 'Overwrite Sections', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="full">		
		<ul class="checklist">                    			
			<?php
			foreach ( $overwrite_sections as $key => $label ) {
	            ?>
	            <li>
					<label>
						<input type="checkbox" name="<?php echo $this->base->plugin->name; ?>[overwrite_sections][<?php echo $key; ?>]" value="1"<?php echo ( isset( $this->settings['overwrite_sections'][ $key ] ) ? ' checked' : '' ); ?> />
						<?php echo $label; ?>
						<br />  
					</label>
				</li>
	            <?php
			}	
			?>
		</ul>
	</div>
	<p class="description">
		<?php _e( 'If generation would overwrite an existing Page, choose which items of the existing Page to overwrite.', 'page-generator-pro' ); ?>
	</p>
</div>

<div class="option">
	<div class="left">
		<strong><?php echo sprintf( __( 'No. %s', 'page-generator-pro' ), $labels['plural'] ); ?></strong>
	</div>
	<div class="right">
		<input type="number" name="<?php echo $this->base->plugin->name; ?>[numberOfPosts]" value="<?php echo $this->settings['numberOfPosts']; ?>" step="1" min="0" class="widefat" />
	</div>
	<p class="description">
		<?php echo sprintf( __( 'The number of %s to generate. If zero or blank, all %s will be generated.', 'page-generator-pro' ), $labels['plural'], $labels['plural'] ); ?>
	</p>
</div>

<div class="option">
	<div class="left">
		<strong><?php _e( 'Resume Index', 'page-generator-pro' ); ?></strong>
	</div>
	<div class="right">
		<input type="number" name="<?php echo $this->base->plugin->name; ?>[resumeIndex]" value="<?php echo $this->settings['resumeIndex']; ?>" step="1" min="0" class="widefat" />
	</div>
	<p class="description">
		<?php _e( 'Optional: If generation did not fully complete (e.g. 50 / 100 only), or you specified a limit, you can set the Resume Index = 50.', 'page-generator-pro' ); ?>
	</p>
</div>