<form class="wpzinc-tinymce-popup">
    <!-- Vertical Tabbed UI -->
    <div class="wpzinc-vertical-tabbed-ui">
        <!-- Tabs -->
        <ul class="wpzinc-nav-tabs wpzinc-js-tabs" data-panels-container="#media-library-container" data-panel=".media-library" data-active="wpzinc-nav-tab-vertical-active" data-match-height="#page-generator-pro-modal-body-body">
            <li class="wpzinc-nav-tab link">
                <a href="#media-library-search-parameters" class="wpzinc-nav-tab-vertical-active">
                    <?php _e( 'Search Parameters', $this->base->plugin->name ); ?>
                </a>
            </li>
            <li class="wpzinc-nav-tab tag">
                <a href="#media-library-output">
                    <?php _e( 'Output', $this->base->plugin->name ); ?>
                </a>
            </li>
        </ul>

        <!-- Content -->
        <div id="media-library-container" class="wpzinc-nav-tabs-content no-padding">

            <!-- Search Parameters -->
            <div id="media-library-search-parameters" class="media-library">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Search Parameters', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php _e( 'Defines search query parameters to fetch an image at random from the Media Library.', 'page-generator-pro' ); ?>
                        </p>
                    </header>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Title', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_media_library_title' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="title" id="tinymce_media_library_title" data-shortcode="title" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Fetch an image at random with a partial or full match to the given Title.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Caption', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_media_library_caption' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="caption" id="tinymce_media_library_caption" data-shortcode="caption" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Fetch an image at random with a partial or full match to the given Caption.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Alt Text', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_media_library_alt' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="alt" id="tinymce_media_library_alt" data-shortcode="alt" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Fetch an image at random with a partial or full match to the given Alt Text.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Description', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_media_library_description' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="description" id="tinymce_media_library_description" data-shortcode="description" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                            <p class="description">
                                <?php _e( 'Fetch an image at random with a partial or full match to the given Description.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option wpzinc-tinymce-popup-columns wpzinc-tinymce-popup-columns-2">
                        <div>
                            <div class="full">
                                <strong><?php _e( 'Image IDs', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="text" name="ids" data-shortcode="ids" value="" placeholder="<?php _e( 'e.g. 100, 150, 200', 'page-generator-pro' ); ?>" class="widefat" />
                            <p class="description">
                                <?php _e( 'Comma separated list of Media Library Image ID(s) to use.  If multiple image IDs are specified, one will be chosen at random for each generated Page', 'page-generator-pro' ); ?>
                            </p>
                        </div>

                        <div>
                            <div class="full">
                                <strong><?php _e( 'Image ID Range', 'page-generator-pro' ); ?></strong>
                            </div>
                            <input type="number" name="min_id" data-shortcode="min_id" min="0" max="999999999" step="1" value="" placeholder="<?php _e( 'e.g. 100', 'page-generator-pro' ); ?>" />
                            -
                            <input type="number" name="max_id" data-shortcode="max_id" min="0" max="999999999" step="1" value="" placeholder="<?php _e( 'e.g. 200', 'page-generator-pro' ); ?>" />
                            <p class="description">
                                <?php _e( 'Fetch an image whose ID matches or is within the given range.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.postbox -->
            </div>
            <!-- /Search Parameters -->

            <!-- Output -->
            <div id="media-library-output" class="media-library">
                <div class="postbox">
                    <header>
                        <h3><?php _e( 'Output', 'page-generator-pro' ); ?></h3>
                        <p class="description">
                            <?php _e( 'Defines output parameters for the Media Library image.', 'page-generator-pro' ); ?>
                        </p>
                    </header>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Image Size', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php
                            $image_sizes = $this->base->get_class( 'common' )->get_media_library_image_size_options();
                            ?>
                            <select name="size" data-shortcode="size" size="1">
                                <?php
                                foreach ( $image_sizes as $key => $image_size ) {
                                    ?>
                                    <option value="<?php echo $image_size; ?>"><?php echo $image_size; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="description">
                                <?php _e( 'The image size to output.', 'page-generator-pro' ); ?>
                            </p>
                        </div>
                    </div>

                    <div class="option">
                        <div class="left">
                            <strong><?php _e( 'Alt Text', 'page-generator-pro' ); ?></strong>
                        </div>
                        <div class="right">
                            <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_media_library_alt_tag' ); ?>
                        </div>
                        <div class="full">
                            <input type="text" name="alt_tag" id="tinymce_media_library_alt_tag" data-shortcode="alt_tag" value="" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
                        </div>
                    </div>
                </div>
                <!-- /.postbox -->
            </div>
            <!-- /Output -->

        </div>
        <!-- /Content -->

    </div>

    <div class="option buttons">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-media-library" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>     