<form class="wpzinc-tinymce-popup">
    <div>
        <div class="option">
            <div class="left">
                <strong><?php _e( 'Term(s)', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_wikipedia_term' ); ?>
            </div>
            <div class="full">
                <input type="text" name="term" id="tinymce_wikipedia_term" data-shortcode="term" data-delimiter=";" class="wpzinc-selectize-freeform" />
                <p class="description">
                    <?php 
                    _e( 'Specify one or more terms to search for on Wikipedia, in order.  Contents will be used from the first term that produces a matching Wikipedia Page', 'page-generator-pro' );
                    ?>
                </p>
            </div>
        </div>

        <div class="option">
            <div class="full">
                <strong><?php _e( 'Sections', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="full">
                <input type="text" name="sections" data-shortcode="sections" data-delimiter=";" class="wpzinc-selectize-freeform" />
                <p class="description">
                    <?php 
                    echo sprintf(
                        __( 'Optional; specify one or more Wikipedia top level Table of Content sections to pull content from.  
                            If no sections are specified, the summary (text before the Table of Contents) will be used. 
                            Refer to the <a href="%s" target="_blank">Documentation</a> for more information.', 'page-generator-pro' ),
                        'https://www.wpzinc.com/documentation/page-generator-pro/generate-shortcodes-wikipedia-content/'
                    );
                    ?>
                </p>
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Use Similar Page', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <select name="use_similar_page" data-shortcode="use_similar_page" size="1">
                    <option value="" selected><?php _e( 'No', 'page-generator-pro' ); ?></option>
                    <option value="1"><?php _e( 'Yes', 'page-generator-pro' ); ?></option>
                </select>
                <p class="description">
                    <?php 
                    echo sprintf(
                        __( 'If enabled, a similar Wikipedia Article will be used where a Term specified above could not be found, and Wikipedia provides 
                            alternate Articles when viewing said Term.
                            Refer to the <a href="%s" target="_blank">Documentation</a> for more information.', 'page-generator-pro' ),
                        'https://www.wpzinc.com/documentation/page-generator-pro/generate-shortcodes-wikipedia-content/'
                    );
                    ?>
                </p>
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Max. Paragraphs', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <input type="number" name="paragraphs" data-shortcode="paragraphs" min="0" max="999" step="1" value="5" />

                <p class="description">
                    <?php _e( 'The maximum number of paragraphs to output after all above sections have been fetched and combined.', 'page-generator-pro' ); ?>
                </p>
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Spin?', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <select name="apply_synonyms" data-shortcode="apply_synonyms" size="1">
                    <option value=""><?php _e( 'No', 'page-generator-pro' ); ?></option>
                    <option value="1"><?php _e( 'Yes', 'page-generator-pro' ); ?></option>
                </select>

                <p class="description">
                    <?php _e( 'If enabled, the Wikipedia content will be spun to produce a unique variation.', 'page-generator-pro' ); ?>
                </p>
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Language', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <select name="language" data-shortcode="language" size="1">
                    <?php
                    // Get Wikipedia Languages
                    $languages = $this->base->get_class( 'common' )->get_wikipedia_languages();
                    foreach ( $languages as $key => $value ) {
                        ?>
                        <option value="<?php echo $key; ?>"<?php selected( $key, 'en' ); ?>><?php echo $value; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>

    <div class="option buttons">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-wikipedia" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>