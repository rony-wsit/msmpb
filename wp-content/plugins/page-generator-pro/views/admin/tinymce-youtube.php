<form class="wpzinc-tinymce-popup">
    <div>
        <div class="option">
            <div class="left">
                <strong><?php _e( 'Term', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_term' ); ?>
            </div>
            <div class="full">
                <input type="text" name="term" id="tinymce_unsplash_term" data-shortcode="term" placeholder="<?php _e( 'e.g. building', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
            </div>
        </div>

        <div class="option">
            <div class="left">
                <strong><?php _e( 'Location (optional)', 'page-generator-pro' ); ?></strong>
            </div>
            <div class="right">
                <?php $this->base->get_class( 'keywords' )->output_dropdown( $keywords, 'tinymce_unsplash_location' ); ?>
            </div>
            <div class="full">
                <input type="text" name="location" id="tinymce_unsplash_location" data-shortcode="location" value="" placeholder="<?php _e( 'e.g. Birmingham, UK', 'page-generator-pro' ); ?>" class="widefat wpzinc-autocomplete" />
            </div>
        </div>
    </div>

    <div class="option buttons">
        <div class="left">
            <button type="button" class="close button"><?php _e( 'Cancel', 'page-generator-pro' ); ?></button>
        </div>
        <div class="right">
            <input type="hidden" name="shortcode" value="page-generator-pro-youtube" />
            <input name="submit" type="submit" value="<?php _e( 'Insert', 'page-generator-pro' ); ?>" class="button button-primary right" />
        </div>
    </div>
</form>