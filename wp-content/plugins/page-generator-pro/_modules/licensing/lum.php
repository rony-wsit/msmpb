<?php
/**
 * Licensing and Update Manager Class
 * 
 * @package      Licensing Update Manager
 * @author       Tim Carr
 * @version      3.0.0
 * @copyright    WP Zinc
 */
class LicensingUpdateManager {

    /**
     * Flag to determine if we've queried the remote endpoint
     * for updates. Prevents plugin update checks running
     * multiple times
     *
     * @since   1.0.0
     *
     * @var     boolean
     */
    public $update_check = false;

    /**
     * Constructor.
     *
     * @since   1.0.0
     * 
     * @param   object  $plugin    WordPress Plugin
     * @param   string  $endpoint  Licensing Endpoint
     */
    public function __construct( $plugin, $endpoint ) {

        global $pagenow;

        // Set Plugin and Endpoint
        $this->plugin = $plugin;
        $this->endpoint = $endpoint;
        
        // Admin Notice
        $this->notice = new stdClass;

        if ( is_admin() ) {
            /**
             * Updates
             * - Delete cache if we're forcing an update check via WordPress Admin > Updates
             */
            if ( $pagenow == 'update-core.php' && isset( $_GET['force-check'] ) ) {
                $this->cache_delete();
            }

            /**
             * Licensing Screen
             */
            if ( isset( $_GET['page'] ) && sanitize_text_field( $_GET['page'] ) == $this->plugin->name ) {
                // Force a license key check if we're on the Licensing screen

                if ( isset( $_POST[ $this->plugin->name ] ) && is_array( $_POST[ $this->plugin->name ] ) && array_key_exists( 'licenseKey', $_POST[ $this->plugin->name ] ) ) {
                    // Save the new license key before checking
                    update_option( $this->plugin->name . '_licenseKey', sanitize_text_field( $_POST[ $this->plugin->name ]['licenseKey'] ) );
                }

                // Force license key check
                $this->check_license_key_valid( true );
            } else {
                // Check license key, trusting cache
                $this->check_license_key_valid( false );
            }

            // Hooks and Filters
            add_action( 'admin_notices', array( $this, 'admin_notices' ) );
            add_filter( 'plugins_api', array( $this, 'plugins_api' ), 10, 3 );
            add_filter( 'all_plugins', array( $this, 'maybe_filter_plugin_name' ) );
        } else {
            // Check license key, trusting cache
            $this->check_license_key_valid( false );
        }

        // Check for updates, outside of is_admin() so WP-CLI is supported
        add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'api_check' ), 50 );
        add_action( 'delete_site_transient_update_plugins', array( $this, 'cache_delete' ) );
    
    }

    /**
     * Outputs Administration Notices relating to license key validation
     *
     * @since 3.0.0
     */
    public function admin_notices() {
        
        // If no message has been set, bail
        if ( ! isset( $this->notice->message ) ) {
            return false;
        }

        // Only show message if an error, or a success + we are on the licensing screen
        $screen = get_current_screen();
        if ( ( ! isset( $this->notice->error ) || $this->notice->error == 0 ) && $screen->base != 'toplevel_page_' . $this->plugin->name ) {
            // Success message, but we're not on the licensing screen - bail
            return false;
        }

        // Output message
        echo ( '<div class="notice ' . ( ( isset( $this->notice->error ) && $this->notice->error == 1 ) ? 'error' : 'updated' ) . '">
            <p>' . $this->notice->message . '</p>
        </div>' );
    
    }

    /**
     * Gets the license key from either the wp-config constant, or the options table
     *
     * @since   3.0.0
     *
     * @return  string  License Key
     */
    public function get_license_key() {

        // If the license key is defined in wp-config, use that
        if ( $this->is_license_key_a_constant() ) {
            // Get from wp-config
            $license_key = constant( strtoupper( $this->plugin->name ) . '_LICENSE_KEY' );
        } else {
            // Get from options table
            $license_key = get_option( $this->plugin->name . '_licenseKey' );
        }

        return $license_key;

    }

    /**
     * Returns a flag denoting whether the license key is stored as a PHP constant
     *
     * @since   3.0.0
     *
     * @return  bool
     */
    public function is_license_key_a_constant() {

        return defined( strtoupper( $this->plugin->name ) . '_LICENSE_KEY' );

    }
    
    /**
     * Checks whether a license key has been specified in the settings table.
     * 
     * @since   3.0.0
     *
     * @return  bool    License Key Exists
     */                   
    public function check_license_key_exists() {

        // Get license key
        $license_key = $this->get_license_key();
        
        // Return license key
        return ( ( isset( $license_key ) && trim( $license_key ) != '' ) ? true : false );
    
    }   
    
    /**
     * Checks whether the license key stored in the settings table exists and is valid.
     *
     * If so, we store the latest remote plugin details in our own 'cache', which can then be used when
     * updating plugins.
     * 
     * @since   3.0.0
     *
     * @param   bool $force     Force License Key Check, ignoring cache
     * @return  bool            License Key Valid
     */
    public function check_license_key_valid( $force = false ) { 

        // Check last result from cache
        if ( ! $force ) {
            $cache = $this->cache_get();
            if ( $cache['valid'] ) {
                return true;
            }
        }

        // If here, we're either forcing a check, the cache does not exist / has expired,
        // or the license key wasn't valid last time around, so we need to keep checking.
        if ( ! $this->check_license_key_exists() ) {
            $this->cache_delete();
            $this->notice->error = 1;
            $this->notice->message = $this->plugin->displayName . __( ': Please specify a license key on the Licensing screen.', $this->plugin->name );
            return false;
        }

        // Get site URL, excluding http(s), and whether this is an MS install
        $site_url = str_replace( parse_url( get_bloginfo( 'url' ), PHP_URL_SCHEME ) . '://', '', get_bloginfo( 'url' ) );
        $is_multisite = ( is_multisite() ? '1' : '0' );

        // Get license key
        $license_key = $this->get_license_key();

        // Build endpoint
        $url = $this->endpoint . "/index.php?request=checkLicenseKeyIsValid&params[]=" . $license_key . '&params[]=' . $this->plugin->name . '&params[]=' . urlencode( $site_url ) . '&params[]=' . $is_multisite . '&params[]=' . $this->plugin->version . '&params[]=' . get_bloginfo( 'version' );
       
        // Send license key check
        // Set user agent to beat aggressive caching
        $response = wp_remote_get( $url, array(
            'user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36',
        ) );
        
        // Check response
        if ( is_wp_error( $response ) ) {
            // Permit plugin usage but no updates
            $this->cache_set( true );

            // Show an error
            $this->notice->error = 1;
            $this->notice->message = $this->plugin->displayName . ': ' . $response->get_error_message();

            return true;
        }

        // Retrieve the response code and body content
        $code = wp_remote_retrieve_response_code( $response );
        $body = wp_remote_retrieve_body( $response );

        // Bail if the HTTP response code is an error
        if ( $code != 200 && $code != 301 ) {
            // Permit plugin usage but no updates
            $this->cache_set( true );

            // Show an error
            $this->notice->error = 1;
            $this->notice->message = $this->plugin->displayName . ': ' . sprintf(
                __( 'Licensing Server HTTP %s Error.', 'lum' ),
                $code
            );

            return true;
        }

        // Bail if the response body is empty
        if ( empty( $body ) ) {
            // Permit plugin usage but no updates
            $this->cache_set( true );

            // Show an error
            $this->notice->error = 1;
            $this->notice->message = $this->plugin->displayName . ': ' . sprintf(
                __( 'Licensing Server HTTP %s Error.', 'lum' ),
                $code
            );

            return true;
        }

        // Decode the body JSON into an array
        $result = json_decode( $body );

        // Check license key is valid
        if ( (int) $result->code != 1 ) {
            $this->cache_delete();
            $this->notice->error = 1;
            $this->notice->message = $this->plugin->displayName . ': ' . (string) $result->codeDescription;
            return false;   
        }

        // If here, license key is valid
        // Update in plugin settings, and store the remote version and packages available
        $this->notice->error = 0;
        $this->notice->message = (string) $result->codeDescription;
        $this->cache_set( 
            true,
            (string) $result->productVersion,
            ( isset( $result->product ) ? $result->product : '' ),
            ( isset( $result->features ) ? $result->features : '' )
        );
        return true;

    }  

    /**
     * Checks to see if the License has access to a given Feature.
     *
     * @since   1.0.0
     *
     * @param   string  $feature    Feature
     */
    public function has_feature( $feature ) {

        // Get features
        $cache = $this->cache_get();

        // If no features, bail
        if ( empty( $cache['features'] ) || ! $cache['features'] || ! is_array( $cache['features'] ) ) {
            return false;
        }

        // If the feature isn't set, bail
        if ( ! in_array( $feature, $cache['features'] ) ) {
            return false;
        }

        return true;

    }
    
    /**
     * Hooks into the plugin update check process, telling WordPress if a newer version of our
     * Plugin is available.
     *
     * @since   3.0.0
     *
     * @param   array   $transient  Transient
     * @return  array               Transient Plugin Data
     */
    public function api_check( $transient ) {

        // If we haven't called the licensing endpoint (which includes product update info),
        // do so now.
        if ( ! $this->update_check ) {
            $this->update_check = true;

            // If the license key isn't valid, bail
            if ( ! $this->check_license_key_valid( true ) ) {
                return $transient;
            }
        }

        // Get remote package data from cache
        // This was populated by the update/license checks earlier
        $cache = $this->cache_get();

        // If cache has a newer version available, show this in WordPress
        if ( ! empty( $cache['version'] ) && $cache['version'] > $this->plugin->version ) {
            // New version available - add to transient
            $response               = new stdClass;
            $response->new_version  = $cache['package']->version;
            $response->slug         = $cache['package']->slug;
            $response->plugin       = $this->plugin->name . '/' . $this->plugin->name . '.php';
            $response->url          = $cache['package']->homepage;
            $response->package      = $cache['package']->download_link;
            
            // Add response to transient array
            $transient->response[ $this->plugin->name . '/' . $this->plugin->name . '.php' ] = $response;
        }
   
        return $transient;

    }

    /**
     * Hooks into the plugins_api process, telling WordPress information about our plugin, such
     * as the WordPress compatible version and the changelog.
     *
     * @since 3.0.0
     *
     * @param object    $api    The original plugins_api object.
     * @param string    $action The action sent by plugins_api.
     * @param array     $args   Additional args to send to plugins_api.
     * @return object           New stdClass with plugin information on success, default response on failure.
     */
    public function plugins_api( $api, $action = '', $args = null ) {

        // Check if we are getting info for our plugin
        $plugin = ( 'plugin_information' == $action ) && isset( $args->slug ) && ( $this->plugin->name == $args->slug );
        if ( ! $plugin ) {
            return $api;
        }

        // Get remote package data from cache
        // This was populated by the update/license checks earlier
        $cache = $this->cache_get();

        // Bail if no package exists
        if ( ! $cache['package'] ) {
            return $api;
        }

        // Create a new stdClass object and populate it with our plugin information.
        $api                        = new stdClass;
        $api->name                  = $cache['package']->name;
        $api->slug                  = $cache['package']->slug;
        $api->plugin                = $this->plugin->name . '/' . $this->plugin->name . '.php';
        $api->version               = $cache['package']->version;
        $api->author                = $cache['package']->author;
        $api->author_profile        = $cache['package']->author_profile;
        $api->requires              = $cache['package']->requires;
        $api->tested                = $cache['package']->tested;
        $api->last_updated          = date( 'Y-m-d H:i:s', $cache['package']->last_updated );
        $api->homepage              = $cache['package']->homepage;
        $api->sections['changelog'] = $cache['package']->changelog;
        $api->download_link         = $cache['package']->download_link;

        // Return the new API object with our custom data.
        return $api;

    }

    /**
     * Filter the Plugin Name, Author Name and Plugin URI
     * if whitelabelling is enabled.
     *
     * @since   1.0.0
     *
     * @param   array   $plugins    All Installed Plugins
     * @return  array               All Installed Plugins
     */
    public function maybe_filter_plugin_name( $plugins ) {

        // Bail if whitelabelling isn't available
        if ( ! $this->has_feature( 'whitelabelling' ) ) {
            return $plugins;
        }

        // Bail if this Plugin isn't in the list
        if ( ! isset( $plugins[ $this->plugin->name . '/' . $this->plugin->name . '.php' ] ) ) {
            return $plugins;
        }

        // Change the Plugin Name, Author Name and URIs
        if ( defined( strtoupper( $this->plugin->name ) . '_DISPLAY_NAME' ) ) {
            $plugins[ $this->plugin->name . '/' . $this->plugin->name . '.php' ]['Name'] = constant( strtoupper( $this->plugin->name ) . '_DISPLAY_NAME' );
            $plugins[ $this->plugin->name . '/' . $this->plugin->name . '.php' ]['Title'] = constant( strtoupper( $this->plugin->name ) . '_DISPLAY_NAME' );
        }
        if ( defined( strtoupper( $this->plugin->name ) . '_AUTHOR_NAME' ) ) {
            $plugins[ $this->plugin->name . '/' . $this->plugin->name . '.php' ]['Author'] = constant( strtoupper( $this->plugin->name ) . '_AUTHOR_NAME' );
            $plugins[ $this->plugin->name . '/' . $this->plugin->name . '.php' ]['AuthorName'] = constant( strtoupper( $this->plugin->name ) . '_AUTHOR_NAME' );
        }
        if ( defined( strtoupper( $this->plugin->name ) . '_SUPPORT_URL' ) ) {
            $plugins[ $this->plugin->name . '/' . $this->plugin->name . '.php' ]['PluginURI'] = constant( strtoupper( $this->plugin->name ) . '_SUPPORT_URL' );
            $plugins[ $this->plugin->name . '/' . $this->plugin->name . '.php' ]['AuthorURI'] = constant( strtoupper( $this->plugin->name ) . '_SUPPORT_URL' );
        }

        // Return
        return $plugins;

    }

    /**
     * Fetches cached data from the WordPress options table
     *
     * @since   1.0.0
     *
     * @return  array   Cached Data
     */
    private function cache_get() {

        // Define defaults
        $defaults = array(
            'valid'     => 0,
            'version'   => 0,
            'package'   => '',
            'features'  => '',
            'expires'   => 0,
        );

        // Get cache
        $cache = get_option( $this->plugin->name . '_lum', $defaults );

        // If the cache has expired, delete it and return the defaults
        if ( strtotime( 'now' ) > $cache['expires'] ) {
            $this->cache_delete();
            return $defaults;
        }

        // Return cached data
        return $cache;

    } 

    /**
     * Sets cached data in the WordPress options table for a day
     *
     * @since   1.0.0
     *
     * @param   bool    $valid      License Key Valid
     * @param   string  $version    Remote Package Version Available
     * @param   object  $package    Package Details
     * @param   array   $features   Package Features
     */
    private function cache_set( $valid = false, $version = '', $package = '', $features = '' ) {

        update_option( $this->plugin->name . '_lum', array(
            'valid'     => $valid,
            'version'   => $version,
            'package'   => $package,
            'features'  => $features,
            'expires'   => time() + DAY_IN_SECONDS,
        ) );

        // Clear options cache, so that persistent caching solutions
        // have to fetch the latest options data from the DB
        wp_cache_delete( 'alloptions', 'options' );
        wp_cache_delete( $this->plugin->name . '_lum', 'options' );

    }

    /**
     * Deletes the cached data in the WordPress option table
     *
     * @since   1.0.0
     */
    public function cache_delete() {

        delete_option( $this->plugin->name . '_lum' );

        // Clear options cache, so that persistent caching solutions
        // have to fetch the latest options data from the DB
        wp_cache_delete( 'alloptions', 'options' );
        wp_cache_delete( $this->plugin->name . '_lum', 'options' );

    } 

}