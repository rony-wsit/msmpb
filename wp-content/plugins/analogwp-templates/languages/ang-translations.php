<?php
/* THIS IS A GENERATED FILE. DO NOT EDIT DIRECTLY. */
$generated_i18n_strings = array(
	/* Copyright (C) 2019 AnalogWP
	   This file is distributed under the same license as the Style Kits for Elementor plugin. */

	// Reference: src/js/app/Header.js:108
	// Reference: src/js/app/popup.js:134
	// Reference: languages/ang-translations.php:11
	__( 'Close', 'ang' ),

	// Reference: src/js/app/Header.js:93
	// Reference: languages/ang-translations.php:15
	__( 'Templates library refreshed', 'ang' ),

	// Reference: src/js/app/Header.js:94
	// Reference: languages/ang-translations.php:19
	__( 'Error refreshing templates library, please try again.', 'ang' ),

	// Reference: src/js/app/Header.js:98
	// Reference: languages/ang-translations.php:23
	__( 'Syncing...', 'ang' ),

	// Reference: src/js/app/Header.js:99
	// Reference: languages/ang-translations.php:27
	__( 'Sync Library', 'ang' ),

	// Reference: src/js/app/Nav.js:66
	// Reference: inc/register-settings.php:34
	// Reference: languages/ang-translations.php:32
	__( 'Templates', 'ang' ),

	// Reference: src/js/app/Nav.js:67
	// Reference: inc/elementor/class-ang-action.php:122
	// Reference: inc/elementor/class-finder-shortcuts.php:48
	// Reference: inc/elementor/class-post-type.php:32
	// Reference: inc/elementor/class-post-type.php:41
	// Reference: inc/elementor/class-typography.php:634
	// Reference: inc/register-settings.php:23
	// Reference: inc/register-settings.php:41
	// Reference: inc/register-settings.php:60
	// Reference: languages/ang-translations.php:44
	__( 'Style Kits', 'ang' ),

	// Reference: src/js/app/ProModal.js:37
	// Reference: languages/ang-translations.php:48
	__( 'Template Kits are', 'ang' ),

	// Reference: src/js/app/ProModal.js:37
	// Reference: languages/ang-translations.php:52
	__( 'coming soon with Style Kits Pro.', 'ang' ),

	// Reference: src/js/app/ProModal.js:37
	// Reference: languages/ang-translations.php:56
	__( 'Sign up for an exclusive launch discount.', 'ang' ),

	// Reference: src/js/app/ProModal.js:39
	// Reference: languages/ang-translations.php:60
	__( 'Learn More', 'ang' ),

	// Reference: src/js/app/Template.js:13
	// Reference: languages/ang-translations.php:64
	__( 'New', 'ang' ),

	// Reference: src/js/app/Template.js:20
	// Reference: languages/ang-translations.php:68
	__( 'Preview', 'ang' ),

	// Reference: src/js/app/Template.js:24
	// Reference: src/js/app/stylekits/stylekits.js:242
	// Reference: languages/ang-translations.php:73
	__( 'Import', 'ang' ),

	// Reference: src/js/app/Template.js:51
	// Reference: languages/ang-translations.php:77
	__( 'Pro', 'ang' ),

	// Reference: src/js/app/Templates.js:318
	// Reference: languages/ang-translations.php:81
	__( 'This template requires an updated version, please update your plugin to latest version.', 'ang' ),

	// Reference: src/js/app/collection/Collection.js:152
	// Reference: languages/ang-translations.php:85
	__( 'View Templates', 'ang' ),

	// Reference: src/js/app/filters.js:149
	// Reference: languages/ang-translations.php:89
	__( 'Show All', 'ang' ),

	// Reference: src/js/app/filters.js:154
	// Reference: languages/ang-translations.php:93
	__( 'Latest', 'ang' ),

	// Reference: src/js/app/filters.js:155
	// Reference: languages/ang-translations.php:97
	__( 'Popular', 'ang' ),

	// Reference: src/js/app/filters.js:174
	// Reference: languages/ang-translations.php:101
	__( 'Back to Kits', 'ang' ),

	// Reference: src/js/app/filters.js:176
	// Reference: languages/ang-translations.php:105
	__( 'Template Kit', 'ang' ),

	// Reference: src/js/app/filters.js:189
	// Reference: languages/ang-translations.php:109
	__( 'Back to all', 'ang' ),

	// Reference: src/js/app/filters.js:190
	// Reference: languages/ang-translations.php:113
	__( 'My Favorites', 'ang' ),

	// Reference: src/js/app/filters.js:196
	// Reference: languages/ang-translations.php:117
	__( 'Show Pro Templates', 'ang' ),

	// Reference: src/js/app/filters.js:208
	// Reference: languages/ang-translations.php:121
	__( 'Group by Template Kit', 'ang' ),

	// Reference: src/js/app/filters.js:222
	// Reference: languages/ang-translations.php:125
	__( 'Filter', 'ang' ),

	// Reference: src/js/app/filters.js:234
	// Reference: languages/ang-translations.php:129
	__( 'Sort by', 'ang' ),

	// Reference: src/js/app/filters.js:247
	// Reference: languages/ang-translations.php:133
	__( 'Search templates', 'ang' ),

	// Reference: src/js/app/modal.js:106
	// Reference: languages/ang-translations.php:137
	__( 'Insert Layout', 'ang' ),

	// Reference: src/js/app/modal.js:96
	// Reference: languages/ang-translations.php:141
	__( 'Back to Library', 'ang' ),

	// Reference: src/js/app/modal.js:99
	// Reference: languages/ang-translations.php:145
	__( 'Open Preview in New Tab', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:101
	// Reference: languages/ang-translations.php:149
	__( 'Default', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:105
	// Reference: languages/ang-translations.php:153
	__( 'Installed', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:119
	// Reference: languages/ang-translations.php:157
	__( 'Learn more about this in %s.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:122
	// Reference: languages/ang-translations.php:161
	__( 'Style Kits Docs', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:140
	// Reference: languages/ang-translations.php:165
	__( 'Choose a Style Kit...', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:157
	// Reference: languages/ang-translations.php:169
	__( 'Next', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:173
	// Reference: languages/ang-translations.php:173
	__( 'Change Style Kit', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:177
	// Reference: languages/ang-translations.php:177
	__( 'Import this template to your library to make it available in your Elementor ', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:178
	// Reference: languages/ang-translations.php:181
	__( 'Saved Templates', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:179
	// Reference: languages/ang-translations.php:185
	__( ' list for future use.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:192
	// Reference: languages/ang-translations.php:189
	__( 'Import to Library', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:200
	// Reference: languages/ang-translations.php:193
	__( 'Create a new page from this template to make it available as a draft page in your Pages list.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:204
	// Reference: languages/ang-translations.php:197
	__( 'Enter a Page Name', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:223
	// Reference: languages/ang-translations.php:201
	__( 'Import to page', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:235
	// Reference: languages/ang-translations.php:205
	__( 'All done! The template has been imported.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:240
	// Reference: languages/ang-translations.php:209
	__( 'Edit Template', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:80
	// Reference: languages/ang-translations.php:213
	__( 'Select a Style Kit to apply on this layout', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:102
	// Reference: languages/ang-translations.php:217
	__( 'Manage your Style Kits', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:178
	// Reference: languages/ang-translations.php:221
	__( 'These are some Style Kit presets that you can use as a starting point. Once you import a Style Kit, it will be added to your', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:178
	// Reference: languages/ang-translations.php:225
	__( 'Style Kits list', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:179
	// Reference: languages/ang-translations.php:229
	__( 'You will then be able to apply it on any Elementor page.', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:180
	// Reference: inc/elementor/class-ang-action.php:119
	// Reference: languages/ang-translations.php:236
	/* translators: translators: %s: Link text
translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text */
	__( 'Learn more', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:211
	// Reference: languages/ang-translations.php:240
	__( 'A Style Kit already exists with the same name. To import it again please enter a new name below:', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:215
	// Reference: languages/ang-translations.php:244
	__( 'Please try a different as a Style Kit with same name already exists.', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:219
	// Reference: languages/ang-translations.php:248
	__( 'Enter a Style Kit Name', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:254
	// Reference: languages/ang-translations.php:252
	__( 'The Style Kit has been imported and is now available in the list of the available Style Kits.', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:268
	// Reference: languages/ang-translations.php:256
	__( 'Ok, thanks', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:276
	// Reference: languages/ang-translations.php:260
	__( 'Importing ', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:99
	// Reference: languages/ang-translations.php:264
	__( '%s in WordPress dashboard.', 'ang' ),

	// Reference: inc/admin/class-admin.php:42
	// Reference: inc/register-settings.php:22
	// Reference: languages/ang-translations.php:271
	/* translators: Plugin Name of the plugin
translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin */
	__( 'Style Kits for Elementor', 'ang' ),

	// Reference: languages/ang-translations.php:277
	/* translators: Plugin URI of the plugin
Author URI of the plugin
translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin */
	__( 'https://analogwp.com/', 'ang' ),

	// Reference: languages/ang-translations.php:282
	/* translators: Description of the plugin
translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin */
	__( 'Style Kits adds intuitive styling controls in the Elementor editor that power-up your design workflow.', 'ang' ),

	// Reference: languages/ang-translations.php:287
	/* translators: Author of the plugin
translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin */
	__( 'AnalogWP', 'ang' ),

	// Reference: analogwp-templates.php:71
	// Reference: analogwp-templates.php:81
	// Reference: languages/ang-translations.php:292
	__( 'Cheatin&#8217; huh?', 'ang' ),

	// Reference: analogwp-templates.php:284
	// Reference: inc/elementor/class-finder-shortcuts.php:42
	// Reference: inc/register-settings.php:50
	// Reference: languages/ang-translations.php:298
	__( 'Settings', 'ang' ),

	// Reference: analogwp-templates.php:328
	// Reference: languages/ang-translations.php:302
	__( 'Analog Templates is not working because you need to activate the Elementor plugin.', 'ang' ),

	// Reference: analogwp-templates.php:329
	// Reference: languages/ang-translations.php:306
	__( 'Activate Elementor Now', 'ang' ),

	// Reference: analogwp-templates.php:336
	// Reference: languages/ang-translations.php:310
	__( 'Analog Templates is not working because you need to install the Elementor plugin.', 'ang' ),

	// Reference: analogwp-templates.php:337
	// Reference: languages/ang-translations.php:314
	__( 'Install Elementor Now', 'ang' ),

	// Reference: analogwp-templates.php:351
	// Reference: languages/ang-translations.php:320
	/* translators: translators: %s: WordPress version
translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version */
	__( 'Analog Templates requires WordPress version %s+. Because you are using an earlier version, the plugin is currently NOT RUNNING.', 'ang' ),

	// Reference: inc/admin/class-admin.php:41
	// Reference: languages/ang-translations.php:326
	/* translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review
translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review */
	__( 'Enjoyed %1$s? Please leave us a %2$s rating. We really appreciate your support!', 'ang' ),

	// Reference: inc/admin/class-admin.php:63
	// Reference: languages/ang-translations.php:330
	__( 'View Documentation', 'ang' ),

	// Reference: inc/admin/class-admin.php:63
	// Reference: inc/settings/views/html-admin-settings.php:60
	// Reference: languages/ang-translations.php:335
	__( 'Documentation', 'ang' ),

	// Reference: inc/admin/class-admin.php:64
	// Reference: languages/ang-translations.php:339
	__( 'Get Support', 'ang' ),

	// Reference: inc/api/class-local.php:361
	// Reference: languages/ang-translations.php:343
	__( 'Setting updated.', 'ang' ),

	// Reference: inc/api/class-local.php:449
	// Reference: languages/ang-translations.php:347
	__( 'Unable to create a post', 'ang' ),

	// Reference: inc/api/class-local.php:454
	// Reference: languages/ang-translations.php:351
	__( 'Token saved.', 'ang' ),

	// Reference: inc/api/class-local.php:472
	// Reference: inc/api/class-local.php:502
	// Reference: languages/ang-translations.php:356
	__( 'Please provide a valid post ID.', 'ang' ),

	// Reference: inc/api/class-local.php:476
	// Reference: languages/ang-translations.php:360
	__( 'Invalid Post ID', 'ang' ),

	// Reference: inc/api/class-local.php:547
	// Reference: languages/ang-translations.php:364
	__( 'Invalid Style Kit ID.', 'ang' ),

	// Reference: inc/api/class-local.php:567
	// Reference: languages/ang-translations.php:368
	__( 'Error occured while requesting Style Kit data.', 'ang' ),

	// Reference: inc/api/class-local.php:597
	// Reference: languages/ang-translations.php:372
	__( 'Style Kit imported', 'ang' ),

	// Reference: inc/api/class-local.php:633
	// Reference: languages/ang-translations.php:376
	__( 'Invalid token data returned', 'ang' ),

	// Reference: inc/class-admin-settings.php:75
	// Reference: languages/ang-translations.php:380
	__( 'Your settings have been saved.', 'ang' ),

	// Reference: inc/class-admin-settings.php:133
	// Reference: languages/ang-translations.php:384
	__( 'The changes you made will be lost if you navigate away from this page.', 'ang' ),

	// Reference: inc/class-admin-settings.php:549
	// Reference: inc/class-admin-settings.php:614
	// Reference: languages/ang-translations.php:389
	__( 'Toggle', 'ang' ),

	// Reference: inc/class-base.php:41
	// Reference: inc/class-base.php:51
	// Reference: languages/ang-translations.php:394
	__( 'Something went wrong.', 'ang' ),

	// Reference: inc/class-cron.php:41
	// Reference: languages/ang-translations.php:398
	__( 'Once Weekly', 'ang' ),

	// Reference: inc/class-elementor.php:42
	// Reference: languages/ang-translations.php:402
	__( 'AnalogWP Classes', 'ang' ),

	// Reference: inc/class-quick-edit.php:62
	// Reference: inc/class-quick-edit.php:133
	// Reference: inc/elementor/class-post-type.php:33
	// Reference: languages/ang-translations.php:408
	__( 'Style Kit', 'ang' ),

	// Reference: inc/class-utils.php:98
	// Reference: languages/ang-translations.php:414
	/* translators: translators: Global Style Kit post title.
translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. */
	__( 'Global: %s', 'ang' ),

	// Reference: inc/elementor/class-analog-settings.php:41
	// Reference: languages/ang-translations.php:418
	__( 'Style Kits for Elementor Settings', 'ang' ),

	// Reference: inc/elementor/class-analog-settings.php:45
	// Reference: languages/ang-translations.php:422
	__( 'Global Style Kit', 'ang' ),

	// Reference: inc/elementor/class-analog-settings.php:50
	// Reference: languages/ang-translations.php:428
	/* translators: translators: %s: Style Kit Documentation link
translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link */
	__( 'This setting has been moved to %s.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:101
	// Reference: languages/ang-translations.php:432
	__( 'This will reset all the settings you configured previously under Page Style Settings from Analog Templates.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:102
	// Reference: languages/ang-translations.php:436
	__( 'Are you sure?', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:103
	// Reference: languages/ang-translations.php:440
	__( 'Save Style Kit as', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:104
	// Reference: languages/ang-translations.php:444
	__( 'Save', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:105
	// Reference: languages/ang-translations.php:448
	__( 'Cancel', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:106
	// Reference: languages/ang-translations.php:452
	__( 'Enter a title', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:107
	// Reference: languages/ang-translations.php:456
	__( 'Insert Style Kit', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:108
	// Reference: inc/elementor/class-ang-action.php:111
	// Reference: languages/ang-translations.php:461
	__( 'Please select a Style Kit first.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:109
	// Reference: inc/settings/class-settings-general.php:36
	// Reference: languages/ang-translations.php:466
	__( '— Select a Style Kit —', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:110
	// Reference: languages/ang-translations.php:470
	__( 'Style Kit Updated.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:112
	// Reference: inc/elementor/class-typography.php:680
	// Reference: languages/ang-translations.php:475
	__( 'Update Style Kit', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:113
	// Reference: languages/ang-translations.php:479
	__( 'This action will update the Style Kit with the latest changes, and will affect all the pages that the style kit is used on. Do you wish to proceed?', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:114
	// Reference: languages/ang-translations.php:483
	__( 'Meet Style Kits by Style Kits for Elementor', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:117
	// Reference: languages/ang-translations.php:489
	/* translators: translators: %s: Link to Style Kits documentation.
translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. */
	__( 'Take control of your design in the macro level, with local or global settings for typography and spacing. %s.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:121
	// Reference: languages/ang-translations.php:493
	__( 'View Styles', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:123
	// Reference: inc/elementor/class-typography.php:752
	// Reference: languages/ang-translations.php:498
	__( 'Export CSS', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:124
	// Reference: languages/ang-translations.php:502
	__( 'Copy CSS', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:125
	// Reference: languages/ang-translations.php:506
	__( 'Style Kit Update Detected', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:126
	// Reference: languages/ang-translations.php:510
	__( '<p>The Style kit used by this page has been updated, click ‘Apply Changes’ to apply the latest changes.</p><p>Click Discard to keep your current page styles and detach the page from the Style Kit</p>', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:127
	// Reference: languages/ang-translations.php:514
	__( 'Discard', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:128
	// Reference: languages/ang-translations.php:518
	__( 'Apply Changes', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:129
	// Reference: languages/ang-translations.php:522
	__( 'Ok, got it.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:130
	// Reference: languages/ang-translations.php:526
	__( 'Go to Page Style', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:131
	// Reference: languages/ang-translations.php:530
	__( 'This template offers global typography and spacing control, through the Page Style tab.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:133
	// Reference: languages/ang-translations.php:534
	__( 'Typography, column gaps and more, are controlled layout-wide at Page Styles Panel, giving you the flexibility you need over the design of this template. You can save the styles and apply them to any other page. <a href="#" target="_blank">Learn More.</a>', 'ang' ),

	// Reference: inc/elementor/class-colors.php:154
	// Reference: languages/ang-translations.php:540
	/* translators: translators: %1$s: Link to documentation, %2$s: Link text.
translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. */
	__( 'Set the colors for Typography, accents and more.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:154
	// Reference: inc/elementor/class-typography.php:243
	// Reference: inc/elementor/class-typography.php:303
	// Reference: inc/elementor/class-typography.php:364
	// Reference: inc/elementor/class-typography.php:424
	// Reference: inc/elementor/class-typography.php:486
	// Reference: languages/ang-translations.php:551
	/* translators: translators: %1$s: Link to documentation, %2$s: Link text.
translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. */
	__( 'Learn more.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:213
	// Reference: languages/ang-translations.php:555
	__( 'Primary Accent', 'ang' ),

	// Reference: inc/elementor/class-colors.php:223
	// Reference: languages/ang-translations.php:559
	__( 'The primary accent color applies on Links.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:231
	// Reference: languages/ang-translations.php:563
	__( 'Secondary Accent', 'ang' ),

	// Reference: inc/elementor/class-colors.php:255
	// Reference: languages/ang-translations.php:567
	__( 'The default Button color. You can also set button colors in the Buttons tab.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:263
	// Reference: languages/ang-translations.php:571
	__( 'Text and Headings Color', 'ang' ),

	// Reference: inc/elementor/class-colors.php:278
	// Reference: languages/ang-translations.php:575
	__( 'Applies on the text and headings in the layout.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:286
	// Reference: inc/elementor/tags/class-light-background.php:13
	// Reference: languages/ang-translations.php:580
	__( 'Light Background', 'ang' ),

	// Reference: inc/elementor/class-colors.php:307
	// Reference: languages/ang-translations.php:584
	__( 'Apply this color to sections or columns, using the <code>sk-light-bg</code>. The text inside will inherit the Text and titles color.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:315
	// Reference: inc/elementor/tags/class-dark-background.php:13
	// Reference: languages/ang-translations.php:589
	__( 'Dark Background', 'ang' ),

	// Reference: inc/elementor/class-colors.php:342
	// Reference: languages/ang-translations.php:593
	__( 'Apply this color to sections or columns, using the <code>sk-dark-bg</code>. The text inside will inherit the <em>Text over Dark Background</em> color that can be set below.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:350
	// Reference: languages/ang-translations.php:597
	__( 'Text over dark background', 'ang' ),

	// Reference: inc/elementor/class-colors.php:364
	// Reference: languages/ang-translations.php:601
	__( 'This color will apply on the text in a section or column with the Dark Background Color, as it has been set above.', 'ang' ),

	// Reference: inc/elementor/class-finder-shortcuts.php:23
	// Reference: languages/ang-translations.php:605
	__( 'Style Kits for Elementor Shortcuts', 'ang' ),

	// Reference: inc/elementor/class-finder-shortcuts.php:36
	// Reference: languages/ang-translations.php:609
	__( 'Templates Library', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:37
	// Reference: languages/ang-translations.php:613
	__( 'Add New Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:38
	// Reference: languages/ang-translations.php:617
	__( 'New Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:39
	// Reference: languages/ang-translations.php:621
	__( 'Edit Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:40
	// Reference: languages/ang-translations.php:625
	__( 'View Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:42
	// Reference: languages/ang-translations.php:629
	__( 'Search Style Kits', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:43
	// Reference: languages/ang-translations.php:633
	__( 'Parent Style Kits:', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:44
	// Reference: languages/ang-translations.php:637
	__( 'No Style Kit found.', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:45
	// Reference: languages/ang-translations.php:641
	__( 'No Style Kit found in Trash.', 'ang' ),

	// Reference: inc/elementor/class-tools.php:178
	// Reference: languages/ang-translations.php:645
	__( 'Export Style Kit', 'ang' ),

	// Reference: inc/elementor/class-tools.php:199
	// Reference: languages/ang-translations.php:649
	__( 'Export', 'ang' ),

	// Reference: inc/elementor/class-tools.php:406
	// Reference: languages/ang-translations.php:653
	__( 'Import Style Kits', 'ang' ),

	// Reference: inc/elementor/class-tools.php:409
	// Reference: languages/ang-translations.php:657
	__( 'Choose an Analog template JSON file or a .zip archive of Analog Style Kits, and add them to the list of Style Kits available in your library.', 'ang' ),

	// Reference: inc/elementor/class-tools.php:420
	// Reference: languages/ang-translations.php:661
	__( 'Import Now', 'ang' ),

	// Reference: inc/elementor/class-tools.php:570
	// Reference: languages/ang-translations.php:665
	__( 'Error occurred, the version selected is invalid. Try selecting different version.', 'ang' ),

	// Reference: inc/elementor/class-tools.php:615
	// Reference: languages/ang-translations.php:669
	__( 'Rollback to Previous Version', 'ang' ),

	// Reference: inc/elementor/class-tools.php:646
	// Reference: languages/ang-translations.php:675
	/* translators: translators: %s: Style kit title.
translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. */
	__( 'Style Kit: %s <span style="color:#5C32B6;">&#9679;</span>', 'ang' ),

	// Reference: inc/elementor/class-tools.php:671
	// Reference: languages/ang-translations.php:679
	__( 'Apply Global Style Kit', 'ang' ),

	// Reference: inc/elementor/class-tools.php:712
	// Reference: languages/ang-translations.php:683
	__( 'Invalid/empty Post ID.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:108
	// Reference: languages/ang-translations.php:687
	__( 'Headings Typography', 'ang' ),

	// Reference: inc/elementor/class-typography.php:116
	// Reference: languages/ang-translations.php:691
	__( 'These settings apply to all Headings in your layout. You can still override individual values at each element.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:131
	// Reference: languages/ang-translations.php:695
	__( 'Default Headings Font', 'ang' ),

	// Reference: inc/elementor/class-typography.php:146
	// Reference: languages/ang-translations.php:701
	/* translators: translators: %s: Heading 1-6 type
translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type */
	__( 'Heading %s', 'ang' ),

	// Reference: inc/elementor/class-typography.php:189
	// Reference: inc/elementor/class-typography.php:211
	// Reference: languages/ang-translations.php:706
	__( 'Body Typography', 'ang' ),

	// Reference: inc/elementor/class-typography.php:197
	// Reference: languages/ang-translations.php:710
	__( 'Recently Imported', 'ang' ),

	// Reference: inc/elementor/class-typography.php:235
	// Reference: languages/ang-translations.php:714
	__( 'Heading Sizes', 'ang' ),

	// Reference: inc/elementor/class-typography.php:243
	// Reference: languages/ang-translations.php:718
	__( 'Tweak the size of your Heading elements using these presets.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:250
	// Reference: inc/elementor/class-typography.php:310
	// Reference: languages/ang-translations.php:723
	__( 'XXL', 'ang' ),

	// Reference: inc/elementor/class-typography.php:251
	// Reference: inc/elementor/class-typography.php:311
	// Reference: inc/elementor/class-typography.php:497
	// Reference: languages/ang-translations.php:729
	__( 'XL', 'ang' ),

	// Reference: inc/elementor/class-typography.php:252
	// Reference: inc/elementor/class-typography.php:312
	// Reference: inc/elementor/class-typography.php:349
	// Reference: inc/elementor/class-typography.php:797
	// Reference: languages/ang-translations.php:736
	__( 'Large', 'ang' ),

	// Reference: inc/elementor/class-typography.php:253
	// Reference: inc/elementor/class-typography.php:313
	// Reference: inc/elementor/class-typography.php:348
	// Reference: inc/elementor/class-typography.php:796
	// Reference: languages/ang-translations.php:743
	__( 'Medium', 'ang' ),

	// Reference: inc/elementor/class-typography.php:254
	// Reference: inc/elementor/class-typography.php:314
	// Reference: inc/elementor/class-typography.php:347
	// Reference: inc/elementor/class-typography.php:795
	// Reference: languages/ang-translations.php:750
	__( 'Small', 'ang' ),

	// Reference: inc/elementor/class-typography.php:295
	// Reference: languages/ang-translations.php:754
	__( 'Text Sizes', 'ang' ),

	// Reference: inc/elementor/class-typography.php:303
	// Reference: languages/ang-translations.php:758
	__( 'Tweak the size of text elements using these presets.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:346
	// Reference: inc/elementor/class-typography.php:794
	// Reference: languages/ang-translations.php:763
	__( 'Normal', 'ang' ),

	// Reference: inc/elementor/class-typography.php:350
	// Reference: inc/elementor/class-typography.php:798
	// Reference: languages/ang-translations.php:768
	__( 'Extra Large', 'ang' ),

	// Reference: inc/elementor/class-typography.php:356
	// Reference: inc/elementor/class-typography.php:787
	// Reference: languages/ang-translations.php:773
	__( 'Outer Section Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:364
	// Reference: languages/ang-translations.php:777
	__( 'Add padding to the outer sections of your layouts by using these controls.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:406
	// Reference: languages/ang-translations.php:781
	__( 'Default Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:407
	// Reference: languages/ang-translations.php:785
	__( 'Narrow Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:408
	// Reference: languages/ang-translations.php:789
	__( 'Extended Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:409
	// Reference: languages/ang-translations.php:793
	__( 'Wide Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:410
	// Reference: languages/ang-translations.php:797
	__( 'Wider Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:416
	// Reference: languages/ang-translations.php:801
	__( 'Column Gaps', 'ang' ),

	// Reference: inc/elementor/class-typography.php:424
	// Reference: languages/ang-translations.php:805
	__( 'Column Gap presets add padding to the columns of a section.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:451
	// Reference: languages/ang-translations.php:809
	__( 'Space Between Widgets', 'ang' ),

	// Reference: inc/elementor/class-typography.php:452
	// Reference: languages/ang-translations.php:813
	__( 'Sets the default space between widgets, overrides the default value set in Elementor > Style > Space Between Widgets.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:478
	// Reference: languages/ang-translations.php:817
	__( 'Buttons', 'ang' ),

	// Reference: inc/elementor/class-typography.php:486
	// Reference: languages/ang-translations.php:821
	__( 'Define the default styles for every button size.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:493
	// Reference: languages/ang-translations.php:825
	__( 'XS', 'ang' ),

	// Reference: inc/elementor/class-typography.php:494
	// Reference: languages/ang-translations.php:829
	__( 'S', 'ang' ),

	// Reference: inc/elementor/class-typography.php:495
	// Reference: languages/ang-translations.php:833
	__( 'M', 'ang' ),

	// Reference: inc/elementor/class-typography.php:496
	// Reference: languages/ang-translations.php:837
	__( 'L', 'ang' ),

	// Reference: inc/elementor/class-typography.php:509
	// Reference: languages/ang-translations.php:841
	__( 'Typography', 'ang' ),

	// Reference: inc/elementor/class-typography.php:519
	// Reference: languages/ang-translations.php:845
	__( 'Text Color', 'ang' ),

	// Reference: inc/elementor/class-typography.php:531
	// Reference: languages/ang-translations.php:849
	__( 'Text Hover Color', 'ang' ),

	// Reference: inc/elementor/class-typography.php:543
	// Reference: languages/ang-translations.php:853
	__( 'Background Color', 'ang' ),

	// Reference: inc/elementor/class-typography.php:555
	// Reference: languages/ang-translations.php:857
	__( 'Background Hover Color', 'ang' ),

	// Reference: inc/elementor/class-typography.php:577
	// Reference: languages/ang-translations.php:861
	__( 'Border Radius', 'ang' ),

	// Reference: inc/elementor/class-typography.php:599
	// Reference: languages/ang-translations.php:865
	__( 'Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:654
	// Reference: languages/ang-translations.php:869
	__( 'You are editing the Global Style Kit.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:663
	// Reference: languages/ang-translations.php:873
	__( 'A style kit is a collection of all the custom styles added at page styling settings. Your Style Kit is updated every time you click the Update Style Kit Button below.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:667
	// Reference: languages/ang-translations.php:877
	__( 'Page Style Kit', 'ang' ),

	// Reference: inc/elementor/class-typography.php:677
	// Reference: languages/ang-translations.php:881
	__( 'Update Your Style Kit', 'ang' ),

	// Reference: inc/elementor/class-typography.php:687
	// Reference: languages/ang-translations.php:885
	__( 'Save all the styles as a Style Kit that you can apply on other pages or globally. Please note that only the custom styles added in the styles page are saved with the stylekit.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:691
	// Reference: languages/ang-translations.php:889
	__( 'Save Style Kit as...', 'ang' ),

	// Reference: inc/elementor/class-typography.php:694
	// Reference: languages/ang-translations.php:893
	__( 'Save as...', 'ang' ),

	// Reference: inc/elementor/class-typography.php:703
	// Reference: languages/ang-translations.php:899
	/* translators: translators: %s: Link to Style Kits
translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits */
	__( 'You can set a Global Style Kit <a href="%s" target="_blank">here</a>.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:729
	// Reference: languages/ang-translations.php:903
	__( 'Tools', 'ang' ),

	// Reference: inc/elementor/class-typography.php:734
	// Reference: languages/ang-translations.php:907
	__( 'This will reset all the custom style values added in the Style tab, and detach this page from any Style kits', 'ang' ),

	// Reference: inc/elementor/class-typography.php:738
	// Reference: languages/ang-translations.php:911
	__( 'Reset all styling', 'ang' ),

	// Reference: inc/elementor/class-typography.php:741
	// Reference: languages/ang-translations.php:915
	__( 'Reset all', 'ang' ),

	// Reference: inc/elementor/class-typography.php:745
	// Reference: languages/ang-translations.php:919
	__( 'Export styles as custom CSS text.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:749
	// Reference: languages/ang-translations.php:923
	__( 'Export Custom CSS', 'ang' ),

	// Reference: inc/elementor/class-typography.php:788
	// Reference: languages/ang-translations.php:927
	__( 'A Style Kits control that adds padding to your outer sections. You can edit the values', 'ang' ),

	// Reference: inc/elementor/class-typography.php:793
	// Reference: languages/ang-translations.php:931
	__( 'No Padding', 'ang' ),

	// Reference: inc/register-settings.php:33
	// Reference: languages/ang-translations.php:935
	__( 'Style Kits Library', 'ang' ),

	// Reference: inc/register-settings.php:42
	// Reference: languages/ang-translations.php:939
	__( 'Library', 'ang' ),

	// Reference: inc/register-settings.php:49
	// Reference: languages/ang-translations.php:943
	__( 'Style Kits Settings', 'ang' ),

	// Reference: inc/register-settings.php:61
	// Reference: languages/ang-translations.php:947
	__( 'Manage Style Kits', 'ang' ),

	// Reference: inc/register-settings.php:186
	// Reference: languages/ang-translations.php:951
	__( 'Imported Count', 'ang' ),

	// Reference: inc/register-settings.php:198
	// Reference: languages/ang-translations.php:955
	__( 'Imported templates', 'ang' ),

	// Reference: inc/settings/class-settings-extensions.php:25
	// Reference: languages/ang-translations.php:959
	__( 'Extensions', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:25
	// Reference: languages/ang-translations.php:963
	__( 'General', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:42
	// Reference: languages/ang-translations.php:967
	__( 'Elementor Settings', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:50
	// Reference: languages/ang-translations.php:973
	/* translators: translators: %s: Style Kit Documentation link
translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link */
	__( 'Choosing a Style Kit will make it global and apply site-wide. Learn more about %s.', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:51
	// Reference: languages/ang-translations.php:977
	__( 'Style kits', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:59
	// Reference: languages/ang-translations.php:981
	__( 'Sync Color Palettes and Style Kit colors by default', 'ang' ),

	// Reference: inc/settings/class-settings-gopro.php:27
	// Reference: languages/ang-translations.php:985
	__( 'Style Kits Pro', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:25
	// Reference: languages/ang-translations.php:989
	__( 'Misc', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:56
	// Reference: languages/ang-translations.php:993
	__( 'Usage Data Tracking', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:61
	// Reference: languages/ang-translations.php:997
	__( 'Opt-in to our anonymous plugin data collection and to updates. We guarantee no sensitive data is collected.', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:66
	// Reference: inc/settings/class-settings-misc.php:107
	// Reference: languages/ang-translations.php:1002
	__( 'More Info', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:73
	// Reference: languages/ang-translations.php:1006
	__( 'Rollback Version', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:76
	// Reference: languages/ang-translations.php:1010
	__( 'If you are having issues with current version of Style Kits for Elementor, you can rollback to a previous stable version.', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:90
	// Reference: languages/ang-translations.php:1014
	__( 'Reinstall this version', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:97
	// Reference: languages/ang-translations.php:1018
	__( 'Template Settings', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:102
	// Reference: languages/ang-translations.php:1022
	__( 'Remove Styling from typographic elements', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:107
	// Reference: languages/ang-translations.php:1026
	__( 'This setting will remove any values that have been manually added in the templates. Existing templates are not affected.', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:114
	// Reference: languages/ang-translations.php:1030
	__( 'Remove Data on Uninstall', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:119
	// Reference: languages/ang-translations.php:1034
	__( 'Check this box to remove all data stored by Style Kit for Elementor plugin, including license info, user settings, import history etc. Any imported or manually saved Style Kits are not removed.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:14
	// Reference: languages/ang-translations.php:1038
	__( 'An inter-connected collection of Template Kits and advanced design control is <u>coming soon</u> with Style Kits Pro.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:16
	// Reference: languages/ang-translations.php:1042
	__( 'You can <strong>sign up for an exclusive discount</strong> that we will send to your email when we are close to launch. Click the button below to see the Pro features and road map, and sign up for the exclusive discount.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:18
	// Reference: languages/ang-translations.php:1046
	__( 'More about Style Kits Pro', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:50
	// Reference: languages/ang-translations.php:1050
	__( 'Save changes', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:61
	// Reference: languages/ang-translations.php:1054
	__( 'Need help setting up? We have a number of handy articles to get you started.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:62
	// Reference: languages/ang-translations.php:1058
	__( 'Read Documentation', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:65
	// Reference: languages/ang-translations.php:1062
	__( 'Join our Facebook group', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:66
	// Reference: languages/ang-translations.php:1066
	__( 'Get insights, tips and updates in our facebook community. Let\'s take Elementor design to a whole new level.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:67
	// Reference: languages/ang-translations.php:1070
	__( 'Join the AnalogWP community', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:70
	// Reference: languages/ang-translations.php:1074
	__( 'Sign up for updates', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:71
	// Reference: languages/ang-translations.php:1078
	__( 'Sign up to Analog Newsletter and get notified about product updates, freebies and more.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:74
	// Reference: languages/ang-translations.php:1082
	__( 'Subscribe up to newsletter', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:76
	// Reference: languages/ang-translations.php:1086
	__( 'By signing up you agree to our', 'ang' ),

	// Reference: languages/ang-translations.php:1089
	__( 'Theme License', 'ang' ),

	// Reference: languages/ang-translations.php:1092
	__( 'Enter your theme license key received upon purchase from <a target="_blank" href="https://analogwp.com/account/">AnalogWP</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1095
	__( 'License Key', 'ang' ),

	// Reference: languages/ang-translations.php:1098
	__( 'License Action', 'ang' ),

	// Reference: languages/ang-translations.php:1101
	__( 'Deactivate License', 'ang' ),

	// Reference: languages/ang-translations.php:1104
	__( 'Activate License', 'ang' ),

	// Reference: languages/ang-translations.php:1107
	__( 'License status is unknown.', 'ang' ),

	// Reference: languages/ang-translations.php:1110
	__( 'Renew?', 'ang' ),

	// Reference: languages/ang-translations.php:1113
	__( 'unlimited', 'ang' ),

	// Reference: languages/ang-translations.php:1116
	__( 'License key is active.', 'ang' ),

	// Reference: languages/ang-translations.php:1120
	/* translators: translators: translators: translators: translators: translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date */
	__( 'Expires %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1123
	__( 'Lifetime License.', 'ang' ),

	// Reference: languages/ang-translations.php:1127
	/* translators: translators: translators: translators: translators: translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit */
	__( 'You have %1$s / %2$s sites activated.', 'ang' ),

	// Reference: languages/ang-translations.php:1131
	/* translators: translators: translators: translators: translators: translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name */
	__( 'License key expired %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1134
	__( 'License key has expired.', 'ang' ),

	// Reference: languages/ang-translations.php:1137
	__( 'License keys do not match. <br> Enter your theme license key received upon purchase from <a target="_blank" href="https://analogwp.com/account/">AnalogWP</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1140
	__( 'License is inactive.', 'ang' ),

	// Reference: languages/ang-translations.php:1143
	__( 'License key is disabled.', 'ang' ),

	// Reference: languages/ang-translations.php:1146
	__( 'Site is inactive.', 'ang' ),

	// Reference: languages/ang-translations.php:1149
	__( 'Updating this theme will lose any customizations you have made. \'Cancel\' to stop, \'OK\' to update.', 'ang' ),

	// Reference: languages/ang-translations.php:1152
	__( '<strong>%1$s %2$s</strong> is available. <a href="%3$s" class="thickbox" title="%4$s">Check out what\'s new</a> or <a href="%5$s" %6$s>update now</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1155
	__( 'An error occurred, please try again.', 'ang' ),

	// Reference: languages/ang-translations.php:1159
	/* translators: translators: translators: translators: translators: translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date */
	__( 'Your license key expired on %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1162
	__( 'Your license key has been disabled.', 'ang' ),

	// Reference: languages/ang-translations.php:1165
	__( 'Invalid license.', 'ang' ),

	// Reference: languages/ang-translations.php:1168
	__( 'Your license is not active for this URL.', 'ang' ),

	// Reference: languages/ang-translations.php:1172
	/* translators: translators: translators: translators: translators: translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email */
	__( 'This appears to be an invalid license key for %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1175
	__( 'Your license key has reached its activation limit.', 'ang' ),

	// Reference: languages/ang-translations.php:1178
	__( 'License', 'ang' ),

	// Reference: languages/ang-translations.php:1181
	__( 'License Status', 'ang' ),

	// Reference: languages/ang-translations.php:1184
	__( 'Pro license', 'ang' ),

	// Reference: languages/ang-translations.php:1187
	__( 'If you own an AnalogPro License, then please enter your license key here.', 'ang' ),

	// Reference: languages/ang-translations.php:1190
	__( 'If you do not have a license key, you can get one from ', 'ang' ),

	// Reference: languages/ang-translations.php:1193
	__( '<strong>You are editing the style kit that has been set as global.</strong> You can optionally choose a different Style Kit for this page below.', 'ang' ),

	// Reference: languages/ang-translations.php:1196
	__( 'Blocks', 'ang' ),

	// Reference: languages/ang-translations.php:1199
	__( 'Back to all Blocks', 'ang' ),

	// Reference: languages/ang-translations.php:1202
	__( 'Show Pro Blocks', 'ang' ),

	// Reference: languages/ang-translations.php:1205
	__( 'Loading blocks...', 'ang' ),

	// Reference: languages/ang-translations.php:1208
	__( 'The Block has been imported and is now available in the list of the available Sections.', 'ang' ),

	// Reference: languages/ang-translations.php:1211
	__( 'Invalid Block ID.', 'ang' ),

	// Reference: languages/ang-translations.php:1214
	__( 'Global Stylekit Settings Saved. It\'s recommended to close any open elementor tabs in your browser, and re-open them, for the effect to apply.', 'ang' ),

	// Reference: languages/ang-translations.php:1218
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link */
	__( 'Choosing a Style Kit will make it global and apply site-wide. Learn more about <a href="%s" target="_blank">Style kits</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1221
	__( 'You will then be able to apply on any page.', 'ang' ),

	// Reference: languages/ang-translations.php:1224
	__( 'The Style Kit has been imported to your library.', 'ang' ),

	// Reference: languages/ang-translations.php:1227
	__( '%s has been imported and is now available in the Style Kits dropdown', 'ang' ),

	// Reference: languages/ang-translations.php:1230
	__( 'Do not apply link color on active titles', 'ang' ),

	// Reference: languages/ang-translations.php:1233
	__( 'Blimey! Your template has been imported.', 'ang' ),

	// Reference: languages/ang-translations.php:1236
	__( 'Premium template kits are coming soon with Style Kits Pro. You can only preview these layouts for now but feel free to sign up to our mailing list if you want to learn when they become available.', 'ang' ),

	// Reference: languages/ang-translations.php:1239
	__( 'View Kit', 'ang' ),

	// Reference: languages/ang-translations.php:1242
	__( 'Choose', 'ang' ),

	// Reference: languages/ang-translations.php:1245
	__( 'Global: ', 'ang' ),

	// Reference: languages/ang-translations.php:1248
	__( 'Go Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1251
	__( 'Access an interconnected library of Template Kits, blocks and additional design control with Style kits Pro.', 'ang' ),

	// Reference: languages/ang-translations.php:1254
	__( 'Template Kits with theme builder templates.', 'ang' ),

	// Reference: languages/ang-translations.php:1257
	__( 'Blocks Library.', 'ang' ),

	// Reference: languages/ang-translations.php:1260
	__( 'Global design control.', 'ang' ),

	// Reference: languages/ang-translations.php:1263
	__( 'Advanced Style Kit control.', 'ang' ),

	// Reference: languages/ang-translations.php:1266
	__( 'Explore Style Kits Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1269
	__( 'Back to library', 'ang' ),

	// Reference: languages/ang-translations.php:1272
	__( 'Elevate your Elementor design with Analog Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1275
	__( 'Step up your workflow with unlimited design resources for your Elementor-powered projects.', 'ang' ),

	// Reference: languages/ang-translations.php:1278
	__( 'Why Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1281
	__( 'Access to all template library', 'ang' ),

	// Reference: languages/ang-translations.php:1284
	__( 'Templates for singles, archives, popups and more', 'ang' ),

	// Reference: languages/ang-translations.php:1287
	__( 'Multi-niche, human made design that makes sense', 'ang' ),

	// Reference: languages/ang-translations.php:1290
	__( 'Unlimited license for peace of mind', 'ang' ),

	// Reference: languages/ang-translations.php:1293
	__( '* Requires Elementor Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1296
	__( 'Please update Analog Template plugin to latest version.', 'ang' ),

	// Reference: languages/ang-translations.php:1299
	__( 'Enter your email', 'ang' ),

	// Reference: languages/ang-translations.php:1302
	__( 'Sending...', 'ang' ),

	// Reference: languages/ang-translations.php:1305
	__( 'Sign up to newsletter', 'ang' ),

	// Reference: languages/ang-translations.php:1308
	__( 'Docs', 'ang' ),

	// Reference: languages/ang-translations.php:1311
	__( 'Follow on Social', 'ang' ),

	// Reference: languages/ang-translations.php:1314
	__( 'Elevate to Analog Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1317
	__( 'Do more with Analog Pro, the design library for complete Elementor-powered sites.', 'ang' ),

	// Reference: languages/ang-translations.php:1320
	__( 'Access to all templates', 'ang' ),

	// Reference: languages/ang-translations.php:1323
	__( 'New designs every week', 'ang' ),

	// Reference: languages/ang-translations.php:1326
	__( 'Flexible Licensing', 'ang' ),

	// Reference: languages/ang-translations.php:1329
	__( 'Pro Elements, theme builder layouts', 'ang' ),

	// Reference: languages/ang-translations.php:1332
	__( 'Requires Elementor Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1335
	__( 'More Details', 'ang' ),

	// Reference: languages/ang-translations.php:1338
	__( 'An error occured', 'ang' ),

	// Reference: languages/ang-translations.php:1341
	__( 'Imported ', 'ang' ),

	// Reference: languages/ang-translations.php:1344
	__( 'Settings updated.', 'ang' ),

	// Reference: languages/ang-translations.php:1347
	__( 'Deactivate', 'ang' ),

	// Reference: languages/ang-translations.php:1350
	__( 'Activate', 'ang' ),

	// Reference: languages/ang-translations.php:1353
	__( 'Plugin Settings', 'ang' ),

	// Reference: languages/ang-translations.php:1356
	__( 'Pro License', 'ang' ),

	// Reference: languages/ang-translations.php:1359
	__( 'Connection timeout, please try again.', 'ang' ),

	// Reference: languages/ang-translations.php:1362
	__( 'Processing...', 'ang' ),

	// Reference: languages/ang-translations.php:1365
	__( 'If you do not have a license key, you can get one from', 'ang' ),

	// Reference: languages/ang-translations.php:1368
	__( 'The Elementor color palette will be populated with the Style Kit’s global colors', 'ang' ),

	// Reference: languages/ang-translations.php:1371
	__( 'Reinstall version %s', 'ang' ),

	// Reference: languages/ang-translations.php:1374
	__( 'Style Kits Settings Page', 'ang' ),

	// Reference: languages/ang-translations.php:1377
	__( 'License keys do not match. <br><br> Enter your theme license key received upon purchase from <a target="_blank" href="https://analogwp.com/account/">AnalogWP</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1380
	__( 'filter', 'ang' ),

	// Reference: languages/ang-translations.php:1383
	__( 'sort by', 'ang' ),

	// Reference: languages/ang-translations.php:1386
	__( 'show only free', 'ang' ),

	// Reference: languages/ang-translations.php:1390
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: Style kit title. */
	__( 'Style Kit: %s <span style="color:#3152FF;">&#9679;</span>', 'ang' ),

	// Reference: languages/ang-translations.php:1393
	__( 'This padding applies <strong>only on the Outer Sections</strong> of your layout, and not on Inner Section widgets.', 'ang' ),

	// Reference: languages/ang-translations.php:1396
	__( 'Set the default values of the column gaps. Based on Elementor&apos;s default sizes.', 'ang' ),

	// Reference: languages/ang-translations.php:1399
	__( 'Create the styles for each button size', 'ang' ),

	// Reference: languages/ang-translations.php:1402
	__( 'View Library', 'ang' ),

	// Reference: languages/ang-translations.php:1405
	__( 'Kits', 'ang' ),

	// Reference: languages/ang-translations.php:1408
	__( 'Below are the available Style Kits. When you choose to import a Style Kit, it will be added to your available', 'ang' ),

	// Reference: languages/ang-translations.php:1411
	__( 'Importing Style Kit ', 'ang' ),

	// Reference: languages/ang-translations.php:1414
	__( 'Blimey! Your Style Kit has been imported to library.', 'ang' ),

	// Reference: languages/ang-translations.php:1417
	__( 'Normal Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1420
	__( 'Small Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1423
	__( 'Medium Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1426
	__( 'Large Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1429
	__( 'Extra Large Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1432
	__( 'No Gap', 'ang' ),

	// Reference: languages/ang-translations.php:1435
	__( 'Sets the primary brand color, applies on Links.', 'ang' ),

	// Reference: languages/ang-translations.php:1438
	__( 'Sets the default color for Buttons. You can also apply button colors in the Global buttons tab.', 'ang' ),

	// Reference: languages/ang-translations.php:1441
	__( 'Text over light background', 'ang' ),

	// Reference: languages/ang-translations.php:1444
	__( 'Applies on the text and titles over a section or column with light bg', 'ang' ),

	// Reference: languages/ang-translations.php:1447
	__( 'Applies on the text and titles over a section or column with dark bg', 'ang' ),

	// Reference: languages/ang-translations.php:1450
	__( 'Apply the class <strong>sk-light-bg</strong> to a section or column to apply this color as a background. Text will inherit the <strong>Text over Light bg</strong> color.', 'ang' ),

	// Reference: languages/ang-translations.php:1453
	__( 'Apply the class <strong>sk-dark-bg</strong> to a section or column to apply this color as a background. Text will inherit the <strong>Text over Dark bg</strong> color.', 'ang' ),

	// Reference: languages/ang-translations.php:1456
	__( 'Narrow', 'ang' ),

	// Reference: languages/ang-translations.php:1459
	__( 'Extended', 'ang' ),

	// Reference: languages/ang-translations.php:1462
	__( 'Wide', 'ang' ),

	// Reference: languages/ang-translations.php:1465
	__( 'Wider', 'ang' ),

	// Reference: languages/ang-translations.php:1468
	__( 'Outer Section Gaps', 'ang' ),

	// Reference: languages/ang-translations.php:1471
	__( 'A handcrafted design library for Elementor templates.', 'ang' ),

	// Reference: languages/ang-translations.php:1474
	__( 'Outer Section Gap', 'ang' ),

	// Reference: languages/ang-translations.php:1477
	__( 'Set the padding <strong>only for the outer section</strong>, does not apply on inner section widgets, in case you have any. You can tweak the section gap', 'ang' ),

	// Reference: languages/ang-translations.php:1480
	__( 'Set the padding <strong>only for the outer section</strong>, does not apply on inner section widgets, in case you have any. You can tweak the section gap here.', 'ang' ),

	// Reference: languages/ang-translations.php:1483
	__( 'AnalogWP Templates', 'ang' ),

	// Reference: languages/ang-translations.php:1486
	__( 'Analog Templates', 'ang' ),

	// Reference: languages/ang-translations.php:1489
	__( 'AnalogWP Settings', 'ang' ),

	// Reference: languages/ang-translations.php:1492
	__( 'Meet Style Kits by AnalogWP', 'ang' ),

	// Reference: languages/ang-translations.php:1495
	__( 'Page Styles', 'ang' ),

	// Reference: languages/ang-translations.php:1498
	__( 'Font Size', 'ang' ),

	// Reference: languages/ang-translations.php:1501
	__( 'Line Height', 'ang' ),

	// Reference: languages/ang-translations.php:1504
	__( 'AnalogWP Shortcuts', 'ang' ),

	// Reference: languages/ang-translations.php:1507
	__( 'Applies to all links by default', 'ang' ),

	// Reference: languages/ang-translations.php:1510
	__( 'Adds a background color to all buttons on page.', 'ang' ),

	// Reference: languages/ang-translations.php:1513
	__( 'Text and Headings (Dark)', 'ang' ),

	// Reference: languages/ang-translations.php:1516
	__( 'Applies on the text and titles over light bg', 'ang' ),

	// Reference: languages/ang-translations.php:1519
	__( 'Text and Headings (Light)', 'ang' ),

	// Reference: languages/ang-translations.php:1522
	__( 'Applies on the text and titles over dark bg', 'ang' ),

	// Reference: languages/ang-translations.php:1525
	__( 'If you are having issues with current version of Analog Templates, you can rollback to a previous stable version.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:145
	// Reference: languages/ang-translations.php:1529
	_x( 'Global Colors', 'Section Title', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:34
	// Reference: languages/ang-translations.php:1533
	_x( 'Style Kits', 'admin menu', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:35
	// Reference: languages/ang-translations.php:1537
	_x( 'Style Kit', 'add new on admin bar', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:36
	// Reference: languages/ang-translations.php:1541
	_x( 'Add New', 'book', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:47
	// Reference: languages/ang-translations.php:1545
	_x( 'Global Style Kit', 'settings title', 'ang' )
);
/* THIS IS THE END OF THE GENERATED FILE */
