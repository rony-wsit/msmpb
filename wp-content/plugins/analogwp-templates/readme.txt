=== Style Kits for Elementor ===
Contributors: analogwp, mauryaratan
Requires at least: 5.0
Requires PHP: 5.4
Tested up to: 5.3
Stable tag: 1.3.17
Tags: elementor, landing page, design, website builder, templates, page builder
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Style Kits adds intuitive styling controls in the Elementor editor that power-up your design workflow with unparalleled flexibility.

== Description ==

[Style Kits for Elementor](https://analogwp.com/?utm_medium=wp.org&utm_source=wordpressorg&utm_campaign=readme&utm_content=analogwp) (formerly Analog Templates for Elementor) adds a number of intuitive styling controls in the Elementor editor that allow you to apply styles globally or per page.

Stop manually adding / copying / pasting styles across layouts and experience macro control of the most essential aspects of your layout system:

https://www.youtube.com/watch?v=FZfTG3l-7T8

- Body text
- Headings
- Headings and text sizes
- Column Gaps
- Buttons
- Colors and more are coming soon

#### Typography

Edit the styles for Body and Headings and see the effect taking place in your design immediately, right from within the Editor.

#### Column Gap controls

Achieve site-wide spacing consistency through the column gap controls.

Apply the native column gaps on your outer and inner sections and manage the spacing of your layout system from a single place.

#### Save your page Styles as a Style Kit and apply on any page, or globally

All your custom page styles can be saved as a Stylekit and then apply it on any page.

You can also make a Global Style Kit, and it will apply on your entire site.

#### Button Styles

Now you have a single source of control for your different button sizes. Set the button styles on your Style kit and enjoy consistent, site-wide button control.

#### A collection of fine-tuned, Style-kit powered templates. For free.

Trigger the template gallery popup, preview and import any of the templates in our collection.

In most of the templates, styles are managed from the Style Kit panel, so you can experience design-macro control right away.

#### The perfect addition to Elementor Hello Theme

Hello Elementor is a great, lightweight theme but it lacks basic typography controls. Now with Style Kits, you can set the rules for your Typography save them as a global Style Kit and enjoy site-wide typographic control.

**Find us**:
- [Visit the AnalogWP Website](https://analogwp.com/?utm_medium=wp.org&utm_source=wordpressorg&utm_campaign=readme&utm_content=analogwp)
- [Follow on Twitter](https://twitter.com/AnalogWP/)
- [Like us on Facebook](https://www.facebook.com/analogwp)
- [Join the AnalogWP community](https://www.facebook.com/groups/analogwp/)

== Installation ==

This section describes how to install the plugin and get it working. e.g.

1. Upload the plugin files to the `/wp-content/plugins/analogwp-templates` directory, or install the plugin through the WordPress plugins screen directly..
2. Activate the plugin through the 'Plugins' screen in WordPress.
3. Head to 'Style Kits' page from WordPress sidebar menu.

== Frequently Asked Questions ==

= Do the templates require Elementor Pro? =
No, you do not need Elementor Pro at this point. All the templates that are provided for free are only using the Contact Form widget (which is an Elementor Pro feature). But you can always replace with your favorite contact form plugin.

= What fonts are you using in designs? =
All font pairs in our templates are Google web fonts, we love typography and are always in the look-out for great font combinations.

= Can I use the templates for client projects as well? =
Yes, you can. You can use the templates in any number of personal or client projects. However you cannot re-distribute the templates without our prior consent. You can view the full terms and template licensing here: https://analogwp.com/terms-of-use/.

= What are Style Kits? =
Style Kits (since v1.2) is a collection of the custom Typography and Spacing styles that you have added to your templates, which can be reused with other templates, making the process of creating new templates a lot easier.

= How frequently do you add new designs? =
We try to follow a consistent release cycle of two templates per week.

= Will the templates work with my theme? =
Definitely, given that you have Elementor in your theme setup, the templates will get imported and work just fine.

= Where can I get help? =
Our dedicated support team has your back. Please reach out via our website at https://analogwp.com/.

== Screenshots ==

1. Style Kits Settings page.
2. Import screen: import to Elementor library or create a new page.
3. Access Style Kits templates directly under Elementor.

== Changelog ==

= 1.3.17 - December 12, 2019 =
* Fix: Added backwards compatibility for SK contextual menu
* Fix: Icons box links not respecting inline colors

= 1.3.16 - December 10, 2019 =
* Fix: Issue with Style Kits tab not showing up
* Fix: Issue with templates imported directly from Settings page
* Fix: Accordion/counter widget not respecting inline colors with custom classes
* Tweak: Styled updated for better compatibility with Dark Mode
* Improve: Move Global Style Kit setting from Elementor to plugin settings
* Improve: Tweak rollback versions to be ordered numerically

= 1.3.15 - November 29, 2019 =
* Fix: Custom CSS inconsistency between saving SKs, properly slash data before saving in post meta
* Fix: Remove stored template values from existing SKs
* Fix: Issue with undefined array key in a rare cases
* Fix: Link color within content not respecting inline color
* Fix: Global color not overridding colors as expected when using multiple templates on page
* Improve: Added new method for updating SKs, preserve settings not related to SK

= 1.3.14 - November 11, 2019 =
* Fix: Don't save template value from in SK
* Fix: Post meta terms accent color
* Tweak: Added SK accent color on Comment submit button
* Tweak: Change SK imported popup text, redirect to SK panel after import
* Improve: Update Style Kit button is now more consistent
* Improve: Clear Elementor cache on uninstall
* Improve: Use core version of React and ReactDOM

= 1.3.13 - November 04, 2019 =
* Fix: Issue with buttons taking accent color

= 1.3.12 - November 04, 2019 =
* Fix: Added back selectors to apply accent colors on titles
* Fix: Show SK updated popup only on open editors
* Fix: Rare issue where SK dropdown would insert invalid items when no items exist
* Tweak: Include all page settings as part of SK
* Tweak: Update Column padding selectors to remove !important
* Tweak: Better compatibility with Nav styles

= 1.3.11 - October 31, 2019 =
* Fix: Templates not being imported due to a bug introduced in v1.3.10
* Fix: Style Kit panels/settings not showing up on some template types, as a result, the setting "Do not apply link color on active titles" has been removed
* Tweak: Better compatibility with Nav widget and Style Kits
* Improve: Popups, once again can have a Style Kit

= 1.3.10 - October 29, 2019 =
* New: Make Nav item/pointer inherit SK accent color
* Fix: Image/Icon box widget selectors accent color override
* Fix: Heading widget selectors accent color override
* Improve: Ensure “Do not apply link” setting is respected

= 1.3.9 - October 26, 2019 =
* Fix: Disable Style Kit settings from [Popups](https://github.com/mauryaratan/analogwp-templates/issues/202)
* Fix: Divider widget not being able to [override accent color](https://github.com/mauryaratan/analogwp-templates/issues/207)
* Fix: Icon/Image box not being able to [override accent color](https://github.com/mauryaratan/analogwp-templates/issues/203)
* Fix: All Style Kits not being [listed](https://github.com/mauryaratan/analogwp-templates/issues/200) during import
* Fix: A rare case with undefined jQuery error within Elementor editor
* Improve: Tweak Settings page styles

= 1.3.8 - October 22, 2019 =
* New: Added new [Template Collection](https://github.com/mauryaratan/analogwp-templates/issues/176) interface.
* New: Added [new Settings Page](https://github.com/mauryaratan/analogwp-templates/issues/178) in Style Kits > Settings
* New: [Template import](https://github.com/mauryaratan/analogwp-templates/issues/161) flow, allowing to select a Style Kit during import
* Fix: Links inside Headings having 2x size when used with [EM units](https://github.com/mauryaratan/analogwp-templates/issues/175)
* Fix: Issue with a `WP_Query` call interfering with Elementor queries
* Fix: An issue with inline color style on the [Accordion widget title](https://github.com/mauryaratan/analogwp-templates/issues/180) does not override accent color
* Fix: Apply accent color properly on stacked and framed variants of [icons](https://github.com/mauryaratan/analogwp-templates/issues/187).
* Fix: Added `wp-api-fetch` as script dependency inside Elementor editor
* Fix: Error not showing up when an unallowed version of template was being imported
* Fix: H6 selector for Default Font Family control of Headings Typography
* Fix: Style Kits library not force refreshing
* Improve: [Empty state](https://github.com/mauryaratan/analogwp-templates/issues/170) when templates are empty
* Improve: Added Templates/Style Kits count in header
* Improve: Updated readme.txt changelog to include release dates

= 1.3.7 - September 20, 2019 =
* New: Added a settings Style Kit > Settings to be able to rollback to any older version of plugin
* Fix: Bug where Quick edit mode would accidentally break Elementor page settings
* Fix: Hide title setting being overridden on applying a Style Kit.
* Fix: Counter widget not inheriting Style Kit classes
* Fix: SK classes overriding inline Toggle widget title
* Fix: Style Kit library search results listing everything if no matches are found
* Fix: SK classes not working properly with Columns because of tags nesting

= 1.3.6 - September 12, 2019 =
* New: Added an option to disable accent color on titles under Style Kits > Tools
* New: Add more helper classes for color controls
  * `sk-primary-accent`
  * `sk-primary-bg`
  * `sk-secondary-accent`
  * `sk-secondary-bg`
  * `sk-text-light`
  * `sk-text-dark`
  * `sk-light-bg`
  * `sk-dark-bg`
* Tweak: Compatibility with Elementor v2.7.x
* Tweak: Comaptibility with Central Color Palette plugin
* Tweak: Background color inheritence for Global colors
* Fix: Issue with duplicate settings throwing notice
* Fix: Global Color control button selector
* Fix: Issue with invalid default value type causing `array_merge` errors
* Fix: Style Kits not refreshing in Elementor editor upon importing

= 1.3.5 - August 28, 2019 =
* Fix: An issue with Analog modal not launching in Elementor screen

= 1.3.4 - August 28, 2019 =
* New: All new *[Style Kits](https://github.com/mauryaratan/analogwp-templates/issues/135)* library page to import pre-build Style Kits.
* New: Added Quick Edit and Bulk edit actions on posts list, to assign a Style Kit to multiple posts/pages at once.
* New: Reorganized nav menu and settings tab for better access to different pages.
* Fix: Section colors not being able to override Style Kit colors. [#138](https://github.com/mauryaratan/analogwp-templates/issues/138)
* Fix: Rename Dynamic tag classes to be in line with generated classes
* Fix: Elementor's + button taking accent color from Style Kits.
* Improve: Added docs link in description of controls to official documentation. [#140](https://github.com/mauryaratan/analogwp-templates/issues/140)
* Improve: Added link to AnalogWP facebook group on Settings page.
* Improve: Style kit can be imported from Elementor screen as well
* Improve: Loading of data sent to app

= 1.3.3 - August 21, 2019 =
* Fix: An undefined function error when importing a template inside Elementor editor
* Improve: Always shows Style Kits post type page, so users can always import without having to create one first

= 1.3.2 - August 16, 2019 =
* Improve: Rename and reorder Global Color setting labels
* Improve: Rename Outer Section Padding labels to avoid confusions
* Improve: Extend Primary Accent colors to Icon + Progress bar widgets. Feel it should extend to more? [Sound off](https://github.com/mauryaratan/analogwp-templates/issues/new).

= 1.3.1 - August 15, 2019 =
* New: Added Color Controls in page settings
* New: Added Plugin setting to sync Color controls output colors with Elementor color picker
* New: Added Control for spacing between widgets under Style Kits > Column Gaps
* New: Added "Outer Section Padding" control to tweak Section padding
* New: Added plugin action link to settings page on plugins screen
* Improve: Reorganized setting sections
* Improve: Renamed 'Page Styles' to 'Style Kit' in Elementor contextual menu
* Fix: Issue with plugin page app crash on switching tabs while a preview is open
* Fix: Fatal error with `use function` usage in PHP 5.5.x
* Fix: Issue with Style kit post state showing empty title

= 1.3 - August 05, 2019 =
* New: Added Button Controls under Page Styles 🎉
* New: Plugin rebranded as "Style Kits for Elementor"
* Fix: Text/Heading size controls not being persistent on style kit change
* Fix: Column gap individual controls not taking place
* Fix: Style Kit update modal displaying on page where it was updated from
* Fix: Dependency error with Gutenberg 6.2
* Improve: Added a modal window to indicate if page has Global Style Kit

= 1.2.4 - June 19, 2019 =
* Fix: A critical error where templates won't import due to internal error
* Improve: Replace `body` selectors with `{{WRAPPER}}` to scope it to specific template
* Improve: Reorganize settings in Heading and Text sizes sections

= 1.2.3 - May 30, 2019 =
* New: Added a [notice](https://github.com/mauryaratan/analogwp-templates/issues/92) to reopen tabs when setting a global Style Kit.
* New: [Updating](https://github.com/mauryaratan/analogwp-templates/issues/107) an existing Style Kit now shows a modal window on pages using the same kit, to choose whether to pull latest changes or keep old.
* New: Added option to [rollback](https://github.com/mauryaratan/analogwp-templates/issues/99) to a previous stable version under AnalogWP > Settings.
* New: Added a visual [indicator](https://github.com/mauryaratan/analogwp-templates/issues/101) on posts list to display which Style Kit is active.
* New: Added a quick post action to "Apply Global Style Kit"
* Fix: Clear Elementor cache when a Style Kit is [updated](https://github.com/mauryaratan/analogwp-templates/issues/103), so other posts sharing same kit can take effect.
* Fix: [Extend](https://github.com/mauryaratan/analogwp-templates/issues/106) heading selector to include `a` tags inside to match styles.
* Improve: Stop users from creating a new style kit directly from CPT [page](https://github.com/mauryaratan/analogwp-templates/issues/97), as it results in empty style kit.

= 1.2.2 - May 19, 2019 =
* Fixed an issue with padding section media queries
* Fix broken dependency on Settings page ahead of new Gutenberg version

= 1.2.1 - May 09, 2019 =
* New: Added ability to import/export Style Kits 🎉.
* Fix: Issue with page style column gap not being overridden with advanced section padding.
* Fix: Incorrect documentation link.
* Improve: Include page background as part of style kits.
* Improve: Add visual notification when a setting is changed on Settings page.
* Improve: Add a visual indicator in notification based on notification timeout duration on Settings page.

= 1.2 - April 25, 2019=
* New: Introducing [Style Kits](https://analogwp.com/style-kits-for-elementor/?utm_medium=wp.org&utm_source=wordpressorg&utm_campaign=readme&utm_content=analogwp). 🎉
* New: Added Elementor Finder shortcuts to quickly navigate to Style Kits settings/library.
* New: Added a new settings to assign global style kit under Elementor > Settings > Styles > Global Style kit.
* New: Added Contextual menu to Style Kit for quick access in Elementor.
* New: Close Import modal via ESC key on settings screen.
* Improve: Use minified versions of scripts
* Improve: Added better and visible server side error handling.
* Improve: Added notices if importing a template doesn't meet specified requirements.

= 1.1.2 - March 26, 2019 =
* New: Added [Page style settings](https://analogwp.com/testing-global-page-styles-in-analog-templates). 🎉
* New: Added option to enable beta features under Style Kits > Settings.
* Fix: Remove User First/Last name collection on newsletter signup, as it wasn't communicated.
* Tweak: Added documentation links to settings with more instructions.
* Tweak: Show 'New' badge for 14 days, to match new template publish timing.

= 1.1.1 - March 14, 2019 =
* Fixed a styling issue with Elementor popup modal, caused with Elementor v2.5.5

= 1.1 - March 13, 2019 =
* New: All new designed settings page. 🎉
* New: Added option to Strip Typography under settings tab. [More info](https://docs.analogwp.com/article/544-remove-styling-from-typographic-elements).
* New: Lazy load images on templates page to avoid unnecessary loading, making page load faster.
* New: Keep a log of imported templates for user.
* New: Added an option to opt-in data tracking, optional.
* Fix: Install and import count not being when importing from within Elementor.
* Improve: Switched to custom version for React better upgrades.
* Improve: Added loading indicator for when template preview is loading.
* Improve: Check WordPress version before loading files to avoid errors.
* Fix: Set right data type to _ang_import_type post meta

= 1.0.0 - February 15, 2019 =
* Initial Release

== Upgrade Notice ==

= 1.2.1 =
Added Style kit import/export feature. Fixed an issue with column gaps.

= 1.2 =
Introducing Style Kits for Elementor, added a bunch of QoL improvements.

= 1.3.4 =
Fixed a critical bug inside Elementor when attempting to import a template.
